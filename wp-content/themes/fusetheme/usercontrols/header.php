
<!-- start of header.html -->
<div id="TriSysWeb-Header_dy">

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <a href="<?php bloginfo("siteurl") ?>" title="Opus Laboris Recruitment Home">
                <img src="<?php echo get_option('fldLogoUrl'); ?>" height="70" class="pull-left" alt="Opus Laboris Recruitment" id="header-site-logo">
            </a>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="menuItem-index" >
                    <a href="http://opuslaboris.ajtechbd.com">Home</a>
                </li>

                <li class="dropdown" id="menuItem-candidate-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="menuItem-Candidate" >
                        Candidate 
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li id="menuItem-vacancysearch" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.CandidateVacancySearchPage)">Vacancy Search</a>
                        </li>
                        <li id="menuItem-favouritevacancies" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.FavouriteVacanciesPage)">Favourite Vacancies</a>
                        </li>
                        <li id="menuItem-register" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.CandidateRegistrationPage)">Register/Sign-Up</a>
                        </li>
                        <li id="menuItem-forgottenpassword" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ForgottenPasswordPage)">Forgotten Password</a>
                        </li>
                        <li class="divider">
                        </li>
                        <li id="menuItem-editcandidateprofile" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.EditProfilePage)">Edit Account Details</a>
                        </li>
                        <li id="menuItem-vacancyapplications" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.VacancyApplicationsPage)">Vacancy Applications</a>
                        </li>
                        <li id="menuItem-interviews" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.InterviewsPage)">Interviews</a>
                        </li>
                        <li id="menuItem-placements" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.PlacementsPage)">Placements</a>
                        </li>
                        <li id="menuItem-timesheets" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.TimesheetsPage)">Timesheets</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown" id="menuItem-client-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="menuItem-Client" >
                        Client 
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li id="menuitem-registerclient" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientRegistrationPage)">Register/Sign-Up</a>
                        </li>
                        <li id="menuitem-forgottenpasswordclient" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ForgottenPasswordPage)">Forgotten Password</a>
                        </li>
                        <li class="divider">
                        </li>
                        <li id="menuItem-editclientprofile" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.EditProfilePage)">Edit Account Details</a>
                        </li>
                        <li id="menuItem-clientvacancies" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientVacanciesPage)">My Vacancies</a>
                        </li>
                        <li id="menuItem-clientplacements" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientPlacementsPage)">My Placements</a>
                        </li>
                        <li id="menuItem-clienttimesheets" >
                            <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientTimesheetsPage)">My Timesheets</a>
                        </li>
                    </ul>
                </li>

                <li id="menuItem-contactus">
                    <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ContactUsPage)">Contact</a>
                </li>
                <li id="menuItem-aboutus">
                    <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.AboutUsPage)">About</a>
                </li>
            </ul>

            <!-- Pre Login -->
            <div class="navbar-form navbar-right" id="loginPanel">
                <!--<div class="form-group">
                    <input type="email" placeholder="E-Mail" id="txtLoginEMail" maxlength="100" class="form-control" style="min-width:120px;">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" id="txtLoginPassword" maxlength="50" class="form-control" style="min-width:100px">
                </div>-->
                <button type="submit" class="btn btn-success" onclick="TriSysWeb.Security.ShowLoginPage();" id="btnLogin">
                    Login
                </button>
            </div>

            <!-- Post Login -->
            <div class="navbar-form navbar-right" id="loggedinPanel" style="display: none">
                <div class="form-group">
                    <a href="#" onclick="TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.AboutUsPage)" title="Click to edit your profile">
                        <div style="color: white" id="loggedInNameAndCompany">&nbsp;</div>
                    </a>
                </div>
                <div class="form-group">
                    &nbsp;
                </div>
                <button type="submit" class="btn btn-success" onclick="TriSysWeb.Security.LogoffButtonClick()" id ="btnLogoff">
                    Logout
                </button>
            </div>

        </div>
        <!--/.navbar-collapse -->
    </div>
</div>
    <!-- End of Menu -->

    <!-- http://dotnetspeak.com/2013/05/creating-simple-please-wait-dialog-with-twitter-bootstrap/comment-page-1 -->
    <div class="modal" id="pleaseWaitDiv" role="dialog" style="display:none">

        <div id="modal-position" style="height: 1px"></div>

        <div class="modal-dialog" data-backdrop="static" data-keyboard="false" id="modal-dialog-div">
            <div class="modal-content" style="padding-left: 10px; padding-right: 10px">
                <div style="height: 10px"></div>
                <p id="modal-message" style="color: gray">
                    Processing Data...
                </p>
                <div class="progress progress-striped active">
                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">

                    </div>

                </div>

            </div>

        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function ()
        {
            // Fix up site logo
            //SiteLogoCorrection();

            // After header is loaded, commence firing of business functionality
            TriSysWeb.Pages.AfterLoadingHeader();
        });

        function SiteLogoCorrection()
        {
            try
            {
                var util = TriSysLoad.Utility;
                if (util.FilePathPrefix().length > 0)
                {
                    var sID = "header-site-logo";
                    var img = document.getElementById(sID);
                    if( img )
                    {
                        var src = $('#'+sID).attr('src');
                        if (src.indexOf("/") > 0 && src.indexOf("../") !== 0)
                            $('#'+sID).attr('src', util.FilePathPrefix() + src);
                    }
                }
            }
            catch(ex)
            {
                // Not a show stopper!
            }
        }

    </script>

</div>
<!-- end of header.html -->