<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?> 

<div class="container">
            <div class="row" id="trisys-cms-opuslaboris-index-headertext" ondblclick="TriSysCMS.Editor.Open(this);">
                <div class="col-md-9">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                    <?php the_post_thumbnail(); ?>
                </div>
            </div>
            <p id="trisys-cms-opuslaboris-index-welcome" ondblclick="TriSysCMS.Editor.Open(this);">
            <?php the_content(); ?>
			</p>
            <p style="text-align: center">
            </p>
        </div>
 <?php endwhile; ?> 
<?php else : ?> 
<h3><?php _e('404 Error&#58; Not Found', 'zahantech'); ?></h3> 
<?php endif; ?>