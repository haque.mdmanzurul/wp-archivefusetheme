<?php  
/* 
* Template name: Favorite Vacancy Page
*/
get_header(); ?>
<div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-8">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>                
                    <img src="<?php bloginfo('template_directory'); ?>/images/FavouriteJobs.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default" id="pnlLiveVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Favourite Vacancies
                </h3>
            </div>
            <div class="panel-body">
                <!-- The vacancy grid populated by JS -->
                <div id="divFavouriteVacanciesGrid" style="border-width: 1px; width: 100%; ">
                </div>

                <!-- The google map populated by JS -->
                <div id="divVacancyMap" style="border-width: 1px; width: 100%; height:600px; display:none ">
                </div>
            </div>
        </div>
    </div>
    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="divActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnApply" onclick="ApplyButton()">Apply</a>
                    <a class="btn btn-default" id="btnRemoveFromFavourites" onclick="RemoveFromFavouritesButton()">Remove</a>
                    <a class="btn btn-default" id="btnSendToFriend" onclick="SendToFriendButton()">Send to Friend</a>
                    <a class="btn btn-default" id="btnShowOnMap" onclick="ShowFavouritesOnMap()">Show on Map</a>
                    <a class="btn btn-default" id="btnShowInGrid" onclick="ShowInGrid()" style="display:none">Show in Grid</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
                <div class="btn-group" style="float:right">
                    <a class="btn btn-default" id="btnSelectAll" onclick="TriSysSDK.Grid.SelectAllRows('divFavouriteVacanciesGrid', true);">Select All</a>
                    <a class="btn btn-default" id="btnSelectNone" onclick="TriSysSDK.Grid.SelectAllRows('divFavouriteVacanciesGrid', false)">Select None</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->

    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "favouritevacancies",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Show favourite vacancies after we can be sure that the form is loaded
                        setTimeout(TriSysWeb.Pages.CandidateFavouriteVacancies.LoadGrid, 50);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function ApplyButton()
        {
            var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRecordIds)
                if (multipleSelectedRecordIds.length > 0)
                {
                    var sURL = window.location.href.toLowerCase().replace("favouritevacancies.html", "vacancy.html") + "?RequirementId=";
                    TriSysWeb.Pages.CandidateVacancy.ApplyToMultiple(sURL, multipleSelectedRecordIds);
                }
        }
        function RemoveFromFavouritesButton()
        {
            var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRecordIds)
                if (multipleSelectedRecordIds.length > 0)
                    TriSysWeb.Pages.CandidateVacancy.RemoveListFromFavourites(multipleSelectedRecordIds);
        }
        function SendToFriendButton()
        {
            var multipleSelectedRequirementIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRequirementIds)
            {
                var sURL = window.location.href.toLowerCase().replace("favouritevacancies.html", "vacancy.html") + "?RequirementId=";

                if (multipleSelectedRequirementIds.length > 0)
                {
                    TriSysWeb.Pages.CandidateVacancy.SendMultipleVacanciesToFriend(sURL, multipleSelectedRequirementIds);
                }
            }
        }
        function ShowFavouritesOnMap()
        {
            var sRequirementIdList = '', sWhereSQL = '';
            var currentFavourites = TriSysWeb.Pages.CandidateVacancy.getFavouriteList();
            if (currentFavourites)
            {
                if (currentFavourites.length > 0)
                {
                    for (var i = 0; i < currentFavourites.length; i++)
                    {
                        var vacancy = currentFavourites[i];
                        var lRequirementId = vacancy.RequirementId;
                        if (sRequirementIdList.length > 0)
                            sRequirementIdList += ',';
                        sRequirementIdList += lRequirementId;
                    }

                    sWhereSQL = ' And v.Requirement_RequirementId in (' + sRequirementIdList + ')';
                }
            }

            var vacanciesObject = TriSysWeb.Pages.CandidateVacancies;
            vacanciesObject.GridDivTag = 'divFavouriteVacanciesGrid';
            vacanciesObject.ShowOnMap(sWhereSQL);
        }
        function ShowInGrid()
        {
            $('#divFavouriteVacanciesGrid').show();
            $('#divVacancyMap').hide();
            $('#btnShowOnMap').show();
            $('#btnShowInGrid').hide();
        }

    </script>
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->
