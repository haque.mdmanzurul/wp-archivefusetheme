﻿<?php  
/* 
* Template name: Create CV Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/CV-Builder.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default" id="pnlBuildCV">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Create a CV
                </h3>
            </div>
            <div class="panel-body">

                <p>CV Builder loading...</p>
            </div>
        </div>

    </div>
    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="Div1">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnApply" onclick="WithdrawApplication()">Save CV</a>
                    <a class="btn btn-default" id="btnSendToFriend" onclick="SendToFriendButton()">Register my Details</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "createcv",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Show wizard...
                        //setTimeout(func, 50);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->