﻿<?php  
/* 
* Template name: Regster Client Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        Client Registration
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>                
                    <img src="<?php bloginfo('template_directory'); ?>/images/Register.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                By registering as a client, you may advertise jobs on this web site.
                You will also be able to track all of the candidate applications and organise your interview preferences
                as well as authorise timesheets.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Register
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group" style="width:100%; float:left;">
                    <label style="width:100%;">
                        Title
                    </label>

                    <input id="Contact_ContactTitle" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Forename(s)
                    </label>
                    <input type="text" class="form-control" name="txtForename" id="txtForename" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Surname
                    </label>
                    <input type="text" class="form-control" name="txtSurname" id="txtSurname" maxlength="50">
                </div>
                <div class="form-group" style="width:49.5%; float:left;">
                    <label>
                        Password
                    </label>
                    <input type="password" class="form-control" name="pwPassword" id="pwPassword" maxlength="20">
                </div>
                <div class="form-group" style="width:49.5%; float:right;">
                    <label>
                        Confirm Password
                    </label>
                    <input type="password" class="form-control" name="pwConfirm" id="pwConfirm" maxlength="20">
                </div>
                <div class="form-group">
                    <label>
                        E-Mail Address
                    </label>
                    <input type="text" class="form-control" name="txtEmail" id="txtEmail" maxlength="100">
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle" id="txtJobTitle" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Company
                    </label>
                    <input type="text" class="form-control" name="txtCompanyName" id="Contact_CompanyName" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Street
                    </label>
                    <input type="text" class="form-control" name="txtStreet" id="txtStreet" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Town/City
                    </label>
                    <input type="text" class="form-control" name="txtTownCity" id="txtTownCity" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        County/State
                    </label>
                    <input type="text" class="form-control" name="txtCounty" id="txtCounty" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Post Code / ZIP
                    </label>
                    <div class="input-group" style="width: 200px;">
                        <input type="text" class="form-control" name="txtPostCode" id="txtPostCode" maxlength="10" style="width: 120px; text-transform: uppercase">
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="button" id="btnPostCodeLookup" onclick="PostCodelookup()">
                                Lookup
                            </button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label>
                        Country
                    </label>
                    <input id="Contact_WorkAddressCountry" style="width:100%" />
                    </>
                </div>
                <div class="form-group">
                    <label>
                        Work Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtWorkTel" id="txtWorkTel" maxlength="20">
                </div>
                <div class="form-group">
                    <label>
                        Work Mobile Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtMobTel" id="txtMobTel" maxlength="20">
                </div>

                <hr />
                <div class="btn-group">
                    <a class="btn btn-primary" id="btnRegister" onclick="TriSysWeb.Pages.ClientRegistration.RegistrationButtonClick()">Register</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "registerclient",
                    ParentMenuName: "client",
                    CallbackFunction: TriSysWeb.Pages.ClientRegistration.Load
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function PostCodelookup()
        {
            TriSysSDK.PostCode.Lookup("txtPostCode", "txtStreet", "txtTownCity", "txtCounty", "Contact_WorkAddressCountry");
        }

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->