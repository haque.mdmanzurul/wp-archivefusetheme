
<h4>Homepage Blocks</h4><br />

<div class="smallpromo">

<h4>Block one</h4>

<div class="row">

	<div class="label">Block 1 Image</div>

    <div class="option-field">

    	<input type="text" name="fldPromo1BgImage" id="fldPromo1BgImage" size="60" value="<?php  echo get_option('fldPromo1BgImage'); ?>" /><a id="upload_promo1bg_button" href="#">Upload</a>

        <div id="showPromo1BgImage"><img src="<?php echo get_option('fldPromo1BgImage'); ?>" /></div>

    </div>

</div>



<div class="row">

	<div class="label">Block 1 Title</div>

    <div class="option-field">

    	<input type="text" name="fldPromo1ReadMoreLink" id="fldPromo1ReadMoreLink" size="60" value="<?php  echo get_option('fldPromo1ReadMoreLink'); ?>" />

    </div> 

</div> 

<div class="row">

	<div class="label">Block 1 Text</div>

    <div class="option-field">
		<textarea name="fldPromo1Text" id="fldPromo1Text" size="60"><?php  echo get_option('fldPromo1Text'); ?></textarea>
    </div>

</div>


</div>

<div class="smallpromo">

<h4>Block two</h4>

<div class="row">

	<div class="label">Block 2 Image</div>

    <div class="option-field">

    	<input type="text" name="fldPromo2BgImage" id="fldPromo2BgImage" size="60" value="<?php  echo get_option('fldPromo2BgImage'); ?>" /><a id="upload_promo2bg_button" href="#">Upload</a>

        <div id="showPromo2BgImage"><img src="<?php echo get_option('fldPromo2BgImage'); ?>" /></div>

    </div>

</div>


 

<div class="row">

	<div class="label">Block 2 Title</div>

    <div class="option-field">

    	<input type="text" name="fldPromo2ReadMoreLink" id="fldPromo2ReadMoreLink" size="60" value="<?php  echo get_option('fldPromo2ReadMoreLink'); ?>" />

    </div> 

</div> 


<div class="row">

	<div class="label">Block 2 Text</div>

    <div class="option-field">

    	<textarea name="fldPromo2Text" id="fldPromo2Text" size="60"><?php  echo get_option('fldPromo2Text'); ?></textarea>

    </div>

</div>




</div>



