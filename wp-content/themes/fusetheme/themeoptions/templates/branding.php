
<div class="row">

	<div class="label">Logo</div>

    <div class="option-field">

    	<input type="text" name="fldLogoUrl" id="fldLogoUrl" size="60" value="<?php  echo get_option('fldLogoUrl'); ?>" /><a id="upload_logo_button" href="#">Upload</a>

        <div id="showLogo">

        <img src="<?php echo get_bloginfo( 'url' ).'/'.get_option('fldLogoUrl'); ?>" />

        </div>

    </div>

</div>



<div class="row">

	<div class="label">Favicon</div>

    <div class="option-field">

    	<input type="text" name="fldFaviconUrl" id="fldFaviconUrl" size="60" /><a id="upload_favicon_button" href="#">Upload</a>

        <div id="showFavicon"></div>

    </div>

</div>



<div class="row">

	<div class="label">Copyright</div>

    <div class="option-field">

    	<textarea name="copyright" id="copyright" cols="50" rows="3"></textarea>

    </div>

</div>