<?php

// For selecting the sidebar banner size when uploading image

if ( function_exists( 'add_image_size' ) ) {

	add_image_size( 'Favicon', 15, 15, true );

	add_image_size( 'MainPromoImage', 974, 405, true );

}





/*  This function is for editing image sizes in wp image uploader screen  START*/ 

function kc_get_additional_image_sizes() {

	$sizes = array();

	global $_wp_additional_image_sizes;

	if ( isset($_wp_additional_image_sizes) && count($_wp_additional_image_sizes) ) {

		$sizes = apply_filters( 'intermediate_image_sizes', $_wp_additional_image_sizes );

		$sizes = apply_filters( 'kc_get_additional_image_sizes', $_wp_additional_image_sizes );

	}



	return $sizes;

}





function kc_additional_image_size_input_fields( $fields, $post ) {

	if ( !isset($fields['image-size']['html']) || substr($post->post_mime_type, 0, 5) != 'image' )

		return $fields;



	$sizes = kc_get_additional_image_sizes();

	if ( !count($sizes) )

		return $fields;



	$items = array();

	foreach ( array_keys($sizes) as $size ) {

		$downsize = image_downsize( $post->ID, $size );

		$enabled = $downsize[3];

		$css_id = "image-size-{$s}-{$post->ID}";

		$label = apply_filters( 'kc_image_size_name', $size );



		$html  = "<div class='image-size-item'>\n";

		$html .= "\t<input type='radio' " . disabled( $enabled, false, false ) . "name='attachments[{$post->ID}][image-size]' id='{$css_id}' value='{$size}' />\n";

		$html .= "\t<label for='{$css_id}'>{$label}</label>\n";

		if ( $enabled )

			$html .= "\t<label for='{$css_id}' class='help'>" . sprintf( "(%d � %d)", $downsize[1], $downsize[2] ). "</label>\n";

		$html .= "</div>";



		$items[] = $html;

	}



	$items = join( "\n", $items );

	$fields['image-size']['html'] = "{$fields['image-size']['html']}\n{$items}";



	return $fields;

}



add_filter( 'attachment_fields_to_edit', 'kc_additional_image_size_input_fields', 11, 2 );

/*  This function is for editing image sizes in wp image uploader screen  END */ 





/* call the function for adding defualt values in for theme

   @action after_theme_setup,wp_ajax_admin 

*/

function theme_options_default() {

	if(get_option('fldLogoUrl')=='')

	update_option('fldLogoUrl','/wp-content/themes/footbaltheme/images/logo.png');

	if(get_option('fldMainPromoUrl')=='')

	update_option('fldMainPromoUrl','/wp-content/themes/footbaltheme/images/mainpromo.png');

	

	if(get_option('fldFooterLogo')=='')

	update_option('fldFooterLogo','/wp-content/themes/footbaltheme/images/footer_logo.png');

}

add_action( 'after_setup_theme', 'theme_options_default' );





/**

* Adding necessary jquery plugins to admin panel

**/

if($_REQUEST['page']=='cvmaker_theme_options')

add_action('admin_print_scripts', 'cvmaker_admin_js');

function cvmaker_admin_js() {



/*		wp_deregister_script('jquery'); 

		wp_register_script('jquery', 'http://code.jquery.com/jquery-1.9.1.js'); 

		wp_enqueue_script('jquery');	*/

		

		

		wp_deregister_script( 'hashchange');

    	wp_register_script( 'hashchange', get_bloginfo('template_directory') . '/themeoptions/js/benelmam.hashchange.js');

		wp_enqueue_script('hashchange');



		

		wp_enqueue_script('media-upload');

		wp_enqueue_script('thickbox');



		wp_register_script('my-upload',  get_bloginfo('template_directory') . '/themeoptions/js/custom.js', array('media-upload','thickbox'));

		wp_enqueue_script('my-upload');

}



/**

* Adding necessary stylesheet to admin panel

**/

if($_REQUEST['page']=='cvmaker_theme_options')

add_action('admin_print_styles', 'cvmaker_admin_css');

function cvmaker_admin_css() {



		wp_enqueue_style('thickbox');

		$theme_options_style = get_bloginfo('template_directory') . '/themeoptions/style/theme_options_style.css';

		wp_register_style('theme_options_style', $theme_options_style);

		wp_enqueue_style( 'theme_options_style');

		

	

}



/**

** Register the theme options menu

*/

add_action('admin_menu', 'register_custom_menu_page');

function register_custom_menu_page() {

   	 add_menu_page( "Theme Options", "Theme Options", 10, "cvmaker_theme_options", "cvmaker_theme_options" );

}





/**

** Callback function for theme option register

*/

function cvmaker_theme_options(){

	include('templates/base.php');

}





/**

** AJAX CALL BACK FOR SAVING THEME OPTION

*/

add_action('wp_ajax_save_theme_options', 'save_theme_options_all');

function save_theme_options_all(){

	//include('templates/base.php');

	$posted_data = explode('&',$_REQUEST['data']);

	

//exit;

	foreach($posted_data as $data){

		$data = explode('=',$data);

		//if()

		//echo count($data);

		if(get_option($data[0])!=$data[1]){

		update_option($data[0],urldecode($data[1]));

		echo '3';

		}



	}

	echo '1';

}





?>