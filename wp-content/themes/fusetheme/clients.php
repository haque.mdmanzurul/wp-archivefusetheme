﻿<?php  
/* 
* Template name: Client Page
*/
get_header(); ?>

    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/Client.png"
                         height="150"><?php } ?>    
                </div>
                <div class="col-md-4" style="display: none;">

                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a client, you can conduct an ad-hoc search for suitable candidates within our database.
                You can search their skills profile and CV and compile a list of favourite candidates
                whos CV you may wish to read and potentially interview for a role within your organisation.' ); ?></p>
			<?php endif; ?>
            <div class="btn-group">
                <a href="#" class="btn btn-default">Search Candidates</a>
                <a href="#" class="btn btn-default">Manage Vacancies</a>
                <a href="#" class="btn btn-default">Placements &amp; Timesheets</a>
            </div>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Candidate Search
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input type="text" class="form-control" name="txtReference">
                </div>
                <div class="form-group">
                    <label>
                        Sector
                    </label>
                    <select class="form-control" name="cmbSector">
                        <option value="Any">
                            Any
                        </option>
                        <option value="Technology">
                            Technology
                        </option>
                        <option value="Science">
                            Science
                        </option>
                        <option value="Engineering">
                            Engineering
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>
                        Job Title / Keyword
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle">
                </div>
                <div class="btn-group">
                    <a href="#" class="btn btn-default">Search</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default" id="live-vacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Live Vacancies
                </h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>
                                Reference
                            </td>
                            <td>
                                Job Type
                            </td>
                            <td>
                                Job Title
                            </td>
                            <td>
                                Start Date
                            </td>
                            <td>
                                Location
                            </td>
                            <td>
                                Max Salary/Rate
                            </td>
                        </tr>
                        <tr>
                            <td>
                                JM/Req/000765
                            </td>
                            <td>
                                Temporary
                            </td>
                            <td>
                                Chef de Partie
                            </td>
                            <td>
                                01 Jul 2014
                            </td>
                            <td>
                                London
                            </td>
                            <td>
                                £20 per hour
                            </td>
                        </tr>
                        <tr>
                            <td>
                                JM/Req/000764
                            </td>
                            <td>
                                Permanent
                            </td>
                            <td>
                                Java Developer
                            </td>
                            <td>
                                01 Aug 2014
                            </td>
                            <td>
                                Cambridge
                            </td>
                            <td>
                                £20,000 per annum
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: true,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "clients",
                    ParentMenuName: "client"
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->