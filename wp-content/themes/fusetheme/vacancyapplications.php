﻿<?php  
/* 
* Template name: Vacancy Applications
*/
get_header(); ?>

    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h1>
              Vacancy Applications
            </h1>
          </div>
          <div class="col-md-4" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>         
            <img src="<?php bloginfo('template_directory'); ?>/images/VacancyApplications.png" height="150"><?php } ?>    
          </div>
        </div>
        <p>
          As a logged in candidate, the grid below shows all of the vacancies to which you have applied.
            You may open each vacancy to see the progress of your respective application, including
            helping schedule an interview, or accepting or rejecting a job offer, or withdrawing your application.
        </p>
        <p style="text-align: center">
        </p>
      </div>
    </div>

    
    <div class="container">
        <div class="panel panel-default" id="pnlLiveVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Applications
                </h3>
            </div>
            <div class="panel-body">


                <!-- The vacancy grid populated by JS -->
                <div id="divVacancyApplicationsGrid" style="border-width: 1px; width: 100%; ">
                </div>

                      
                <!-- The google map populated by JS -->
                <div id="divVacancyMap" style="border-width: 1px; width: 100%; height:600px; display:none ">
                </div>

            </div>
        </div>
      
    </div>

    <!-- Grid Actions -->
    <div class="container">
      <div class="panel panel-default" id="Div1">
        <div class="panel-heading">
          <h3 class="panel-title">
            Actions
          </h3>
        </div>
        <div class="panel-body">
         
         <div class="btn-group">
              <a class="btn btn-danger" id="btnApply" onclick="WithdrawApplication()">Withdraw Application</a>
              <a class="btn btn-default" id="btnSendToFriend" onclick="SendToFriendButton()">Send to Friend</a>
              <a class="btn btn-default" id="btnShowOnMap" onclick="ShowApplicationsOnMap()">Show on Map</a>
              <a class="btn btn-default" id="btnShowInGrid" onclick="ShowInGrid()" style="display:none">Show in Grid</a>
              <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
          </div>

         <div class="btn-group" style="float:right">
              <a class="btn btn-default" id="btnSelectAll" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyApplicationsGrid', true);">Select All</a>
              <a class="btn btn-default" id="btnSelectNone" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyApplicationsGrid', false)">Select None</a>
          </div>

        </div>
      </div>
    </div>
    <!-- End of Grid Actions -->

    <!-- /container -->

    
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: true,
                    SQLLogin: true,
                    PageName: "vacancyapplications",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Show vacancy applications after we can be sure that the form is loaded
                        setTimeout(TriSysWeb.Pages.VacancyApplications.LoadGrid, 50);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function WithdrawApplication()
        {
            var multipleSelectedRequirementIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRequirementIds)
                if (multipleSelectedRequirementIds.length > 0)
                {
                    var sURL = window.location.href.toLowerCase().replace(TriSysWeb.Constants.VacancyApplicationsPage, TriSysWeb.Constants.CandidateVacancyPage) + "?RequirementId=";
                    TriSysWeb.Pages.VacancyApplications.WithdrawApplication(sURL, multipleSelectedRequirementIds);
                }
        }
        function SendToFriendButton()
        {
            var multipleSelectedRequirementIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRequirementIds)
            {
                var sURL = window.location.href.toLowerCase().replace(TriSysWeb.Constants.VacancyApplicationsPage, TriSysWeb.Constants.CandidateVacancyPage) + "?RequirementId=";

                if (multipleSelectedRequirementIds.length > 0)
                {
                    TriSysWeb.Pages.CandidateVacancy.SendMultipleVacanciesToFriend(sURL, multipleSelectedRequirementIds);
                }
            }
        }
        function ShowApplicationsOnMap()
        {
            var vacanciesObject = TriSysWeb.Pages.CandidateVacancies;
            vacanciesObject.GridDivTag = 'divVacancyApplicationsGrid';
            vacanciesObject.ShowOnMap('', 'Vacancy/ApplicationsMapData');
        }
        function ShowInGrid()
        {
            $('#divVacancyApplicationsGrid').show();
            $('#divVacancyMap').hide();
            $('#btnShowOnMap').show();
            $('#btnShowInGrid').hide();
        }

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->