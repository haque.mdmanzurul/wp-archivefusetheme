﻿<?php  
/* 
* Template name: Edit Profile Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory');?>/images/EditProfile.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <!-- Contact Record Details -->
    <div class="container">
        <div class="panel panel-default" id="trisys-cms-opuslaboris-editprofile-form" ondblclick="TriSysCMS.Editor.Open(this);">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Account/Contact Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group" style="width:100%; float:left;">
                    <label style="width:100%;">
                        Title
                    </label>

                    <input id="Contact_ContactTitle" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Forename(s)
                    </label>
                    <input type="text" class="form-control" name="txtForename" id="Contact_Forenames" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Surname
                    </label>
                    <input type="text" class="form-control" name="txtSurname" id="Contact_Surname" maxlength="50">
                </div>
                <div class="form-group" style="width:49.5%; float:left;">
                    <label>
                        Password
                    </label>
                    <input type="password" class="form-control" name="pwPassword" id="ContactConfigFields_WebPassword" maxlength="20">
                </div>
                <div class="form-group" style="width:49.5%; float:right;">
                    <label>
                        Confirm Password
                    </label>
                    <input type="password" class="form-control" name="pwConfirm" id="pwConfirm" maxlength="20">
                </div>
                <div class="form-group" id="grpClientEMail" style="display:none">
                    <label>
                        Work E-Mail Address
                    </label>
                    <input type="text" class="form-control" id="Contact_EMail" maxlength="100">
                </div>
                <div class="form-group" id="grpCandidateEMail" style="display:none">
                    <label>
                        Personal E-Mail Address
                    </label>
                    <input type="text" class="form-control" id="ContactConfigFields_AlternativeEMailAddress1" maxlength="100">
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle" id="txtJobTitle" maxlength="50">
                </div>
                <div class="form-group" id="grpClientCompany" style="display:none">
                    <label>
                        Company
                    </label>
                    <input type="text" class="form-control" name="txtCompanyName" id="Contact_CompanyName" maxlength="50" readonly="readonly">
                </div>
                <!-- HOME ADDRESS  -->
                <div id="divCandidateHomeAddress" style="display:none">
                    <div class="form-group">
                        <label>
                            Street
                        </label>
                        <textarea class="form-control" name="HomeAddressStreet" id="Contact_HomeAddressStreet" rows="3" style="resize: none"></textarea>
                    </div>
                    <div class="form-group">
                        <label>
                            Town/City
                        </label>
                        <input type="text" class="form-control" name="txtTownCity" id="Contact_HomeAddressCity" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label>
                            County/State
                        </label>
                        <input type="text" class="form-control" name="txtCounty" id="Contact_HomeAddressCounty" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label>
                            Post Code / ZIP
                        </label>
                        <div class="input-group" style="width: 200px;">
                            <input type="text" class="form-control" name="txtPostCode" id="Contact_HomeAddressPostCode" maxlength="10" style="width: 120px; text-transform: uppercase">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="button" id="btnPostCodeLookup" onclick="PostCodelookup()">
                                    Lookup
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            Country
                        </label>
                        <input id="Contact_HomeAddressCountry" style="width:100%" />
                        </>
                    </div>
                </div>
                <!-- END OF HOME ADDRESS  -->
                <!-- WORK/COMPANY ADDRESS  -->
                <div id="divClientCompanyAddress" style="display:none">
                    <div class="form-group" style="display:none">
                        <label>
                            Company Address
                        </label>
                        <textarea class="form-control" name="CompanyAddress" id="Contact_CompanyAddress" style="resize: none;" readonly="readonly" rows="7"></textarea>
                    </div>
                    <div class="form-group">
                        <label>
                            Company Address
                        </label>
                        <input id="Contact_CompanyAddressList" style="width:100%" />
                    </div>
                    <div class="form-group" style="display:none">
                        <label>
                            Company Address ID
                        </label>
                        <div id="Contact_CompanyAddressId"></div>
                    </div>
                </div>
                <!-- END OF WORK/COMPANY ADDRESS  -->
                <!-- TELEPHONE NUMBERS -->
                <div class="form-group" id="grpCandidateHomeTelNo" style="display:none">
                    <label>
                        Home Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtHomeTel" id="Contact_HomeAddressTelNo" maxlength="20">
                </div>
                <div class="form-group" id="grpClientWorkTelNo" style="display:none">
                    <label>
                        Work Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtWorkTel" id="Contact_WorkTelNo" maxlength="20">
                </div>
                <div class="form-group" id="grpCandidatePersonalMobile" style="display:none">
                    <label>
                        Personal Mobile Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtMobTel" id="Contact_MobileTelNo" maxlength="20">
                </div>
                <div class="form-group" id="grpClientWorkMobileTelNo" style="display:none">
                    <label>
                        Work Mobile Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtWorkMobTel" id="ContactConfigFields_WorkMobile" maxlength="20">
                </div>
                <!-- END OF TELEPHONE NUMBERS -->
                <div class="form-group" id="grpCandidateCVFileRef" style="display:none">
                    <label>
                        CV File Attachment
                    </label>
                    <div id="ContactConfigFields_CVFileRef" style="width:100%; margin-right:110px;"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of Contact Record Details -->
    <!-- TriSys Web Profile Details -->
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Profile Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group" style="width:100%; float:left;">
                    <label style="width:100%;">
                        Photo
                    </label>

                    <img id="imgLoggedInUserImageEditor" />
                    <br />
                    <div id="ContactConfigFields_ContactPhoto" style="width:100%;"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of TriSys Web Profile Details -->
    <!-- Form Actions -->
    <div class="container">
        <div class="panel panel-default" id="pnlProfileActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Form Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-primary" id="btnSaveProfile" onclick="TriSysWeb.Pages.EditProfile.SaveButtonClick()">Save</a>
                    <a class="btn btn-default" id="btnDeleteProfile" onclick="TriSysWeb.Pages.EditProfile.DeleteButtonClick()" style="display:none">Delete</a>
                    <a class="btn btn-default" id="btnAskQuestion" onclick="TriSysWeb.Pages.ContactUs.Open()">Ask a Question</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Form Actions -->

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "editprofile",
                    ParentMenuName: "candidate",
                    CallbackFunction: TriSysWeb.Pages.EditProfile.Load
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);

                // CMS functionality
                TriSysCMS.PageManager.LoadContent(true);
            });
        });

        function PostCodelookup()
        {
            TriSysSDK.PostCode.Lookup("Contact_HomeAddressPostCode", "Contact_HomeAddressStreet", "Contact_HomeAddressCity", "Contact_HomeAddressCounty", "Contact_HomeAddressCountry");
        }

    </script>
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->