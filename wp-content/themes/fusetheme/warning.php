﻿<?php  
/* 
* Template name: Waring
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h1>
                        Warning
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/warning.png" height="150">
                <?php } ?>    
                </div>
            </div>
            <p>
                The page you are attempting to view requires you to be logged in.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Warning Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        This web site operates using a security system which displays data appropriate to the users credentials.
                        <br />
                        Please login using your e-mail address and password using the fields and button at the top of this page.
                        <br />
                        If you have forgotten your password, please request it by e-mail using the button below.
                    </label>
                </div>
                <div class="btn-group">
                    <a href="forgottenpassword.html" class="btn btn-primary">Forgotten Password</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: false,
                    PageName: "warning"
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
</body>

</html>