<?php  
/* 
* Page Template
*/
get_header(); ?>

    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/OpusLaborisRecruitment-AboutUs-2014.png"
                         height="150"><?php } ?>    
                </div>
            </div>
             <p>
                <?php 
				$content= get_post_meta( get_the_ID(),'subheading',true );  
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a candidate, you can search our database of live vacancies, register
                your details, build your CV, apply for jobs and complete on-line timesheets.' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">            
    <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-12">
             <p>
                <?php 
				$content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a candidate, you can search our database of live vacancies, register
                your details, build your CV, apply for jobs and complete on-line timesheets.' ); ?></p>
			<?php endif; ?>
 </div>
 </div>
    </div>
    <!-- /container -->
    

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: false,
                    PageName: "aboutus"
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);

                // CMS functionality
                TriSysCMS.PageManager.LoadContent(true);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->
</html>
