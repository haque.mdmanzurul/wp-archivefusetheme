<?php 
/* Template name: Vacancy Search Page
*/
get_header(); 

?>

    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>                
                    <img src="<?php bloginfo('template_directory') ?>/images/Vacancy-Search.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
            </p>
            <p style="text-align: center">
            </p>
            <?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Search
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input type="text" class="form-control" name="txtReference" id="txtReference">
                </div>
                <div class="form-group">
                    <label>
                        Job Title / Keyword
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle" id="txtJobTitle">
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <select id="cmbTypeOfWorkRequired" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <select id="cmbLocation" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <table>
                        <tr>
                            <td style="width: 120px">
                                <select id="cmbStartDateOperator" style="width:auto;" />
                            </td>
                            <td style="width: 200px">
                                <input id="dtStartDate" style="width: 200px; " />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <label>
                        Salary/Rate Range Minimum
                    </label>
                    <div id="salaryMinimum" style="width:100%;"></div>
                </div>
                <div class="form-group">
                    <label>
                        Salary/Rate Range Maximum
                    </label>
                    <div id="salaryMaximum" style="width:100%"></div>
                </div>
                <hr />
                <div class="btn-group">
                    <a href="#" class="btn btn-primary" id="btnSearch" onclick="TriSysWeb.Pages.CandidateVacancies.SearchPersistence.VacancySearchButton()">Search</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default" id="pnlLiveVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Live Vacancies
                </h3>
            </div>
            <div class="panel-body">

                <!-- The vacancy grid populated by JS -->
                <div id="divVacancyGrid" style="border-width: 1px; width: 100%; ">
                </div>
                <!-- The google map populated by JS -->
                <div id="divVacancyMap" style="border-width: 1px; width: 100%; height:600px; display:none ">
                </div>
            </div>
        </div>

    </div>

    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="divVacancySearchActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnApply" onclick="ApplyButton()">Apply</a>
                    <a class="btn btn-default" id="btnAddToFavourites" onclick="AddToFavouritesButton()">Add to Favourites</a>
                    <a class="btn btn-info" id="btnSendToFriend" onclick="SendToFriendButton()">Send to Friend</a>
                    <a class="btn btn-default" id="btnShowOnMap" onclick="TriSysWeb.Pages.CandidateVacancies.ShowOnMap()">Show on Map</a>
                    <a class="btn btn-default" id="btnShowInGrid" onclick="TriSysWeb.Pages.CandidateVacancies.ShowInGrid()" style="display:none">Show in Grid</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
                <div class="btn-group" style="float:right">
                    <a class="btn btn-default" id="btnSelectAll" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyGrid', true);">Select All</a>
                    <a class="btn btn-default" id="btnSelectNone" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyGrid', false)">Select None</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->

   <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "vacancysearch",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysSDK.CShowForm.skillList("cmbTypeOfWorkRequired", "TypeOfWorkRequired");
                        TriSysWeb.Pages.CandidateVacancies.PopulateLocationLookup("cmbLocation");
                        TriSysWeb.Pages.CandidateVacancies.PopulateStartDateSearchWidgets("cmbStartDateOperator", "dtStartDate");
                        TriSysWeb.Pages.CandidateVacancies.PopulateCurrencyAmountPeriodWidget("salaryMinimum");
                        TriSysWeb.Pages.CandidateVacancies.PopulateCurrencyAmountPeriodWidget("salaryMaximum");

                        // Show all live vacancies after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.CandidateVacancies.SearchPersistence.ReloadLastSearchCriteriaAndReSearch, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function ApplyButton()
        {
            var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRecordIds)
                if (multipleSelectedRecordIds.length > 0)
                {
                    var sURL = window.location.href.toLowerCase().replace(TriSysWeb.Constants.CandidateVacancySearchPage, TriSysWeb.Constants.CandidateVacancyPage) + "?RequirementId=";
                    TriSysWeb.Pages.CandidateVacancy.ApplyToMultiple(sURL, multipleSelectedRecordIds);
                }
        }
        function AddToFavouritesButton()
        {
            var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRecordIds)
                if (multipleSelectedRecordIds.length > 0)
                    TriSysWeb.Pages.CandidateVacancy.AddListToFavourites(multipleSelectedRecordIds);
        }
        function SendToFriendButton()
        {
            var multipleSelectedRequirementIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            if (multipleSelectedRequirementIds)
            {
                var sURL = window.location.href.toLowerCase().replace(TriSysWeb.Constants.CandidateVacancySearchPage, TriSysWeb.Constants.CandidateVacancyPage) + "?RequirementId=";

                if (multipleSelectedRequirementIds.length > 0)
                {
                    TriSysWeb.Pages.CandidateVacancy.SendMultipleVacanciesToFriend(sURL, multipleSelectedRequirementIds);
                }
            }
        }

    </script>
<?php get_footer() ?>