﻿<?php  
/* 
* Template name: Client Vacancy Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/Vacancy.png"  height="150"><?php } ?>    
                </div>
                <div class="col-md-4" style="display: none;">

                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input id="RequirementConfigFields_RequirementReference" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <br />
                    <input id="RequirementConfigFields_RequirementStatus" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <br />
                    <input id="RequirementConfigFields_JobTitle" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <input id="Requirement_Type" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <br />
                    <input id="RequirementConfigFields_EarliestStartDate" style="width:240px" />
                </div>
                <div class="form-group" id="fieldGroup-ContractDuration">
                    <label>
                        Duration in Weeks
                    </label>
                    <br />
                    <input id="RequirementConfigFields_DurationInWeeks" style="width:100px" />
                </div>
                <div class="form-group">
                    <label>
                        Date Posted
                    </label>
                    <br />
                    <input id="RequirementConfigFields_DateInitiated" readonly="readonly" style="width:240px" />
                </div>
                <div class="form-group">
                    <label>
                        Site Address
                    </label>
                    <input id="RequirementConfigFields_SiteAddressList" style="width:100%" />
                </div>
                <div class="form-group" style="display:none">
                    <label>
                        Site Address ID
                    </label>
                    <div id="RequirementConfigFields_SiteAddressId"></div>
                </div>
                <div class="form-group" style="display:none">
                    <label>
                        Site Address
                    </label>
                    <div id="RequirementConfigFields_SiteAddress"></div>
                </div>
                <div class="form-group">
                    <label>
                        Job Description
                    </label>
                    <textarea class="k-textbox" id="RequirementConfigFields_WebText" rows="5" style="width:100%; resize: none"></textarea>
                </div>
                <div class="form-group" style="display:none">
                    <label>
                        Internal Comments
                    </label>
                    <textarea class="k-textbox" id="RequirementConfigFields_Comments" rows="5" style="width:100%; resize: none"></textarea>
                </div>
                <div class="form-group" id="fieldGroup-JobDescriptionDocument">
                    <label>
                        Job Description Document
                    </label>
                    <div id="RequirementConfigFields_JobDescription" style="width:100%;"></div>
                </div>
                <br />
                <div class="form-group" id="fieldGroup-Salary">
                    <label>
                        Maximum Salary
                    </label>
                    <div id="RequirementConfigFields_MaximumSalary" style="width:100%"></div>
                </div>
                <div class="form-group" id="fieldGroup-Rate">
                    <label>
                        Maximum Charge Rate
                    </label>
                    <div id="RequirementConfigFields_MaximumChargeRate" style="width:100%"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="pnlClientVacancy">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-primary" id="btnSave" onclick="TriSysWeb.Pages.ClientVacancy.Save()">Save</a>
                    <a class="btn btn-default" id="btnDelete" onclick="TriSysWeb.Pages.ClientVacancy.Delete()">Delete</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->
    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "clientvacancy",
                    ParentMenuName: "client",
                    CallbackFunction: function ()
                    {
                        // We expect a URL with ?RequirementId= parameter
                        TriSysWeb.Pages.ClientVacancy.LoadRecordFromURL(window.location.href);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });
    </script>
</body>

</html>
