<?php  
/* 
* Template name: Vacancy Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-8">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3">
                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>                
                    <img src="<?php bloginfo('template_directory'); ?>/images/Vacancy.png"
                         height="150"><?php } ?>    
                </div>
                <div class="col-md-4" style="display: none;">

                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input id="RequirementConfigFields_RequirementReference" readonly style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <br />
                    <input id="RequirementConfigFields_RequirementStatus" readonly style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <br />
                    <input id="RequirementConfigFields_JobTitle" readonly style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <input id="Requirement_Type" readonly style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <br />
                    <input id="RequirementConfigFields_EarliestStartDate" readonly style="width:240px" />
                </div>
                <div class="form-group" id="fieldGroup-ContractDuration">
                    <label>
                        Duration in Weeks
                    </label>
                    <br />
                    <input id="RequirementConfigFields_DurationInWeeks" readonly style="width:100px" />
                </div>
                <div class="form-group">
                    <label>
                        Date Posted
                    </label>
                    <br />
                    <input id="RequirementConfigFields_DateInitiated" readonly style="width:240px" />
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <input id="RequirementConfigFields_SiteAddress" readonly style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Job Description
                    </label>
                    <textarea class="k-textbox" id="RequirementConfigFields_WebText" rows="5" readonly style="width:100%"></textarea>
                </div>
                <div class="form-group" id="fieldGroup-JobDescriptionDocument">
                    <label>
                        Job Description Document
                    </label>
                    <div id="RequirementConfigFields_JobDescription" style="width:100%;"></div>

                </div>
                <br />
                <div class="form-group" id="fieldGroup-Salary">
                    <label>
                        Maximum Salary
                    </label>
                    <input id="RequirementConfigFields_MaximumSalary" readonly style="width:100%" />
                </div>
                <div class="form-group" id="fieldGroup-Rate">
                    <label>
                        Maximum Rate
                    </label>
                    <input id="RequirementConfigFields_MaximumRate" readonly style="width:100%" />
                </div>

            </div>
        </div>
    </div>
    <!-- Interviews Grid -->
    <div class="container" id="divInterviewsContainer">
        <div class="panel panel-default" id="pnlCandidateInterviews">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Stage (Application/Interviews/Offers)
                </h3>
            </div>
            <div class="panel-body">

                <!-- The shortlist stages grid populated by JS -->
                <div id="divVacancyShortlistGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>
    </div>
    <!-- End of Interviews Grid -->
    <!-- Actions -->
    <div class="container">
        <div class="panel panel-default" id="pnlLiveVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnApply" onclick="ApplyButton()">Apply</a>
                    <a class="btn btn-default" id="btnWithdrawVacancyApplication" onclick="WithdrawVacancyApplicationButton()" style="display:none">Withdraw Application</a>
                    <a class="btn btn-info" id="btnAddToFavourites" onclick="AddToFavouritesButton()">Add to Favourites</a>
                    <a class="btn btn-default" id="btnRemoveFromFavourites" onclick="RemoveFromFavouritesButton()" style="display:none">Remove from Favourites</a>
                    <a class="btn btn-default" id="btnSendToFriend" onclick="SendToFriendButton()">Send to Friend</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "vacancy",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // We expect a URL with ?RequirementId= parameter
                        TriSysWeb.Pages.CandidateVacancy.LoadRecordFromURL(window.location.href);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function ApplyButton()
        {
            TriSysWeb.Pages.CandidateVacancy.Apply(window.location.href);
        }
        function WithdrawVacancyApplicationButton()
        {
            TriSysWeb.Pages.VacancyApplications.WithdrawApplicationFromURL(window.location.href);
        }
        function AddToFavouritesButton()
        {
            TriSysWeb.Pages.CandidateVacancy.AddToFavourites(window.location.href);
        }
        function RemoveFromFavouritesButton()
        {
            TriSysWeb.Pages.CandidateVacancy.RemoveFromFavourites(window.location.href);
        }
        function SendToFriendButton()
        {
            TriSysWeb.Pages.CandidateVacancy.SendToFriend(window.location.href);
        }

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->
