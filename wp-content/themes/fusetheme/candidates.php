﻿<?php  
/* 
* Template name: Candidate Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3">
               <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/Candidate.png"
                         height="150">
                <?php } ?>         
                </div>
                <div class="col-md-4" style="display: none;">
                    <h3>
                        Span 4
                    </h3>
                    <p>
                        Content
                    </p>
                </div>
            </div>
             <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a candidate, you can search our database of live vacancies, register
                your details, build your CV, apply for jobs and complete on-line timesheets.' ); ?></p>
			<?php endif; ?>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Search
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input type="text" class="form-control" name="txtReference" id="txtReference">
                </div>
                <div class="form-group">
                    <label>
                        Job Title / Keyword
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle" id="txtJobTitle">
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <select id="cmbTypeOfWorkRequired" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <select id="cmbLocation" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <table>
                        <tr>
                            <td style="width: 120px">
                                <select id="cmbStartDateOperator" style="width:auto;" />
                            </td>
                            <td style="width: 200px">
                                <input id="dtStartDate" style="width: 200px; " />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <label>
                        Salary/Rate Range
                    </label>
                    <table>
                        <tr>
                            <td style="width: 340px">
                                <div id="salaryMinimum" style="width:100%;"></div>
                            </td>
                            <td style="width: 50px; text-align: center">
                                to
                            </td>
                            <td style="width: 340px">
                                <div id="salaryMaximum" style="width:100%;"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr />
                <div class="btn-group">
                    <a href="#" class="btn btn-default" id="btnSearch" onclick="VacancySearchButton()">Search</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default" id="pnlLiveVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Live Vacancies
                </h3>
            </div>
            <div class="panel-body">

                <!-- The vacancy grid populated by JS -->
                <div id="divVacancyGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "candidates",
                    ParentMenuName: "services",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysSDK.CShowForm.skillList("cmbTypeOfWorkRequired", "TypeOfWorkRequired");
                        TriSysWeb.Pages.CandidateVacancies.PopulateLocationLookup("cmbLocation");
                        TriSysWeb.Pages.CandidateVacancies.PopulateStartDateSearchWidgets("cmbStartDateOperator", "dtStartDate");
                        TriSysWeb.Pages.CandidateVacancies.PopulateCurrencyAmountPeriodWidget("salaryMinimum");
                        TriSysWeb.Pages.CandidateVacancies.PopulateCurrencyAmountPeriodWidget("salaryMaximum");

                        // Show all live vacancies
                        //TriSysWeb.Pages.CandidateVacancies.VacancySearch('divVacancyGrid');
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function VacancySearchButton()
        {
            // Show all live vacancies
            TriSysWeb.Pages.CandidateVacancies.VacancySearch('divVacancyGrid');
        }

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->