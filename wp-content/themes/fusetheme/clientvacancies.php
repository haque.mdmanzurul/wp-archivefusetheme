﻿<?php  
/* 
* Template name: Client Vacancies Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/Vacancy-Search.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a client, you can conduct an ad-hoc search for suitable candidates within our database.
                You can search their skills profile and CV and compile a list of favourite candidates
                whos CV you may wish to read and potentially interview for a role within your organisation.' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <!-- Search Box -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancy Search
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input type="text" class="form-control" name="txtReference" id="txtReference">
                </div>
                <div class="form-group">
                    <label>
                        Job Title / Keyword
                    </label>
                    <input type="text" class="form-control" name="txtJobTitle" id="txtJobTitle">
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <select id="cmbTypeOfWorkRequired" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <select id="cmbLocation" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <table>
                        <tr>
                            <td style="width: 120px">
                                <select id="cmbStartDateOperator" style="width:auto;" />
                            </td>
                            <td style="width: 200px">
                                <input id="dtStartDate" style="width: 200px; " />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <select id="cmbStatus" style="width:auto;"></select>
                </div>
                <hr />
                <div class="btn-group">
                    <a class="btn btn-default" id="btnSearch" onclick="TriSysWeb.Pages.ClientVacancies.SearchPersistence.VacancySearchButton()">Search</a>
                </div>
            </div>
        </div>
    </div>   <!-- End of Search Box -->
    <div class="container">
        <div class="panel panel-default" id="pnlClientVacancies">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Vacancies
                </h3>
            </div>
            <div class="panel-body">

                <!-- The vacancy grid populated by JS -->
                <div id="divVacancyGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="divVacancySearchActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnAdd" onclick="AddVacancyButton()">Add New Vacancy</a>
                    <a class="btn btn-default" id="btnChangeStatus" onclick="ChangeStatusButton()">Change Status</a>
                    <a class="btn btn-default" id="btnDelete" onclick="DeleteButton()">Delete</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
                <div class="btn-group" style="float:right">
                    <a class="btn btn-default" id="btnSelectAll" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyGrid', true);">Select All</a>
                    <a class="btn btn-default" id="btnSelectNone" onclick="TriSysSDK.Grid.SelectAllRows('divVacancyGrid', false)">Select None</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->

    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: true,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "clientvacancies",
                    ParentMenuName: "client",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysSDK.CShowForm.skillList("cmbTypeOfWorkRequired", "TypeOfWorkRequired");
                        TriSysWeb.Pages.ClientVacancies.PopulateLocationLookup("cmbLocation");
                        TriSysWeb.Pages.ClientVacancies.PopulateStatusLookup("cmbStatus");
                        TriSysWeb.Pages.CandidateVacancies.PopulateStartDateSearchWidgets("cmbStartDateOperator", "dtStartDate");

                        // Show all vacancies after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.ClientVacancies.SearchPersistence.ReloadLastSearchCriteriaAndReSearch, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

        function AddVacancyButton()
        {
            window.location = TriSysWeb.Constants.ClientVacancyPage + "?RequirementId=0";
        }
        function ChangeStatusButton()
        {
            TriSysApex.UI.ShowMessage("Change status of selected vacancies...");
            //var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            //if (multipleSelectedRecordIds)
            //    if (multipleSelectedRecordIds.length > 0)
            //    {
            //        //var sURL = window.location.href.toLowerCase().replace("vacancysearch.html", "vacancy.html") + "?RequirementId=";
            //        //TriSysWeb.Pages.CandidateVacancy.ApplyToMultiple(sURL, multipleSelectedRecordIds);
            //    }
        }
        function DeleteButton()
        {
            TriSysApex.UI.ShowMessage("Delete selected vacancies...");
            //var multipleSelectedRecordIds = TriSysSDK.Grid.GetSelectedRowRecordIds();
            //if (multipleSelectedRecordIds)
            //    if (multipleSelectedRecordIds.length > 0)
            //        TriSysWeb.Pages.CandidateVacancy.AddListToFavourites(multipleSelectedRecordIds);
        }

    </script>
</body>


</html>