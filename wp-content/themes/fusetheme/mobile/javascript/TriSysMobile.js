﻿var ApplicationName = "TriSys Mobile";
var ApplicationVersion = "1.4.131219.1408";

$(document).ready(function ()
{
    console.log('$(document).ready(function ()');

    $('#loginPageHeader').text(ApplicationName);
    $('#lblApplicationName').text(ApplicationName);
    $('#lblSoftwareVersion').text(ApplicationVersion);

    DisplayCookieKeys();
    InitialiseControlEventHandlers();
});

// Must explictly reference ONLY after their page is displayed or there
// appropriate initialisation events fire.
//
function InitialiseControlEventHandlers()
{
    $("#SavePasswordToggle").change(function ()
    {
        var bSaveLoginCredentials = TriSys.Operators.stringToBoolean(TriSys.UI.GetCheckBoxSliderValue('#SavePasswordToggle'));

        if (!bSaveLoginCredentials)
        {
            // Clear password in storage to prevent auto-login
            var sPassword = '';
            $("#txtPassword").val(sPassword);
            TriSys.Session.SetPassword(sPassword);
            TriSys.Session.SetSaveLoginCredentials(false);
        }
    });

    $(document).on('keydown', '#txtContactSearchText', function (event)
    {
        // Look for 13 - which is the CR/LF
        if (event.which == 13)
        {
            var sSearchString = $("#txtContactSearchText").val();
            console.log(sSearchString);
            loadContacts(1);
        }
    });

    $(document).on('click', '.ui-input-clear', function (event)
    {
        loadContacts(1);
    });

    $(document).on('pagebeforeshow', '#pgSettings', function ()
    {
        var iRecordsPerPage = TriSys.Session.RecordsPerPage();
        console.log(iRecordsPerPage);
        $('#sliderGridPageRecords').val(iRecordsPerPage).slider('refresh');
    });
}

function DisplayCookieKeys()
{

    var sUserName = TriSys.Session.UserName();
    var sCompanyName = TriSys.Session.CompanyName();
    var sPassword = TriSys.Session.Password();
    var bSaveLoginCredentials = TriSys.Session.SaveLoginCredentials();

    $("#txtUserName").val(sUserName);
    $("#txtCompanyName").val(sCompanyName);

    TriSys.UI.SetCheckBoxSliderValue('#SavePasswordToggle', TriSys.Operators.booleanToString(bSaveLoginCredentials));

    if (bSaveLoginCredentials && sPassword)
    {
        $("#txtPassword").val(sPassword);

        // Auto-login 
        Login();
    }
}

function Login()
{
    var sUserName = $('input[id=txtUserName]').val();
    var sCompanyName = $('input[id=txtCompanyName]').val();
    var sPassword = $('input[id=txtPassword]').val();
    var bSaveLoginCredentials = TriSys.UI.GetCheckBoxSliderValue('#SavePasswordToggle');

    if (!sUserName || !sCompanyName || !sPassword)
    {
        alert('Please enter all user login credentials.');
        return;
    }

    showLoadingMessagePopup(true, "Logging in...");

    var sSiteKey = TriSys.Session.DeveloperSiteKey();
    if (!sSiteKey)
    {
        sSiteKey = "20131219-ba09-4e4c-8bcb-abcguikeisj6";
        TriSys.Session.SetDeveloperSiteKey(sSiteKey);
    }

    var dataPacket = {
        'FullName': sUserName,
        'CompanyName': sCompanyName,
        'Password': sPassword
    };

    var payloadObject = {};

    payloadObject.URL = "Security/AuthenticateTriSysCRMCredentials";

    payloadObject.OutboundDataPacket = dataPacket;

    payloadObject.InboundDataFunction = function (data)
    {

        var sDataServicesKey = data;

        if (sDataServicesKey && sDataServicesKey.length > 0)
        {

            // Write to screen
            $("#txtDataServicesKey").val(sDataServicesKey);
            var sCaption = ApplicationName + ': ' + sUserName;
            $("#mainMenuHeader").text(sCaption);

            // Record in session state
            TriSys.Session.SetDataServicesKey(sDataServicesKey);
            TriSys.Session.SetUserName(sUserName);
            TriSys.Session.SetCompanyName(sCompanyName);
            TriSys.Session.SetSaveLoginCredentials(bSaveLoginCredentials);

            if (bSaveLoginCredentials)
                TriSys.Session.SetPassword(sPassword);

            // Inform user
            showLoadingMessagePopup(false);

            showLoadingMessagePopup(true, "Loading Main Menu...");

            // Switch to main menu page
            changePageToMainMenu();

            // Calculate the entity counters for the main menu
            refreshEntityCounters();
        }
        else
        {
            showLoadingMessagePopup(false);
            alert('Incorrect login credentials.');
        }
    };

    payloadObject.ErrorHandlerFunction = function (request, status, error)
    {
        showLoadingMessagePopup(false);
        alert('Login: ' + status + ": " + error + ". responseText: " + request.responseText);
    }

    TriSys.Data.PostToWeb(payloadObject);
}

function changePageToMainMenu()
{
    // Switch to main menu page
    $.mobile.changePage($("#pgMenu"), "slide", true, true);
}

// Called after the user has logged in
function refreshEntityCounters()
{
    var payloadObject = {};

    payloadObject.URL = "Counters/EntityCount";

    payloadObject.InboundDataFunction = function (data)
    {

        var objCCountersSearchResults = data;

        showLoadingMessagePopup(false);

        if (objCCountersSearchResults)
        {

            // Assign local variables to server counters
            var lContacts = objCCountersSearchResults.Contacts;
            var lCompanies = objCCountersSearchResults.Companies;
            var lRequirements = objCCountersSearchResults.Requirements;
            var lPlacements = objCCountersSearchResults.Placements;
            var lTimesheets = objCCountersSearchResults.Timesheets;
            var lUsers = objCCountersSearchResults.Users;
            var lTasks = objCCountersSearchResults.Tasks;
            var lGDrive = lUsers;    // TODO

            // Write to screen
            $("#entityCounter_Contacts").text(lContacts);
            $("#entityCounter_Companies").text(lCompanies);
            $("#entityCounter_Requirements").text(lRequirements);
            $("#entityCounter_Placements").text(lPlacements);
            $("#entityCounter_Timesheets").text(lTimesheets);
            $("#entityCounter_Tasks").text(lTasks);
            $("#entityCounter_GDrive").text(lGDrive);
        }
        else
        {
            alert('No counters found.');
        }
    };

    payloadObject.ErrorHandlerFunction = function (request, status, error)
    {
        showLoadingMessagePopup(false);
        alert('Counters: ' + status + ": " + error + ". responseText: " + request.responseText);
    }

    TriSys.Data.PostToWeb(payloadObject);
}

function loadContacts(iPageNumber)
{
    showLoadingMessagePopup(true, "Loading Contacts...");

    var payloadObject = {};

    var sWildcard = $("#txtContactSearchText").val();
    var sForename = "";
    var sSurname = "";
    var sCompanyName = "";
    var iRecordsPerPage = $('#sliderGridPageRecords').val();

    var dataPacket = {
        'Wildcard': sWildcard,
        'Forenames': sForename,
        'Surname': sSurname,
        'CompanyName': sCompanyName,
        'PageNumber': iPageNumber,
        'RecordsPerPage': iRecordsPerPage,
        'SortColumnName': ''
    };

    payloadObject.OutboundDataPacket = dataPacket;

    payloadObject.URL = "Contacts/Search";

    payloadObject.InboundDataFunction = function (data)
    {
        var objSearchResults = data;

        showLoadingMessagePopup(false);

        if (objSearchResults)
        {
            var sDynamic = entityCollectionToDisplayList(objSearchResults, objSearchResults.Contacts, "Contact");

            //http://stackoverflow.com/questions/15885863/dynamically-populating-li-within-jquery-mobile-data-role-listview
            document.querySelector('#listOfContacts').innerHTML = sDynamic;

            try
            {
                $('#listOfContacts').listview('refresh');
            }
            catch (ex)
            {
                // JQuery raises an error the first time we populate this
                // Simply ignore it!
            }

            $.mobile.changePage($("#pgContacts"), "slide", true, true);

            showLoadingMessagePopup(false);
        }
        else
        {
            alert('No contacts found.');
        }
    };

    payloadObject.ErrorHandlerFunction = function (request, status, error)
    {
        showLoadingMessagePopup(false);
        alert('Contacts: ' + status + ": " + error + ". responseText: " + request.responseText);
    }

    TriSys.Data.PostToWeb(payloadObject);
}

function entityCollectionToDisplayList(CEntitySearchResults_Object, entityCollection, sEntityName)
{
    var sRecordCountText = CEntitySearchResults_Object.TotalRecordCount + " Records";
    var sGridHeaderText = "Page " + CEntitySearchResults_Object.PageNumber + " of " +
                                        CEntitySearchResults_Object.TotalPageCount;
    sGridHeaderText = '<table width="100%"><tr>' +
                        '<td style="width: 50%">' +
                        sGridHeaderText +
                        '</td>' +
                        '<td style="width: 50%; text-align: right;">' +
                        sRecordCountText +
                        '</td>' +
                        '</tr></table>';


    $('#listViewHeaderMetric').html(sGridHeaderText);
    $('#listViewFooterMetric').html(sGridHeaderText);

    if (!entityCollection || CEntitySearchResults_Object.TotalRecordCount <= 0)
        return null;

    var iPreviousPageNumber = parseInt(CEntitySearchResults_Object.PageNumber) - 1;
    var sPreviousFunction = "javascript:loadContacts(" + iPreviousPageNumber + ");";
    if (CEntitySearchResults_Object.PageNumber == 1)
        sPreviousFunction = '#';

    var aPrevious = document.getElementById("gridPagerPrevious");
    aPrevious.setAttribute("href", sPreviousFunction);

    var iNextPageNumber = parseInt(CEntitySearchResults_Object.PageNumber) + 1;
    var sNextFunction = "javascript:loadContacts(" + iNextPageNumber + ");";
    if (CEntitySearchResults_Object.PageNumber >= CEntitySearchResults_Object.TotalPageCount)
        sNextFunction = '#';

    var aNext = document.getElementById("gridPagerNext");
    aNext.setAttribute("href", sNextFunction);

    var sContent = '';
    var sLastInitial = '';

    for (i = 0; i < entityCollection.length; i++)
    {
        entityObject = entityCollection[i];
        var sText = null;
        var sInitial = '';
        var lEntityId = 0;

        switch (sEntityName)
        {
            case 'Contact':
                lEntityId = entityObject.ContactId;
                sText = entityObject.FullName
                sInitial = entityObject.SurnameInitial;

                if (entityObject.CompanyName)
                    sText = sText + ", " + entityObject.CompanyName;
                break;

            case 'User':
                // TODO
                break;
        }

        var sPage = "#pg" + sEntityName;
        var sHyperlink = "javascript:void(0)";
        var sOnClick = "loadEntityRecord('" + sEntityName + "', '" + sPage + "'," + lEntityId + ");";
        console.log(sHyperlink);

        if (!sLastInitial || sLastInitial == '' || sLastInitial.toUpperCase() != sInitial.toUpperCase())
        {
            sContent = sContent +
                '<li data-role="list-divider" role="heading">' +
                sInitial.toUpperCase() +
                '</li>';
            sLastInitial = sInitial;
        }


        // Item line
        sContent = sContent + '<li>' +
            '    <a href="' + sHyperlink + '" onclick="' + sOnClick + '" data-transition="slide" >' +
                        sText + '</a>' +
            '    </li>';
    }

    return sContent;
}

// Use this to call a multi-parameter function from a hyperlink e.g. "myFunc(a, b c)"
function onClickHyperlink(sFunction)
{
    var sLink = "'javascript:void(0)' onclick='" + sFunction + "'";
    return sLink;
}

function loadEntityRecord(sEntityName, sPageID, lEntityId)
{
    console.log('loadEntityRecord: ' + sEntityName + ',' + sPageID + ',' + lEntityId);

    // Get the data from the server
    showLoadingMessagePopup(true, "Loading " + sEntityName + "...");

    var payloadObject = {};

    var dataPacket = {
        'RecordId': lEntityId
    };

    payloadObject.OutboundDataPacket = dataPacket;

    payloadObject.URL = sEntityName + "s/" + sEntityName + "Record";        // "Contacts/ContactRecord"

    payloadObject.InboundDataFunction = function (data)
    {
        var objEntityRecord = data;

        showLoadingMessagePopup(false);

        if (objEntityRecord)
        {
            switch (sEntityName)
            {
                case 'Contact':
                    populateContactRecordFields(objEntityRecord);
                    break;

                    // More...
            }

            // Must we initialise this page before populating it?
            $.mobile.changePage($(sPageID), "slide", true, true);

            showLoadingMessagePopup(false);
        }
        else
        {
            alert('No ' + sEntityName + ' found: ' + lEntityId);
        }
    };

    payloadObject.ErrorHandlerFunction = function (request, status, error)
    {
        showLoadingMessagePopup(false);
        alert('loadEntityRecord: ' + status + ": " + error + ". responseText: " + request.responseText);
    }

    TriSys.Data.PostToWeb(payloadObject);
}

function populateContactRecordFields(objContactRecord)
{
    $('#txtContactName').text(objContactRecord.FullName);
    $('#txtContactName_Edit').val(objContactRecord.FullName);

    $('#txtContactJobTitle').text(objContactRecord.JobTitle);
    $('#txtContactJobTitle_Edit').val(objContactRecord.JobTitle);

    $('#txtContactType').text("Client or Candidate?");
    $('#txtContactAvailabilityDate').text("01 Sep 2013");

    populateCompanyNameDiv("#hrefContactCompanyName", -1, objContactRecord.CompanyName, "#liContactCompanyName");
    populateAddressMapDiv("#hrefContactCompanyAddress", objContactRecord.CompanyAddressStreet + ", " + objContactRecord.CompanyAddressCity,
                                                            "#lifContactCompanyAddress");
    populateAddressMapDiv("#hrefContactHomeAddress", objContactRecord.HomeAddressStreet + ", " + objContactRecord.HomeAddressCity,
                                                            "#liContactHomeAddress");

    populateTelNoDiv("#hrefContactWorkTel", "Work: ", objContactRecord.WorkTelNo, "#liContactWorkTel");
    populateTelNoDiv("#hrefContactMobileTel", "Mobile: ", objContactRecord.MobileTelNo, "#liContactMobileTel");
    populateTelNoDiv("#hrefContactHomeTel", "Home: ", objContactRecord.HomeAddressTelNo, "#liContactHomeTel");
    //populateTelNoDiv("#hrefContactSkype",     "Skype: ",  objContactRecord.Skype, "#liContactSkype");

    $('#txtContactWorkTel_Edit').val(TriSys.Operators.valueToString(objContactRecord.WorkTelNo));
    $('#txtContactMobileTel_Edit').val(TriSys.Operators.valueToString(objContactRecord.MobileTelNo));
    $('#txtContactHomeTel_Edit').val(TriSys.Operators.valueToString(objContactRecord.HomeAddressTelNo));
    //$('#txtContactSkype_Edit'    ).val(TriSys.Operators.valueToString(objContactRecord.Skype));

    populateEMailDiv("#hrefContactWorkEMail", "Work: ", objContactRecord.EMail, "#liContactWorkEMail");
    //populateEMailDiv("#hrefContactPersonalEMail", "Personal: ", objContactRecord.PersonalEMail, "#liContactPersonalEMail");

    $('#txtContactWorkEMail_Edit').val(TriSys.Operators.valueToString(objContactRecord.EMail));
    //$('#txtContactPersonalEMail_Edit').val(TriSys.Operators.valueToString(objContactRecord.PersonalEMail));

    populateEntityHyperlink('Requirement', '#hrefContactFormRequirements', 'Contact', objContactRecord.ContactId)
    populateEntityHyperlink('Placement', '#hrefContactFormPlacements', 'Contact', objContactRecord.ContactId)
    populateEntityHyperlink('Timesheet', '#hrefContactFormTimesheets', 'Contact', objContactRecord.ContactId)
    populateEntityHyperlink('Task', '#hrefContactFormTasks', 'Contact', objContactRecord.ContactId)
}

function populateEntityHyperlink(sDestinationEntity, sTag, sSourceEntity, lRecordId)
{
    var sHyperlink = 'javascript:void(0);';
    var sOnClick = 'load' + sDestinationEntity + 's("' + sSourceEntity + '",' + lRecordId.toString() + ');';

    $(sTag).attr("href", sHyperlink);
    $(sTag).attr("onClick", sOnClick);
}

function populateCompanyNameDiv(sTag, lCompanyId, sCompanyName, liTag)
{
    var sHyperlink = 'javascript:void(0);';
    var sOnClick = '';

    lCompanyId = 1    // CHEAT to display it during dev
    if (lCompanyId > 0)
    {
        sOnClick = 'loadCompany(' + lCompanyId.toString() + ');';
        $(liTag).show();
    }
    else
        $(liTag).hide();

    $(sTag).attr("href", sHyperlink);
    $(sTag).attr("onClick", sOnClick);
    $(sTag).text(TriSys.Operators.valueToString(sCompanyName));
}

function populateAddressMapDiv(sTag, sAddress, liTag)
{
    var sHyperlink = '#';
    if (sAddress)
    {
        var sPlusAddress = sAddress.replace(/ /g, '+');
        sHyperlink = "http://maps.google.com/maps?q=" + sPlusAddress;        // Patent/Legal issues using Google Maps on iOS
        $(liTag).show();
    }
    else
        $(liTag).hide();

    $(sTag).attr("href", sHyperlink);
    $(sTag).text(TriSys.Operators.valueToString(sAddress));
}

function populateTelNoDiv(sTag, sPrefix, telValue, liTag)
{
    var sTelNo = '#';
    if (telValue)
    {
        sTelNo = "tel:" + telValue;
        $(liTag).show();
    }
    else
        $(liTag).hide();

    $(sTag).attr("href", sTelNo);
    $(sTag).text(sPrefix + TriSys.Operators.valueToString(telValue));
}

function populateEMailDiv(sTag, sPrefix, sEMailAddress, liTag)
{
    var sEMailHyperlink = '#';
    if (sEMailAddress)
    {
        sEMailHyperlink = "mailto:" + sEMailAddress;
        $(liTag).show();
    }
    else
        $(liTag).hide();

    $(sTag).attr("href", sEMailHyperlink);
    $(sTag).text(sPrefix + TriSys.Operators.valueToString(sEMailAddress));
}

function editModeContactForm()
{
    var bEditing = $('#divContactFormButtonSave').is(':visible');

    if (bEditing)
    {
        // Turn off editing

        // Buttons
        $('#divContactFormButtonBack').show();
        $('#divContactFormButtonEdit').show();
        $('#divContactFormButtonCancel').hide();
        $('#divContactFormButtonSave').hide();

        // Fields
        $('#txtContactName_Edit').hide();
        $('#txtContactName').show();
        $('#txtContactJobTitle_Edit').hide();
        $('#txtContactJobTitle').show();
        $('#txtContactAvailabilityDate_Edit').hide();
        $('#txtContactAvailabilityDate').show();
        $('#divContactFormAddresses').show();
        $('#divContactFormPhoneNumbers').show();
        $('#divContactFormPhoneNumbers_Edit').hide();
        $('#divContactFormEMailAddresses_Edit').hide();
        $('#divContactFormCVLinks').show();
        $('#divContactFormEntityLinks').show();
    }
    else
    {
        // Turn on editing

        // Buttons
        $('#divContactFormButtonBack').hide();
        $('#divContactFormButtonEdit').hide();
        $('#divContactFormButtonCancel').show();
        $('#divContactFormButtonSave').show();

        // Fields
        $('#txtContactName_Edit').show();
        $('#txtContactName').hide();
        $('#txtContactJobTitle_Edit').show();
        $('#txtContactJobTitle').hide();
        $('#txtContactAvailabilityDate_Edit').show();
        $('#txtContactAvailabilityDate').hide();
        $('#divContactFormAddresses').hide();
        $('#divContactFormPhoneNumbers').hide();
        $('#divContactFormPhoneNumbers_Edit').show();
        $('#divContactFormEMailAddresses_Edit').show();
        $('#divContactFormCVLinks').hide();
        $('#divContactFormEntityLinks').hide();
    }
}

function editModeContactFormSave()
{
    alert('Save Contact Form');
    editModeContactForm();
}

function loadRequirements(sEntityName, lRecordId)
{
    alert('loadRequirements: ' + sEntityName + ', ' + lRecordId);
}

function loadPlacements(sEntityName, lRecordId)
{
    alert('loadPlacements: ' + sEntityName + ', ' + lRecordId);
}

function loadTimesheets(sEntityName, lRecordId)
{
    alert('loadTimesheets: ' + sEntityName + ', ' + lRecordId);
}

function loadTasks(sEntityName, lRecordId)
{
    alert('loadTasks: ' + sEntityName + ', ' + lRecordId);
}

function loadCompany(lCompanyId)
{
    alert('loadCompany: ' + lCompanyId);
}

function SaveSettings(bSave)
{
    var iRecordsPerPage = 25;

    if (bSave)
    {
        iRecordsPerPage = $('#sliderGridPageRecords').val();
    }
    else
    {
        $('#sliderGridPageRecords').val(iRecordsPerPage).slider('refresh');
    }

    TriSys.Session.SetRecordsPerPage(iRecordsPerPage);
    changePageToMainMenu();
}

function showLoadingMessagePopup(bShow, sMessage)
{

    if (bShow)
    {

        var $this = $(this);
        var theme = $this.jqmData("theme") || $.mobile.loader.prototype.options.theme;

        $.mobile.loading('show', {
            text: sMessage,
            textVisible: true,
            theme: theme,
            html: ""
        });
    }
    else
        $.mobile.loading('hide');
}
