﻿/* TriSys Web  Specific Javascript Code
 *
 * (c) 2013 TriSys Business Software
 *
 */

// The TriSys namespace
var TriSys =
{
    // User Interface namespace
    UI:
    {
        alert: function (sMessage)
        {
            alert(sMessage);
        },

        DisplayLoginStatus: function ()
        {

            var sUserName = TriSys.Session.UserName();
            var sCompanyName = TriSys.Session.CompanyName();
            var sSessionKey = TriSys.Session.DataServicesKey();

            if (TriSys.Operators.isEmpty(sSessionKey))
            {
                // Not logged in
                sUserName = '';
                sCompanyName = '';
                $('#SiteHeader_LogoffHyperlink').hide();
                $('#SiteHeader_LoginHyperlink').show();
            }
            else
            {
                $('#SiteHeader_LogoffHyperlink').show();
                $('#SiteHeader_LoginHyperlink').hide();
            }

            $('#SiteHeader_UserName').html("<b>" + sUserName + "</b>");
            $('#SiteHeader_CompanyName').html("<b>" + sCompanyName + "</b>");
        },

        SetCheckBoxSliderValue: function (sTag, sValue)
        {
            $(sTag).val(sValue);
            $(sTag).slider('refresh');
        },
        GetCheckBoxSliderValue: function (sTag)
        {
            var s = $(sTag).val();
            return s;
        }
    },

    Data:
    {
        // Wrapper for all JQuery/Ajax calls so that we have centralised control.
        // We have found that POST is the most versatile HTTP verb.
        PostToWeb: function (payloadObject)
        {

            var bAsynchronous = true;
            var sURL = null;
            var surl = TriSys.Data.BaseAddress();
            var dataPacketOutbound = null;
            var fDataPacketInboundFunction = null;  // function (data) { so something with data; }
            var fErrorHandlerFunction = null;       // function (request, status, error) { so something with data; }
            var sHTTPVerb = "POST";
            var sFunction = "TriSys.Data.PostToWeb(): ";
            var bShowErrorAlert = true;

            if (payloadObject)
            {
                try
                {
                    if (!TriSys.Operators.isEmpty(payloadObject.URL))
                    {
                        sURL = surl + payloadObject.URL;
                        //alert(': ' + sURL);
                    }

                    if (!TriSys.Operators.isEmpty(payloadObject.OutboundDataPacket))
                        dataPacketOutbound = payloadObject.OutboundDataPacket;

                    if (!TriSys.Operators.isEmpty(payloadObject.InboundDataFunction))
                        fDataPacketInboundFunction = payloadObject.InboundDataFunction;

                    if (!TriSys.Operators.isEmpty(payloadObject.ErrorHandlerFunction))
                    {
                        fErrorHandlerFunction = payloadObject.ErrorHandlerFunction;
                        bShowErrorAlert = false;
                    }

                    if (!TriSys.Operators.isEmpty(payloadObject.Asynchronous))
                    {
                        bAsynchronous = payloadObject.Asynchronous;
                    }

                }
                catch (err)
                {
                    alert(sFunction + err.message);
                }
            }

            if (!sURL)
            {
                alert(sFunction + 'missing URL parameter.');
                return;
            }

            if (!fDataPacketInboundFunction)
            {
                alert(sFunction + 'missing inbound data function.');
                return;
            }


            if (!bAsynchronous)
            {
                // Turn off asynchronicity where calls must be synchronised
                $.ajaxSetup({ async: false });
            }

            //alert('About to post $.ajax() to ' + sURL);

            $.ajaxSetup({
                accepts: {
                    xml: "application/xml, text/xml",
                    html: "text/html",
                    script: "text/javascript, application/javascript",
                    json: "application/json, text/javascript",
                    text: "text/plain",
                    // Hack for Chrome/Safari
                    _default: ""
                }
            });

            $.ajax({
                url: sURL,
                data: dataPacketOutbound,
                type: sHTTPVerb,
                dataType: 'json',
                //contentType: "application/json",      FUCKING EVIL LINE BREAKS POST PARAMETERS
                crossDomain: true,
                beforeSend: function (xhr)
                {
                    xhr.setRequestHeader('SiteKey', TriSys.Session.DeveloperSiteKey());

                    var sDataServicesKey = TriSys.Session.DataServicesKey();
                    if (!TriSys.Operators.isEmpty(sDataServicesKey))
                        xhr.setRequestHeader('DataServicesKey', sDataServicesKey);
                },
                success: function (data)
                {
                    fDataPacketInboundFunction(data);
                },

                error: function (request, status, error)
                {
                    if (fErrorHandlerFunction)
                        fErrorHandlerFunction(request, status, error);

                    if (bShowErrorAlert)
                        alert(sFunction + "Error: " + status + ": " + error + ". responseText: " + request.responseText);
                }
            });


            if (!bAsynchronous)
            {
                // Turn on asynchronicity as this should be the default for all Ajax calls
                $.ajaxSetup({ async: true });
            }
        },

        BaseAddress: function ()
        {

            var surl = 'https://api.trisys.co.uk/';    //    SECURE VERSION
            //var surl = 'http://api.trisys.co.uk/';           //   TESTING VERSION

            var bDebuggingServerCode = true;

            if (bDebuggingServerCode)
            {
                var sThisURL = window.location.href;
                try
                {
                    if (sThisURL.indexOf("http://localhost") == 0)
                        surl = '/';
                }
                catch (ex)
                {
                    alert('BaseAddress exception: ' + ex.message);
                }
            }

            return surl;
        },

        DrawTable: function (sDivTag, sEntityName, entityCollection)
        {

            var sTable = '<table width="100%" border="1">' +
                        ' <tr>' +
                        TriSys.Data.TableHeader(sEntityName) +
                        ' </tr>  \r\n';
            var sRow = null;
            var entityObject = null;

            switch (sEntityName)
            {

                case 'Counters':
                    entityObject = entityCollection;

                    sRow = TriSys.Data.TableColumn("Contact") +
                                    TriSys.Data.TableColumn(entityObject.Contacts)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("Company") +
                                    TriSys.Data.TableColumn(entityObject.Companies)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("Requirement") +
                                    TriSys.Data.TableColumn(entityObject.Requirements)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("Placement") +
                                    TriSys.Data.TableColumn(entityObject.Placements)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("Timesheet") +
                                    TriSys.Data.TableColumn(entityObject.Timesheets)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("Task") +
                                    TriSys.Data.TableColumn(entityObject.Tasks)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSys.Data.TableColumn("User") +
                                    TriSys.Data.TableColumn(entityObject.Users)
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";

                    entityCollection = null;
                    break;

            }

            // Enumerate through data collection
            if (entityCollection)
            {

                for (i = 0; i < entityCollection.length; i++)
                {

                    entityObject = entityCollection[i];
                    var sHyperlink = null;
                    sRow = null;

                    switch (sEntityName)
                    {

                        case 'Contact':
                            sHyperlink = TriSys.Data.HyperlinkColumn("Contact.aspx?ContactId=",
                                                            entityObject.ContactId, entityObject.ContactId);
                            sRow = sHyperlink +
                                    TriSys.Data.TableColumn(entityObject.FullName) +
                                    TriSys.Data.TableColumn(entityObject.CompanyName) +
                                    TriSys.Data.TableColumn(entityObject.JobTitle)
                            break;

                        case 'User':
                            sHyperlink = TriSys.Data.HyperlinkColumn("User.aspx?UserId=",
                                                            entityObject.UserId, entityObject.UserId);
                            sRow = sHyperlink +
                                    TriSys.Data.TableColumn(entityObject.FullName) +
                                    TriSys.Data.TableColumn(entityObject.LoginName)
                            break;
                    }

                    if (sRow)
                        sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                }
            }

            sTable = sTable + "</table>";

            $(sDivTag).html(sTable);
        },

        TableHeader: function (sEntityName)
        {

            var sHTML = null;

            switch (sEntityName)
            {

                case 'Contact':

                    sHTML = TriSys.Data.TableColumn("ContactId", true) +
                            TriSys.Data.TableColumn("Full Name", true) +
                            TriSys.Data.TableColumn("Company Name", true) +
                            TriSys.Data.TableColumn("job Title", true)
                    break;

                case 'User':

                    sHTML = TriSys.Data.TableColumn("UserId", true) +
                            TriSys.Data.TableColumn("Full Name", true) +
                            TriSys.Data.TableColumn("Login Name", true)
                    break;

                case 'Counters':

                    sHTML = TriSys.Data.TableColumn("Table/Entity", true) +
                            TriSys.Data.TableColumn("Record Count", true)
                    break;
            }

            return sHTML;
        },

        TableColumn: function (sText, bHeader)
        {
            if (bHeader)
                sText = '<b>' + sText + '</b>';

            var sHTML = "<td>" + sText + "</td>";
            return sHTML;
        },

        HyperlinkColumn: function (sPageURL, lRecordId, sDisplayText)
        {

            var sText = '<a href="' + sPageURL + lRecordId + '">' + sDisplayText + '</a>';

            var sHTML = TriSys.Data.TableColumn(sText);
            return sHTML;
        }
    },

    Session:
    {
        DeveloperSiteKey: function ()
        {
            var s = TriSys.Cookies.getCookie("DeveloperSiteKey");
            return s;
        },

        SetDeveloperSiteKey: function (sKey)
        {
            TriSys.Cookies.setCookie("DeveloperSiteKey", sKey, 1);
        },

        SetDataServicesKey: function (sKey)
        {
            TriSys.Cookies.setCookie("DataServicesKey", sKey, 1);
        },

        DataServicesKey: function ()
        {
            try
            {
                var s = TriSys.Cookies.getCookie("DataServicesKey");
                return s;
            }
            catch (ex)
            {
                // Ignore 'not found'
                return null;
            }
        },

        UserName: function ()
        {
            var s = TriSys.Cookies.getCookie('UserName');
            return s;
        },
        SetUserName: function (s)
        {
            TriSys.Cookies.setCookie('UserName', s, 1);
        },
        CompanyName: function ()
        {
            var s = TriSys.Cookies.getCookie('CompanyName');
            return s;
        },
        SetCompanyName: function (s)
        {
            TriSys.Cookies.setCookie('CompanyName', s, 1);
        },
        Password: function ()
        {
            var s = TriSys.Cookies.getCookie('Password');
            return s;
        },
        SetPassword: function (s)
        {
            TriSys.Cookies.setCookie('Password', s, 1);
        },
        SaveLoginCredentials: function ()
        {
            var s = TriSys.Cookies.getCookie('SaveLoginCredentials');
            var b = TriSys.Operators.stringToBoolean(s, false);
            return b;
        },
        SetSaveLoginCredentials: function (b)
        {
            TriSys.Cookies.setCookie('SaveLoginCredentials', b, 1);
        },
        RecordsPerPage: function ()
        {
            var s = TriSys.Cookies.getCookie('RecordsPerPage');
            if (!s)
                s = "25";

            var i = parseInt(s);
            return i;
        },
        SetRecordsPerPage: function (i)
        {
            TriSys.Cookies.setCookie('RecordsPerPage', i, 1);
        }
    },

    Cookies:
    {
        CookiePrefix: 'TriSys_',

        setCookie: function (c_name, value, exdays)
        {

            c_name = TriSys.Cookies.CookiePrefix + c_name;

            try
            {
                // PhoneGap Compatible!
                window.localStorage.setItem(c_name, value);
                return;

                // Browser cookies
                var exdate = new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                document.cookie = c_name + "=" + c_value;
            }
            catch (err)
            {
                alert('setCookie error: ' + err.message);
            }
        },

        getCookie: function (c_name)
        {

            c_name = TriSys.Cookies.CookiePrefix + c_name;

            //alert('getCookie: ' + c_name);

            try
            {
                // PhoneGap Compatible!
                var value = window.localStorage.getItem(c_name);
                return value;

                // Browser cookies
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1)
                {
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1)
                {
                    c_value = null;
                }
                else
                {
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1)
                    {
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start, c_end));
                }

                var sDisplay = c_value;
                if (sDisplay)
                    sDisplay = sDisplay.substring(0, 50) + '...';

                alert('getCookie(' + c_name + ') = \n ' + sDisplay);
                return c_value;
            }
            catch (err)
            {
                alert('getCookie error: ' + err.message);
            }

        }
    },

    Operators:
    {
        isEmpty: function (obj)
        {

            try
            {
                if (
                    obj === "" ||
                    obj === null ||
                    obj === "NULL" ||
                    obj === undefined //|| obj === false || obj === 0 || obj === "0" ||
                )
                {
                    return true;
                }

                if (typeof (obj) === 'object')
                {
                    var i = 0;
                    for (key in obj)
                    {
                        i++;
                    }
                    if (i === 0) { return true; }
                }
                return false;
            }
            catch (err)
            {
            }

            return true;
        },

        stringToBoolean: function (str, bDefault)
        {
            if (str)
            {
                switch (str.toLowerCase())
                {
                    case "true": case "yes": case "1": return true;
                    case "false": case "no": case "0": case null: return false;
                    default: return Boolean(str);
                }
            }
            else
            {
                return bDefault;
            }
        },

        booleanToString: function (b)
        {
            try
            {
                if (b)
                    return 'true';
                else
                    return 'false';
            }
            catch (ex)
            {
                return false;
            }
        },

        valueToString: function (v)
        {
            try
            {
                return v.toString();
            }
            catch (ex)
            {
                return "";
            }
        }

    }
}
