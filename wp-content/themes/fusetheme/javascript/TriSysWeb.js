/* TriSys Web Javascript Code
 * The TriSys Web framework is the operational behaviour of the recruitment agency web browser application.
 * It takes care of layout, events, session state, login mechanism,
 * forms, paging, and all other application framework stuff.
 *
 * (c) 2013-2014 TriSys Business Software
 *
 */

// The TriSysWeb Namespace
var TriSysWeb =
{
    // Copyright Notice
    Copyright:      // TriSysWeb.Copyright
    {
        AllRightsReserved: "TriSys Business Software",
        LongProductDescription: "Applicant Tracking System for Recruitment Agencies",
        ShortProductName: "TriSysWeb",
        HomePage: "www.opuslaboris.com"
    },

    //#region TriSysWeb.Pages
    Pages:
    {
        CurrentPageDocumentReadyObject: null,

        // Called after every page is loaded
        DocumentReady: function (documentReadyObject)
        {
            if (!TriSysApex.Pages.Index.BrowserCompatibilityCheck())
                return;

            TriSysSDK.Browser.CORSProxyJavascriptLoad();

            // Override base library settings
            TriSysAPI.Cookies.CookiePrefix = TriSysWeb.Constants.Cookie_Prefix;
            TriSysApex.Constants.DeveloperSiteKey = TriSysWeb.Constants.DeveloperSiteKey;
            TriSysAPI.Session.SetDeveloperSiteKey(TriSysWeb.Constants.DeveloperSiteKey);
            TriSysApex.LoginCredentials.ValidationMessageTitle = TriSysWeb.Constants.ApplicationName + ' Login Credentials';
            TriSysApex.Constants.ProfileImageSize = 128;

            CurrentPageDocumentReadyObject = documentReadyObject;

            TriSysWeb.Pages.InjectUserControlIntoDiv("TriSysWeb-Header", TriSysApex.Constants.UserControlFolder + "header.html");
            TriSysWeb.Pages.InjectUserControlIntoDiv("TriSysWeb-Footer", TriSysApex.Constants.UserControlFolder + "footer.html");

            // Set up window resize events to catch mobile device settings
            TriSysApex.Pages.Index.WindowsResizeEvents();
        },

        // Check that page adheres to security levels
        SecurityRedirector: function (documentReadyObject)
        {
            if (TriSysWeb.Security.AutoLoginFromSavedCredentials())
                return false;

            var bRedirectToWarning = false;

            if (documentReadyObject.ClientOrCandidateLogin)
            {
                // User must be logged in as a "Client" or a "Candidate"
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    bRedirectToWarning = true;

            }
            if (documentReadyObject.ClientLogin)
            {
                // User must be logged in as a "Client"
                if (!TriSysWeb.Security.IsClientLoggedIn())
                    bRedirectToWarning = true;
            }
            if (documentReadyObject.CandidateLogin)
            {
                // User must be logged in as a "Candidate"
                if (!TriSysWeb.Security.IsCandidateLoggedIn())
                    bRedirectToWarning = true;
            }

            if (documentReadyObject.SQLLogin)
            {
                // CRM account must be logged in: Verify the connection
                if (!TriSysWeb.Security.IsDataConnectionOpen() || !TriSysApex.Cache.isCached())
                {
                    // Re-connect, and do the callback once connected
                    TriSysWeb.Security.ConnectToAgencyDatabaseOnly(documentReadyObject.PageName, documentReadyObject.CallbackFunction);
                    return false;
                }
            }

            if (bRedirectToWarning)
            {
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.WarningPage);
                return true;
            }

            // If we are connected correctly, then callback the page function
            if (documentReadyObject.CallbackFunction)
                documentReadyObject.CallbackFunction();

            return false;
        },

        // See header.html as this is called after the header/menu is fully loaded
        AfterLoadingHeader: function ()
        {
            // Customer specific javascript initialisation
            TriSysWeb.Pages.LoadCustomerSpecificStack(TriSysWeb.Pages.AfterLoadingHeaderAsyncCallback);
        },

        // Because we must wait until the customer specific stack loads, we do this AFTER the javascript is loaded and executed
        AfterLoadingHeaderAsyncCallback: function ()
        {
            var sSelectedMenuID = "menuItem-" + CurrentPageDocumentReadyObject.PageName;
            $("#" + sSelectedMenuID).addClass("active");

            // Show the parent menu as active too
            if (CurrentPageDocumentReadyObject.ParentMenuName)
            {
                sSelectedMenuID = "menuItem-" + CurrentPageDocumentReadyObject.ParentMenuName + "-dropdown";
                $("#" + sSelectedMenuID).addClass("active");
                //$("#" + sSelectedMenuID).removeClass('dropdown-toggle').addClass("dropdown-toggle.active");
            }

            // Show the login credentials after connection to database
            TriSysWeb.Security.ShowLoginCredentialsAfterPageLoad();

            // Only after the header is loaded do we do security checks as we now have a full framework ready with dynamic controls etc..
            TriSysWeb.Pages.SecurityRedirector(CurrentPageDocumentReadyObject);
        },

        // Called to dynamically load customer specific business logic to override default behaviours.
        LoadCustomerSpecificStack: function (fnCallback)
        {
            // Load script dynamically
            var sInitialisationObjectFunction = TriSysWeb.Constants.CustomerSpecificFunction;
            var sFolder = TriSysWeb.Constants.CustomerSpecificFolder;
            if (sFolder && sInitialisationObjectFunction)
            {
                var sFile = sFolder + "/trisyscustom.js";           // File is always called this

                TriSysLoad.ApexFramework.LoadScript(sFile, function ()
                {
                    //  do something now that the script is loaded and code has been executed
                    var fn = TriSysApex.DynamicClasses.GetObject(sInitialisationObjectFunction);

                    if (fn)
                    {
                        try
                        {
                            // Make the call now to the customised business logic
                            fn();
                        }
                        catch (ex)
                        {
                            // Programmer fault
                        }

                        // Also very important to make the callback AFTER executing the customisation function
                        fnCallback();
                    }
                });
                    
                return;     // We will fire the callback after script has loaded
            }

            // Callback now only if there are no customisations or developer induced an error
            fnCallback();
        },

        InjectUserControlIntoDiv: function (sDivTag, sPagePath)
        {
            try
            {
                sPagePath = TriSysLoad.Utility.FilePathPrefix() + sPagePath;
				
                // Working great on trisys.co.uk
                var sURL = sPagePath + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache();
                $("#" + sDivTag).load(sURL);
            }
            catch (err)
            {
                // If this error occurs it is probably when debugging using Visual Studio.
                // It is because an old cached page is being used.
                alert('Error loading: ' + sDivTag + ": " + sPagePath + " (" + err + ")");
            }
        },

        // Centralised to allow menus to easily be customised at run-time
        LoadPage: function (sPageName)
        {
            //window.location = TriSysLoad.Utility.FilePathPrefix() + sPageName;
			window.location = TriSysApex.Constants.BasePath + sPageName;
        },

        //#region TriSysWeb.Pages.Home
        Home:
        {
            Redirect: function ()
            {
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.HomePage);
                return true;
            }
        },
        //#endregion TriSysWeb.Pages.Home

        //#region TriSysWeb.Pages.Login
        Login:
        {
            PersistenceDiv: 'cmbLoginPersistence',
            PersistCredentialsOn: function ()
            {
                return TriSysSDK.Resources.Message("TriSysWeb.Pages.Login.PersistCredentialsOn");
            },
            PersistCredentialsOff: function ()
            {
                return TriSysSDK.Resources.Message("TriSysWeb.Pages.Login.PersistCredentialsOff");
            },
            Load: function ()
            {
                var persistenceData = [
                    {
                        value: TriSysWeb.Pages.Login.PersistCredentialsOn(),
                        text: TriSysWeb.Pages.Login.PersistCredentialsOn()
                    },
                    {
                        value: TriSysWeb.Pages.Login.PersistCredentialsOff(),
                        text: TriSysWeb.Pages.Login.PersistCredentialsOff()
                    }
                ];
                TriSysSDK.CShowForm.dropDownComboFieldEditableProperties(TriSysWeb.Pages.Login.PersistenceDiv, TriSysWeb.Pages.Login.PersistCredentialsOn(), persistenceData);

                // Auto-login memory settings
                var sEMailAddress = TriSysAPI.Session.Memory.Get(TriSysWeb.Security.AutoLoginNameSetting);
                var sPassword = TriSysAPI.Session.Memory.Get(TriSysWeb.Security.AutoLoginPasswordSetting);
                TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginNameSetting, null);
                TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginPasswordSetting, null);

                if (sEMailAddress)
                    $('#txtLoginEMail').val(sEMailAddress);
                if (sPassword)
                    $('#txtLoginPassword').val(sPassword);

                if (sEMailAddress && sPassword)
                {
                    // Commence auto-login sequence
                    TriSysWeb.Security.LoginButtonClick();
                }
            }
        },
        //#endregion TriSysWeb.Pages.Login

        //#region TriSysWeb.Pages.CandidateVacancies
        CandidateVacancies:
        {
            // We only want locations of actual live jobs
            PopulateLocationLookup: function (sDiv)
            {
                var sSQL = "Select distinct isnull(dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId), 'London') as Location" +
                            " From v_Requirement_AllFields " +
                            " Where Requirement_RequirementStatus in ('Active', 'Open')" +
                            "   and (case Requirement_RequirementEntityType when 'Permanent' then Requirement_MaximumSalary else Requirement_MaximumRate end) is not null" +
                            " Order by 1";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Location");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            PopulateStartDateSearchWidgets: function (sComboOperatorDiv, sDateDiv)
            {
                TriSysSDK.CShowForm.datePicker(sDateDiv);

                var dataSource = [
                    { text: "On or after", value: ">=" },
                    { text: "On or before", value: "<=" }];
                TriSysSDK.CShowForm.populateComboFromDataSource(sComboOperatorDiv, dataSource, 0);
            },

            PopulateCurrencyAmountPeriodWidget: function (sDiv)
            {
                var fieldDescription = {
                    MinValue: 0,
                    SpinIncrement: 1000
                };

                var fnSetDefaultValue = function ()
                {
                    var sPoundSign = "\u00A3";  // �
                    var sDefaultAmount = sPoundSign + "0 per Annum";
                    TriSysSDK.Controls.CurrencyAmountPeriod.SetCurrencyAmountPeriod(sDiv, sDefaultAmount);
                };
                TriSysSDK.Controls.CurrencyAmountPeriod.Load(sDiv, fieldDescription, fnSetDefaultValue);
            },

            SearchPersistence:
            {
                CookiePrefix: "CandidateVacancySearch-",

                VacancySearchButton: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.CandidateVacancies.SearchPersistence.CookiePrefix;

                    try
                    {
                        var sReference = $('#txtReference').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Reference", sReference);

                        var sJobTitle = $('#txtJobTitle').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "JobTitle", sJobTitle);

                        var sTypeOfWorkRequired = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbTypeOfWorkRequired");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "TypeOfWorkRequired", sTypeOfWorkRequired);

                        var sLocation = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbLocation");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Location", sLocation);

                        var sDateOperatorSQL = $('#cmbStartDateOperator').val();
                        var dtStartDate = $('#dtStartDate').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDateOperator", sDateOperatorSQL);
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDate", dtStartDate);

                        var sMinSalary = TriSysSDK.Controls.CurrencyAmountPeriod.GetCurrencyAmountPeriod("salaryMinimum");
                        var sMaxSalary = TriSysSDK.Controls.CurrencyAmountPeriod.GetCurrencyAmountPeriod("salaryMaximum");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "salaryMinimum", sMinSalary);
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "salaryMaximum", sMaxSalary);
                    }
                    catch (err)
                    {
                        // control bugs?
                    }

                    // Show all live vacancies
                    TriSysWeb.Pages.CandidateVacancies.VacancySearch('divVacancyGrid');
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        // Re-instate all search parameters
                        var CookiePrefix = TriSysWeb.Pages.CandidateVacancies.SearchPersistence.CookiePrefix;

                        var sReference = TriSysAPI.Cookies.getCookie(CookiePrefix + "Reference");
                        $('#txtReference').val(sReference);

                        var sJobTitle = TriSysAPI.Cookies.getCookie(CookiePrefix + "JobTitle");
                        $('#txtJobTitle').val(sJobTitle);

                        var sTypeOfWorkRequired = TriSysAPI.Cookies.getCookie(CookiePrefix + "TypeOfWorkRequired");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbTypeOfWorkRequired", sTypeOfWorkRequired);

                        var sLocation = TriSysAPI.Cookies.getCookie(CookiePrefix + "Location");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbLocation", sLocation);

                        var sDateOperatorSQL = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDateOperator");
                        TriSysSDK.CShowForm.SetValueInCombo("cmbStartDateOperator", sDateOperatorSQL);

                        var dtStartDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDate");
                        TriSysSDK.CShowForm.setDatePickerValue('dtStartDate', dtStartDate);

                        var sMinSalary = TriSysAPI.Cookies.getCookie(CookiePrefix + "salaryMinimum");
                        var sMaxSalary = TriSysAPI.Cookies.getCookie(CookiePrefix + "salaryMaximum");
                        TriSysSDK.Controls.CurrencyAmountPeriod.SetCurrencyAmountPeriod("salaryMinimum", sMinSalary);
                        TriSysSDK.Controls.CurrencyAmountPeriod.SetCurrencyAmountPeriod("salaryMaximum", sMaxSalary);

                        // Finally show all search results again
                        TriSysWeb.Pages.CandidateVacancies.SearchPersistence.VacancySearchButton();
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            },

            // Called when the user clicks the search button
            VacancySearch: function (sDiv)
            {
                // Are we connected to the database?
                if (!TriSysWeb.Security.IsDataConnectionOpen())
                {
                    alert('Not connected to API yet - programmer error');
                    return;
                }

                // Read search parameters
                var sWhereSQL = TriSysWeb.Pages.CandidateVacancies.SearchWhereClause();

                // Populate the grid
                TriSysWeb.Pages.CandidateVacancies.PopulateGrid(sDiv, "grdVacancies", "Vacancies", sWhereSQL);
            },

            SearchWhereClause: function ()
            {
                // Read search parameters
                var sWhereSQL = '';

                var sReference = $('#txtReference').val();
                if (sReference)
                    sWhereSQL += " and Requirement_RequirementReference like '%" + TriSysSDK.Database.SQLQuoterNull(sReference) + "%'";

                var sJobTitle = $('#txtJobTitle').val();
                if (sJobTitle)
                    sWhereSQL += " and Requirement_JobTitle like '%" + TriSysSDK.Database.SQLQuoterNull(sJobTitle) + "%'";

                var sTypeOfWorkSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbTypeOfWorkRequired", "Requirement_RequirementEntityType");
                if (sTypeOfWorkSQL)
                    sWhereSQL += " and " + sTypeOfWorkSQL;

                var sLocationSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbLocation", "dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId)");
                if (sLocationSQL)
                    sWhereSQL += " and " + sLocationSQL;

                var sDateOperatorSQL = $('#cmbStartDateOperator').val();
                var dtStartDate = $('#dtStartDate').val();
                if (dtStartDate)
                    sWhereSQL += " and Requirement_EarliestStartDate " + sDateOperatorSQL + TriSysSDK.Database.SQLDateFormat(dtStartDate);

                try
                {
                    var fMinSalaryPerHour = TriSysSDK.Controls.CurrencyAmountPeriod.GetCurrencyAmountPeriodPerHour("salaryMinimum");
                    var fMaxSalaryPerHour = TriSysSDK.Controls.CurrencyAmountPeriod.GetCurrencyAmountPeriodPerHour("salaryMaximum");
                    var sSalaryField = "floor(dbo.f_GetCurrencyAmountPerHour(MaximumSalary))";
                    var sRateField = "floor(dbo.f_GetCurrencyAmountPerHour(MaximumRate))";
                    var sInJoinSQL = " and Requirement_RequirementId in (select EntityId From RequirementConfigFields where ";
                    var sMaxSalarySQL = "(" +
                                        " (" + sSalaryField + " <= " + fMaxSalaryPerHour + " and " + sSalaryField + " > 0) " +
                                        " or " +
                                        " (" + sRateField + " <= " + fMaxSalaryPerHour + " and " + sRateField + " > 0)" +
                                        ")";
                    var sMinSalarySQL = "(" + sSalaryField + " >= " + fMinSalaryPerHour + " or " + sRateField + " >= " + fMinSalaryPerHour + ")";

                    if (fMinSalaryPerHour > 0 && fMaxSalaryPerHour > 0)
                        sWhereSQL += sInJoinSQL + sMinSalarySQL + "  and " + sMaxSalarySQL + ")";
                    else if (fMinSalaryPerHour > 0)
                        sWhereSQL += sInJoinSQL + sMinSalarySQL + ")";
                    else if (fMaxSalaryPerHour > 0)
                        sWhereSQL += sInJoinSQL + sMaxSalarySQL + ")";
                }
                catch (err)
                {
                    // No salary widget
                }

                return sWhereSQL;
            },

            PopulateGrid: function (sDiv, sGridName, sTitle, sWhereSQL)
            {
                // Modify the SQL statement
                var sSQL = TriSysWeb.Pages.CandidateVacancies.SQL + sWhereSQL;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridName,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.CandidateVacancies.OrderBy,
                    Columns: TriSysWeb.Pages.CandidateVacancies.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.CandidateVacancies.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.CandidateVacancies.MobileRowTemplate,

                    KeyColumnName: "RequirementId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load vacancy Form with specified ID
                        var lRequirementId = rowData.RequirementId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.CandidateVacancyPage + "?RequirementId=" + lRequirementId);
                    },
                    MultiRowSelect: true,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Requirement_RequirementId as RequirementId, Requirement_RequirementReference as Reference, Requirement_RequirementEntityType as Type, " +
                "  Requirement_JobTitle as JobTitle, " +
                "  Requirement_EarliestStartDate as StartDate, " +
                "  isnull(dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId), 'Unknown') as Location, " +
                "  case Requirement_RequirementEntityType when 'Permanent' then Requirement_MaximumSalary else Requirement_MaximumRate end as Salary" +
                " from v_Requirement_AllFields" +
                " where Requirement_RequirementStatus in ('Active', 'Open')" +
                "   and (case Requirement_RequirementEntityType when 'Permanent' then Requirement_MaximumSalary else Requirement_MaximumRate end) is not null",
            OrderBy: ' Order by Requirement_RequirementReference',
            Columns: [
                        { field: "RequirementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Reference", type: "string" },
                        { field: "Type", title: "Type", type: "string" },
                        { field: "JobTitle", title: "Job Title", type: "string" },
                        { field: "StartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Location", title: "Location", type: "string" },
                        { field: "Salary", title: "Salary", type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Job Details" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference # (#: Type #)</strong><br />#: JobTitle #<br /><i>#: Location #</i><br />' +
                                '#: Salary #<br />Start Date: #: kendo.format("{0:dd MMM yyyy}", StartDate) #<hr></td>',

            GridDivTag: 'divVacancyGrid',
            MapDivTag: 'divVacancyMap',
            ShowGridButton: 'btnShowInGrid',
            ShowMapButton: 'btnShowOnMap',

            // After a search has been constructed, the user can choose to view the jobs on a map.
            ShowOnMap: function (sWhereSQL, sAPI)
            {
                TriSysWeb.Pages.CandidateVacancies.GridAndMapVisibility(false);

                if (!sWhereSQL)
                {
                    // Read search parameters
                    sWhereSQL = TriSysWeb.Pages.CandidateVacancies.SearchWhereClause();
                }

                // Call API
                TriSysApex.UI.ShowWait(null, "Retrieving Map Data...");

                var dataPacket = {
                    WhereSQL: sWhereSQL,
                    Limit: 100
                };

                if (!sAPI)
                    sAPI = "Vacancy/SearchMapData";
                else
                    dataPacket = null;

                var payloadObject = {};
                payloadObject.URL = sAPI;
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CVacanciesWithLatitudeLongitudeResult = data;
                    if (CVacanciesWithLatitudeLongitudeResult)
                    {
                        if (CVacanciesWithLatitudeLongitudeResult.Success)
                            TriSysWeb.Pages.CandidateVacancies.PopulateMapCallback(CVacanciesWithLatitudeLongitudeResult);
                        else
                            TriSysApex.UI.errorAlert(CVacanciesWithLatitudeLongitudeResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('ShowOnMap: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // Show the grid again!
            ShowInGrid: function ()
            {
                TriSysWeb.Pages.CandidateVacancies.GridAndMapVisibility(true);
            },

            GridAndMapVisibility: function (bGridVisible)
            {
                if (bGridVisible)
                {
                    $('#' + TriSysWeb.Pages.CandidateVacancies.ShowGridButton).hide();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.ShowMapButton).show();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.GridDivTag).show();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.MapDivTag).hide();
                }
                else
                {
                    $('#' + TriSysWeb.Pages.CandidateVacancies.ShowGridButton).show();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.ShowMapButton).hide();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.GridDivTag).hide();
                    $('#' + TriSysWeb.Pages.CandidateVacancies.MapDivTag).show();
                }
            },

            // When the API returns the list of requirements with latitude/longitude, draw them on a map.
            PopulateMapCallback: function (CVacanciesWithLatitudeLongitudeResult)
            {
                if (!CVacanciesWithLatitudeLongitudeResult)
                    return;
                var vacancies = CVacanciesWithLatitudeLongitudeResult.Vacancies;
                if (!vacancies)
                    return;
                if (vacancies.length === 0)
                    return;

                // Calculate centre of the map
                var iWest = 0, iEast = 0, iNorth = 0, iSouth = 0;
                for (var i = 0; i < vacancies.length; i++)
                {
                    var vacancy = vacancies[i];
                    if (vacancy.Latitude !== 0 && vacancy.Longitude !== 0)
                    {
                        if (iWest === 0 || iWest > vacancy.Latitude)
                            iWest = vacancy.Latitude;
                        if (iEast === 0 || iEast < vacancy.Latitude)
                            iEast = vacancy.Latitude;
                        if (iNorth === 0 || iNorth > vacancy.Longitude)
                            iNorth = vacancy.Longitude;
                        if (iSouth === 0 || iSouth < vacancy.Longitude)
                            iSouth = vacancy.Longitude;
                    }
                }

                var iLatitude = (iWest + iEast) / 2;
                var iLongitude = (iNorth + iSouth) / 2;

                var centrePosition = null;
                try
                {
                    centrePosition = new google.maps.LatLng(iLatitude, iLongitude);
                }
                catch(err)
                {
                    TriSysApex.UI.ShowError(err, "Google Maps Error");
                    return;
                }

                // Cluster the markers rather than overlay them
                for (var i = 0; i < vacancies.length; i++)
                {
                    var vacancy = vacancies[i];
                    if (vacancy.Latitude != 0 && vacancy.Longitude != 0)
                    {
                        var bFound = false;
                        for (var k = 0; k < vacancies.length; k++)
                        {
                            var vacancy2 = vacancies[k];
                            if (vacancy.Latitude == vacancy2.Latitude && vacancy.Longitude == vacancy2.Longitude)
                            {
                                // Move the pin randomly to the N/S/E/W
                                var iLatRandom = Math.random() / 100;
                                var iLongRandom = Math.random() / 100;
                                var iMultiplier1 = (Math.random() > 0.5 ? 1 : -1);
                                var iMultiplier2 = (Math.random() > 0.5 ? -1 : 1);
                                vacancy.Latitude += (iLatRandom * iMultiplier1);
                                vacancy.Longitude += (iLongRandom * iMultiplier2);
                                break;
                            }
                        }
                    }
                }



                // Create the map object
                var mapOptions = {
                    center: centrePosition,
                    zoom: 7        // 13 is best for close-up, 7 for a country view
                };

                TriSysWeb.Pages.CandidateVacancies.MapObject = new google.maps.Map(document.getElementById(TriSysWeb.Pages.CandidateVacancies.MapDivTag), mapOptions);

                // Add all markers
                TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory = [];
                for (var i = 0; i < vacancies.length; i++)
                {
                    var vacancy = vacancies[i];

                    if (vacancy.Latitude !== 0 && vacancy.Longitude !== 0)
                    {
                        var markerPosition = new google.maps.LatLng(vacancy.Latitude, vacancy.Longitude);

                        var sTitle = vacancy.Reference + ": " + vacancy.JobTitle;
                        var marker = new google.maps.Marker({
                            position: markerPosition,
                            map: TriSysWeb.Pages.CandidateVacancies.MapObject,
                            title: sTitle
                        });

                        // Add a popup so that user can drill down
                        var sHyperlink = '<a href=' + TriSysWeb.Constants.CandidateVacancyPage + '?RequirementId=' +
                                        vacancy.RequirementId + '>' + vacancy.Reference + ': ' + vacancy.JobTitle + '</a>';

                        var jobInfo = '<div id="divRequirement-"' + vacancy.RequirementId + ' style="width: 400px">' +
                          ' <h5>' + sHyperlink + '</h5>' +
                          ' <h6>' +
                           vacancy.Type +
                          ' </h6>' +
                          ' <p>' +
                           vacancy.Location +
                          ' </p>' +
                          ' <p>' +
                           vacancy.Description +
                          ' </p>' +
                          '</div>';

                        var jobInfoWindow = new google.maps.InfoWindow({
                            content: jobInfo
                        });

                        var iIndex = TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory.length + 1;
                        var arrayItem = {
                            myMarker: marker,
                            myJobInfo: jobInfo,
                            myJobInfoWindow: jobInfoWindow,
                            myIndex: iIndex,
                            myTitle: sTitle,
                            myVacancy: vacancy
                        };
                        TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory.push(arrayItem);
                    }
                }

                // Now add callbacks to open pop-up
                for (var i = 0; i < TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory.length; i++)
                {
                    var arrayItem = TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory[i];

                    google.maps.event.addListener(arrayItem.myMarker, 'click', function ()
                    {
                        TriSysWeb.Pages.CandidateVacancies.MapMarkerClickCallback(this);
                    });
                }
            },

            MapObject: null,
            MarkerArrayInMemory: [],    // Our own collection to ensure unique identification of selected marker

            // When the user clicks on a map marker, pop open the job details and asynchronously retrieve the
            // requirement details with quick buttons allowing drill down, favourite or apply.
            MapMarkerClickCallback: function (myMarker)
            {
                if (TriSysWeb.Pages.CandidateVacancies.MapObject)
                {
                    var sTitle = myMarker.title;

                    for (var i = 0; i < TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory.length; i++)
                    {
                        var arrayItem = TriSysWeb.Pages.CandidateVacancies.MarkerArrayInMemory[i];
                        if (sTitle == arrayItem.myTitle)
                        {
                            arrayItem.myJobInfoWindow.open(TriSysWeb.Pages.CandidateVacancies.MapObject, arrayItem.myMarker);
                            return;
                        }
                    }
                }
            }
        },
        //#endregion TriSysWeb.Pages.CandidateVacancies

        //#region TriSysWeb.Pages.ClientVacancies
        ClientVacancies:
        {
            // We only want locations of actual jobs associated with this client login
            PopulateLocationLookup: function (sDiv)
            {
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var sSQL = "Select distinct isnull(dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId), '?') as Location" +
                            " From v_Requirement_AllFields " +
                            " Where RequirementContact_ContactId = " + dataConnectionKey.LoggedInAgencyContact.ContactId;
                " Order by 1";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Location");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            PopulateStatusLookup: function (sDiv)
            {
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var sSQL = "Select distinct Requirement_RequirementStatus as Status" +
                            " From v_Requirement_AllFields " +
                            " Where RequirementContact_ContactId = " + dataConnectionKey.LoggedInAgencyContact.ContactId;
                " Order by 1";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Status");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            SearchPersistence:
            {
                CookiePrefix: "ClientVacancySearch-",

                VacancySearchButton: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.ClientVacancies.SearchPersistence.CookiePrefix;

                    try
                    {
                        var sReference = $('#txtReference').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Reference", sReference);

                        var sJobTitle = $('#txtJobTitle').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "JobTitle", sJobTitle);

                        var sTypeOfWorkRequired = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbTypeOfWorkRequired");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "TypeOfWorkRequired", sTypeOfWorkRequired);

                        var sLocation = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbLocation");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Location", sLocation);

                        var sStatus = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStatus");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Status", sStatus);

                        var sDateOperatorSQL = $('#cmbStartDateOperator').val();
                        var dtStartDate = $('#dtStartDate').val();
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDateOperator", sDateOperatorSQL);
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDate", dtStartDate);

                    }
                    catch (err)
                    {
                        // control bugs?
                    }

                    // Show all live vacancies
                    TriSysWeb.Pages.ClientVacancies.VacancySearch('divVacancyGrid');
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        // Re-instate all search parameters
                        var CookiePrefix = TriSysWeb.Pages.ClientVacancies.SearchPersistence.CookiePrefix;

                        var sReference = TriSysAPI.Cookies.getCookie(CookiePrefix + "Reference");
                        $('#txtReference').val(sReference);

                        var sJobTitle = TriSysAPI.Cookies.getCookie(CookiePrefix + "JobTitle");
                        $('#txtJobTitle').val(sJobTitle);

                        var sTypeOfWorkRequired = TriSysAPI.Cookies.getCookie(CookiePrefix + "TypeOfWorkRequired");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbTypeOfWorkRequired", sTypeOfWorkRequired);

                        var sLocation = TriSysAPI.Cookies.getCookie(CookiePrefix + "Location");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbLocation", sLocation);

                        var sStatus = TriSysAPI.Cookies.getCookie(CookiePrefix + "Status");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStatus", sStatus);

                        var sDateOperatorSQL = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDateOperator");
                        TriSysSDK.CShowForm.SetValueInCombo("cmbStartDateOperator", sDateOperatorSQL);

                        var dtStartDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDate");
                        TriSysSDK.CShowForm.setDatePickerValue('dtStartDate', dtStartDate);

                        // Finally show all search results again
                        TriSysWeb.Pages.ClientVacancies.SearchPersistence.VacancySearchButton();
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            },

            VacancySearch: function (sDiv)
            {
                // Are we connected to the database as a client?
                if (!TriSysWeb.Security.IsClientLoggedIn())
                {
                    TriSysApex.UI.ShowMessage("You must be logged in as a client.");
                    return;
                }

                // Read search parameters
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var sWhereSQL = " and RequirementContact_ContactId = " + dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sReference = $('#txtReference').val();
                if (sReference)
                    sWhereSQL += " and Requirement_RequirementReference like '%" + TriSysSDK.Database.SQLQuoterNull(sReference) + "%'";

                var sJobTitle = $('#txtJobTitle').val();
                if (sJobTitle)
                    sWhereSQL += " and Requirement_JobTitle like '%" + TriSysSDK.Database.SQLQuoterNull(sJobTitle) + "%'";

                var sTypeOfWorkSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbTypeOfWorkRequired", "Requirement_RequirementEntityType");
                if (sTypeOfWorkSQL)
                    sWhereSQL += " and " + sTypeOfWorkSQL;

                var sLocationSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbLocation", "dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId)");
                if (sLocationSQL)
                    sWhereSQL += " and " + sLocationSQL;

                var sStatusSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStatus", "Requirement_RequirementStatus");
                if (sStatusSQL)
                    sWhereSQL += " and " + sStatusSQL;

                var sDateOperatorSQL = $('#cmbStartDateOperator').val();
                var dtStartDate = $('#dtStartDate').val();
                if (dtStartDate)
                    sWhereSQL += " and Requirement_EarliestStartDate " + sDateOperatorSQL + TriSysSDK.Database.SQLDateFormat(dtStartDate);

                // Populate the grid
                TriSysWeb.Pages.ClientVacancies.PopulateGrid(sDiv, "grdVacancies", "Vacancies", sWhereSQL);
            },

            PopulateGrid: function (sDiv, sGridName, sTitle, sWhereSQL)
            {
                // Modify the SQL statement
                var sSQL = TriSysWeb.Pages.ClientVacancies.SQL + sWhereSQL;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridName,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.ClientVacancies.OrderBy,
                    Columns: TriSysWeb.Pages.ClientVacancies.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.ClientVacancies.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.ClientVacancies.MobileRowTemplate,

                    KeyColumnName: "RequirementId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load vacancy Form with specified ID
                        var lRequirementId = rowData.RequirementId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientVacancyPage + "?RequirementId=" + lRequirementId);
                    },
                    MultiRowSelect: true,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Requirement_RequirementId as RequirementId, Requirement_RequirementReference as Reference, Requirement_RequirementEntityType as Type, " +
                "  Requirement_JobTitle as JobTitle, " +
                "  Requirement_EarliestStartDate as StartDate, " +
                "  isnull(dbo.f_GetRequirementSiteAddressCity(Requirement_RequirementId), 'Unknown') as Location, " +
                "  case Requirement_RequirementEntityType when 'Permanent' then Requirement_MaximumSalary else Requirement_MaximumRate end as Salary, " +
                "  Requirement_RequirementStatus as Status " +
                " from v_Requirement_AllFields" +
                " where Requirement_RequirementId > 0",
            OrderBy: ' Order by Requirement_RequirementReference',
            Columns: [
                        { field: "RequirementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Reference", type: "string" },
                        { field: "Type", title: "Type", type: "string" },
                        { field: "JobTitle", title: "Job Title", type: "string" },
                        { field: "StartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Location", title: "Location", type: "string" },
                        { field: "Salary", title: "Salary", type: "string" },
                        { field: "Status", title: "Status", type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Job Details" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference # (#: Type #: #: Status #)</strong><br />#: JobTitle #<br /><i>#: Location #</i><br />' +
                                '#: Salary #</br >Start Date: #: kendo.format("{0:dd MMM yyyy}", StartDate) #<hr></td>'

        },
        //#endregion TriSysWeb.Pages.ClientVacancies

        //#region TriSysWeb.Pages.CandidateVacancy
        CandidateVacancy:
        {
            LoadRecordFromURL: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if ($.isNumeric(lRequirementId))
                {
                    // Load entity record
                    // Tell CShowForm that we are initialised and it can customise the widgets
                    TriSysSDK.CShowForm.InitialiseFields("Requirement");

                    setTimeout(function ()
                    {
                        // Load forms data
                        TriSysSDK.CShowForm.readEntityFormDataFromServer('Requirement', lRequirementId,
                                                    TriSysWeb.Pages.CandidateVacancy.AfterFormDataLoaded);

                        // Load interviews etc..
                        TriSysWeb.Pages.CandidateVacancy.ShortlistStagesGrid.Load(lRequirementId, 'divVacancyShortlistGrid');

                    }, TriSysApex.Constants.FormLoadDataDelay);

                    // Favourite/Apply buttons
                    TriSysWeb.Pages.CandidateVacancy.PostActionButtons(lRequirementId);

                    return;
                }

                // Something went wrong
                TriSysApex.UI.ShowError("Unable to load the specified vacancy.", TriSysWeb.Constants.ApplicationName);
            },

            // When loading the page of after carrying out an action, show/hide action buttons
            PostActionButtons: function (lRequirementId)
            {
                // Favourite buttons
                if (TriSysWeb.Pages.CandidateVacancy.isFavourite(lRequirementId))
                {
                    $('#btnAddToFavourites').hide();
                    $('#btnRemoveFromFavourites').show();
                }

                // Apply buttons
                if (TriSysWeb.Data.Vacancy.isShortlisted(lRequirementId))
                {
                    $('#btnApply').hide();
                    $('#btnWithdrawVacancyApplication').show();
                }

                // Visibility of interviews grid
                if (!TriSysWeb.Security.IsCandidateLoggedIn())
                    $('#divInterviewsContainer').hide();
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lRequirementId, fieldDescriptions)
            {
                // Appearances
                var sType = TriSysSDK.CShowForm.GetTextFromCombo('Requirement_Type');
                if (sType == 'Permanent')
                {
                    $('#fieldGroup-Rate').hide();
                    $('#fieldGroup-ContractDuration').hide();
                }
                else
                    $('#fieldGroup-Salary').hide();

                var bDisplayedJobDescriptionDocument = true;
                var sJobDescriptionDocument = TriSysSDK.Controls.FileReference.GetFile("RequirementConfigFields_JobDescription");
                if (sJobDescriptionDocument)
                {
                    var fieldDescription_DisplayOnWeb = TriSysSDK.CShowForm.getFieldDescriptionFromCollection(fieldDescriptions, "RequirementConfigFields", "DisplayOnWeb");
                    if (fieldDescription_DisplayOnWeb)
                    {
                        if (fieldDescription_DisplayOnWeb.value == 'false')
                            bDisplayedJobDescriptionDocument = false;
                    }

                    TriSysSDK.Controls.FileReference.FindButtonVisible("RequirementConfigFields_JobDescription", false);
                    TriSysSDK.Controls.FileReference.DeleteButtonVisible("RequirementConfigFields_JobDescription", false);
                }
                else
                    bDisplayedJobDescriptionDocument = false;

                if (!bDisplayedJobDescriptionDocument)
                    $('#fieldGroup-JobDescriptionDocument').hide();


                // Enumerate through field descriptions to identify special fields for additional population
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);
                var fieldDescriptionsForAsyncPopulation = [];

                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "RequirementConfigFields_SiteAddress":
                            // Do not want to show the full address, only the city/country
                            fieldDescription.SQL = "Select isnull(dbo.f_GetRequirementSiteAddressCity(" + lRequirementId + "), 'Unknown') as " + sDivID;
                            fieldDescriptionsForAsyncPopulation.push(fieldDescription);
                            break;
                    }
                }

                if (fieldDescriptionsForAsyncPopulation.length > 0)
                    TriSysSDK.CShowForm.PopulateAsyncFieldDescriptionsWithSingletonSQLStatement(fieldDescriptionsForAsyncPopulation);
            },

            // Show all of the stages for this candidate i.e. when they were shortlisted, contacted, CV sent, tel interview etc..
            ShortlistStagesGrid:
            {
                Load: function (lRequirementId, sGridDiv)
                {
                    var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                    var sSQL = TriSysWeb.Pages.CandidateVacancy.ShortlistStagesGrid.SQL;
                    sSQL = sSQL.replace(/@RequirementId/g, lRequirementId);
                    if (dataConnectionKey.LoggedInAgencyContact)
                        sSQL = sSQL.replace(/@ContactId/g, dataConnectionKey.LoggedInAgencyContact.ContactId);

                    // Populate the grid
                    TriSysWeb.Pages.CandidateVacancy.ShortlistStagesGrid.PopulateGrid(sGridDiv, "grdShortlistStages", "Vacancy Shortlist Stages", sSQL);
                },
                SQL: "   Select 1 as Ordering, 'Shortlisted' as Stage, DateShortlisted as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and DateShortlisted is not null" +
                     "   UNION" +
                     "   Select 2 as Ordering, 'Interested' as Stage, Interested as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and Interested is not null" +
                     "   UNION" +
                     "   Select 3 as Ordering, 'Called' as Stage, Called as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and Called is not null" +
                     "   UNION" +
                     "   Select 4 as Ordering, 'CV/Resume Sent' as Stage, CVSend as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and CVSend is not null" +
                     "   UNION" +
                     "   Select 5 as Ordering, 'Telephone Interview' as Stage, TelephoneInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and TelephoneInterview is not null" +
                     "   UNION" +
                     "   Select 6 as Ordering, 'First Interview' as Stage, FirstInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and FirstInterview is not null" +
                     "   UNION" +
                     "   Select 7 as Ordering, 'Second Interview' as Stage, SecondInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and SecondInterview is not null" +
                     "   UNION" +
                     "   Select 8 as Ordering, 'Third Interview' as Stage, ThirdInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and ThirdInterview is not null" +
                     "   UNION" +
                     "   Select 9 as Ordering, 'Fourth Interview' as Stage, FourthInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and FourthInterview is not null" +
                     "   UNION" +
                     "   Select 10 as Ordering, 'Fifth Interview' as Stage, FifthInterview as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and FifthInterview is not null" +
                     "   UNION" +
                     "   Select 11 as Ordering, 'References Taken' as Stage, ReferencesTaken as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and ReferencesTaken is not null" +
                     "   UNION" +
                     "   Select 12 as Ordering, 'Rejected' as Stage, Rejected as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and Rejected is not null" +
                     "   UNION" +
                     "   Select 13 as Ordering, 'Offer Made' as Stage, OfferMade as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and OfferMade is not null" +
                     "   UNION" +
                     "   Select 14 as Ordering, 'Offer Accepted' as Stage, OfferAccepted as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and OfferAccepted is not null" +
                     "   UNION" +
                     "   Select 15 as Ordering, 'Offer Rejected' as Stage, OfferRejected as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and OfferRejected is not null" +
                     "   UNION" +
                     "   Select 16 as Ordering, 'Withdrawn' as Stage, Withdrawn as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and Withdrawn is not null" +
                     "   UNION" +
                     "   Select 17 as Ordering, 'Placed' as Stage, Placed as StageDate from v_RequirementShortlistStage Where RequirementId = @RequirementId and ContactId = @ContactId and Placed is not null",
                OrderBy: ' Order by 1',
                Columns: [
                            { field: "Ordering", title: "Ordering", type: "number", width: 70, hidden: true },
                            { field: "Stage", title: "Stage", type: "string" },
                            { field: "StageDate", title: "Date", type: "date", format: "{0:dddd dd MMMM yyyy}" }
                ],
                //MobileVisibleColumns: [
                //    { field: "Stage", title: "Stage/Date" }
                //],
                //MobileRowTemplate: '<td colspan="1"><strong>#: Stage # </strong> on #: kendo.format("{0:dddd dd MMMM yyyy}", StageDate) # <hr></td>',

                PopulateGrid: function (sDiv, sGridName, sTitle, sSQL)
                {
                    var myPopulateObject = TriSysWeb.Pages.CandidateVacancy.ShortlistStagesGrid;

                    // Populate the grid now
                    TriSysSDK.Grid.VirtualMode({
                        Div: sDiv,
                        ID: sGridName,
                        Title: sTitle,
                        RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                        SQL: sSQL,
                        OrderBy: myPopulateObject.OrderBy,
                        Columns: myPopulateObject.Columns,
                        MobileVisibleColumns: myPopulateObject.MobileVisibleColumns,
                        MobileRowTemplate: myPopulateObject.MobileRowTemplate,

                        ColumnFilters: true,
                        Grouping: false
                    });
                }
            },


            AddToFavourites: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if (!$.isNumeric(lRequirementId))
                    return;

                var vacancySummary = TriSysWeb.Pages.CandidateVacancy.ReadVacancySummaryFromPageFields(lRequirementId);

                // No prompt required, just add to the 'cart'.
                // User does not even need to be logged in.
                TriSysWeb.Pages.CandidateVacancy.AddJobDetailsToFavourites(vacancySummary, true);
            },

            // Called from VacancySearch.html
            AddListToFavourites: function (multipleSelectedRequirementIds)
            {
                for (var i in multipleSelectedRequirementIds)
                {
                    var vacancySummary = {};
                    vacancySummary.RequirementId = multipleSelectedRequirementIds[i];

                    var bOpenFavouriteGrid = false;
                    TriSysWeb.Pages.CandidateVacancy.AddJobDetailsToFavourites(vacancySummary, bOpenFavouriteGrid);
                }

                // Now show favourites after adding all
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.FavouriteVacanciesPage);
            },

            RemoveFromFavourites: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if (!$.isNumeric(lRequirementId))
                    return;

                var vacancySummary = TriSysWeb.Pages.CandidateVacancy.ReadVacancySummaryFromPageFields(lRequirementId);

                // No prompt required, just remove from the 'cart'.
                // User does not even need to be logged in.
                TriSysWeb.Pages.CandidateVacancy.RemoveJobDetailsFromFavourites(vacancySummary, true);
            },

            // Called from FavouriteVacancies.html
            RemoveListFromFavourites: function (multipleSelectedRequirementIds)
            {
                for (var i in multipleSelectedRequirementIds)
                {
                    var vacancySummary = {};
                    vacancySummary.RequirementId = multipleSelectedRequirementIds[i];

                    var bOpenFavouriteGrid = false;
                    TriSysWeb.Pages.CandidateVacancy.RemoveJobDetailsFromFavourites(vacancySummary, bOpenFavouriteGrid);
                }

                // Now show favourites after adding all
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.FavouriteVacanciesPage);
            },

            Apply: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if (!$.isNumeric(lRequirementId))
                    return;

                var vacancySummary = TriSysWeb.Pages.CandidateVacancy.ReadVacancySummaryFromPageFields(lRequirementId);

                if (TriSysWeb.Pages.CandidateVacancy.CandidateLoginSecurity(lRequirementId, vacancySummary))
                {
                    // User is logged in, so pop-up a vacancy application dialogue allowing the user to write an application,
                    // or answer a set of qualification questions etc..
                    var fnLoadRequirementSummary = function ()
                    {
                        $('#ApplyToVacancy_JobReference').html(vacancySummary.Reference);
                        $('#ApplyToVacancy_JobTitle').html(vacancySummary.JobTitle);
                    };

                    var fnApplyToJob = function ()
                    {
                        var sComments = $('#ApplyToVacancy_Comments').val();

                        var dataPacket = {
                            RequirementId: lRequirementId,
                            URL: sURL,
                            Comments: sComments,
                            Shortlist: TriSysWeb.Constants.CandidateApplyToVacancy_Shortlist,
                            Longlist: TriSysWeb.Constants.CandidateApplyToVacancy_Longlist
                        };

                        TriSysWeb.Pages.VacancyApplications.ApplyToVacancyViaServer(dataPacket, vacancySummary.Reference, vacancySummary.JobTitle);

                        return true;
                    };

                    // Show the dialogue now
                    TriSysWeb.Pages.VacancyApplications.ApplyToVacancyModalDialogue("Apply to Vacancy", "Confirm Application", fnApplyToJob, fnLoadRequirementSummary);
                    return;
                }
            },

            ApplyToMultiple: function (sURL, multipleRequirementIds)
            {
                if (!multipleRequirementIds)
                    return;
                else if (multipleRequirementIds.length === 0)
                    return;

                var lFirstRequirementId = multipleRequirementIds[0];
                if (TriSysWeb.Pages.CandidateVacancy.CandidateLoginSecurity(lFirstRequirementId, null))
                {
                    // User is logged in, so pop-up a vacancy application dialogue allowing the user to write an application,
                    // or answer a set of qualification questions etc..
                    var fnLoadRequirementSummary = function ()
                    {
                        $('#ApplyToVacancy_JobReference').html(multipleRequirementIds.length + " Vacancies");
                        $('#ApplyToVacancy_JobTitle').html("");
                    };

                    var fnApplyToJob = function ()
                    {
                        var sComments = $('#ApplyToVacancy_Comments').val();

                        var dataPacket = {
                            RequirementIdCollection: multipleRequirementIds,
                            URL: sURL,
                            Comments: sComments,
                            Shortlist: TriSysWeb.Constants.CandidateApplyToVacancy_Shortlist,
                            Longlist: TriSysWeb.Constants.CandidateApplyToVacancy_Longlist
                        };

                        TriSysWeb.Pages.VacancyApplications.ApplyToMultipleVacanciesViaServer(dataPacket, multipleRequirementIds.length);
                    };

                    // Show the dialogue now
                    TriSysWeb.Pages.VacancyApplications.ApplyToVacancyModalDialogue("Apply to Vacancy", "Confirm Application", fnApplyToJob, fnLoadRequirementSummary);
                    return;
                }
            },

            CandidateLoginSecurity: function (lRequirementId, vacancySummary)
            {
                if (!TriSysWeb.Security.IsCandidateLoggedIn())
                {
                    // User is not logged in so redirect to login page
                    var sMessage = TriSysSDK.Resources.Message("CandidateLoginSecurity");

                    var fnRegister = function ()
                    {
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.CandidateRegistrationPage + "?RequirementId=" + lRequirementId);
                    };

                    TriSysApex.UI.questionYesNo(sMessage, TriSysWeb.Constants.ApplicationName + " Login/Registration", "Register", fnRegister, "Close", null);
                    return false;
                }

                return true;
            },

            ReadVacancySummaryFromPageFields: function (lRequirementId)
            {
                var fieldDescription1 = {
                    TableName: "RequirementConfigFields",
                    TableFieldName: "RequirementReference",
                    FieldTypeId: TriSysSDK.Controls.FieldTypes.Text
                };
                var sReferenceFromForm = TriSysSDK.CShowForm.FieldWidgetValue(fieldDescription1);

                var fieldDescription2 = {
                    TableName: "RequirementConfigFields",
                    TableFieldName: "JobTitle",
                    FieldTypeId: TriSysSDK.Controls.FieldTypes.JobTitle
                };
                var sJobTitleFromForm = TriSysSDK.CShowForm.FieldWidgetValue(fieldDescription2);

                return {
                    RequirementId: lRequirementId,
                    Reference: sReferenceFromForm,
                    JobTitle: sJobTitleFromForm
                };
            },

            // Pop open a modal dialogue allowing the candidate to send themselves or a friend the job details
            SendToFriend: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if ($.isNumeric(lRequirementId))
                {
                    var vacancySummary = TriSysWeb.Pages.CandidateVacancy.ReadVacancySummaryFromPageFields(lRequirementId);

                    var fnLoadRequirementSummary = function ()
                    {
                        $('#SendVacancyToFriend_JobReference').html(vacancySummary.Reference);
                        $('#SendVacancyToFriend_JobTitle').html(vacancySummary.JobTitle);

                        // As a nice touch, restore the last recipient and sender details
                        var sLastToEMail = TriSysAPI.Cookies.getCookie("SendVacancyToFriend_ToEMail");
                        $('#SendVacancyToFriend_ToEMail').val(sLastToEMail);
                        var sFrom = TriSysAPI.Cookies.getCookie("SendVacancyToFriend_From");
                        $('#SendVacancyToFriend_From').val(sFrom);
                    };

                    var fnSendJob = function ()
                    {
                        var sToEMail = $('#SendVacancyToFriend_ToEMail').val();
                        if (!TriSysApex.LoginCredentials.validateEmail(sToEMail))
                        {
                            // Nice popup to replace browser thing
                            TriSysApex.UI.ShowMessage("Please enter a valid e-mail address to which to send the job.");
                            return false;
                        }

                        var sFrom = $('#SendVacancyToFriend_From').val();
                        if (!sFrom)
                        {
                            // Nice popup to replace browser thing
                            TriSysApex.UI.ShowMessage("Please enter your name/e-mail as the sender.");
                            return false;
                        }

                        // As a nice touch, store the last recipient and sender details
                        TriSysAPI.Cookies.setCookie("SendVacancyToFriend_ToEMail", sToEMail);
                        TriSysAPI.Cookies.setCookie("SendVacancyToFriend_From", sFrom);

                        var sComments = $('#SendVacancyToFriend_Comments').val();

                        var dataPacket = {
                            RequirementId: lRequirementId,
                            URL: sURL,
                            ToEMail: sToEMail,
                            From: sFrom,
                            Comments: sComments
                        };

                        TriSysWeb.Pages.CandidateVacancy.SendJobDetailsToFriendViaServer(dataPacket, vacancySummary.Reference, vacancySummary.JobTitle, sToEMail);

                        return true;
                    };

                    // Show the dialogue now
                    TriSysWeb.Pages.CandidateVacancy.SendToFriendModalDialogue("Send Vacancy to Friend", "Send Job", fnSendJob, fnLoadRequirementSummary);
                    return true;
                }

                // Something went wrong
                TriSysApex.UI.ShowError("Unable to load the specified vacancy.", TriSysWeb.Constants.ApplicationName)
                return false;
            },

            // Pop open a modal dialogue allowing the candidate to send themselves or a friend the job details
            SendMultipleVacanciesToFriend: function (sURL, multipleSelectedRequirementIds)
            {
                var sVacancySummary = multipleSelectedRequirementIds.length + " Vacancies";

                var fnLoadRequirementSummary = function ()
                {
                    $('#SendVacancyToFriend_JobReference').html(sVacancySummary);
                    $('#SendVacancyToFriend_JobTitle').html('');

                    // As a nice touch, restore the last recipient and sender details
                    var sLastToEMail = TriSysAPI.Cookies.getCookie("SendVacancyToFriend_ToEMail");
                    $('#SendVacancyToFriend_ToEMail').val(sLastToEMail);
                    var sFrom = TriSysAPI.Cookies.getCookie("SendVacancyToFriend_From");
                    $('#SendVacancyToFriend_From').val(sFrom);
                };

                var fnSendJobs = function ()
                {
                    var sToEMail = $('#SendVacancyToFriend_ToEMail').val();
                    if (!TriSysApex.LoginCredentials.validateEmail(sToEMail))
                    {
                        // Nice popup to replace browser thing
                        TriSysApex.UI.ShowMessage("Please enter a valid e-mail address to which to send the job.");
                        return false;
                    }

                    var sFrom = $('#SendVacancyToFriend_From').val();
                    if (!sFrom)
                    {
                        // Nice popup to replace browser thing
                        TriSysApex.UI.ShowMessage("Please enter your name/e-mail as the sender.");
                        return false;
                    }

                    // As a nice touch, store the last recipient and sender details
                    TriSysAPI.Cookies.setCookie("SendVacancyToFriend_ToEMail", sToEMail);
                    TriSysAPI.Cookies.setCookie("SendVacancyToFriend_From", sFrom);

                    var sComments = $('#SendVacancyToFriend_Comments').val();

                    var dataPacket = {
                        RequirementIdCollection: multipleSelectedRequirementIds,
                        URL: sURL,
                        ToEMail: sToEMail,
                        From: sFrom,
                        Comments: sComments
                    };

                    TriSysWeb.Pages.CandidateVacancy.SendMultipleJobDetailsToFriendViaServer(dataPacket, multipleSelectedRequirementIds.length, sToEMail);

                    return true;
                };

                // Show the dialogue now
                TriSysWeb.Pages.CandidateVacancy.SendToFriendModalDialogue("Send Vacancies to Friend", "Send Jobs", fnSendJobs, fnLoadRequirementSummary);
            },

            SendToFriendModalDialogue: function (sTitle, sButtonSendText, fnSendJob, fnLoadRequirementSummary)
            {
                var fnCancelSend = function ()
                {
                    // OK to close
                    return true;
                };

                // Show the dialogue now
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sTitle;
                parametersObject.Image = 'question';

                parametersObject.ContentURL = TriSysApex.Constants.ThemeFolder+TriSysApex.Constants.UserControlFolder + 'ctrlSendVacancyToFriend.html';

                parametersObject.Height = 225;
                parametersObject.Resizable = false;
                parametersObject.Maximize = false;

                //parametersObject.UserControlFolder = TriSysApex.Constants.UserControlFolder;
                //parametersObject.ModalControl = 'ctrlSendVacancyToFriend.html';
                //parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                // Buttons
                parametersObject.ButtonLeftText = sButtonSendText;
                parametersObject.ButtonLeftFunction = fnSendJob;
                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonLeftWidth = 100;
                parametersObject.ButtonRightWidth = 100;
                parametersObject.ButtonRightVisible = true;
                parametersObject.ButtonRightText = "Cancel";
                parametersObject.ButtonRightFunction = fnCancelSend;

                parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                TriSysApex.UI.PopupMessage(parametersObject);
                return;
            },

            // If the user is logged in, add on the server via the Web API.
            // If user not logged-in, store in memory on the browser cookies.
            AddJobDetailsToFavourites: function (vacancySummary, bOpenFavouriteGrid)
            {
                // This is the function to display the favourite jobs/cart after storing details
                var fnPostStoringOfFavouriteVacancy = function ()
                {
                    TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.FavouriteVacanciesPage);
                };

                var bCookieCart = true;
                if (TriSysWeb.Security.IsCandidateLoggedIn())
                {
                    // User is logged-in so send to server via Web API
                    // TODO
                }

                if (bCookieCart)
                {
                    // User not logged-in so add job to cookies for later analysis
                    var currentFavourites = TriSysWeb.Pages.CandidateVacancy.getFavouriteList();

                    // Duplicate check
                    var bAddToQueue = !TriSysWeb.Pages.CandidateVacancy.isFavourite(vacancySummary.RequirementId);
                    if (bAddToQueue)
                    {
                        currentFavourites.push(vacancySummary);
                        sCookie = JSON.stringify(currentFavourites);
                        TriSysAPI.Cookies.setCookie(TriSysWeb.Constants.Cookie_AddJobDetailsToFavourites, sCookie);
                    }

                    if (bOpenFavouriteGrid)
                    {
                        // Redirect to favourites/cart so that user can see all favourite jobs
                        fnPostStoringOfFavouriteVacancy();
                    }
                }
            },

            RemoveJobDetailsFromFavourites: function (vacancySummary, bOpenFavouriteGrid)
            {
                // This is the function to display the favourite jobs/cart after storing details
                var fnPostRemovalOfFavouriteVacancy = function ()
                {
                    TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.FavouriteVacanciesPage);
                };

                var bCookieCart = true;
                if (TriSysWeb.Security.IsCandidateLoggedIn())
                {
                    // User is logged-in so send to server via Web API
                    // TODO
                }

                if (bCookieCart)
                {
                    // User not logged-in so remove job from cookies for later analysis
                    var currentFavourites = TriSysWeb.Pages.CandidateVacancy.getFavouriteList();

                    if (TriSysWeb.Pages.CandidateVacancy.isFavourite(vacancySummary.RequirementId))
                    {
                        for (var i = 0; i < currentFavourites.length; i++)
                        {
                            var summary = currentFavourites[i];
                            if (summary.RequirementId == vacancySummary.RequirementId)
                            {
                                currentFavourites.splice(i, 1);
                                sCookie = JSON.stringify(currentFavourites);
                                TriSysAPI.Cookies.setCookie(TriSysWeb.Constants.Cookie_AddJobDetailsToFavourites, sCookie);
                                break;
                            }
                        }
                    }

                    if (bOpenFavouriteGrid)
                    {
                        // Redirect to favourites/cart so that user can see all favourite jobs
                        fnPostRemovalOfFavouriteVacancy();
                    }
                }
            },

            getFavouriteList: function ()
            {
                var sCookie = TriSysAPI.Cookies.getCookie(TriSysWeb.Constants.Cookie_AddJobDetailsToFavourites);
                var currentFavourites = null;
                if (sCookie)
                    currentFavourites = JSON.parse(sCookie);
                else
                    currentFavourites = [];

                return currentFavourites;
            },

            isFavourite: function (lRequirementId)
            {
                var sCookie = TriSysAPI.Cookies.getCookie(TriSysWeb.Constants.Cookie_AddJobDetailsToFavourites);
                var currentFavourites = null;
                if (sCookie)
                    currentFavourites = JSON.parse(sCookie);
                else
                    currentFavourites = [];

                // Duplicate check
                var bInQueue = false;
                for (var i = 0; i < currentFavourites.length; i++)
                {
                    var summary = currentFavourites[i];
                    if (summary.RequirementId == lRequirementId)
                        bInQueue = true;
                }

                return bInQueue;
            },

            SendJobDetailsToFriendViaServer: function (dataPacket, sJobRef, sJobTitle, sToEMail)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Sending Vacancy E-Mail...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/SendToFriend";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CVacancySendToFriendResult = data;
                    if (CVacancySendToFriendResult)
                    {
                        TriSysApex.UI.HideWait();

                        if (CVacancySendToFriendResult.Success)
                        {
                            var sMessage = "Vacancy details: " + sJobRef + " - " + sJobTitle +
                                "<br />" + "has been sent to: " + sToEMail;
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CVacancySendToFriendResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SendJobDetailsToFriendViaServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            SendMultipleJobDetailsToFriendViaServer: function (dataPacket, iNumberOfJobs, sToEMail)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Sending " + iNumberOfJobs + " Vacancy E-Mails...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/SendMultipleToFriend";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CVacancySendToFriendResult = data;
                    if (CVacancySendToFriendResult)
                    {
                        TriSysApex.UI.HideWait();

                        if (CVacancySendToFriendResult.Success)
                        {
                            var sMessage = "Vacancy details of " + iNumberOfJobs + " jobs" +
                                "<br />" + "has been sent to: " + sToEMail;
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CVacancySendToFriendResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SendMultipleJobDetailsToFriendViaServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        },
        //#endregion TriSysWeb.Pages.CandidateVacancy

        //#region TriSysWeb.Pages.ClientVacancy
        ClientVacancy:
        {
            LoadRecordFromURL: function (sURL)
            {
                TriSysSDK.CurrentRecord.RecordId = 0;
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if ($.isNumeric(lRequirementId))
                {
                    // Load entity record
                    // Tell CShowForm that we are initialised and it can customise the widgets
                    TriSysSDK.CShowForm.InitialiseFields("Requirement");

                    if (lRequirementId > 0)
                    {
                        setTimeout(function ()
                        {
                            TriSysSDK.CShowForm.readEntityFormDataFromServer('Requirement', lRequirementId,
                                                        TriSysWeb.Pages.ClientVacancy.AfterFormDataLoaded);

                        }, TriSysApex.Constants.FormLoadDataDelay);
                    }
                    else
                    {
                        // New record
                        var fieldDescriptions = TriSysSDK.CShowForm.getFieldDescriptionValuesList("Requirement");
                        TriSysWeb.Pages.ClientVacancy.AfterFormDataLoaded(0, fieldDescriptions);
                    }
                    return;
                }

                // Something went wrong
                TriSysApex.UI.ShowError("Unable to load the specified vacancy.", TriSysWeb.Constants.ApplicationName);
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lRequirementId, fieldDescriptions)
            {
                TriSysSDK.CurrentRecord.RecordId = lRequirementId;

                // Enumerate through field descriptions to identify special fields for additional population
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);
                var fieldDescriptionsForAsyncPopulation = [];

                var sSiteAddress = null, lSiteAddressId = 0;
                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "RequirementConfigFields_SiteAddress":
                            sSiteAddress = fieldDescription.Value;
                            break;

                        case "RequirementConfigFields_SiteAddressId":
                            lSiteAddressId = fieldDescription.Value;
                            break;

                        case "RequirementConfigFields_JobTitle":
                            TriSysSDK.CShowForm.SetTextInCombo(sDivID, fieldDescription.DisplayValue);
                            break;
                    }
                }

                // Client contact can choose any existing site address, or add a new one
                TriSysWeb.Pages.ClientVacancy.PopulateSiteAddresses(lSiteAddressId, sSiteAddress,
                                                "RequirementConfigFields_SiteAddressList", "RequirementConfigFields_SiteAddressId");

                // Display relevant fields when type field changed
                var fnTypeSelection = function (sValue, sText)
                {
                    var sType = TriSysSDK.CShowForm.GetTextFromCombo('Requirement_Type');
                    if (sType == 'Permanent')
                    {
                        $('#fieldGroup-Salary').show();
                        $('#fieldGroup-Rate').hide();
                        $('#fieldGroup-ContractDuration').hide();
                    }
                    else
                    {
                        $('#fieldGroup-Salary').hide();
                        $('#fieldGroup-Rate').show();
                        $('#fieldGroup-ContractDuration').show();
                    }
                };
                TriSysSDK.CShowForm.addCallbackOnComboSelect("Requirement_Type", fnTypeSelection);
                fnTypeSelection();
            },

            // Get all company addresses for the currently logged in client and populate the drop down combo
            // with all and select the specified default if it exists.
            PopulateSiteAddresses: function (lSiteAddressId, sSiteAddress, sAddressListFieldID, sAddressIdFieldID)
            {
                var sNewAddress = "New Address...", iNewAddress = 0;
                var addNewAddress = { text: sNewAddress, value: iNewAddress };

                var fnAddressSelection = function (sValue, sText)
                {
                    TriSysSDK.Controls.NumericSpinner.SetValue(sAddressIdFieldID, sValue);

                    if (sText == sNewAddress)
                    {
                        // User wishes to add a new address to their company.
                        // Throw up a modal dialogue to capture this, then refresh this control with all data
                        TriSysWeb.Pages.ClientVacancy.PopupAddCompanyAddressForm(sAddressListFieldID, sAddressIdFieldID);
                    }
                };

                // Initialise a drop down combo and add this site address

                // Asynchronous Web API call
                var payloadObject = {};
                payloadObject.URL = "Companies/SiteAddresses";
                payloadObject.OutboundDataPacket = null;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CCompanySiteAddressesResponse = data;
                    if (CCompanySiteAddressesResponse)
                    {
                        TriSysApex.UI.HideWait();

                        if (CCompanySiteAddressesResponse.Success)
                        {
                            // Populate the combo with a list of addresses
                            var dataSource = [];

                            for (var i = 0; i < CCompanySiteAddressesResponse.Addresses.length; i++)
                            {
                                var address = CCompanySiteAddressesResponse.Addresses[i];

                                var addressArrayItem = { text: address.Display, value: address.AddressId };
                                dataSource.push(addressArrayItem);
                            }

                            // Add the new address item at the end
                            dataSource.push(addNewAddress);

                            TriSysSDK.CShowForm.populateComboFromDataSource(sAddressListFieldID, dataSource, 0);
                            if (lSiteAddressId > 0)
                                TriSysSDK.CShowForm.SetValueInCombo(sAddressListFieldID, lSiteAddressId);
                            else
                            {
                                lSiteAddressId = TriSysSDK.CShowForm.GetValueFromCombo(sAddressListFieldID);
                                sSiteAddress = TriSysSDK.CShowForm.GetTextFromCombo(sAddressListFieldID);
                            }

                            fnAddressSelection(lSiteAddressId, sSiteAddress);

                            TriSysSDK.CShowForm.addCallbackOnComboSelect(sAddressListFieldID, fnAddressSelection);
                        }
                        else
                            TriSysApex.UI.errorAlert(CCompanySiteAddressesResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.ClientVacancy.PopulateSiteAddresses: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            PopupAddCompanyAddressForm: function (sAddressListFieldID, sAddressIdFieldID)
            {
                // Throw up a modal dialogue to capture this, then refresh this control with all data
                var fnShowCompanyNameAndCountry = function ()
                {
                    var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                    var sCompany = dataConnectionKey.LoggedInAgencyContact.CompanyName;
                    $('#AddCompanyAddress_CompanyName').html(sCompany);

                    TriSysSDK.Countries.CurrentCountryName(function (sCurrentCountry)
                    {
                        TriSysSDK.CShowForm.countriesCombo("AddCompanyAddress_Country", sCurrentCountry);
                    });
                };

                var fnSaveCompanyAddressCallback = function ()
                {
                    TriSysWeb.Pages.ClientVacancy.SaveCompanyAddressCallback(sAddressListFieldID, sAddressIdFieldID);
                    return true;
                };

                TriSysWeb.Pages.ClientVacancy.AddCompanyAddressModalDialogue("Add New Company Address", "Save", fnSaveCompanyAddressCallback, fnShowCompanyNameAndCountry);
            },

            AddCompanyAddressModalDialogue: function (sTitle, sButtonSaveText, fnSaveCallback, fnDisplayCallback)
            {
                var fnCancelSave = function ()
                {
                    return true;
                };

                // Show the dialogue now
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sTitle;
                parametersObject.Image = 'upload';
                parametersObject.Height = 370;

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlAddCompanyAddress.html';

                //parametersObject.UserControlFolder = TriSysApex.Constants.UserControlFolder;
                //parametersObject.ModalControl = 'ctrlAddCompanyAddress.html';
                //parametersObject.OnLoadCallback = fnDisplayCallback;

                // Buttons
                parametersObject.ButtonLeftText = sButtonSaveText;
                parametersObject.ButtonLeftFunction = fnSaveCallback;
                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonLeftWidth = 100;
                parametersObject.ButtonRightWidth = 100;
                parametersObject.ButtonRightVisible = true;
                parametersObject.ButtonRightText = "Cancel";
                parametersObject.ButtonRightFunction = fnCancelSave;

                parametersObject.OnLoadCallback = fnDisplayCallback;

                TriSysApex.UI.PopupMessage(parametersObject);
            },

            SaveCompanyAddressCallback: function (sAddressListFieldID, sAddressIdFieldID)
            {
                var sStreet = $('#AddCompanyAddress_Street').val();
                var sCity = $('#AddCompanyAddress_City').val();
                var sCounty = $('#AddCompanyAddress_County').val();
                var sPostCode = $('#AddCompanyAddress_PostCode').val();
                var sCountry = $('#AddCompanyAddress_Country').val();
                var sTelNo = $('#AddCompanyAddress_TelNo').val();
                var sFaxNo = $('#AddCompanyAddress_FaxNo').val();

                if (!sStreet || !sCity)
                {
                    alert('Please supply a street and city.');
                    return;
                }

                // Send back to server for storage
                var dataPacket = {
                    Street: sStreet,
                    City: sCity,
                    County: sCounty,
                    PostCode: sPostCode,
                    Country: sCountry,
                    TelNo: sTelNo,
                    FaxNo: sFaxNo
                };

                var fnOnSave = function (CAddCompanySiteAddressResponse)
                {
                    // Called when the data has been saved on the server, and we can now add this to the list
                    // of company addresses with this new one the top default
                    var sNewAddress = CAddCompanySiteAddressResponse.Address.Display;
                    var lAddressId = CAddCompanySiteAddressResponse.Address.AddressId;

                    TriSysApex.UI.CloseTopmostModalPopup();

                    // Re-Populate all addresses, this time with the newly added one
                    TriSysWeb.Pages.ClientVacancy.PopulateSiteAddresses(lAddressId, sNewAddress, sAddressListFieldID, sAddressIdFieldID);
                };

                TriSysApex.UI.ShowWait(null, "Saving Address...");

                var payloadObject = {};
                payloadObject.URL = "Companies/AddSiteAddress";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CAddCompanySiteAddressResponse = data;
                    if (CAddCompanySiteAddressResponse)
                    {
                        TriSysApex.UI.HideWait();

                        if (CAddCompanySiteAddressResponse.Success)
                            fnOnSave(CAddCompanySiteAddressResponse);
                        else
                            TriSysApex.UI.errorAlert(CAddCompanySiteAddressResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SaveCompanyAddressCallback: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            Save: function ()
            {
                // Validate
                var lRequirementId = TriSysSDK.CurrentRecord.RecordId;
                var lSiteAddressId = TriSysSDK.Controls.NumericSpinner.GetValue("RequirementConfigFields_SiteAddressId");
                var sJobTitle = TriSysSDK.CShowForm.GetTextFromCombo("RequirementConfigFields_JobTitle");
                var sJobDescription = $('#RequirementConfigFields_WebText').val();
                var sValidationError = null;

                if (!sJobTitle || sJobTitle == '-')
                    sValidationError = 'Please choose a valid job title.';
                else
                {
                    if (!sJobDescription)
                        sValidationError = "Please enter a job description.";
                    else if (lSiteAddressId <= 0)
                        sValidationError = "Please choose or enter a new address.";
                }
                if (sValidationError)
                {
                    TriSysApex.UI.ShowMessage(sValidationError, TriSysWeb.Constants.ApplicationName);
                    return;
                }


                var fnBeforeSaveFieldDescriptions = function (fieldDescriptions)
                {
                    for (var i = 0; i < fieldDescriptions.length; i++)
                    {
                        var fieldDescription = fieldDescriptions[i];

                        switch (fieldDescription.TableFieldName)
                        {
                            case 'JobTitle':
                                fieldDescription.Value = sJobTitle;
                                break;

                            case 'SiteAddress':
                                var sSiteAddress = TriSysSDK.CShowForm.GetTextFromCombo("RequirementConfigFields_SiteAddressList");
                                sSiteAddress = sSiteAddress.replace(/, /g, '\n\r');
                                fieldDescription.Value = sSiteAddress;
                                break;

                            case 'Comments':
                                if (!fieldDescription.Value)
                                    fieldDescription.Value = sJobDescription;
                                break;
                        }
                    }

                    // Eliminate all fields which are not in the DOM
                    var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

                    return fieldDescriptionsInDOM;
                };

                var fnAfterSaveRecord = function (lRecordId)
                {
                    if (lRecordId > 0)
                        TriSysWeb.Pages.ClientVacancy.BusinessRules.AfterUpdateOrDelete(lRecordId, true);
                };

                TriSysApex.Forms.EntityForm.SaveEvent("Requirement", lRequirementId, fnBeforeSaveFieldDescriptions, fnAfterSaveRecord);
            },

            Delete: function ()
            {
                var lRequirementId = TriSysSDK.CurrentRecord.RecordId;
                if (lRequirementId > 0)
                    TriSysApex.UI.questionYesNo("Delete Client Vacancy", TriSysWeb.Constants.ApplicationName, "Yes", TriSysWeb.Pages.ClientVacancy.DeleteConfirmation, "No", null);
                else
                    TriSysApex.UI.ShowMessage("No requirement found.", TriSysWeb.Constants.ApplicationName);
            },

            DeleteConfirmation: function ()
            {
                var fnPostDelete = function (lRequirementId)
                {
                    // Do business rules
                    TriSysWeb.Pages.ClientVacancy.BusinessRules.AfterUpdateOrDelete(lRequirementId, false, function ()
                    {
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientVacanciesPage);
                    });
                };

                var lRequirementId = TriSysSDK.CurrentRecord.RecordId;
                var dataPacket = {
                    RequirementId: lRequirementId
                };

                TriSysApex.UI.ShowWait(null, "Deleting Vacancy...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/Delete";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CVacancyDeleteResponse = data;
                    if (CVacancyDeleteResponse)
                    {
                        if (CVacancyDeleteResponse.Success)
                            fnPostDelete(lRequirementId);
                        else
                            TriSysApex.UI.errorAlert(CVacancyDeleteResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.ClientVacancy.DeleteConfirmation: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            BusinessRules:
            {
                AfterUpdateOrDelete: function (lRequirementId, bInsertOrUpdate, fnPostDelete)
                {
                    // TODO: Business Rules for auto-e-mail etc..
                    var fnReload = function ()
                    {
                        if (fnPostDelete)
                            fnPostDelete();
                        else
                        {
                            // Cannot re-load data because if it is a new record, then the URL is wrong!
                            //TriSysSDK.CShowForm.readEntityFormDataFromServer('Requirement', lRequirementId, TriSysWeb.Pages.ClientVacancy.AfterFormDataLoaded);
                            TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ClientVacancyPage + "?RequirementId=" + lRequirementId);
                        }
                    };
                    var sMessage = "Thank you for " + (bInsertOrUpdate ? "updating" : "deleting") + " this vacancy.";
                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName, fnReload);
                }
            }
        },
        //#endregion TriSysWeb.Pages.ClientVacancy

        //#region TriSysWeb.Pages.VacancyApplications
        VacancyApplications:
        {
            // Candidate must be logged in to view their applications
            LoadGrid: function ()
            {
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var sSQL = TriSysWeb.Pages.VacancyApplications.GridSQL + dataConnectionKey.LoggedInAgencyContact.ContactId;

                // Populate the grid
                TriSysWeb.Pages.VacancyApplications.PopulateGrid("divVacancyApplicationsGrid", "grdVacancyApplications", "Vacancy Applications", sSQL);
            },

            PopulateGrid: function (sDiv, sGridName, sTitle, sSQL)
            {
                var myPopulateObject = TriSysWeb.Pages.VacancyApplications;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridName,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: myPopulateObject.OrderBy,
                    Columns: myPopulateObject.Columns,
                    MobileVisibleColumns: myPopulateObject.MobileVisibleColumns,
                    MobileRowTemplate: myPopulateObject.MobileRowTemplate,

                    KeyColumnName: "RequirementId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load vacancy Form with specified ID
                        var lRequirementId = rowData.RequirementId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.CandidateVacancyPage + "?RequirementId=" + lRequirementId);
                    },
                    MultiRowSelect: true,
                    ColumnFilters: true,
                    Grouping: false
                });
            },

            GridSQL: "Select v.RequirementId, r.Requirement_RequirementReference, r.Requirement_RequirementEntityType, " +
                     "          r.Requirement_JobTitle, " +
                     "          r.Requirement_RequirementStatus,  " +
                     "          r.Requirement_EarliestStartDate, " +
                     "          isnull(dbo.f_GetRequirementSiteAddressCity(v.RequirementId), 'Unknown') as Location, " +
                     "      case r.Requirement_RequirementEntityType when 'Permanent' then r.Requirement_MaximumSalary else r.Requirement_MaximumRate end as Salary, " +
	                 "      v.DateShortlisted,  " +
	                 "      isnull(dbo.f_RequirementShortlistCurrentStage(v.RequirementId, v.COntactId), 'Waiting for Consultant') as CurrentStage, " +
	                 "      dbo.f_RequirementShortlistCurrentStageDate(v.RequirementId, v.COntactId) as CurrentStageDate " +
                     "  from v_RequirementShortlistStage v, v_Requirement_AllFields r " +
                     "  where v.RequirementId = r.Requirement_RequirementId " +
                     "  and v.ContactId = ",
            OrderBy: ' Order by r.Requirement_EarliestStartDate desc',
            Columns: [
                        { field: "RequirementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Requirement_RequirementReference", title: "Reference", type: "string" },
                        { field: "Requirement_RequirementEntityType", title: "Type", type: "string" },
                        { field: "Requirement_JobTitle", title: "Job Title", type: "string" },
                        { field: "Requirement_RequirementStatus", title: "Status", type: "string" },
                        { field: "Requirement_EarliestStartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Location", title: "Location", type: "string" },
                        { field: "Salary", title: "Salary", type: "string" },
                        { field: "DateShortlisted", title: "Shortlisted", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "CurrentStage", title: "Current Stage", type: "string" },
                        { field: "CurrentStageDate", title: "Stage Date", type: "date", format: "{0:dd MMM yyyy}", hidden: true }
            ],
            MobileVisibleColumns: [
                { field: "Requirement_RequirementReference", title: "Job Details" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Requirement_RequirementReference # (#: Requirement_RequirementEntityType #: #: Requirement_RequirementStatus #)</strong><br />' +
                                '<i>#: Requirement_JobTitle #</i><br />#: Location #<br />#: Salary #<br />' +
                                'Start Date: #: kendo.format("{0:dd MMM yyyy}", Requirement_EarliestStartDate) #<br />Current Stage: #: CurrentStage #<hr></td>',

            ApplyToVacancyModalDialogue: function (sTitle, sButtonSendText, fnApplyToJob, fnLoadRequirementSummary)
            {
                var fnCancelApply = function ()
                {
                    return true;
                };

                // Show the dialogue now
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sTitle;
                parametersObject.Image = 'question';

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlApplyToVacancy.html';

                //parametersObject.UserControlFolder = TriSysApex.Constants.UserControlFolder;
                //parametersObject.ModalControl = 'ctrlApplyToVacancy.html';
                //parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                // Buttons
                parametersObject.ButtonLeftText = sButtonSendText;
                parametersObject.ButtonLeftFunction = fnApplyToJob;
                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonLeftWidth = 200;
                parametersObject.ButtonRightWidth = 100;
                parametersObject.ButtonRightVisible = true;
                parametersObject.ButtonRightText = "Cancel";
                parametersObject.ButtonRightFunction = fnCancelApply;

                parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                TriSysApex.UI.PopupMessage(parametersObject);
                return;
            },

            ApplyToVacancyViaServer: function (dataPacket, sJobRef, sJobTitle)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Applying to Vacancy...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/Apply";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CVacancyApplyResult = data;
                    if (CVacancyApplyResult)
                    {
                        TriSysApex.UI.HideWait();

                        if (CVacancyApplyResult.Success)
                        {
                            // Favourite/Apply buttons
                            TriSysWeb.Pages.CandidateVacancy.PostActionButtons(dataPacket.RequirementId);

                            // Show confirmation
                            var sMessage = TriSysSDK.Resources.Message("ApplyToVacancyViaServer");
                            sMessage = sMessage.replace("##Summary##", sJobRef + " - " + sJobTitle);
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CVacancyApplyResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('ApplyToVacancyViaServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            ApplyToMultipleVacanciesViaServer: function (dataPacket, iNumberOfJobs)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Applying to " + iNumberOfJobs + " Vacancies...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/ApplyToMultiple";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CVacancyApplyResult = data;
                    if (CVacancyApplyResult)
                    {
                        TriSysApex.UI.HideWait();

                        if (CVacancyApplyResult.Success)
                        {
                            var sMessage = TriSysSDK.Resources.Message("ApplyToMultipleVacanciesViaServer");
                            sMessage = sMessage.replace("##Summary##", iNumberOfJobs + " vacancies");
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CVacancyApplyResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('ApplyToMultipleVacanciesViaServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // When a candidate withdraws from a job they have applied to, we can prompt them for a reason.
            // 
            WithdrawApplicationFromURL: function (sURL)
            {
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");
                if ($.isNumeric(lRequirementId))
                {
                    var singleRequirementIdList = [lRequirementId];
                    sURL = sURL.replace(lRequirementId, '');
                    TriSysWeb.Pages.VacancyApplications.WithdrawApplication(sURL, singleRequirementIdList);
                }
            },

            WithdrawApplication: function (sURL, multipleRequirementIds)
            {
                var vacancySummary = null;
                if (!multipleRequirementIds)
                    return;
                else if (multipleRequirementIds.length == 0)
                    return;

                var lFirstRequirementId = multipleRequirementIds[0];

                if (TriSysWeb.Pages.CandidateVacancy.CandidateLoginSecurity(lFirstRequirementId, null))
                {
                    // User is logged in, so pop-up a withdraw vacancy application dialogue allowing the user to explain their
                    // reasons for withdrawing.

                    if (multipleRequirementIds.length == 1)
                        vacancySummary = TriSysWeb.Data.Vacancy.Summary(lFirstRequirementId);

                    var fnLoadRequirementSummary = function ()
                    {
                        if (vacancySummary)
                        {
                            $('#WithdrawFromVacancy_JobReference').html(vacancySummary.Reference);
                            $('#WithdrawFromVacancy_JobTitle').html(vacancySummary.JobTitle);
                        }
                        else
                        {
                            $('#WithdrawFromVacancy_JobReference').html(multipleRequirementIds.length + " Vacancies");
                            $('#WithdrawFromVacancy_JobTitle').hide();
                        }
                    };

                    var fnWithdrawFromJob = function ()
                    {
                        var sReason = $('#WithdrawFromVacancy_Reason').val();
                        if (!sReason)
                        {
                            var sWhat = multipleRequirementIds.length == 1 ? "this vacancy" : "these " + multipleRequirementIds.length + " vacancies";
                            TriSysApex.UI.ShowMessage("Please provide a reason why you are withdrawing your application for " + sWhat + "?", TriSysWeb.Constants.ApplicationName);
                            return;
                        }

                        var dataPacket = {
                            RequirementIdList: multipleRequirementIds,
                            URL: sURL,
                            Reason: sReason
                        };

                        var sCompletionVacancySummary = multipleRequirementIds.length == 1 ? vacancySummary.Reference : multipleRequirementIds.length + " vacancies";

                        TriSysWeb.Pages.VacancyApplications.WithdrawFromVacancyViaServer(dataPacket, sCompletionVacancySummary);

                        return true;
                    };

                    // Show the dialogue now
                    TriSysWeb.Pages.VacancyApplications.WithdrawFromVacancyModalDialogue("Withdraw Vacancy Application", "Confirm Withdrawal", fnWithdrawFromJob, fnLoadRequirementSummary);
                    return;
                }
            },

            WithdrawFromVacancyModalDialogue: function (sTitle, sButtonSendText, fnWithdrawFromJob, fnLoadRequirementSummary)
            {
                var fnCancelApply = function ()
                {
                    return true;
                };

                // Show the dialogue now
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sTitle;
                parametersObject.Image = 'question';

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlWithdrawFromVacancy.html';

                //parametersObject.UserControlFolder = TriSysApex.Constants.UserControlFolder;
                //parametersObject.ModalControl = 'ctrlWithdrawFromVacancy.html';
                //parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                // Buttons
                parametersObject.ButtonLeftText = sButtonSendText;
                parametersObject.ButtonLeftFunction = fnWithdrawFromJob;
                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonLeftWidth = 140;
                parametersObject.ButtonRightWidth = 100;
                parametersObject.ButtonRightVisible = true;
                parametersObject.ButtonRightText = "Cancel";
                parametersObject.ButtonRightFunction = fnCancelApply;

                parametersObject.OnLoadCallback = fnLoadRequirementSummary;

                TriSysApex.UI.PopupMessage(parametersObject);
                return;
            },

            WithdrawFromVacancyViaServer: function (dataPacket, sCompletionVacancySummary)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Withdrawing from " + sCompletionVacancySummary + "...");

                var payloadObject = {};
                payloadObject.URL = "Vacancy/WithdrawApplication";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CVacancyWithdrawApplicationResult = data;
                    if (CVacancyWithdrawApplicationResult)
                    {
                        TriSysApex.UI.HideWait();

                        if (CVacancyWithdrawApplicationResult.Success)
                        {
                            var sMessage = TriSysSDK.Resources.Message("WithdrawFromVacancyViaServer");
                            sMessage = sMessage.replace("##Summary##", sCompletionVacancySummary);
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName,
                                function ()
                                {
                                    TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.VacancyApplicationsPage);
                                });
                        }
                        else
                            TriSysApex.UI.errorAlert(CVacancyWithdrawApplicationResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('WithdrawFromVacancyViaServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        },
        //#endregion TriSysWeb.Pages.VacancyApplications

        //#region TriSysWeb.Pages.CandidateFavouriteVacancies
        CandidateFavouriteVacancies:
        {
            LoadGrid: function ()
            {
                var sCookie = TriSysAPI.Cookies.getCookie(TriSysWeb.Constants.Cookie_AddJobDetailsToFavourites);
                var currentFavourites = null;
                if (sCookie)
                    currentFavourites = JSON.parse(sCookie);
                else
                    currentFavourites = [];

                if (currentFavourites.length === 0)
                    return;      // No favourites

                // Modify the SQL statement with favourites
                var sWhereSQL = '';
                for (var i = 0; i < currentFavourites.length; i++)
                {
                    var summary = currentFavourites[i];
                    if (sWhereSQL.length > 0)
                        sWhereSQL += ",";

                    sWhereSQL += summary.RequirementId;
                }

                sWhereSQL = " and Requirement_RequirementId in (" + sWhereSQL + ")";

                // Populate the grid
                TriSysWeb.Pages.CandidateVacancies.PopulateGrid("divFavouriteVacanciesGrid", "grdFavourites", "Favourite Vacancies", sWhereSQL);
            }
        },
        //#endregion TriSysWeb.Pages.CandidateFavouriteVacancies

        //#region TriSysWeb.Pages.CandidateRegistration
        CandidateRegistration:
        {
            Load: function ()
            {
                if (TriSysWeb.Pages.CandidateRegistration.ProcessGUIDConfirmation())
                    return true;

                // If already logged-in, jump out
                if (TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return TriSysWeb.Pages.Home.Redirect();

                // Load defaults for all fields
                TriSysWeb.Pages.CandidateRegistration.InitialiseProfileFields(false, true);
            },

            InitialiseProfileFields: function (bCVFileRefDeleteButton, bCVFileRefOpenButton)
            {
                // Populate all lookups
                TriSysSDK.CShowForm.contactTitleFieldEditableProperties("Contact_ContactTitle", "Mr");

                // Get the current country
                TriSysSDK.Countries.CurrentCountryName(function (sCurrentCountry)
                {
                    TriSysSDK.CShowForm.countriesCombo("Contact_HomeAddressCountry", sCurrentCountry);
                });

                // CV File Ref
                var sCVFileRefDiv = "ContactConfigFields_CVFileRef";
                var fieldDescription = {
                    TableName: "ContactConfigFields",
                    TableFieldName: "CVFileRef",
                    FieldTypeId: TriSysSDK.Controls.FieldTypes.FileReference
                };
                TriSysSDK.Controls.FileReference.Load(sCVFileRefDiv, fieldDescription, null);

                setTimeout(function ()
                {
                    TriSysSDK.Controls.FileReference.DeleteButtonVisible(sCVFileRefDiv, bCVFileRefDeleteButton);
                    TriSysSDK.Controls.FileReference.OpenButtonVisible(sCVFileRefDiv, bCVFileRefOpenButton);
                }, 100);
            },

            // If the candidate has responded to an e-mail confirmation, then the URL will contain a GUID.
            // Check the GUID against the web service and if it returns success, then the contact
            // record will be created and we will apply for the job if a requirementid URL parameter
            // is set.
            ProcessGUIDConfirmation: function ()
            {
                var sURL = window.location.href;
                var sSite = TriSysApex.URL.Parser(sURL).getHost();
                var sGUID = TriSysApex.URL.Parser(sURL).getParam("GUID");
                var lRequirementId = TriSysApex.URL.Parser(sURL).getParam("RequirementId");

                if (sGUID)
                {
                    // There is a GUID present so re-connect back to server as candidate has confirmed

                    TriSysApex.UI.CloseTopmostModalPopup();
                    TriSysApex.UI.ShowWait(null, "Confirming Registration...");

                    // Wipe out all credentials which exist i.e. they may already have a login which we will 'forget'
                    TriSysWeb.Security.WritePersistedLoginCredentials(null);

                    var sContactComments = "Registered at " + sSite + " on " + moment().format('dddd DD MMMM YYYY');

                    var CConfirmCandidateSignUpRequest = {
                        EMailGUIDKey: sGUID,
                        Comments: sContactComments,
                        SiteURL: sSite
                    };

                    var payloadObject = {};
                    payloadObject.URL = "Security/RegisterCandidateConfirmation";
                    payloadObject.OutboundDataPacket = CConfirmCandidateSignUpRequest;
                    payloadObject.InboundDataFunction = function (data)
                    {
                        TriSysApex.UI.HideWait();

                        var CConfirmCandidateSignUpResponse = data;
                        if (CConfirmCandidateSignUpResponse)
                        {
                            if (CConfirmCandidateSignUpResponse.Success)
                                TriSysWeb.Pages.CandidateRegistration.GUIDConfirmationSuccess(CConfirmCandidateSignUpResponse);
                            else
                                TriSysApex.UI.errorAlert(CConfirmCandidateSignUpResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.ShowMessage("No response from " + payloadObject.URL, TriSysWeb.Constants.ApplicationName);

                        return true;
                    };
                    payloadObject.ErrorHandlerFunction = function (request, status, error)
                    {
                        TriSysApex.UI.HideWait();
                        TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.CandidateRegistration.ProcessGUIDConfirmation: ', request, status, error);
                    };
                    TriSysAPI.Data.PostToWebAPI(payloadObject);

                    return true;
                }

                return false;
            },

            // Called after the candidate has confirmed their e-mail address.
            // Prompt the user to auto-login.
            GUIDConfirmationSuccess: function (CConfirmCandidateSignUpResponse)
            {
                var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.CandidateRegistration.ProcessGUIDConfirmation");
                var sYes = TriSysSDK.Resources.Message("Yes");
                var sNo = TriSysSDK.Resources.Message("No");

                var fnRedirect = function ()
                {
                    var sPage = TriSysWeb.Constants.HomePage;
                    if ($.isNumeric(lRequirementId))
                        sPage = TriSysWeb.Constants.CandidateVacancyPage;

                    var sURL = window.location.href;
                    sURL = sURL.replace(TriSysWeb.Constants.CandidateRegistrationPage, sPage);
                    TriSysWeb.Pages.LoadPage(sURL);
                };

                var fnYes = function ()
                {
                    // Store credentials in cookies, then commence login sequence
                    var credentialPacket = {
                        EMailAddress: CConfirmCandidateSignUpResponse.EMail,
                        Password: CConfirmCandidateSignUpResponse.Password
                    };
                    TriSysWeb.Security.WritePersistedLoginCredentials(credentialPacket);
                    if (!TriSysWeb.Security.AutoLoginFromSavedCredentials())
                        fnRedirect();
                };

                sMessage = sMessage.replace("##EMail##", CConfirmCandidateSignUpResponse.EMail);
                TriSysApex.UI.questionYesNo(sMessage, TriSysWeb.Constants.ApplicationName, sYes, fnYes, sNo, fnRedirect);
            },

            RegistrationButtonClick: function ()
            {
                var sForenames = $('#txtForename').val();
                var sSurname = $('#txtSurname').val();
                var sPassword = $('#pwPassword').val();
                var sVerifyPassword = $('#pwConfirm').val();
                var sEMail = $('#txtEmail').val();
                var sTitle = TriSysSDK.CShowForm.GetTextFromCombo('Contact_ContactTitle');
                var sJobTitle = $('#txtJobTitle').val();
                var sStreet = $('#txtStreet').val();
                var sCity = $('#txtTownCity').val();
                var sCounty = $('#txtCounty').val();
                var sPostCode = $('#txtPostCode').val();
                var sCountry = $('#Contact_HomeAddressCountry').val();
                var sHomeTelNo = $('#txtHomeTel').val();
                var sMobileTelNo = $('#txtMobTel').val();
                var sCVFileRef = TriSysSDK.Controls.FileReference.GetFile("ContactConfigFields_CVFileRef");
                //var bAgree = TriSysApex.UI.CheckBoxFieldValueToBoolean('chkSignUp_Agree');

                // Validation rules
                var sRuleFailure = '';
                if (!sForenames) sRuleFailure += 'Please enter your forename(s)' + '<br />';
                if (!sSurname) sRuleFailure += 'Please enter your surname' + '<br />';
                if (!sPassword)
                    sRuleFailure += 'Please enter a password' + '<br />';
                else
                {
                    var iMinLength = TriSysApex.Constants.iMinimumPasswordLength;
                    if (sPassword.length < iMinLength)
                        sRuleFailure += 'Please enter a password with a mininum length of ' + iMinLength + ' characters' + '<br />';
                }
                if (sPassword != sVerifyPassword) sRuleFailure += 'Please confirm your password' + '<br />';

                if (!TriSysApex.LoginCredentials.validateEmail(sEMail)) sRuleFailure += "Please enter a valid e-mail address." + '<br />';

                if (sRuleFailure.length > 0)
                {
                    TriSysApex.UI.ShowMessage(sRuleFailure, TriSysWeb.Constants.ApplicationName);
                    return;
                }

                // Validation rules are now passed, so submit to the server
                var sURL = window.location.href;

                var CNewCandidateSignUpRequest = {
                    Forenames: sForenames,
                    Surname: sSurname,
                    Password: sPassword,
                    EMail: sEMail,
                    Title: sTitle,
                    JobTitle: sJobTitle,
                    Street: sStreet,
                    City: sCity,
                    County: sCounty,
                    PostCode: sPostCode,
                    Country: sCountry,
                    HomeTelNo: sHomeTelNo,
                    MobileTelNo: sMobileTelNo,
                    CVFileRef: sCVFileRef,
                    URL: sURL
                };

                TriSysWeb.Pages.CandidateRegistration.SubmitToServer(CNewCandidateSignUpRequest);
            },

            SubmitToServer: function (CNewCandidateSignUpRequest)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Registering...");

                var payloadObject = {};
                payloadObject.URL = "Security/RegisterCandidate";
                payloadObject.OutboundDataPacket = CNewCandidateSignUpRequest;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CNewCandidateSignUpResponse = data;
                    if (CNewCandidateSignUpResponse)
                    {
                        if (CNewCandidateSignUpResponse.Success)
                        {
                            var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.CandidateRegistration.SubmitToServer");
                            sMessage = sMessage.replace("##EMail##", CNewCandidateSignUpRequest.EMail);
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName, TriSysWeb.Pages.Home.Redirect);
                        }
                        else
                            TriSysApex.UI.errorAlert(CNewCandidateSignUpResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }
                    else
                        TriSysApex.UI.ShowMessage("No response from " + payloadObject.URL, TriSysWeb.Constants.ApplicationName);

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.HideWait();
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.CandidateRegistration.SubmitToServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        },
        //#endregion TriSysWeb.Pages.CandidateRegistration

        //#region TriSysWeb.Pages.ClientRegistration
        ClientRegistration:
        {
            Load: function ()
            {
                if (TriSysWeb.Pages.ClientRegistration.ProcessGUIDConfirmation())
                    return true;

                // If already logged-in, jump out
                if (TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return TriSysWeb.Pages.Home.Redirect();

                // Load defaults for all fields
                TriSysWeb.Pages.ClientRegistration.InitialiseProfileFields();

                return true;
            },

            InitialiseProfileFields: function ()
            {
                // Populate all lookups
                TriSysSDK.CShowForm.contactTitleFieldEditableProperties("Contact_ContactTitle", "Mr");

                // Get the current country
                TriSysSDK.Countries.CurrentCountryName(function (sCurrentCountry)
                {
                    TriSysSDK.CShowForm.countriesCombo("Contact_WorkAddressCountry", sCurrentCountry);
                });
            },

            RegistrationButtonClick: function ()
            {
                var sForenames = $('#txtForename').val();
                var sSurname = $('#txtSurname').val();
                var sPassword = $('#pwPassword').val();
                var sVerifyPassword = $('#pwConfirm').val();
                var sEMail = $('#txtEmail').val();
                var sTitle = TriSysSDK.CShowForm.GetTextFromCombo('Contact_ContactTitle');
                var sJobTitle = $('#txtJobTitle').val();
                var sCompany = $('#Contact_CompanyName').val();
                var sStreet = $('#txtStreet').val();
                var sCity = $('#txtTownCity').val();
                var sCounty = $('#txtCounty').val();
                var sPostCode = $('#txtPostCode').val();
                var sCountry = $('#Contact_WorkAddressCountry').val();
                var sWorkTelNo = $('#txtWorkTel').val();
                var sMobileTelNo = $('#txtMobTel').val();

                // Validation rules
                var sRuleFailure = '';
                if (!sForenames) sRuleFailure += 'Please enter your forename(s)' + '<br />';
                if (!sSurname) sRuleFailure += 'Please enter your surname' + '<br />';
                if (!sPassword)
                    sRuleFailure += 'Please enter a password' + '<br />';
                else
                {
                    var iMinLength = TriSysApex.Constants.iMinimumPasswordLength;
                    if (sPassword.length < iMinLength)
                        sRuleFailure += 'Please enter a password with a mininum length of ' + iMinLength + ' characters' + '<br />';
                }
                if (sPassword != sVerifyPassword) sRuleFailure += 'Please confirm your password' + '<br />';

                if (!TriSysApex.LoginCredentials.validateEmail(sEMail)) sRuleFailure += "Please enter a valid e-mail address." + '<br />';
                if (!sJobTitle) sRuleFailure += 'Please enter your job title' + '<br />';
                if (!sCompany) sRuleFailure += 'Please enter your company name' + '<br />';

                if (sRuleFailure.length > 0)
                {
                    TriSysApex.UI.ShowMessage(sRuleFailure, TriSysWeb.Constants.ApplicationName);
                    return;
                }

                // Validation rules are now passed, so submit to the server
                var sURL = window.location.href;

                var CNewClientSignUpRequest = {
                    Forenames: sForenames,
                    Surname: sSurname,
                    Password: sPassword,
                    EMail: sEMail,
                    Title: sTitle,
                    JobTitle: sJobTitle,
                    Company: sCompany,
                    Street: sStreet,
                    City: sCity,
                    County: sCounty,
                    PostCode: sPostCode,
                    Country: sCountry,
                    WorkTelNo: sWorkTelNo,
                    MobileTelNo: sMobileTelNo,
                    URL: sURL
                };

                TriSysWeb.Pages.ClientRegistration.SubmitToServer(CNewClientSignUpRequest);
            },

            SubmitToServer: function (CNewClientSignUpRequest)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Registering...");

                var payloadObject = {};
                payloadObject.URL = "Security/RegisterClient";
                payloadObject.OutboundDataPacket = CNewClientSignUpRequest;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CNewClientSignUpResponse = data;
                    if (CNewClientSignUpResponse)
                    {
                        if (CNewClientSignUpResponse.Success)
                        {
                            var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.ClientRegistration.SubmitToServer");
                            sMessage = sMessage.replace("##EMail##", CNewClientSignUpRequest.EMail);
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName, TriSysWeb.Pages.Home.Redirect);
                        }
                        else
                            TriSysApex.UI.errorAlert(CNewClientSignUpResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }
                    else
                        TriSysApex.UI.ShowMessage("No response from " + payloadObject.URL, TriSysWeb.Constants.ApplicationName);

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.HideWait();
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.ClientRegistration.SubmitToServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // If the client has responded to an e-mail confirmation, then the URL will contain a GUID.
            // Check the GUID against the web service and if it returns success, then the contact
            // record will be created and we will display their vacancies.
            ProcessGUIDConfirmation: function ()
            {
                var sURL = window.location.href;
                var sSite = TriSysApex.URL.Parser(sURL).getHost();
                var sGUID = TriSysApex.URL.Parser(sURL).getParam("GUID");

                if (sGUID)
                {
                    // There is a GUID present so re-connect back to server as client has confirmed

                    TriSysApex.UI.CloseTopmostModalPopup();
                    TriSysApex.UI.ShowWait(null, "Confirming Registration...");

                    // Wipe out all credentials which exist i.e. they may already have a login which we will 'forget'
                    TriSysWeb.Security.WritePersistedLoginCredentials(null);

                    var sContactComments = "Registered at " + sSite + " on " + moment().format('dddd DD MMMM YYYY');

                    var CConfirmClientSignUpRequest = {
                        EMailGUIDKey: sGUID,
                        Comments: sContactComments,
                        SiteURL: sSite
                    };

                    var payloadObject = {};
                    payloadObject.URL = "Security/RegisterClientConfirmation";
                    payloadObject.OutboundDataPacket = CConfirmClientSignUpRequest;
                    payloadObject.InboundDataFunction = function (data)
                    {
                        TriSysApex.UI.HideWait();

                        var CConfirmClientSignUpResponse = data;
                        if (CConfirmClientSignUpResponse)
                        {
                            if (CConfirmClientSignUpResponse.Success)
                                TriSysWeb.Pages.ClientRegistration.GUIDConfirmationSuccess(CConfirmClientSignUpResponse);
                            else
                                TriSysApex.UI.errorAlert(CConfirmClientSignUpResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.ShowMessage("No response from " + payloadObject.URL, TriSysWeb.Constants.ApplicationName);

                        return true;
                    };
                    payloadObject.ErrorHandlerFunction = function (request, status, error)
                    {
                        TriSysApex.UI.HideWait();
                        TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.ClientRegistration.ProcessGUIDConfirmation: ', request, status, error);
                    };
                    TriSysAPI.Data.PostToWebAPI(payloadObject);

                    return true;
                }

                return false;
            },

            // Called after the client has confirmed their e-mail address.
            // Prompt the user to auto-login.
            GUIDConfirmationSuccess: function (CConfirmClientSignUpResponse)
            {
                var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.ClientRegistration.ProcessGUIDConfirmation");
                var sYes = TriSysSDK.Resources.Message("Yes");
                var sNo = TriSysSDK.Resources.Message("No");

                var fnRedirect = function ()
                {
                    var sURL = window.location.href;
                    sURL = sURL.replace(TriSysWeb.Constants.ClientRegistrationPage, TriSysWeb.Constants.ClientVacanciesPage);
                    TriSysWeb.Pages.LoadPage(sURL);
                };

                var fnYes = function ()
                {
                    // Store credentials in cookies, then commence login sequence
                    var credentialPacket = {
                        EMailAddress: CConfirmClientSignUpResponse.EMail,
                        Password: CConfirmClientSignUpResponse.Password
                    };
                    TriSysWeb.Security.WritePersistedLoginCredentials(credentialPacket);
                    if (!TriSysWeb.Security.AutoLoginFromSavedCredentials())
                        fnRedirect();
                };

                sMessage = sMessage.replace("##EMail##", CConfirmClientSignUpResponse.EMail);
                TriSysApex.UI.questionYesNo(sMessage, TriSysWeb.Constants.ApplicationName, sYes, fnYes, sNo, fnRedirect);
            }
        },
        //#endregion TriSysWeb.Pages.ClientRegistration

        //#region TriSysWeb.Pages.ForgottenPassword
        ForgottenPassword:
        {
            RequestButtonClick: function ()
            {
                var MessageIDPrefix = "TriSysWeb.Pages.ForgottenPassword.RequestButtonClick.";
                var sMessage;
                if (TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                {
                    // User is logged in so do not allow
                    sMessage = TriSysSDK.Resources.Message(MessageIDPrefix + "AlreadyloggedIn");
                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                    return;
                }

                var sEMail = $('#txtEmailAddress').val();
                if (!TriSysApex.LoginCredentials.validateEmail(sEMail))
                {
                    sMessage = TriSysSDK.Resources.Message(MessageIDPrefix + "InvalidEMailAddress");
                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                    return;
                }

                var sURL = window.location.href;
                var sSite = TriSysApex.URL.Parser(sURL).getHost();

                // Send this e-mail address now to the API so that it can take appropriate action
                var dataPacket = {
                    EMail: sEMail,
                    SiteURL: sSite
                };

                var fnOnSuccessfulRequest = function ()
                {
                    sMessage = TriSysSDK.Resources.Message(MessageIDPrefix + "SuccessfulRequest");
                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                };

                var fnOnFailedRequest = function ()
                {
                    sMessage = TriSysSDK.Resources.Message(MessageIDPrefix + "FailedRequest");
                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                };

                var sRequestingPasswordMessage = TriSysSDK.Resources.Message(MessageIDPrefix + "RequestingPassword");
                TriSysWeb.Pages.ForgottenPassword.SendRequestToAPI(dataPacket, sRequestingPasswordMessage, fnOnSuccessfulRequest, fnOnFailedRequest);
            },

            SendRequestToAPI: function (CAgencyForgottenPasswordRequest, sWaitMessage, fnSuccess, fnFailure)
            {
                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, sWaitMessage);

                var payloadObject = {};
                payloadObject.URL = "Security/ForgottenAgencyClientOrCandidatePasswordRequest";
                payloadObject.OutboundDataPacket = CAgencyForgottenPasswordRequest;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CAgencyForgottenPasswordResponse = data;
                    if (CAgencyForgottenPasswordResponse)
                    {
                        TriSysApex.UI.HideWait();

                        if (CAgencyForgottenPasswordResponse.Success)
                            fnSuccess();
                        else
                            fnFailure();
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SendRequestToAPI: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        },
        //#endregion TriSysWeb.Pages.ForgottenPassword

        //#region TriSysWeb.Pages.EditProfile
        EditProfile:
        {
            Load: function ()
            {
                // If not logged-in, jump out
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                {
                    TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.EditProfile.Load.NotLoggedIn"), TriSysWeb.Constants.ApplicationName, TriSysWeb.Pages.Home.Redirect);
                    return TriSysWeb.Security.ShowLoginPage();
                }

                // Tell CShowForm that we are initialised and it can customise the widgets
                TriSysSDK.CShowForm.InitialiseFields("Contact");

                // Load defaults for all fields
                TriSysWeb.Pages.CandidateRegistration.InitialiseProfileFields(true, true);

                // Contact Photo
                TriSysWeb.Pages.EditProfile.LoadContactPhotoWidgets();

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                // Load the contact
                TriSysWeb.Pages.EditProfile.LoadContactRecord(dataConnectionKey.LoggedInAgencyContact.ContactId);

                return true;
            },

            LoadContactPhotoWidgets: function ()
            {
                var fnContactPhotoCallback = function (sOperation, divTag, sFilePath)
                {
                    var sField = "ContactConfigFields_ContactPhoto";
                    var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                    var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                    switch (sOperation)
                    {
                        case 'find':
                            // Move the file as the new contact photo for the logged in user
                            TriSysApex.UserOptions.UpdateContactPhoto(sFilePath, "imgLoggedInUserImageEditor", sField, lContactId);
                            break;

                        case 'delete':
                            TriSysApex.UserOptions.DeleteContactPhoto("imgLoggedInUserImageEditor", sField, lContactId);
                            break;

                        default:
                            break;
                    }
                };

                var sContactPhotoFileRefDiv = "ContactConfigFields_ContactPhoto";
                TriSysSDK.Controls.FileReference.Load(sContactPhotoFileRefDiv, null, fnContactPhotoCallback);
            },

            // Use CShowForm to load the contact record asynchronously
            LoadContactRecord: function (lContactId)
            {
                TriSysSDK.CShowForm.readEntityFormDataFromServer('Contact', lContactId, TriSysWeb.Pages.EditProfile.AfterContactDataLoaded);
            },

            AfterContactDataLoaded: function (lContactId, fieldDescriptions)
            {
                var lSiteAddressId = 0;

                // Populate the standard fields
                TriSysApex.Pages.ContactForm.AfterFormDataLoaded(lContactId, fieldDescriptions);

                // Now do special fields for this application
                $('#pwConfirm').val($('#ContactConfigFields_WebPassword').val());

                for (var i = 0; i < fieldDescriptions.length; i++)
                {
                    var fieldDescription = fieldDescriptions[i];
                    var objValue = fieldDescription.Value;

                    switch (fieldDescription.TableFieldName)
                    {
                        case 'JobTitle':
                            $('#txtJobTitle').val(objValue);
                            break;

                        case 'ContactPhoto':
                            TriSysWeb.Pages.EditProfile.ShowContactPhoto(objValue);
                            break;

                        case 'CompanyAddressId':
                            lSiteAddressId = objValue;
                            break;

                        default:
                            break;
                    }
                }

                var bCandidate = TriSysWeb.Security.IsCandidateLoggedIn();
                if (bCandidate)
                {
                    $('#grpCandidateEMail').show();
                    $('#grpCandidateHomeTelNo').show();
                    $('#divCandidateHomeAddress').show();
                    $('#grpCandidatePersonalMobile').show();
                    $('#grpCandidateCVFileRef').show();
                }
                else
                {
                    $('#grpClientEMail').show();
                    $('#grpClientCompany').show();
                    $('#grpClientWorkTelNo').show();
                    $('#grpClientWorkMobileTelNo').show();
                    $('#divClientCompanyAddress').show();

                    // Client contact can choose any existing site address, or add a new one
                    var sSiteAddress = null;
                    TriSysWeb.Pages.ClientVacancy.PopulateSiteAddresses(lSiteAddressId, sSiteAddress,
                                                    "Contact_CompanyAddressList", "Contact_CompanyAddressId");
                }
            },

            ShowContactPhoto: function (sContactPhoto)
            {
                var sField = "ContactConfigFields_ContactPhoto";

                if (sContactPhoto)
                {
                    TriSysSDK.Controls.FileReference.SetFile(sField, sContactPhoto);

                    // Have to call an API to get a URL of the file
                    var fnShowContactPhotoCallback = function (sURL)
                    {
                        TriSysApex.Pages.Index.ShowImageThumbnail(sURL, 'imgLoggedInUserImageEditor', TriSysApex.Constants.ProfileImageSize, TriSysApex.Constants.ProfileImageSize);
                    };

                    var CFileReferenceInternalToExternalPathRequest = {
                        FolderFilePath: sContactPhoto
                    };

                    var payloadObject = {};
                    payloadObject.URL = "Files/FileReferenceInternalToExternalPath";
                    payloadObject.OutboundDataPacket = CFileReferenceInternalToExternalPathRequest;
                    payloadObject.InboundDataFunction = function (data)
                    {
                        var CFileReferenceInternalToExternalPathResult = data;
                        if (CFileReferenceInternalToExternalPathResult)
                        {
                            if (CFileReferenceInternalToExternalPathResult.Success)
                            {
                                fnShowContactPhotoCallback(CFileReferenceInternalToExternalPathResult.URL);
                            }
                            else
                                TriSysApex.UI.errorAlert(CFileReferenceInternalToExternalPathResult.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.ShowMessage("No response from " + payloadObject.URL, TriSysWeb.Constants.ApplicationName);

                        return true;
                    };
                    payloadObject.ErrorHandlerFunction = function (request, status, error)
                    {
                        // Can occur when user gets impatient waiting for photo to load and navigates away
                        TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.EditProfile.ShowContactPhoto: ', request, status, error, false);
                    };
                    TriSysAPI.Data.PostToWebAPI(payloadObject);
                }
            },

            // User wishes to save their own profile which is a contact record in the TriSys database
            SaveButtonClick: function ()
            {
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;
                var lSiteAddressId = TriSysSDK.Controls.NumericSpinner.GetValue("Contact_CompanyAddressId");

                var fnBeforeSaveFieldDescriptions = function (fieldDescriptions)
                {
                    for (var i = 0; i < fieldDescriptions.length; i++)
                    {
                        var fieldDescription = fieldDescriptions[i];

                        switch (fieldDescription.TableFieldName)
                        {
                            case 'JobTitle':
                                fieldDescription.Value = $('#txtJobTitle').val();
                                break;

                            case 'CVFileRef':
                                // We do not have the 'CV Manager' expected by the TriSys SDK, so get the path now
                                fieldDescription.Value = TriSysWeb.Pages.EditProfile.ProcessUploadedCVFileRefField(fieldDescription, lContactId);
                                TriSysApex.Logging.LogMessage("CVFileRef=" + fieldDescription.Value);
                                break;

                                //case 'ContactPhoto':
                                //    fieldDescription.Value = TriSysSDK.Controls.FileReference.GetFile("ContactConfigFields_ContactPhoto");
                                //    break;
                            default:
                                break;
                        }
                    }

                    // Eliminate all fields which are not in the DOM
                    var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);
                    return fieldDescriptionsInDOM;
                };

                TriSysApex.Forms.EntityForm.SaveEvent("Contact", lContactId, fnBeforeSaveFieldDescriptions);
            },

            // When saving the profile, the new uploaded CV must be moved from the default uploaded location to the
            // CVData folder prior to updating the contact entity record.
            ProcessUploadedCVFileRefField: function (fieldDescription, lContactId)
            {
                var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                var sFileRef = TriSysSDK.Controls.FileReference.GetFile(sDivID);

                if (sFileRef)
                {
                    var bCVDataPath = (sFileRef.indexOf(":\\CVData") > 0);
                    if (!bCVDataPath)
                    {
                        // We have an uploaded CV which has to be moved to the correct location for CV's on the server.
                        // Call a synchronous API do do this now so that we can return the correct path to be saved along with other data
                        sFileRef = TriSysWeb.Pages.EditProfile.UploadCVFileRef(lContactId, sFileRef, fieldDescription.TableFieldName);
                        TriSysSDK.Controls.FileReference.SetFile(sDivID, sFileRef);
                    }
                }

                return sFileRef;
            },

            // Call the ContactCV/MoveUploadedCVFileReference API synchronously
            UploadCVFileRef: function (lContactId, sFileRef, sTableFieldName)
            {
                var payloadObject = {};

                payloadObject.URL = "ContactCV/MoveUploadedCVFileReference";
                payloadObject.Asynchronous = false;

                payloadObject.OutboundDataPacket = {
                    UploadedFilePath: sFileRef,
                    ContactId: lContactId,
                    TableFieldName: sTableFieldName
                };

                payloadObject.InboundDataFunction = function (data)
                {
                    var JSONdataset = data;

                    if (JSONdataset)
                    {
                        if (JSONdataset.Success)
                        {
                            sFileRef = JSONdataset.CVFolderFilePath;
                        }
                        else
                        {
                            TriSysApex.UI.errorAlert(JSONdataset.ErrorMessage);
                        }
                    } else
                    {
                        // Something else has gone wrong!
                    }
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('UploadCVFileRef: ', request, status, error);
                };

                TriSysAPI.Data.PostToWebAPI(payloadObject);

                return sFileRef;
            }
        },
        //#endregion TriSysWeb.Pages.EditProfile

        //#region TriSysWeb.Pages.ContactUs
        ContactUs:
        {
            Open: function ()
            {
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.ContactUsPage);
            },

            Load: function ()
            {
                // Load defaults for all fields
                TriSysWeb.Pages.CandidateRegistration.InitialiseProfileFields(true, true);

                // If logged-in, fill in existing known fields
                if (TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                {
                    // Who is the logged in client/candidate?
                    var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                    // Load the contact
                    TriSysWeb.Pages.EditProfile.LoadContactRecord(dataConnectionKey.LoggedInAgencyContact.ContactId);
                }

                var sTypeOfEnquiryDiv = "typeOfEnquiry";
                var typeData = [
                    {
                        value: '',
                        text: ''
                    },
                    {
                        value: 'Candidate/Job Seeker',
                        text: 'Candidate/Job Seeker'
                    },
                    {
                        value: 'Client/Hiring Manager',
                        text: 'Client/Hiring Manager'
                    }
                ];
                TriSysSDK.CShowForm.dropDownComboFieldEditableProperties(sTypeOfEnquiryDiv, '', typeData);

            },

            ButtonClick: function ()
            {
                // Capture all data from form
                var sTitle = TriSysSDK.CShowForm.GetTextFromCombo('Contact_ContactTitle');
                var sForenames = $('#Contact_Forenames').val();
                var sSurname = $('#Contact_Surname').val();
                var sCompany = $('#Contact_CompanyName').val();
                var sHomeTelNo = $('#Contact_HomeAddressTelNo').val();
                var sWorkTelNo = $('#Contact_WorkTelNo').val();
                var sPersonalMobile = $('#Contact_MobileTelNo').val();
                var sWorkMobile = $('#ContactConfigFields_WorkMobile').val();
                var sWorkEMail = $('#Contact_EMail').val();
                var sPersonalEMail = $('#ContactConfigFields_AlternativeEMailAddress1').val();
                var sTypeOfEnquiry = $('#typeOfEnquiry').val();
                var sMessage = $('#txtMessage').val();

                // Validate mandatory data
                var sError = '';
                if (!sForenames || !sSurname)
                    sError = "Please supply your name.";
                else
                {
                    if (!sHomeTelNo && !sWorkTelNo && !sPersonalMobile && !sWorkMobile)
                        sError = "Please supply a phone number.";
                    else
                    {
                        if (!TriSysApex.LoginCredentials.validateEmail(sWorkEMail) && !TriSysApex.LoginCredentials.validateEmail(sPersonalEMail))
                            sError = "Please supply an e-mail address.";
                        else
                            if (sTypeOfEnquiry.length <= 1)
                                sError = "Please specify the type of enquiry.";
                            else
                                if (sMessage.length <= 1)
                                    sError = "Please enter your message to us.";
                    }
                }

                if (sError.length > 1)
                {
                    TriSysApex.UI.ShowMessage(sError, TriSysWeb.Constants.ApplicationName);
                    return;
                }

                // Bundle up into a JSON object for transfer across the wire
                var CEnquiryContactUsRequest = {
                    Title: sTitle,
                    Forenames: sForenames,
                    Surname: sSurname,
                    Company: sCompany,
                    HomeTelNo: sHomeTelNo,
                    WorkTelNo: sWorkTelNo,
                    PersonalMobile: sPersonalMobile,
                    WorkMobile: sWorkMobile,
                    WorkEMail: sWorkEMail,
                    PersonalEMail: sPersonalEMail,
                    TypeOfEnquiry: sTypeOfEnquiry,
                    Message: sMessage
                };

                // Send this data packet to the API for processing
                TriSysWeb.Pages.ContactUs.SendToAPI(CEnquiryContactUsRequest);
            },

            SendToAPI: function (CEnquiryContactUsRequest)
            {
                TriSysApex.UI.ShowWait(null, "Recording Request...");

                var payloadObject = {};
                payloadObject.URL = "Enquiry/ContactUs";
                payloadObject.OutboundDataPacket = CEnquiryContactUsRequest;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CEnquiryContactUsResponse = data;
                    if (CEnquiryContactUsResponse)
                    {
                        TriSysApex.UI.HideWait();

                        if (CEnquiryContactUsResponse.Success)
                            TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.ContactUs.SendToAPI.Success"), TriSysWeb.Constants.ApplicationName);
                        else
                            TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.ContactUs.SendToAPI.Failure"), TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SendToAPI: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        },
        //#endregion TriSysWeb.Pages.ContactUs

        //#region TriSysWeb.Pages.CandidatePlacements
        CandidatePlacements:
        {
            PopulateFieldLookup: function (sField, sAlias, sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select distinct " + sField + " as " + sAlias + " from v_Placement_AllFields where Placement_PlacementId in " +
                            " (Select EntityId From PlacementConfigFields Where Candidate = " + lContactId + ")";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, sAlias);
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            PopulateReferenceLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("Placement_Reference", "Reference", sDiv);
            },

            PopulateJobTitleLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("Placement_JobTitle", "JobTitle", sDiv);
            },

            PopulateTypeLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("Placement_PlacementEntityType", "EntityType", sDiv);
            },

            // We only want status' of placements where candidate has worked
            PopulateStatusLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("Placement_PlacementStatus", "Status", sDiv);
            },

            PopulateStartDateLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("convert(varchar(11), Placement_StartDate, 106)", "StartDate", sDiv);
            },

            PopulateEndDateLookup: function (sDiv)
            {
                TriSysWeb.Pages.CandidatePlacements.PopulateFieldLookup("convert(varchar(11), Placement_EndDate, 106)", "EndDate", sDiv);
            },

            // We only want locations of places where candidate has worked
            PopulateLocationLookup: function (sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select distinct Location from placementconfigfields where candidate = " + lContactId;

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Location");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            SearchPersistence:
            {
                CookiePrefix: "CandidatePlacementSearch-",

                PlacementSearchButton: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.CandidatePlacements.SearchPersistence.CookiePrefix;

                    try
                    {
                        var sReference = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbReference");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Reference", sReference);

                        var sJobTitle = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbJobTitle");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "JobTitle", sJobTitle);

                        var sType = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbType");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Type", sType);

                        var sLocation = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbLocation");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Location", sLocation);

                        var sStatus = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStatus");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Status", sStatus);

                        var sStartDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStartDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDate", sStartDate);

                        var sEndDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbEndDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "EndDate", sEndDate);
                    }
                    catch (err)
                    {
                        // control bugs?
                    }

                    // Show all matching placements
                    TriSysWeb.Pages.CandidatePlacements.PlacementSearch('divPlacementGrid');
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        // Re-instate all search parameters
                        var CookiePrefix = TriSysWeb.Pages.CandidatePlacements.SearchPersistence.CookiePrefix;

                        var sReference = TriSysAPI.Cookies.getCookie(CookiePrefix + "Reference");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbReference", sReference);

                        var sJobTitle = TriSysAPI.Cookies.getCookie(CookiePrefix + "JobTitle");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbJobTitle", sJobTitle);

                        var sType = TriSysAPI.Cookies.getCookie(CookiePrefix + "Type");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbType", sType);

                        var sLocation = TriSysAPI.Cookies.getCookie(CookiePrefix + "Location");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbLocation", sLocation);

                        var sStatus = TriSysAPI.Cookies.getCookie(CookiePrefix + "Status");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStatus", sStatus);

                        var sStartDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStartDate", sStartDate);

                        var sEndDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "EndDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbEndDate", sEndDate);

                        // Finally show all search results again
                        TriSysWeb.Pages.CandidatePlacements.SearchPersistence.PlacementSearchButton();
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            },

            // Called when the user clicks the search button
            PlacementSearch: function (sDiv)
            {
                // Are we connected to the database?
                if (!TriSysWeb.Security.IsDataConnectionOpen())
                {
                    alert('Not connected to API yet - programmer error');
                    return;
                }

                // Read search parameters
                var sWhereSQL = TriSysWeb.Pages.CandidatePlacements.SearchWhereClause();

                // Populate the grid
                TriSysWeb.Pages.CandidatePlacements.PopulateGrid(sDiv, "grdPlacements", "Placements", sWhereSQL);
            },

            SearchWhereClause: function ()
            {
                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;
                var sWhereSQL = ' and pcf.Candidate = ' + lContactId;

                var sReferenceSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbReference", "Placement_Reference");
                if (sReferenceSQL)
                    sWhereSQL += " and " + sReferenceSQL;

                var sJobTitleSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbJobTitle", "Placement_JobTitle");
                if (sJobTitleSQL)
                    sWhereSQL += " and " + sJobTitleSQL;

                var sTypeSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbType", "Placement_PlacementEntityType");
                if (sTypeSQL)
                    sWhereSQL += " and " + sTypeSQL;

                var sLocationSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbLocation", "Location");
                if (sLocationSQL)
                    sWhereSQL += " and " + sLocationSQL;

                var sStatusSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStatus", "Placement_PlacementStatus");
                if (sStatusSQL)
                    sWhereSQL += " and " + sStatusSQL;

                var sStartDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStartDate", "convert(varchar(11), Placement_StartDate, 106)");
                if (sStartDateSQL)
                    sWhereSQL += " and " + sStartDateSQL;

                var sEndDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbEndDate", "convert(varchar(11), Placement_EndDate, 106)");
                if (sEndDateSQL)
                    sWhereSQL += " and " + sEndDateSQL;

                return sWhereSQL;
            },

            PopulateGrid: function (sDiv, sGridName, sTitle, sWhereSQL)
            {
                // Modify the SQL statement
                var sSQL = TriSysWeb.Pages.CandidatePlacements.SQL + sWhereSQL;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridName,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.CandidatePlacements.OrderBy,
                    Columns: TriSysWeb.Pages.CandidatePlacements.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.CandidatePlacements.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.CandidatePlacements.MobileRowTemplate,

                    KeyColumnName: "PlacementId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load placement Form with specified ID
                        var lPlacementId = rowData.PlacementId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.PlacementPage + "?PlacementId=" + lPlacementId);
                    },
                    MultiRowSelect: false,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Placement_PlacementId as PlacementId, Placement_Reference as Reference, Placement_PlacementEntityType as Type, " +
                "  PlacementCompany_Name as Company, Placement_JobTitle as JobTitle, " +
                "  Placement_StartDate as StartDate, Placement_EndDate as EndDate," +
                "  Location, dbo.f_GetPlacementSiteAddressCity(Placement_PlacementId) as City," +
                "  Placement_PlacementStatus as Status" +
                " From v_Placement_AllFields, PlacementConfigFields pcf" +
                " Where v_Placement_AllFields.Placement_PlacementId = pcf.EntityId",
            OrderBy: ' Order by Placement_Reference',
            Columns: [
                        { field: "PlacementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Reference", type: "string" },
                        { field: "Type", title: "Type", type: "string" },
                        { field: "JobTitle", title: "Job Title", type: "string" },
                        { field: "Company", title: "Company", type: "string" },
                        { field: "StartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        //{ field: "EndDate", title: "End Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Location", title: "Location", type: "string" },
                        { field: "Status", title: "Status", type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Job Details" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference # (#: Type #: #: Status #)</strong><br />' +
                                    '<i>#: JobTitle #</i><br />#: Company #<br />City: #: City #<br >' +
                                    'Start Date: #: kendo.format("{0:dd MMM yyyy}", StartDate) #<hr></td>'

        },
        //#endregion TriSysWeb.Pages.CandidatePlacements

        //#region TriSysWeb.Pages.ClientPlacements
        ClientPlacements:
        {
            PopulateFieldLookup: function (sField, sAlias, sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select distinct " + sField + " as " + sAlias + " from v_Placement_AllFields where Placement_PlacementId in " +
                            " (Select PlacementId From PlacementContact Where ContactId = " + lContactId + ")";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, sAlias);
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            PopulateReferenceLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("Placement_Reference", "Reference", sDiv);
            },

            PopulateJobTitleLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("Placement_JobTitle", "JobTitle", sDiv);
            },

            PopulateTypeLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("Placement_PlacementEntityType", "EntityType", sDiv);
            },

            PopulateCandidateLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("CandidateContact_FullName", "Candidate", sDiv);
            },

            // We only want status' of placements where candidate has worked
            PopulateStatusLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("Placement_PlacementStatus", "Status", sDiv);
            },

            PopulateStartDateLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("convert(varchar(11), Placement_StartDate, 106)", "StartDate", sDiv);
            },

            PopulateEndDateLookup: function (sDiv)
            {
                TriSysWeb.Pages.ClientPlacements.PopulateFieldLookup("convert(varchar(11), Placement_EndDate, 106)", "EndDate", sDiv);
            },

            // We only want locations of places where candidate has worked
            PopulateLocationLookup: function (sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select distinct Location from placementconfigfields where entityid in (select placementid from placementcontact where contactid = " + lContactId + ")";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Location");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            SearchPersistence:
            {
                CookiePrefix: "ClientPlacementSearch-",

                PlacementSearchButton: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.ClientPlacements.SearchPersistence.CookiePrefix;

                    try
                    {
                        var sReference = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbReference");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Reference", sReference);

                        var sJobTitle = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbJobTitle");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "JobTitle", sJobTitle);

                        var sType = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbType");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Type", sType);

                        var sCandidate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbCandidate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Candidate", sCandidate);

                        var sLocation = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbLocation");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Location", sLocation);

                        var sStatus = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStatus");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Status", sStatus);

                        var sStartDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStartDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDate", sStartDate);

                        var sEndDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbEndDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "EndDate", sEndDate);
                    }
                    catch (err)
                    {
                        // control bugs?
                    }

                    // Show all matching placements
                    TriSysWeb.Pages.ClientPlacements.PlacementSearch('divPlacementGrid');
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        // Re-instate all search parameters
                        var CookiePrefix = TriSysWeb.Pages.ClientPlacements.SearchPersistence.CookiePrefix;

                        var sReference = TriSysAPI.Cookies.getCookie(CookiePrefix + "Reference");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbReference", sReference);

                        var sJobTitle = TriSysAPI.Cookies.getCookie(CookiePrefix + "JobTitle");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbJobTitle", sJobTitle);

                        var sType = TriSysAPI.Cookies.getCookie(CookiePrefix + "Type");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbType", sType);

                        var sCandidate = TriSysAPI.Cookies.getCookie(CookiePrefix + "Candidate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbCandidate", sCandidate);

                        var sLocation = TriSysAPI.Cookies.getCookie(CookiePrefix + "Location");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbLocation", sLocation);

                        var sStatus = TriSysAPI.Cookies.getCookie(CookiePrefix + "Status");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStatus", sStatus);

                        var sStartDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStartDate", sStartDate);

                        var sEndDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "EndDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbEndDate", sEndDate);

                        // Finally show all search results again
                        TriSysWeb.Pages.ClientPlacements.SearchPersistence.PlacementSearchButton();
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            },

            // Called when the user clicks the search button
            PlacementSearch: function (sDiv)
            {
                // Are we connected to the database?
                if (!TriSysWeb.Security.IsDataConnectionOpen())
                {
                    alert('Not connected to API yet - programmer error');
                    return;
                }

                // Read search parameters
                var sWhereSQL = TriSysWeb.Pages.ClientPlacements.SearchWhereClause();

                // Populate the grid
                TriSysWeb.Pages.ClientPlacements.PopulateGrid(sDiv, "grdPlacements", "Placements", sWhereSQL);
            },

            SearchWhereClause: function ()
            {
                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;
                var sWhereSQL = ' and pc.ContactId = ' + lContactId;

                var sReferenceSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbReference", "Placement_Reference");
                if (sReferenceSQL)
                    sWhereSQL += " and " + sReferenceSQL;

                var sJobTitleSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbJobTitle", "Placement_JobTitle");
                if (sJobTitleSQL)
                    sWhereSQL += " and " + sJobTitleSQL;

                var sTypeSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbType", "Placement_PlacementEntityType");
                if (sTypeSQL)
                    sWhereSQL += " and " + sTypeSQL;

                var sCandidateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbCandidate", "CandidateContact_FullName");
                if (sCandidateSQL)
                    sWhereSQL += " and " + sCandidateSQL;

                var sLocationSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbLocation", "Location");
                if (sLocationSQL)
                    sWhereSQL += " and " + sLocationSQL;

                var sStatusSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStatus", "Placement_PlacementStatus");
                if (sStatusSQL)
                    sWhereSQL += " and " + sStatusSQL;

                var sStartDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStartDate", "convert(varchar(11), Placement_StartDate, 106)");
                if (sStartDateSQL)
                    sWhereSQL += " and " + sStartDateSQL;

                var sEndDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbEndDate", "convert(varchar(11), Placement_EndDate, 106)");
                if (sEndDateSQL)
                    sWhereSQL += " and " + sEndDateSQL;

                return sWhereSQL;
            },

            PopulateGrid: function (sDiv, sGridName, sTitle, sWhereSQL)
            {
                // Modify the SQL statement
                var sSQL = TriSysWeb.Pages.ClientPlacements.SQL + sWhereSQL;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridName,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.ClientPlacements.OrderBy,
                    Columns: TriSysWeb.Pages.ClientPlacements.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.ClientPlacements.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.ClientPlacements.MobileRowTemplate,

                    KeyColumnName: "PlacementId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load placement Form with specified ID
                        var lPlacementId = rowData.PlacementId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.PlacementPage + "?PlacementId=" + lPlacementId);
                    },
                    MultiRowSelect: false,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Placement_PlacementId as PlacementId, Placement_Reference as Reference, Placement_PlacementEntityType as Type, " +
                "  Placement_JobTitle as JobTitle, CandidateContact_FullName as Candidate, " +
                "  Placement_StartDate as StartDate, Placement_EndDate as EndDate," +
                "  Location, dbo.f_GetPlacementSiteAddressCity(Placement_PlacementId) as City, " +
                "  Placement_PlacementStatus as Status" +
                " From v_Placement_AllFields, PlacementConfigFields pcf, PlacementContact pc" +
                " Where v_Placement_AllFields.Placement_PlacementId = pcf.EntityId" +
                "   And pcf.EntityId = pc.PlacementId",
            OrderBy: ' Order by Placement_Reference',
            Columns: [
                        { field: "PlacementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Reference", type: "string" },
                        { field: "Type", title: "Type", type: "string" },
                        { field: "JobTitle", title: "Job Title", type: "string" },
                        { field: "Candidate", title: "Candidate", type: "string" },
                        { field: "StartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "EndDate", title: "End Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Location", title: "Location", type: "string" },
                        { field: "Status", title: "Status", type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Job Details" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference # (#: Type #: #: Status #)</strong><br />' +
                                    '<i>#: JobTitle #</i><br />' +
                                    'Candidate: #: Candidate #<br />City: #: City #<br >' +
                                    'Start Date: #: kendo.format("{0:dd MMM yyyy}", StartDate) #<hr></td>'

        },
        //#endregion TriSysWeb.Pages.ClientPlacements

        //#region TriSysWeb.Pages.Placement
        Placement:
        {
            Load: function ()
            {
                // If not logged-in, jump out
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                {
                    TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.Placement.Load.NotLoggedIn"),
                        TriSysWeb.Constants.ApplicationName, TriSysWeb.Security.ShowLoginPage());
                    return;
                }

                var sURL = window.location.href;
                var lPlacementId = TriSysApex.URL.Parser(sURL).getParam("PlacementId");
                if (!$.isNumeric(lPlacementId))
                {
                    TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.Placement.Load.NoRequirementId"), TriSysWeb.Constants.ApplicationName, TriSysWeb.Pages.Home.Redirect);
                    return;
                }

                if (TriSysWeb.Security.IsCandidateLoggedIn())
                    $('#containerPlacementActions').show();

                // Prepare asynchronous retrieval of placement details entirely subject to security and hard work by the API
                TriSysWeb.Pages.Placement.LoadRecord(lPlacementId);
            },

            // Use API to load the record asynchronously
            LoadRecord: function (lPlacementId, bClientContact, bCandidateContact)
            {
                TriSysApex.UI.ShowWait(null, "Retrieving Placement...");

                var dataPacket = {
                    PlacementId: lPlacementId
                };

                var payloadObject = {};
                payloadObject.URL = "Placement/Read";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CPlacementResponse = data;
                    if (CPlacementResponse)
                    {
                        if (CPlacementResponse.Success)
                            TriSysWeb.Pages.Placement.PopulateFields(CPlacementResponse, true);
                        else
                            TriSysApex.UI.errorAlert(CPlacementResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.Placement.LoadRecord: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // The callback after data has been received from the web API
            PopulateFields: function (CPlacementResponse, bShowTimesheetGrid)
            {
                //PlacementId As Long
                //Reference As String
                //JobTitle As String
                //CandidateName As String
                //CandidateContactId As Long
                //ClientName As String
                //ClientContactId As Long
                //CompanyName As String
                //CompanyId As Long
                //Type As String
                //Status As String
                //Location As String
                //StartDate As Date
                //EndDate As Date
                //UserId As Long
                //UserName As String

                //ChargeRate As String      
                //PayRate As String         
                //Salary As String          
                //BonusValue As String
                //BenefitsValue As String
                //Benefits As String
                $("#Reference").val(CPlacementResponse.Reference);
                $("#PlacementId").html(CPlacementResponse.PlacementId);
                $("#JobTitle").val(CPlacementResponse.JobTitle);
                $("#Type").val(CPlacementResponse.Type);
                $("#CandidateName").val(CPlacementResponse.CandidateName);
                $("#ClientName").val(CPlacementResponse.ClientName);
                $("#CompanyName").val(CPlacementResponse.CompanyName);
                $("#Status").val(CPlacementResponse.Status);
                $("#Location").val(CPlacementResponse.Location);
                $("#StartDate").val(moment(CPlacementResponse.StartDate).format('dddd DD MMMM YYYY'));
                $("#EndDate").val(moment(CPlacementResponse.EndDate).format('dddd DD MMMM YYYY'));
                $("#UserName").val(CPlacementResponse.UserName);

                // Display only what is necessary
                var bClientContact = TriSysWeb.Security.IsClientLoggedIn();
                var bPermanent = (CPlacementResponse.Type == 'Permanent');
                if (bClientContact)
                {
                    if (!bPermanent)
                    {
                        $("#ChargeRate").val(CPlacementResponse.ChargeRate);
                        $('#divChargeRates').show();
                    }
                }
                else
                {
                    if (!bPermanent)
                    {
                        $("#PayRate").val(CPlacementResponse.PayRate);
                        $("#divPayRates").show();
                    }

                    var sPoundSign = "\u00A3";  // � - Pound Sign
                    var sNillPounds = sPoundSign + "0.00";

                    if (CPlacementResponse.BonusValue)
                    {
                        if (CPlacementResponse.BonusValue != sNillPounds)
                        {
                            $("#BonusValue").val(CPlacementResponse.BonusValue);
                            $("#divBonusValue").show();
                        }
                    }

                    if (CPlacementResponse.BenefitsValue)
                    {
                        if (CPlacementResponse.BenefitsValue != sNillPounds)
                        {
                            $("#BenefitsValue").val(CPlacementResponse.BenefitsValue);
                            $("#divBenefitsValue").show();
                        }
                    }


                    if (CPlacementResponse.Benefits)
                    {
                        $("#Benefits").val(CPlacementResponse.Benefits);
                        $("#divBenefits").show();
                    }
                }

                if (bPermanent)
                {
                    $("#Salary").val(CPlacementResponse.Salary);
                    $("#divSalary").show();
                    $("#divEndDate").hide();
                    $("#divTimesheets").hide();
                }
                else if (bShowTimesheetGrid)
                {
                    // Show timesheets in grid
                    TriSysWeb.Pages.Placement.PopulateTimesheetGrid(CPlacementResponse.PlacementId);
                }
            },

            PopulateTimesheetGrid: function (lPlacementId)
            {
                var sWhereSQL = " And tp.PlacementId = " + lPlacementId;

                // Modify the SQL statement
                var sSQL = TriSysWeb.Pages.Placement.SQL + sWhereSQL;

                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: "divTimesheetGrid",
                    ID: "grdTimesheets",
                    Title: "Placement Timesheets",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.Placement.OrderBy,
                    Columns: TriSysWeb.Pages.Placement.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.Placement.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.Placement.MobileRowTemplate,

                    KeyColumnName: "TimesheetId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load timesheet Form with specified ID
                        var lTimesheetId = rowData.TimesheetId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.TimesheetPage + "?TimesheetId=" + lTimesheetId);
                    },
                    MultiRowSelect: false,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Timesheet_TimesheetId as TimesheetId, [Description] as Period, InputDate, TotalTimeWorked, " +
	                "  case PaymentAuthorised when 1 then 'Yes' else 'No' end as Authorised, " +
                    "  AuthorisedDate as DateAuthorised, AuthorisedBy, case Payrolled when 1 then 'Yes' else 'No' end as Paid, NetPay, NetCharge" +
                    " From v_Timesheet_GridDisplay v, TimesheetPlacement tp" +
                    " Where v.Timesheet_TimesheetId = tp.TimesheetId",
            OrderBy: " Order by 1 desc",
            Columns: [
                        { field: "TimesheetId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Period", title: "Period", type: "string" },
                        { field: "InputDate", title: "Input Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "TotalTimeWorked", title: "Total Time Worked", type: "number", format: "{0:n}" },
                        { field: "Authorised", title: "Authorised", type: "string" },
                        //{ field: "DateAuthorised", title: "Date Authorised", type: "date", format: "{0:dd MMM yyyy}" },
                        //{ field: "AuthorisedBy", title: "Authorised By", type: "string" },
                        { field: "Paid", title: "Paid", type: "string" } //,
                        //{ field: "NetPay", title: "Net Pay", type: "number" },
                        //{ field: "NetCharge", title: "Net Charge", type: "number" }
            ],
            MobileVisibleColumns: [
                { field: "Period", title: "Details (Period, Hours)" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Period #</strong><br />Hours: #: kendo.format("{0:n}", TotalTimeWorked) #<br />' +
                                'Authorised: <i>#: Authorised #</i><br />Paid: <i>#: Paid #</i><hr></td>',

            NewTimesheet: function ()
            {
                if (!TriSysWeb.Security.IsCandidateLoggedIn())
                    return;

                var lPlacementId = parseInt($("#PlacementId").html(), 10);
                TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.TimesheetPage + "?PlacementId=" + lPlacementId);
            }
        },
        //#endregion TriSysWeb.Pages.Placement

        //#region TriSysWeb.Pages.CandidateTimesheets
        CandidateTimesheets:
        {
            PopulatePlacementLookup: function (sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();

                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select Placement_Reference + ': ' + Placement_PlacementEntityType + ' ' + Placement_JobTitle + ' at ' + " +
                        " PlacementCompany_Name + '. Started: ' + " +
                        " convert(varchar(2),datepart(day, Placement_StartDate)) + ' ' + datename(mm, Placement_StartDate) + ' ' + convert(varchar(4), datepart(year, Placement_StartDate)) " +
                        "  + '. Status: ' + Placement_PlacementStatus" +
                        "  as Placement" +
                        " From v_Placement_AllFields" +
                        " Where Placement_PlacementId in (Select EntityId From PlacementConfigFields Where Candidate = " + lContactId + ")";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Placement");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            // Called when the user clicks the search button
            Search: function (sDiv)
            {
                // Are we connected to the database?
                if (!TriSysWeb.Security.IsDataConnectionOpen())
                {
                    alert('Not connected to API yet - programmer error');
                    return;
                }

                // Preserve criteria for next time
                TriSysWeb.Pages.CandidateTimesheets.SearchPersistence.StoreCriteria();

                // Read search parameters
                var sSQL = TriSysWeb.Pages.CandidateTimesheets.WhereSQL();

                // Populate the grid
                TriSysWeb.Pages.CandidateTimesheets.PopulateGrid(sDiv, "grdTimesheets", "Timesheets", sSQL);
            },

            WhereSQL: function ()
            {
                // Read selected references from selected placements and pump these into the grid to show all timesheets
                var sWhereSQL = '';
                var arrayOfPlacements = TriSysSDK.CShowForm.GetSelectedSkillsFromListAsArray("cmbPlacement");
                if (arrayOfPlacements)
                {
                    for (var i = 0; i < arrayOfPlacements.length; i++)
                    {
                        var sPlacement = arrayOfPlacements[i];
                        var sReference = sPlacement.substring(0, sPlacement.indexOf(":"));
                        if (sWhereSQL.length > 0)
                            sWhereSQL += ',';
                        sWhereSQL += "'" + sReference + "'";
                    }

                    if (sWhereSQL.length > 0)
                        sWhereSQL = " and Reference in (" + sWhereSQL + ") ";
                }

                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = TriSysWeb.Pages.CandidateTimesheets.SQL +
                    " and tp.PlacementId in (Select EntityId From PlacementConfigFields Where Candidate = " + lContactId + sWhereSQL + ")";

                return sSQL;
            },

            // Same grid as on the placement form!
            PopulateGrid: function (sDiv, sGridID, sTitle, sSQL)
            {
                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridID,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.CandidateTimesheets.OrderBy,
                    Columns: TriSysWeb.Pages.CandidateTimesheets.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.CandidateTimesheets.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.CandidateTimesheets.MobileRowTemplate,

                    KeyColumnName: "TimesheetId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load timesheet Form with specified ID
                        var lTimesheetId = rowData.TimesheetId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.TimesheetPage + "?TimesheetId=" + lTimesheetId);
                    },
                    MultiRowSelect: false,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Timesheet_TimesheetId as TimesheetId, PlacementReference as Reference, Company, " +
                "  [Description] as Period, InputDate, TotalTimeWorked, " +
                "  case PaymentAuthorised when 1 then 'Yes' else 'No' end as Authorised, " +
                "  AuthorisedDate as DateAuthorised, AuthorisedBy, case Payrolled when 1 then 'Yes' else 'No' end as Paid, NetPay, NetCharge" +
                " From v_Timesheet_GridDisplay v, TimesheetPlacement tp" +
                " Where v.Timesheet_TimesheetId = tp.TimesheetId",
            OrderBy: " Order by 1 desc",
            Columns: [
                        { field: "TimesheetId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Placement", type: "string" },
                        { field: "Company", title: "Company", type: "string" },
                        { field: "Period", title: "Period", type: "string" },
                        { field: "InputDate", title: "Input Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "TotalTimeWorked", title: "Total Time Worked", type: "number", format: "{0:n}" },
                        { field: "Authorised", title: "Authorised", type: "string" },
                        //{ field: "DateAuthorised", title: "Date Authorised", type: "date", format: "{0:dd MMM yyyy}" },
                        //{ field: "AuthorisedBy", title: "Authorised By", type: "string" },
                        { field: "Paid", title: "Paid", type: "string" } //,
                        //{ field: "NetPay", title: "Net Pay", type: "number" },
                        //{ field: "NetCharge", title: "Net Charge", type: "number" }
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Details (Placement, Period, Hours)" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference #</strong><br />#: Company #</br />#: Period #<br />Hours: #: kendo.format("{0:n}", TotalTimeWorked) #<br />' +
                        'Authorised: <i>#: Authorised #</i><br />Paid: <i>#: Paid #</i><hr></td>',

            SearchPersistence:
            {
                CookiePrefix: "CandidateTimesheetSearch-",

                StoreCriteria: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.CandidateTimesheets.SearchPersistence.CookiePrefix;

                    try
                    {
                        var arrayOfPlacements = TriSysSDK.CShowForm.GetSelectedSkillsFromListAsArray("cmbPlacement");
                        TriSysAPI.Persistence.Write(CookiePrefix + "Placement", arrayOfPlacements);
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        var CookiePrefix = TriSysWeb.Pages.CandidateTimesheets.SearchPersistence.CookiePrefix;
                        var arrayOfPlacements = TriSysAPI.Persistence.Read(CookiePrefix + "Placement");

                        if (arrayOfPlacements)
                        {
                            TriSysSDK.CShowForm.SetValuesArrayInList("cmbPlacement", arrayOfPlacements);

                            TriSysWeb.Pages.CandidateTimesheets.Search('divTimesheetGrid');
                        }
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            }
        },
        //#endregion TriSysWeb.Pages.CandidateTimesheets


        //#region TriSysWeb.Pages.ClientTimesheets
        ClientTimesheets:
        {
            // Not used by form, but could be for small databases
            PopulatePlacementLookup: function (sDiv)
            {
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                    return;

                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                var sSQL = "Select Placement_Reference + ': ' + Placement_PlacementEntityType + ' ' + CandidateContact_FullName + ', ' + Placement_JobTitle + '. Started: ' + " +
                        " convert(varchar(2),datepart(day, Placement_StartDate)) + ' ' + datename(mm, Placement_StartDate) + ' ' + convert(varchar(4), datepart(year, Placement_StartDate)) " +
                        "  + '. Status: ' + Placement_PlacementStatus" +
                        "  as Placement" +
                        " From v_Placement_AllFields" +
                        " Where Placement_PlacementId in (Select PlacementId From PlacementContact Where ContactId = " + lContactId + ")";

                var fnPopulateCombo = function (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults)
                    {
                        if (CSQLDatabaseSearchResults.DataTable)
                        {
                            var sourceData = TriSysSDK.Database.DataTableToDataSource(CSQLDatabaseSearchResults.DataTable, "Placement");
                            if (sourceData)
                                TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sDiv, sourceData);
                        }
                    }
                };

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateCombo);
            },

            // Called when the user clicks the search button
            Search: function (sDiv)
            {
                // Are we connected to the database?
                if (!TriSysWeb.Security.IsDataConnectionOpen())
                {
                    alert('Not connected to API yet - programmer error');
                    return;
                }

                // Preserve criteria for next time
                TriSysWeb.Pages.ClientTimesheets.SearchPersistence.StoreCriteria();

                // Read search parameters
                var sSQL = TriSysWeb.Pages.ClientTimesheets.WhereSQL();

                // Populate the grid
                TriSysWeb.Pages.ClientTimesheets.PopulateGrid(sDiv, "grdTimesheets", "Timesheets", sSQL);
            },

            WhereSQL: function ()
            {
                //#region Commented Out Placement Combo
                // Read selected references from selected placements and pump these into the grid to show all timesheets
                //var sWhereSQL = '';
                //var arrayOfPlacements = TriSysSDK.CShowForm.GetSelectedSkillsFromListAsArray("cmbPlacement");
                //if (arrayOfPlacements)
                //{
                //    for (var i = 0; i < arrayOfPlacements.length; i++)
                //    {
                //        var sPlacement = arrayOfPlacements[i];
                //        var sReference = sPlacement.substring(0, sPlacement.indexOf(":"));
                //        if (sWhereSQL.length > 0)
                //            sWhereSQL += ',';
                //        sWhereSQL += "'" + sReference + "'";
                //    }

                //    if (sWhereSQL.length > 0)
                //        sWhereSQL = " Where Reference in (" + sWhereSQL + ") ";
                //}

                //var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                //var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;

                //var sSQL = TriSysWeb.Pages.ClientTimesheets.SQL +
                //    " and tp.PlacementId in (Select EntityId From PlacementConfigFields " + sWhereSQL + ")" +
                //    " and tp.PlacementId in (Select PlacementId From PlacementContact Where ContactId = " + lContactId + ")";
                //#endregion Commented Out Placement Combo

                // Who is the logged in client/candidate?
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                var lContactId = dataConnectionKey.LoggedInAgencyContact.ContactId;
                var sWhereSQL = ' and pc.ContactId = ' + lContactId;

                var sReferenceSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbReference", "Placement_Reference");
                if (sReferenceSQL)
                    sWhereSQL += " and " + sReferenceSQL;

                var sJobTitleSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbJobTitle", "Placement_JobTitle");
                if (sJobTitleSQL)
                    sWhereSQL += " and " + sJobTitleSQL;

                var sTypeSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbType", "Placement_PlacementEntityType");
                if (sTypeSQL)
                    sWhereSQL += " and " + sTypeSQL;

                var sCandidateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbCandidate", "CandidateContact_FullName");
                if (sCandidateSQL)
                    sWhereSQL += " and " + sCandidateSQL;

                var sLocationSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbLocation", "Location");
                if (sLocationSQL)
                    sWhereSQL += " and " + sLocationSQL;

                var sStatusSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStatus", "Placement_PlacementStatus");
                if (sStatusSQL)
                    sWhereSQL += " and " + sStatusSQL;

                var sStartDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbStartDate", "convert(varchar(11), Placement_StartDate, 106)");
                if (sStartDateSQL)
                    sWhereSQL += " and " + sStartDateSQL;

                var sEndDateSQL = TriSysSDK.Database.SelectedMultiSelectComboSQLClause("cmbEndDate", "convert(varchar(11), Placement_EndDate, 106)");
                if (sEndDateSQL)
                    sWhereSQL += " and " + sEndDateSQL;

                var sSQL = TriSysWeb.Pages.ClientTimesheets.SQL + sWhereSQL;

                return sSQL;
            },

            // Same grid as on the placement form!
            PopulateGrid: function (sDiv, sGridID, sTitle, sSQL)
            {
                // Populate the grid now
                TriSysSDK.Grid.VirtualMode({
                    Div: sDiv,
                    ID: sGridID,
                    Title: sTitle,
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: sSQL,
                    OrderBy: TriSysWeb.Pages.ClientTimesheets.OrderBy,
                    Columns: TriSysWeb.Pages.ClientTimesheets.Columns,
                    MobileVisibleColumns: TriSysWeb.Pages.ClientTimesheets.MobileVisibleColumns,
                    MobileRowTemplate: TriSysWeb.Pages.ClientTimesheets.MobileRowTemplate,

                    KeyColumnName: "TimesheetId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load timesheet Form with specified ID
                        var lTimesheetId = rowData.TimesheetId;
                        TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.TimesheetPage + "?TimesheetId=" + lTimesheetId);
                    },
                    MultiRowSelect: false,
                    ColumnFilters: false,
                    Grouping: false
                });
            },

            SQL: "Select Timesheet_TimesheetId as TimesheetId, PlacementReference as Reference, Candidate, JobTitle, " +
                "  [Description] as Period, InputDate, TotalTimeWorked, " +
                "  case PaymentAuthorised when 1 then 'Yes' else 'No' end as Authorised, " +
                "  AuthorisedDate as DateAuthorised, AuthorisedBy, NetCharge" +
                " From v_Timesheet_GridDisplay v, TimesheetPlacement tp, PlacementContact pc, v_Placement_AllFields vp" +
                " Where v.Timesheet_TimesheetId = tp.TimesheetId" +
                "   And tp.PlacementId = pc.PlacementId" +
                "   And tp.PlacementId = vp.Placement_PlacementId",
            OrderBy: " Order by 1 desc",
            Columns: [
                        { field: "TimesheetId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Reference", title: "Placement", type: "string" },
                        { field: "Candidate", title: "Candidate", type: "string" },
                        { field: "JobTitle", title: "Job Title", type: "string" },
                        { field: "Period", title: "Period", type: "string" },
                        { field: "InputDate", title: "Input Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "TotalTimeWorked", title: "Total Time Worked", type: "number", format: "{0:n}" },
                        { field: "Authorised", title: "Authorised", type: "string" }
                        //{ field: "DateAuthorised", title: "Date Authorised", type: "date", format: "{0:dd MMM yyyy}" },
                        //{ field: "AuthorisedBy", title: "Authorised By", type: "string" },
            ],
            MobileVisibleColumns: [
                { field: "Reference", title: "Details (Placement, Period, Hours)" }
            ],
            MobileRowTemplate: '<td colspan="1"><strong>#: Reference #</strong><br />#: Candidate #</br />#: JobTitle #<br />' +
                        '#: Period #<br />Hours: #: kendo.format("{0:n}", TotalTimeWorked) #<br />' +
                        'Authorised: <i>#: Authorised #</i><hr></td>',

            SearchPersistence:
            {
                CookiePrefix: "ClientTimesheetSearch-",

                StoreCriteria: function ()
                {
                    // Capture all search parameters
                    var CookiePrefix = TriSysWeb.Pages.ClientTimesheets.SearchPersistence.CookiePrefix;

                    try
                    {
                        var sReference = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbReference");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Reference", sReference);

                        var sJobTitle = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbJobTitle");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "JobTitle", sJobTitle);

                        var sType = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbType");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Type", sType);

                        var sCandidate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbCandidate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Candidate", sCandidate);

                        var sLocation = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbLocation");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Location", sLocation);

                        var sStatus = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStatus");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "Status", sStatus);

                        var sStartDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbStartDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "StartDate", sStartDate);

                        var sEndDate = TriSysSDK.CShowForm.GetSelectedSkillsFromList("cmbEndDate");
                        TriSysAPI.Cookies.setCookie(CookiePrefix + "EndDate", sEndDate);
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                },

                ReloadLastSearchCriteriaAndReSearch: function ()
                {
                    try
                    {
                        var CookiePrefix = TriSysWeb.Pages.ClientTimesheets.SearchPersistence.CookiePrefix;

                        var sReference = TriSysAPI.Cookies.getCookie(CookiePrefix + "Reference");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbReference", sReference);

                        var sJobTitle = TriSysAPI.Cookies.getCookie(CookiePrefix + "JobTitle");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbJobTitle", sJobTitle);

                        var sType = TriSysAPI.Cookies.getCookie(CookiePrefix + "Type");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbType", sType);

                        var sCandidate = TriSysAPI.Cookies.getCookie(CookiePrefix + "Candidate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbCandidate", sCandidate);

                        var sLocation = TriSysAPI.Cookies.getCookie(CookiePrefix + "Location");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbLocation", sLocation);

                        var sStatus = TriSysAPI.Cookies.getCookie(CookiePrefix + "Status");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStatus", sStatus);

                        var sStartDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "StartDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbStartDate", sStartDate);

                        var sEndDate = TriSysAPI.Cookies.getCookie(CookiePrefix + "EndDate");
                        TriSysSDK.CShowForm.SetSkillsInList("cmbEndDate", sEndDate);

                        TriSysWeb.Pages.ClientTimesheets.Search('divTimesheetGrid');
                    }
                    catch (err)
                    {
                        // control bugs?
                    }
                }
            }
        },
        //#endregion TriSysWeb.Pages.ClientTimesheets

        //#region TriSysWeb.Pages.Timesheet
        Timesheet:
        {
            Load: function ()
            {
                // If not logged-in, jump out
                if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
                {
                    TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.Load.NotLoggedIn"),
                            TriSysWeb.Constants.ApplicationName, TriSysWeb.Security.ShowLoginPage());
                    return;
                }

                var sURL = window.location.href;
                var lTimesheetId = TriSysApex.URL.Parser(sURL).getParam("TimesheetId");
                var lPlacementId = TriSysApex.URL.Parser(sURL).getParam("PlacementId");
                if (!$.isNumeric(lTimesheetId) && !$.isNumeric(lPlacementId))
                {
                    TriSysApex.UI.ShowMessage(TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.Load.NoTimesheetId"), TriSysWeb.Constants.ApplicationName, TriSysWeb.Pages.Home.Redirect);
                    return;
                }

                if (!$.isNumeric(lTimesheetId))
                    lTimesheetId = 0;

                // Prepare asynchronous retrieval of timesheet details entirely subject to security and hard work by the API
                TriSysWeb.Pages.Timesheet.LoadRecord(lPlacementId, lTimesheetId);
            },

            // Use API to load the record asynchronously
            LoadRecord: function (lPlacementId, lTimesheetId)
            {
                TriSysApex.UI.ShowWait(null, "Retrieving Timesheet...");

                var dataPacket = {
                    TimesheetId: lTimesheetId,
                    PlacementId: lPlacementId
                };

                var payloadObject = {};
                payloadObject.URL = "Timesheet/Read";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CTimesheetResponse = data;
                    if (CTimesheetResponse)
                    {
                        if (CTimesheetResponse.Success)
                        {
                            var bClientContact = TriSysWeb.Security.IsClientLoggedIn();

                            // Populate the placement fields
                            TriSysWeb.Pages.Timesheet.PopulatePlacementFields(CTimesheetResponse.Placement, lTimesheetId);

                            // Populate the timesheet header
                            TriSysWeb.Pages.Timesheet.PopulateFields(CTimesheetResponse);

                            // Populate the timesheet amounts table
                            TriSysWeb.Pages.Timesheet.PopulateAmountsTable(CTimesheetResponse.Rates, bClientContact);

                            // Populate the timesheet summary table
                            TriSysWeb.Pages.Timesheet.PopulateSummaryTable(CTimesheetResponse, bClientContact);

                            // Populate the timesheet period intervals and hours
                            var bCanEdit = CTimesheetResponse.CanEdit;
                            TriSysWeb.Pages.Timesheet.PopulateGrid(CTimesheetResponse, 'divTimesheetHoursGrid', bCanEdit);

                            // Set visibility of form buttons
                            TriSysWeb.Pages.Timesheet.FormButtonVisibility(CTimesheetResponse);
                        }
                        else
                            TriSysApex.UI.errorAlert(CTimesheetResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.Timesheet.LoadRecord: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // The callback after data has been received from the web API
            PopulatePlacementFields: function (CPlacementResponse, lTimesheetId)
            {
                //PlacementId As Long
                //Reference As String
                //JobTitle As String
                //CandidateName As String
                //CandidateContactId As Long
                //ClientName As String
                //ClientContactId As Long
                //CompanyName As String
                //CompanyId As Long
                //Type As String
                //Status As String
                //Location As String
                //StartDate As Date
                //EndDate As Date
                //UserId As Long
                //UserName As String

                //ChargeRate As String      
                //PayRate As String         
                //Salary As String          
                //BonusValue As String
                //BenefitsValue As String
                //Benefits As String

                // PeriodDescriptionsAll As List(Of String)
                // PeriodDescriptionsUnassignedToTimesheets As List(Of String)


                $("#PlacementId").html(TriSysSDK.Numbers.Pad(CPlacementResponse.PlacementId, 6));
                $("#Reference").html('<a href="' + TriSysWeb.Constants.PlacementPage + '?PlacementId=' + CPlacementResponse.PlacementId + '">' + CPlacementResponse.Reference + '</a>');

                $("#JobTitle").html(CPlacementResponse.JobTitle);
                $("#Type").html(CPlacementResponse.Type);
                $("#CandidateName").html(CPlacementResponse.CandidateName);
                $("#ClientName").html(CPlacementResponse.ClientName);
                $("#CompanyName").html(CPlacementResponse.CompanyName);
                $("#Status").html(CPlacementResponse.Status);
                $("#StartDate").html(moment(CPlacementResponse.StartDate).format('dddd DD MMMM YYYY'));
                $("#EndDate").html(moment(CPlacementResponse.EndDate).format('dddd DD MMMM YYYY'));
                $("#UserName").html(CPlacementResponse.UserName);

                var sSiteAddress = CPlacementResponse.Location;
                if (sSiteAddress)
                {
                    sSiteAddress = sSiteAddress.replace(/\r\n/g, '<br />\r\n');     // VbCrLf replacement with BR
                    $("#Location").html(sSiteAddress);
                }

                // Periods: Either all periods if an existing timesheet, or only those unassigned for new timesheets
                var periodList = null;
                if (lTimesheetId > 0)
                    periodList = CPlacementResponse.PeriodDescriptionsAll;
                else
                    periodList = CPlacementResponse.PeriodDescriptionsUnassignedToTimesheets;

                TriSysSDK.CShowForm.populateComboFromListOfString("cmbPeriod", periodList);
            },

            // The first callback after data has been received from the web API
            PopulateFields: function (CTimesheetResponse)
            {
                //TimesheetId As Long
                // TimesheetId As Long
                // PlacementId As Long
                // Period As String
                // InputDate As Date
                // TotalTimeWorked As Double
                // NetPay As String
                // VATAmount As String
                // PayTotal As String
                // NetCharge As String
                // NetExpenses As String
                // PaymentAuthorised As Boolean
                // Payrolled As Boolean
                // AuthorisedDate As Date
                // AuthorisedBy As String
                $("#TimesheetId").html(TriSysSDK.Numbers.Pad(CTimesheetResponse.TimesheetId, 6));

                var timesheet = CTimesheetResponse.Timesheet;
                if (timesheet)
                {
                    if (CTimesheetResponse.TimesheetId > 0)
                    {
                        TriSysSDK.CShowForm.SetTextInCombo("cmbPeriod", timesheet.Period);
                        TriSysSDK.CShowForm.ComboEnabled("cmbPeriod", false);
                    }
                    else
                    {
                        $("#TimesheetId").html("&lt;Allocated when Saved&gt;");
                        timesheet.InputDate = new Date();

                        // Add a callback so that when a period is selected, that the correct periods are drawn
                        TriSysSDK.CShowForm.addCallbackOnComboSelect("cmbPeriod", TriSysWeb.Pages.Timesheet.PeriodSelectionEvent);
                        var sFirstPeriod = TriSysSDK.CShowForm.GetTextFromCombo("cmbPeriod");
                        TriSysWeb.Pages.Timesheet.PeriodSelectionEvent(sFirstPeriod);
                    }

                    $("#InputDate").html(moment(timesheet.InputDate).format('dddd DD MMMM YYYY'));
                    var bAuthorised = timesheet.PaymentAuthorised;
                    $("#Status").html((bAuthorised ? "" : "Not ") + "Authorised");
                    if (bAuthorised)
                    {
                        $("#AuthorisedDate").html(moment(timesheet.AuthorisedDate).format('dddd DD MMMM YYYY'));
                        $("#trDateAuthorised").show();
                    }
                    var bPaid = timesheet.Payrolled;
                    if (bPaid)
                    {
                        $("#Paid").html(bPaid ? "Yes" : "No");
                        $("#trPaid").show();
                    }
                }
                else
                {
                    TriSysApex.UI.ShowMessage("Unable to find timesheet");
                }
            },

            // When candidate changes the period, go back and get the placement rates, periods and hours for the grid population.
            // This will have no hours because it is only for a new timesheet.
            PeriodSelectionEvent: function (sPeriod)
            {
                //alert(sPeriod);
                var lPlacementId = parseInt($("#PlacementId").html(), 10);

                // Go to the web API to get the rates for the specified period and draw the periods when called back
                TriSysApex.UI.ShowWait(null, sPeriod + "...");

                var dataPacket = {
                    TimesheetId: 0,
                    PlacementId: lPlacementId,
                    Period: sPeriod
                };

                var payloadObject = {};
                payloadObject.URL = "Timesheet/Read";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CTimesheetResponse = data;
                    if (CTimesheetResponse)
                    {
                        if (CTimesheetResponse.Success)
                        {
                            // Populate the timesheet period intervals and hours
                            TriSysWeb.Pages.Timesheet.PopulateGrid(CTimesheetResponse, 'divTimesheetHoursGrid', true);
                        }
                        else
                            TriSysApex.UI.errorAlert(CTimesheetResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.Timesheet.PeriodSelectionEvent: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // The Hours, Rates & Amounts grid 
            PopulateAmountsTable: function (rates, bClientContact)
            {
                var tableDiv = '#amountsTable';
                var bodyDiv = '#tbodyAmounts';

                // Enumerate through all rates
                for (var iColumn = 0; iColumn < rates.length; iColumn++)
                {
                    var rate = rates[iColumn];
                    var sRate = "<b>" + rate.Rate + "</b>";
                    var sRowName = "ID" + rate.PlacementRateId;
                    var sRateDescription = rate.RateDescription;
                    var sRateNotes = rate.Notes;

                    var sTotalHoursDiv = '<div id="' + sRowName + '-TotalHours"></div>';
                    var sPayRate = '<div id="' + sRowName + '-PayRate"></div>';
                    var sChargeRate = '<div id="' + sRowName + '-ChargeRate"></div>';
                    var sPayAmountDiv = '<div id="' + sRowName + '-PayAmount"></div>';
                    var sChargeAmountDiv = '<div id="' + sRowName + '-ChargeAmount"></div>';

                    // Append rate summary to table now
                    $(tableDiv).find(bodyDiv).append(
                        '<tr>' +
                        '  <th class="text-nowrap">' + sRate + '</th>' +
                        '  <td class="RatesNumberColumn">' + sTotalHoursDiv + '</td>' +
                        '  <td class="RatesNumberColumn">' + sPayRate + '</td>' +
                        '  <td class="RatesNumberColumn">' + sChargeRate + '</td>' +
                        '  <td class="RatesNumberColumn">' + sPayAmountDiv + '</td>' +
                        '  <td class="RatesNumberColumn">' + sChargeAmountDiv + '</td>' +
                        '</tr>');

                    // Write rates so that they can be re-used
                    $('#' + sRowName + '-PayRate').html(TriSysSDK.Numbers.Format(rate.PayRate, 2));
                    $('#' + sRowName + '-ChargeRate').html(TriSysSDK.Numbers.Format(rate.ChargeRate, 2));
                }

                // Append total summary to table now
                $(tableDiv).find(bodyDiv).append(
                        '<tr>' +
                        '  <th class="text-nowrap">Totals</th>' +
                        '  <td class="RatesNumberColumn"><div id="amountsTotalHours"></div></td>' +
                        '  <td class="RatesNumberColumn"></td>' +
                        '  <td class="RatesNumberColumn"></td>' +
                        '  <td class="RatesNumberColumn"><div id="amountsTotalPay"></div></td>' +
                        '  <td class="RatesNumberColumn"><div id="amountsTotalCharge"></div></td>' +
                        '</tr>');

                TriSysSDK.Tables.ColumnVisibilityByIndex('amountsTable', 1, !bClientContact);
                TriSysSDK.Tables.ColumnVisibilityByIndex('amountsTable', 2, bClientContact);
                TriSysSDK.Tables.ColumnVisibilityByIndex('amountsTable', 3, !bClientContact);
                TriSysSDK.Tables.ColumnVisibilityByIndex('amountsTable', 4, bClientContact);
            },

            PopulateSummaryTable: function (CTimesheetResponse, bClientContact)
            {
                // Only interested in the first currency/VAT combination
                var currencyVATList = CTimesheetResponse.CurrencyVATAmounts;
                if (currencyVATList)
                {
                    if (currencyVATList.length > 0)
                    {
                        var CPlacementRateCurrencyVATAmountWebAPI = currencyVATList[0];
                        var sPayCurrency = CPlacementRateCurrencyVATAmountWebAPI.PayCurrency;
                        var sChargeCurrency = CPlacementRateCurrencyVATAmountWebAPI.ChargeCurrency;
                        var fPayVATRate = CPlacementRateCurrencyVATAmountWebAPI.PayVATRate;
                        var fChargeVATRate = CPlacementRateCurrencyVATAmountWebAPI.ChargeVATRate;

                        $('#summaryPayCurrency').html(sPayCurrency);
                        $('#summaryChargeCurrency').html(sChargeCurrency);
                        $('#summaryPayVATPercentage').html(TriSysSDK.Numbers.Format(fPayVATRate, 2));
                        $('#summaryChargeVATPercentage').html(TriSysSDK.Numbers.Format(fChargeVATRate, 2));

                        // Other summary fields are filled during the calculation engine
                    }
                }

                TriSysSDK.Tables.ColumnVisibilityByIndex('summaryTable', 0, !bClientContact);
                TriSysSDK.Tables.ColumnVisibilityByIndex('summaryTable', 1, bClientContact);
            },

            // We have enough information to construct the grid, albeit without any data (hours) to put in it!
            PopulateGrid: function (CTimesheetResponse, divTag, bEditable)
            {
                // Build a data set from the periods and rates
                var intervals = CTimesheetResponse.PeriodIntervals;
                var rates = CTimesheetResponse.Rates;
                var hours = CTimesheetResponse.Hours;

                // Create columns
                var iPeriodWidth = 140;
                var iHoursWidth = 110;
                if (TriSysApex.Pages.Index.MobileViewEnabled)
                {
                    iPeriodWidth = 110;
                    iHoursWidth = 90;
                }
                var sWidthPeriod = "width:" + iPeriodWidth + "px;";
                var sHoursWidth = iHoursWidth + "px";
                var sVerticalAlignStyleTop = "vertical-align: top;";
                var verticalAlignStylePeriod = { style: sVerticalAlignStyleTop + sWidthPeriod };
                var verticalAlignStyle = { style: sVerticalAlignStyleTop };
                var DailyTotalStyle = { style: "text-align:right; " + sVerticalAlignStyleTop };
                var fields = {
                    TimeSheetPeriodIntervalId: { editable: false },
                    period: { editable: false }
                };
                var periodColumn = {
                    field: "period", title: "<b>Period</b>",
                    headerAttributes: verticalAlignStylePeriod,
                    attributes: verticalAlignStylePeriod,
                    footerAttributes: verticalAlignStylePeriod
                };
                if (TriSysApex.Pages.Index.MobileViewEnabled)
                {
                    periodColumn["locked"] = true;
                    periodColumn["width"] = iPeriodWidth;
                }

                var columns = [
                        { field: "TimeSheetPeriodIntervalId", title: "ID", hidden: true },
                        periodColumn
                ];
                var aggregates = [];
                var rowTotalTemplate = '';
                var sStyle = "text-align:right; word-break: break-all; white-space: normal;";
                var rightAlignAttribute = { style: sStyle };

                for (var iColumn = 0; iColumn < rates.length; iColumn++)
                {
                    var rate = rates[iColumn];
                    var sColumnHeader = "<b>" + rate.Rate + "</b>";
                    var sColumnName = "ID" + rate.PlacementRateId;
                    var sRateDescription = rate.RateDescription;
                    var sRateNotes = rate.Notes;

                    // Add to fields too
                    var dynamicField = { type: "number", validation: { min: 0, max: 18, step: 0.01 }, editable: bEditable };
                    fields[sColumnName] = dynamicField;

                    // add dynamic column
                    //sColumnHeader += '<br /><div style="font-size: 9px">(' + sRateDescription + ')</div>';
                    //sColumnHeader += '<br />' + sRateDescription;
                    var dynamicColumn = {
                        field: sColumnName, title: sColumnHeader, format: "{0:n2}", decimals: 2, width: sHoursWidth,
                        footerTemplate: '<div style="float: right">#= kendo.format("{0:n2}",sum) # </div>',
                        attributes: rightAlignAttribute,
                        //headerAttributes: { style: sStyle, title: sRateDescription + "\r\n" + sRateNotes }
                        headerAttributes: { style: sStyle + " vertical-align:top; height: 30px;", title: sRateDescription + "\r\n" + sRateNotes },

                        // Have to have a separate editor in order to prevent validation errors
                        editor: function (container, options)
                        {
                            //create input element and add the validation attribute
                            var input = $('<input name="' + options.field + '" required="required" />');
                            //append the editor
                            input.appendTo(container);
                            //enhance the input into NumericTextBox
                            input.kendoNumericTextBox({
                                min: 0,
                                max: 18,
                                step: 0.25
                            });

                            //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                            //append the tooltip element
                            tooltipElement.appendTo(container);
                        }
                    };
                    columns.push(dynamicColumn);

                    // add dynamic column footers
                    var aggregate = { field: sColumnName, aggregate: "sum" };
                    aggregates.push(aggregate);

                    // add row total
                    if (rowTotalTemplate.length > 0)
                        rowTotalTemplate += ' + ';
                    rowTotalTemplate += 'data.' + sColumnName;
                }


                // Add daily totals
                var rowTotal = { type: "number", editable: false };
                fields["Total"] = rowTotal;

                var dailyTotalColumn = {
                    field: "Total", title: "<b>Daily Total</b>", format: "{0:n2}", decimals: 2, width: sHoursWidth,
                    template: '<b>#= kendo.format("{0:n2}", ' + rowTotalTemplate + ') #</b>',
                    footerTemplate: '<div style="float: right">#= kendo.format("{0:n2}", TriSysWeb.Pages.Timesheet.ReCalculateAfterHoursChangeEvent("' + divTag + '", "ID") ) # </div>',    // Calls our own calculation function 
                    attributes: rightAlignAttribute,
                    headerAttributes: DailyTotalStyle
                };
                columns.push(dailyTotalColumn);

                var aggregateTotal = { field: "Total", aggregate: "sum" };
                aggregates.push(aggregateTotal);


                // Enumerate through intervals and hours
                var gridData = [];
                for (var iDate = 0; iDate < intervals.length; iDate++)
                {
                    var interval = intervals[iDate];
                    var gridRow = { TimeSheetPeriodIntervalId: interval.TimeSheetPeriodIntervalId, period: interval.IntervalPeriod };

                    for (var iColumn = 0; iColumn < rates.length; iColumn++)
                    {
                        var rate = rates[iColumn];
                        var sColumnHeader = rate.Rate;
                        var sColumnName = "ID" + rate.PlacementRateId;

                        // Get the value for this cell
                        var fHoursValue = 0.0;
                        for (iHours = 0; iHours < hours.length; iHours++)
                        {
                            var hour = hours[iHours];
                            if (hour.TimeSheetPeriodIntervalId == interval.TimeSheetPeriodIntervalId && hour.PlacementRateId == rate.PlacementRateId)
                                fHoursValue = hour.DailyHours;
                        }
                        gridRow[sColumnName] = fHoursValue;
                    }

                    gridData.push(gridRow);
                }

                var dataSource = new kendo.data.DataSource({
                    pageSize: 20,
                    data: gridData,
                    autoSync: true,
                    schema: {
                        model: {
                            id: "TimeSheetPeriodIntervalId",
                            fields: fields
                        }
                    },
                    aggregate: aggregates
                });

                // Hook the grid to this data source
                $("#" + divTag).kendoGrid({
                    dataSource: dataSource,
                    //pageable: true,
                    height: 320,
                    //toolbar: ["create"],
                    columns: columns,
                    editable: true
                });
            },

            // When the candidate changes an hour in the timesheet grid, recalculate and return the total hours.
            // Also update other totals not in the grid and calculate the whole timesheet financials.
            ReCalculateAfterHoursChangeEvent: function (divTag, sCalculateColumnFieldPrefix)
            {
                var fTotalHoursForColumn = TriSysSDK.Grid.CalculatedColumnsGrandTotal(divTag, sCalculateColumnFieldPrefix);

                // Update totals
                TriSysWeb.Pages.Timesheet.AmountsAndSummaryCalculationEngine();

                return fTotalHoursForColumn;
            },

            // Called every time hours data is changed to reflect all totals and financials in linked grids.
            AmountsAndSummaryCalculationEngine: function ()
            {
                var timesheetPeriodRatesHours = TriSysWeb.Pages.Timesheet.ReadHoursGridData();
                if (!timesheetPeriodRatesHours)
                    return;

                var rateTotals = [], iRateTotal;
                for (var iPeriodInterval = 0; iPeriodInterval < timesheetPeriodRatesHours.length; iPeriodInterval++)    // For each Day
                {
                    var timesheetPeriodIntervalObject = timesheetPeriodRatesHours[iPeriodInterval];
                    var lTimeSheetPeriodIntervalId = timesheetPeriodIntervalObject.TimeSheetPeriodIntervalId;
                    var hoursPerRate = timesheetPeriodIntervalObject.HoursPerRate;

                    for (var iRates = 0; iRates < hoursPerRate.length; iRates++)                                        // For each rate
                    {
                        var rate = hoursPerRate[iRates];
                        var fHoursPerRate = rate.Hours;
                        var lPlacementRateId = rate.PlacementRateId;

                        var rateTotalItem = null;
                        for (iRateTotal = 0; iRateTotal < rateTotals.length; iRateTotal++)
                        {
                            var rateTotal = rateTotals[iRateTotal];
                            if (rateTotal.PlacementRateId == lPlacementRateId)
                            {
                                rateTotalItem = rateTotal;
                                break;
                            }
                        }

                        if (!rateTotalItem)
                        {
                            rateTotalItem = { PlacementRateId: lPlacementRateId, Hours: 0.0 };
                            rateTotals.push(rateTotalItem);
                        }

                        rateTotalItem.Hours += fHoursPerRate;
                    }
                }

                // Enumerate through all rate totals and write
                var fSumOfHours = 0.0;
                var fSumOfPayAmount = 0.0;
                var fSumOfChargeAmount = 0.0;
                var sFormattedNumber;

                for (iRateTotal = 0; iRateTotal < rateTotals.length; iRateTotal++)
                {
                    var dailyTotal = rateTotals[iRateTotal];
                    fSumOfHours += dailyTotal.Hours;
                    var sRowName = "ID" + dailyTotal.PlacementRateId;
                    var sRateTotalHoursDiv = sRowName + '-TotalHours';
                    sFormattedNumber = TriSysSDK.Numbers.Format(dailyTotal.Hours, 2);
                    $('#' + sRateTotalHoursDiv).html(sFormattedNumber);

                    var sPayRate = $('#' + sRowName + '-PayRate').html();
                    var fPayRate = parseFloat(sPayRate);
                    var fPayAmount = dailyTotal.Hours * fPayRate;
                    sFormattedNumber = TriSysSDK.Numbers.Format(fPayAmount, 2);
                    $('#' + sRowName + '-PayAmount').html(sFormattedNumber);
                    fSumOfPayAmount += fPayAmount;

                    var sChargeRate = $('#' + sRowName + '-ChargeRate').html();
                    var fChargeRate = parseFloat(sChargeRate);
                    var fChargeAmount = dailyTotal.Hours * fChargeRate;
                    sFormattedNumber = TriSysSDK.Numbers.Format(fChargeAmount, 2);
                    $('#' + sRowName + '-ChargeAmount').html(sFormattedNumber);
                    fSumOfChargeAmount += fChargeAmount;
                }

                sFormattedNumber = TriSysSDK.Numbers.Format(fSumOfHours, 2);
                $('#amountsTotalHours').html('<b>' + sFormattedNumber + '</b>');
                sFormattedNumber = TriSysSDK.Numbers.Format(fSumOfPayAmount, 2);
                $('#amountsTotalPay').html('<b>' + sFormattedNumber + '</b>');
                $('#summaryTotalNetPay').html(sFormattedNumber);
                sFormattedNumber = TriSysSDK.Numbers.Format(fSumOfChargeAmount, 2);
                $('#amountsTotalCharge').html('<b>' + sFormattedNumber + '</b>');
                $('#summaryTotalNetCharge').html(sFormattedNumber);

                var fPayVATPercentage = parseFloat($('#summaryPayVATPercentage').html());
                var fChargeVATPercentage = parseFloat($('#summaryChargeVATPercentage').html());
                var fPayVATAmount = fSumOfPayAmount * (fPayVATPercentage / 100);
                var fChargeVATAmount = fSumOfChargeAmount * (fChargeVATPercentage / 100);
                $('#summaryPayVATAmount').html(TriSysSDK.Numbers.Format(fPayVATAmount, 2));
                $('#summaryChargeVATAmount').html(TriSysSDK.Numbers.Format(fChargeVATAmount, 2));

                var fPayGrandTotal = fSumOfPayAmount + fPayVATAmount;
                var fChargeGrandTotal = fSumOfChargeAmount + fChargeVATAmount;
                $('#summaryPayGrandTotal').html('<b>' + TriSysSDK.Numbers.Format(fPayGrandTotal, 2) + '</b>');
                $('#summaryChargeGrandTotal').html('<b>' + TriSysSDK.Numbers.Format(fChargeGrandTotal, 2) + '</b>');
            },

            FormButtonVisibility: function (CTimesheetResponse)
            {
                if (CTimesheetResponse.CanEdit)
                {
                    $('#btnSaveTimesheet').show();
                    $('#btnRequestAuthorisation').show();
                }
                if (CTimesheetResponse.CanAuthorise)
                    $('#btnAuthoriseTimesheet').show();
            },

            // Read entered data from grid for calculations or save
            ReadHoursGridData: function ()
            {
                // Read all data from the grid data source
                var gridData = TriSysSDK.Grid.ReadGridContents('divTimesheetHoursGrid');

                // Validate the data
                var iRowCount = gridData.rows.length;
                var iColumnCount = gridData.columns.length;
                if (iRowCount <= 0 || iColumnCount <= 0)
                {
                    // This will happen for new timesheets when the candidate has not yet selected a period
                    return null;
                }

                // Package up for transmission
                var gridHoursByPeriod = [];
                var gridRows = gridData.rows;
                var gridColumns = gridData.columns;

                var timesheetPeriodRatesHours = [];

                for (var iRow = 0; iRow < gridRows.length; iRow++)       // Row data is all rates for each period
                {
                    var row = gridRows[iRow];

                    var lTimeSheetPeriodIntervalId = row.TimeSheetPeriodIntervalId;

                    var dailyHoursPerRateArray = [];

                    for (var iRateCol = 2; iRateCol < gridColumns.length - 1; iRateCol++)
                    {
                        var column = gridColumns[iRateCol];
                        var sFieldName = column.field;

                        var fHours = row[sFieldName];
                        var sPlacementRateId = sFieldName.substring(2);
                        var lPlacementRateId = parseInt(sPlacementRateId, 10);

                        var dailyHoursPerRate = {
                            Hours: fHours,
                            PlacementRateId: lPlacementRateId
                        };

                        dailyHoursPerRateArray.push(dailyHoursPerRate);
                    }

                    var dailyTimesheet = {
                        TimeSheetPeriodIntervalId: lTimeSheetPeriodIntervalId,
                        HoursPerRate: dailyHoursPerRateArray
                    };

                    timesheetPeriodRatesHours.push(dailyTimesheet);
                }

                return timesheetPeriodRatesHours;
            },

            SaveButtonClick: function ()
            {
                //TriSysApex.UI.ShowMessage("Save Timesheet");
                var lPlacementId = parseInt($("#PlacementId").html(), 10);
                var lTimesheetId = parseInt($("#TimesheetId").html(), 10);
                var sPeriod = TriSysSDK.CShowForm.GetTextFromCombo("cmbPeriod");

                // Read all data from the grid data source
                var timesheetPeriodRatesHours = TriSysWeb.Pages.Timesheet.ReadHoursGridData();
                if (!timesheetPeriodRatesHours)
                    return;

                // Send it via the Web API
                TriSysWeb.Pages.Timesheet.SendHoursMatrixToServer(lPlacementId, lTimesheetId, sPeriod, timesheetPeriodRatesHours);
                //var sJSON = JSON.stringify(timesheetPeriodRatesHours);
            },

            SendHoursMatrixToServer: function (lPlacementId, lTimesheetId, sPeriod, timesheetPeriodRatesHours)
            {
                var CTimesheetSaveRequest = {
                    TimesheetId: lTimesheetId,
                    PlacementId: lPlacementId,
                    Period: sPeriod,
                    PeriodIntervalHours: timesheetPeriodRatesHours
                };


                TriSysApex.UI.CloseTopmostModalPopup();
                TriSysApex.UI.ShowWait(null, "Saving Timesheet...");

                var payloadObject = {};
                payloadObject.URL = "Timesheet/Write";
                payloadObject.OutboundDataPacket = CTimesheetSaveRequest;
                payloadObject.InboundDataFunction = function (data)
                {
                    var CTimesheetSaveResponse = data;
                    if (CTimesheetSaveResponse)
                    {
                        TriSysApex.UI.HideWait();

                        if (CTimesheetSaveResponse.Success)
                        {
                            // If a new timesheet, then Update ID on form, prevent period being changed
                            $("#TimesheetId").html(TriSysSDK.Numbers.Pad(CTimesheetSaveResponse.TimesheetId, 6));
                            TriSysSDK.CShowForm.ComboEnabled("cmbPeriod", false);
                            $('#btnAuthoriseTimesheet').attr("disabled", true);

                            var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.SendHoursMatrixToServer.SavedTimesheetId") +
                                TriSysSDK.Numbers.Pad(CTimesheetSaveResponse.TimesheetId, 6) +
                                TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.SendHoursMatrixToServer.AuthorisationReminder");
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CTimesheetSaveResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('SendHoursMatrixToServer: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // Candidates can request authorisation of their timesheet once it is completed.
            // This is a separate stage so that candidates can enter time every shift/day without
            // having to submit it for authorisation by their line manager.
            RequestAuthorisation: function ()
            {
                var fnConfirmAuthorisationRequest = function ()
                {
                    TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationRequest();
                };

                var sCaption = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.RequestAuthorisation.PromptCaption");
                var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.RequestAuthorisation.PromptMessage");
                TriSysApex.UI.questionYesNo(sMessage, sCaption, "Yes", fnConfirmAuthorisationRequest, "No", null);
            },

            // Candidate has confirmed authorisation request
            ConfirmedAuthorisationRequest: function ()
            {
                var lTimesheetId = parseInt($("#TimesheetId").html(), 10);

                if (!TriSysWeb.Security.IsCandidateLoggedIn() || lTimesheetId <= 0)
                    return;

                var sURL = window.location.href;
                var sSite = TriSysApex.URL.Parser(sURL).getHost();

                // Fire into web API now

                TriSysApex.UI.ShowWait(null, "Requesting Authorisation...");

                var dataPacket = {
                    TimesheetId: lTimesheetId,
                    Site: sSite
                };

                var payloadObject = {};
                payloadObject.URL = "Timesheet/RequestAuthorisation";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CTimesheetRequestAuthorisationResponse = data;
                    if (CTimesheetRequestAuthorisationResponse)
                    {
                        if (CTimesheetRequestAuthorisationResponse.Success)
                        {
                            // Timesheet has now been submitted for authorisation
                            $('#btnSaveTimesheet').attr("disabled", true);
                            $('#btnRequestAuthorisation').attr("disabled", true);

                            // There are no back-end flags to indicate whether this has been submitted: TODO

                            var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationRequest.Message");
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CTimesheetRequestAuthorisationResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationRequest: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            // Clients can authorise timesheets for candidates who have explicitly requested authorisation.
            Authorise: function ()
            {
                var fnConfirmRegistration = function ()
                {
                    TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationCallback();
                };

                var sCaption = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.Authorise.PromptCaption");
                var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.Authorise.PromptMessage");
                TriSysApex.UI.questionYesNo(sMessage, sCaption, "Yes", fnConfirmRegistration, "No", null);
            },

            // Client has confirmed authorisation
            ConfirmedAuthorisationCallback: function ()
            {
                var lTimesheetId = parseInt($("#TimesheetId").html(), 10);

                if (!TriSysWeb.Security.IsClientLoggedIn() || lTimesheetId <= 0)
                    return;

                var sURL = window.location.href;
                var sSite = TriSysApex.URL.Parser(sURL).getHost();

                // Fire into web API now

                TriSysApex.UI.ShowWait(null, "Authorising...");

                var dataPacket = {
                    TimesheetId: lTimesheetId,
                    Site: sSite
                };

                var payloadObject = {};
                payloadObject.URL = "Timesheet/Authorise";
                payloadObject.OutboundDataPacket = dataPacket;
                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    var CTimesheetAuthoriseResponse = data;
                    if (CTimesheetAuthoriseResponse)
                    {
                        if (CTimesheetAuthoriseResponse.Success)
                        {
                            // Timesheet has now been authorised, so update status
                            $("#Status").html("Authorised");
                            var dtAuthorisedDate = new Date();
                            $("#AuthorisedDate").html(moment(dtAuthorisedDate).format('dddd DD MMMM YYYY'));
                            $("#trDateAuthorised").show();

                            var sMessage = TriSysSDK.Resources.Message("TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationCallback.Message");
                            TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                        }
                        else
                            TriSysApex.UI.errorAlert(CTimesheetAuthoriseResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                    }

                    return true;
                };
                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.ErrorHandlerRedirector('TriSysWeb.Pages.Timesheet.ConfirmedAuthorisationCallback: ', request, status, error);
                };
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }
        }
        //#endregion TriSysWeb.Pages.Timesheet
    },
    //#endregion TriSysWeb.Pages

    //#region TriSysWeb.Security
    Security:
    {
        // When each page loads, show/hide login credentials in page header
        ShowLoginCredentialsAfterPageLoad: function ()
        {
            // Show appropriate security settings
            if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
            {
                // User is not logged in so clear login credential display
                TriSysWeb.Security.ShowLoginStatus(false);
            }
            else
            {
                // Client or Candidate is logged in, so show name+company
                TriSysWeb.Security.ShowLoginStatus(true);
            }
        },

        // Check cookies to see if local user is logged in
        IsDataConnectionOpen: function ()
        {
            var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
            if (dataConnectionKey)
            {
                // This is not enough to prove that we are connected (statelessly of course) because the
                // connection key could be invalid or timed-out if we are changing keys on the server.
                // Hence test a connection based upon a very simple synchronous API call.
                if (TriSysAPI.Data.VerifyDataServicesKey())
                    return true;
            }
            return false;
        },

        IsClientOrCandidateLoggedIn: function ()
        {
            var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
            if (dataConnectionKey)
            {
                if (dataConnectionKey.LoggedInAgencyContact)
                    return true;
            }
            return false;
        },

        IsClientLoggedIn: function ()
        {
            var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
            if (dataConnectionKey)
            {
                if (dataConnectionKey.LoggedInAgencyContact)
                {
                    if (dataConnectionKey.LoggedInAgencyContact.ContactType == "Client")
                        return true;
                }
            }
            return false;
        },

        IsCandidateLoggedIn: function ()
        {
            var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
            if (dataConnectionKey)
            {
                if (dataConnectionKey.LoggedInAgencyContact)
                {
                    if (dataConnectionKey.LoggedInAgencyContact.ContactType == "Candidate")
                        return true;
                }
            }
            return false;
        },

        // Get the data connection key cached in session memory for the logged in client or candidate
        ClientOrCandidate_CDataConnectionKey: function ()
        {
            var sCookie = TriSysAPI.Session.Memory.Get("CDataConnectionKey");
            if (sCookie)
            {
                try
                {
                    var CDataConnectionKey = JSON.parse(sCookie);
                    return CDataConnectionKey;
                }
                catch (err)
                {
                    // Potential hacker or cookie corruption
                }
            }
            return null;
        },

        ShowLoginStatus: function (bLoggedIn)
        {
            var sLoginMessageText = TriSysWeb.Constants.NotLoggedInText;

            if (bLoggedIn)
            {
                var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                if (dataConnectionKey)
                {
                    var agencyContact = dataConnectionKey.LoggedInAgencyContact;
                    var sContactType = agencyContact.ContactType;
                    var sLoginDetails = agencyContact.FullName;
                    var sCompanyName = agencyContact.CompanyName;
                    if (sCompanyName)
                        sLoginDetails += ', ' + sCompanyName;

                    sLoginMessageText = TriSysWeb.Constants.LoggedInPrefixText + sContactType + ": " + sLoginDetails;
                }

                $('#loggedinPanel').show();
                $('#loginPanel').hide();

                // Show client menu
                var sClientMenu = "#menuItem-client-dropdown";
                var sCandidateMenu = "#menuItem-candidate-dropdown";
                if (TriSysWeb.Security.IsClientLoggedIn())
                {
                    $(sClientMenu).show();
                    $(sCandidateMenu).hide();
                }
                else
                {
                    $(sClientMenu).hide();
                    $(sCandidateMenu).show();
                }
            }
            else
            {
                $('#loggedinPanel').hide();
                $('#loginPanel').show();
            }

            $('#loggedInNameAndCompany').html(sLoginMessageText);
        },

        // Called from TriSysWeb.Pages.SecurityRedirector
        // If we need a database connection e.g. show vacancies, get it now
        ConnectToAgencyDatabaseOnly: function (sPageName, fnCallback)
        {
            // We must connect using the TriSys CRM agency user credentials in order to access the correct cloud database
            var TriSysCRMCredentialAccountTypeEnum_AgencyVacanciesOnly = 3;
            var eAccountType = TriSysCRMCredentialAccountTypeEnum_AgencyVacanciesOnly;
            var dataPacket = {
                'CRMCredentialKey': TriSysWeb.Constants.AgencyUserLoginCredentialsToCRM,
                'AccountType': eAccountType
            };

            TriSysApex.LoginCredentials.ValidateViaWebService(dataPacket, false, function ()
            {
                TriSysWeb.Security.ValidatedLoginCredentialsCallback(fnCallback);
            });
        },

        // Called after user explicitly enters data, or automatically where cookies are set.
        LoginButtonClick: function ()
        {
            var sLoginEMail = $('#txtLoginEMail').val();
            var sPassword = $('#txtLoginPassword').val();
            var sPersistence = $('#' + TriSysWeb.Pages.Login.PersistenceDiv).val();

            TriSysWeb.Security.LoginAsAgencyClientOrCandidate(sLoginEMail, sPassword, sPersistence);
        },

        LoginAsAgencyClientOrCandidate: function (sLoginEMail, sPassword, sPersistence)
        {
            var sMessage;
            if (!TriSysApex.LoginCredentials.validateEmail(sLoginEMail))
            {
                sMessage = TriSysSDK.Resources.Message("LoginButtonClick_EMail");
                TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                return;
            }

            if (!sPassword)
            {
                sMessage = TriSysSDK.Resources.Message("LoginButtonClick_Password");
                TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName);
                return;
            }

            // Persistence
            var bRememberMe = (sPersistence == TriSysWeb.Pages.Login.PersistCredentialsOn());

            // We must connect using the TriSys CRM agency user credentials in order to access the correct cloud database,
            // and also pass in the client/candidate contact details which live in the agency SQL database
            var TriSysCRMCredentialAccountTypeEnum_AgencyClientOrCandidate = 2;
            var eAccountType = TriSysCRMCredentialAccountTypeEnum_AgencyClientOrCandidate;
            var dataPacket = {
                'CRMCredentialKey': TriSysWeb.Constants.AgencyUserLoginCredentialsToCRM,
                'AccountType': eAccountType,
                'AgencyContact_EMail': sLoginEMail,
                'AgencyContact_Password': sPassword
            };

            var fnPostLogin = function ()
            {
                if (bRememberMe)
                    TriSysWeb.Security.CacheLoginCredentialsAsCookies();

                TriSysWeb.Security.ValidatedLoginCredentialsCallback(TriSysWeb.Pages.Home.Redirect);
            };

            var fnIncorrectLogin = function ()
            {
                var sLoginFailedMessage = TriSysSDK.Resources.Message("LoginButtonClick_Invalid");
                var sMessageTitle = TriSysWeb.Copyright.ShortProductName + " Login";
                TriSysApex.UI.ShowMessage(sLoginFailedMessage, sMessageTitle);
            };


            TriSysApex.LoginCredentials.ValidateViaWebService(dataPacket, bRememberMe, fnPostLogin, fnIncorrectLogin);
        },

        // Called when user specifically requests a logoff.
        // If they explicitly logoff, then remove their saved credentials. (See LinkedIn for inspiration)
        LogoffButtonClick: function ()
        {
            var sMessage = TriSysSDK.Resources.Message("LogoffButtonClick");
            var sYes = TriSysSDK.Resources.Message("Yes");
            var sNo = TriSysSDK.Resources.Message("No");
            var sCancel = TriSysSDK.Resources.Message("Cancel");
            var sTitle = TriSysWeb.Constants.ApplicationName;
            var fnLogoffCompletion = function () { TriSysWeb.Security.ShowLoginPage(); return true; };
            var fnLogoffProcess = function ()
            {
                var fnClearKeysAndShowLogin = function ()
                {
                    TriSysApex.UI.HideWait();

                    // Clear the data services key and connection details as it is no longer valid
                    TriSysAPI.Session.Memory.Clear();

                    // Clear all cached login details also
                    TriSysWeb.Security.WritePersistedLoginCredentials(null);
                    TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginNameSetting, null);
                    TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginPasswordSetting, null);

                    TriSysWeb.Security.ShowLoginStatus(false);
                    var sMessage = TriSysSDK.Resources.Message("fnClearKeysAndShowLogin");

                    TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName, fnLogoffCompletion);
                };

                TriSysApex.UI.ShowWait(null, TriSysSDK.Resources.Message("Logging Off..."));
                TriSysApex.LoginCredentials.LogoffWebServiceWithCallback(fnClearKeysAndShowLogin);
            };

            TriSysApex.UI.questionYesNo(sMessage, sTitle, sYes, fnLogoffProcess, sNo, null);
        },

        ValidatedLoginCredentialsCallback: function (fnCallback)
        {
            var sWaitingMessage = TriSysSDK.Resources.Message("ValidatedLoginCredentialsCallback");
            TriSysApex.UI.ShowWait(null, sWaitingMessage);

            // Go back to get the login settings 
            TriSysApex.LoginCredentials.ReadLoggedInUserSettings(function (CDataConnectionKey)
            {
                TriSysWeb.Security.AfterReadingLoggedInAccountSettings(CDataConnectionKey, fnCallback);
            });
        },

        AfterReadingLoggedInAccountSettings: function (CDataConnectionKey, fnCallback)
        {
            // Go back to get the cached standing data settings and meta database
            TriSysApex.LoginCredentials.CacheStandingData(CDataConnectionKey, function (CDataConnectionKey, CCachedStandingDataWithBOLObjects)
            {
                TriSysWeb.Security.AfterCacheingAccountSettings(CDataConnectionKey, CCachedStandingDataWithBOLObjects, fnCallback);
            });
        },

        AfterCacheingAccountSettings: function (CDataConnectionKey, CCachedStandingDataWithBOLObjects, fnCallback)
        {
            TriSysApex.UI.HideWait();

            // User is logged in, so save in session memory
            var sStringifiedJSON = JSON.stringify(CDataConnectionKey);
            TriSysAPI.Session.Memory.Set("CDataConnectionKey", sStringifiedJSON);

            // If an agency contact, let her explicitly know they are logged in
            if (CDataConnectionKey.LoggedInAgencyContact)
            {
                TriSysWeb.Security.ShowLoginStatus(true);

                var sLoginStuff = $('#loggedInNameAndCompany').html();
                var sMessage = TriSysSDK.Resources.Message("AfterCacheingAccountSettings");
                sMessage = sMessage.replace("##LoginType##", sLoginStuff);

                //TriSysApex.Toasters.Success(sMessage);
                TriSysApex.UI.ShowMessage(sMessage, TriSysWeb.Constants.ApplicationName, fnCallback);
                return;
            }

            // Now, finally, we can redirect to the page DocumentReady().Callback function
            if (fnCallback)
                fnCallback();
        },

        // If user has persisted login credentials, then use these
        AutoLoginFromSavedCredentials: function ()
        {
            if (!TriSysWeb.Security.IsClientOrCandidateLoggedIn())
            {
                var credentialPacket = TriSysWeb.Security.ReadPersistedLoginCredentials();
                if (credentialPacket)
                {
                    var sEMailAddress = credentialPacket.EMailAddress;
                    var sPassword = credentialPacket.Password;
                    if (sEMailAddress && sPassword)
                    {
                        // NEW
                        TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginNameSetting, sEMailAddress);
                        TriSysAPI.Session.Memory.Set(TriSysWeb.Security.AutoLoginPasswordSetting, sPassword);

                        var sURL = window.location.href.toLowerCase();
                        if (sURL.indexOf(TriSysWeb.Constants.LoginPage.toLowerCase()) > 0)
                            return false;
                        else
                            return TriSysWeb.Security.ShowLoginPage();
                    }
                }
            }

            return false;
        },

        ShowLoginPage: function ()
        {
            TriSysWeb.Pages.LoadPage(TriSysWeb.Constants.LoginPage);
            return true;
        },

        AutoLoginNameSetting: "AutomaticLogin_EMail",
        AutoLoginPasswordSetting: "AutomaticLogin_Password",

        CacheLoginCredentialsAsCookies: function ()
        {
            var sLoginEMail = $('#txtLoginEMail').val();
            var sPassword = $('#txtLoginPassword').val();
            var credentialPacket = {
                EMailAddress: sLoginEMail,
                Password: sPassword
            };
            TriSysWeb.Security.WritePersistedLoginCredentials(credentialPacket);
        },

        AreLoginCredentialsPersisted: function ()
        {
            var credentialPacket = TriSysWeb.Security.ReadPersistedLoginCredentials();
            if (credentialPacket)
            {
                if (credentialPacket.EMailAddress && credentialPacket.Password)
                {
                    if (credentialPacket.EMailAddress.length > 0 && credentialPacket.Password.length > 0)
                        return true;
                }
            }
        },

        ReadPersistedLoginCredentials: function ()               // TriSysWeb.Security.ReadPersistedLoginCredentials
        {
            try
            {
                var CookiePrefix = TriSysWeb.Constants.Cookie_Prefix;

                var sEMailAddress = TriSysAPI.Cookies.getCookie(CookiePrefix + "Login_EMail");
                var sPassword = TriSysAPI.Cookies.getCookie(CookiePrefix + "Login_Password");

                if (sEMailAddress && sPassword)
                {
                    if (sEMailAddress.length > 0 && sPassword.length > 0)
                    {
                        // Decrypt
                        sEMailAddress = TriSysAPI.Encryption.decrypt(sEMailAddress, TriSysWeb.Security.EncryptionKey);
                        sPassword = TriSysAPI.Encryption.decrypt(sPassword, TriSysWeb.Security.EncryptionKey);

                        var credentialPacket = {
                            EMailAddress: sEMailAddress,
                            Password: sPassword
                        };

                        return credentialPacket;
                    }
                }
            }
            catch (err)
            {
                // encryption bugs
            }

            return null;
        },

        WritePersistedLoginCredentials: function (credentialPacket)     // TriSysWeb.Security.WritePersistedLoginCredentials
        {
            try
            {
                var sEMailAddress = '', sPassword = '';
                if (credentialPacket)
                {
                    sEMailAddress = credentialPacket.EMailAddress;
                    sPassword = credentialPacket.Password;
                }

                var CookiePrefix = TriSysWeb.Constants.Cookie_Prefix;

                if (sEMailAddress && sPassword)
                {
                    // Encrypt
                    sEMailAddress = TriSysAPI.Encryption.encrypt(sEMailAddress, TriSysWeb.Security.EncryptionKey);
                    sPassword = TriSysAPI.Encryption.encrypt(sPassword, TriSysWeb.Security.EncryptionKey);
                }

                TriSysAPI.Cookies.setCookie(CookiePrefix + "Login_EMail", sEMailAddress);
                TriSysAPI.Cookies.setCookie(CookiePrefix + "Login_Password", sPassword);
            }
            catch (err)
            {
                // encryption bugs
            }
        },

        EncryptionKey: (0 + 1 + 5 - 2 + 3 - 0 * 1)       // 7 also works well
    },
    //#endregion TriSysWeb.Security

    //#region TriSysWeb.TopJobs
    TopJobs:
    {
        // Often called from index.html when application is loaded.
        PopulateWidget: function (sDivTag, iNumberOfJobs)
        {
            var cookieName = "TriSysWeb.TopJobs.PopulateWidget.CSQLDatabaseSearchResults";

            var fnPopulateTopJobs = function (CSQLDatabaseSearchResults)
            {
                if (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults.DataTable)
                    {
                        var sJobHTML = '';
                        var dt = CSQLDatabaseSearchResults.DataTable;
                        for (var i = 0; i < dt.length; i++)
                        {
                            var dr = dt[i];
                            var sJobTitle = dr["JobTitle"];
                            var iCounter = dr["Counter"];

                            sJobHTML += '<li class="list-group-item">' +
                                        '<span class="badge">' + iCounter + '</span>' +
                                        sJobTitle +
                                        '</li>';
                        }

                        $('#' + sDivTag).html(sJobHTML);

                        // Cache these for next time
                        TriSysAPI.Persistence.Write(cookieName, CSQLDatabaseSearchResults);
                    }
                }
            };

            if (!TriSysAPI.Session.isConnected())
            {
                // If not connected, then load last loaded data set
                var cachedSearchResults = TriSysAPI.Persistence.Read(cookieName);
                if (cachedSearchResults)
                {
                    fnPopulateTopJobs(cachedSearchResults);
                    return;
                }
            }

            if (!TriSysWeb.Security.IsDataConnectionOpen())
            {
                alert('TriSysWeb.TopJobs: Not connected to API yet - programmer error');
                return;
            }

            var sSQL = TriSysWeb.TopJobs.SQLSelectTop + iNumberOfJobs + TriSysWeb.TopJobs.SQL;

            TriSysSDK.Database.GetDataSet(sSQL, fnPopulateTopJobs);
        },

        SQLSelectTop: "Select Top ",
        SQL: TriSysSDK.Database.SQL.Query("TriSysWeb.TopJobs")
    },
    //#endregion TriSysWeb.TopJobs

    //#region TriSysWeb.Data
    // 
    // This is a convenient server-oriented set of methods to get data from the serve quickly and synchronously.
    // It should be used only for very quick access methods which return quickly, NOT full blown data access transactions.
    //
    Data:
    {
        SynchronousMethod: function (sAPI, dataPacket)
        {
            var payloadObject = {};
            payloadObject.URL = sAPI;
            payloadObject.Asynchronous = false;

            if (dataPacket)
                payloadObject.OutboundDataPacket = dataPacket;

            var objData = null;

            payloadObject.InboundDataFunction = function (data)
            {
                objData = data;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                //alert('TriSysAPI.Data.SynchronousMethod: ' + status + ": " + error + ". responseText: " + request.responseText);
                // We are not connected, incorrect API, or something has gone wrong
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);

            return objData;
        },

        Vacancy:    // TriSysWeb.Data.Vacancy
        {
            Summary: function (lRequirementId)
            {
                // Quickly get a vacancy summary from the server for verification purposes
                var dataPacket = { RequirementId: lRequirementId };
                var vacancySummary = TriSysWeb.Data.SynchronousMethod("Vacancy/Summary", dataPacket);
                return vacancySummary;
            },

            isShortlisted: function (lRequirementId)
            {
                var bShortlisted = false;

                if (TriSysWeb.Security.IsCandidateLoggedIn())
                {
                    var dataConnectionKey = TriSysWeb.Security.ClientOrCandidate_CDataConnectionKey();
                    var dataPacket = {
                        RequirementId: lRequirementId,
                        ContactId: dataConnectionKey.LoggedInAgencyContact.ContactId
                    };
                    var CVacancyCandidateShortlistStatusRequestResult = TriSysWeb.Data.SynchronousMethod("Vacancy/CandidateShortlistStatus", dataPacket);
                    if (CVacancyCandidateShortlistStatusRequestResult)
                    {
                        if (CVacancyCandidateShortlistStatusRequestResult.Success)
                            bShortlisted = (CVacancyCandidateShortlistStatusRequestResult.Longlisted || CVacancyCandidateShortlistStatusRequestResult.Shortlisted);
                    }
                }
                return bShortlisted;
            }
        }
    },
    //#endregion TriSysWeb.Data

    Constants:
    {
        // The name of the application as it appears in prompts and other dialogues
        ApplicationName: 'Opus Laboris Recruitment',

        // The version of this application to the nearest minute
        Version: '2014.12.03.1451', // YYYY.MM.DD.hhmm

        // The developer site key allows access to a single server and single database
        DeveloperSiteKey: '20140625-gl51-1231-wher-opuslaboris.',

        // This key is generated by TriSys Business Software to match the TriSys Customer in CRM who is welded to a database and API Key
        AgencyUserLoginCredentialsToCRM: 'TN1jMwmNpUSGNnHIiPWRW9970X20aQyW2k2XotIkMQD756u9kQt06IfItrF-h5e8lxhpGYpd6fDJe-Ab2BWoZPSuBR22OZbSPgJn40nr193NolNLcoD-3f1TAtqeqn-anrNKOAjvRqqLvgm7Tme3WgxaMRbCIpEfS5XaJM7qZz6wg2QmV8XVg3apixSuQGpYvM5yD@2Djub3KR8Yh8Kzsg__',

        // We have our own cookies for each application so that users can have different accounts
        Cookie_Prefix: 'OpusLaborisRecruitment_',

        // Cookies for favourite vacancies
        Cookie_AddJobDetailsToFavourites: 'AddJobDetailsToFavourites',

        // Defaults for candidate apply to vacancies: can be ovewritten by each customer site: only one can be true
        CandidateApplyToVacancy_Longlist: false,
        CandidateApplyToVacancy_Shortlist: true,

        // Default page names
        HomePage: '/index',
        CandidateVacancyPage: '/vacancy',
        CandidateVacancySearchPage: '/vacancy-search',
        ClientVacanciesPage: '/clientvacancies',
        ClientVacancyPage: '/clientvacancy',
        ClientPlacementsPage: '/clientplacements',
        ClientTimesheetsPage: '/clienttimesheets',
        FavouriteVacanciesPage: '/favourite-vacancies',
        VacancyApplicationsPage: '/vacancyapplications',
        WarningPage: '/warning',
        ContactUsPage: '/contactus',
        AboutUsPage: '/aboutus',
        CandidateRegistrationPage: '/register',
        ClientRegistrationPage: '/registerclient',
        PlacementPage: '/placement',
        TimesheetPage: '/timesheet',
        LoginPage: '/login',
        ForgottenPasswordPage: '/forgottenpassword',
        EditProfilePage: '/editprofile',
        InterviewsPage: '/interviews',
        PlacementsPage: '/placements',
        TimesheetsPage: '/timesheets',

        // Login label defaults
        LoggedInPrefixText: '',
        NotLoggedInText: '',

        // Dynamic customer specific business logic
        // You may leave these in for development debugging as the configurator will replace these at publish time
        //#Start: CustomerSpecificStack
        //CustomerSpecificFolder: 'custom/admiral-recruitment',
        //CustomerSpecificFunction: 'AdmiralRecruitment.Customisation.Initialise',
        //#End: CustomerSpecificStack


        ImportantLastItemWithNoComma: 'OK?'

    } // End of TriSysWeb.Constants

};
