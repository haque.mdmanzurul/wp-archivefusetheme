﻿/* TriSys SDK Javascript Code
 * The Software Development Kit comprise all of the widgets which are used
 * to bring the Apex application to life by connecting it to real-data.
 *
 * (c) 2013 TriSys Business Software
 *
 */

// The TriSysSDK Namespace
var TriSysSDK =
{
    // Copyright Notice
    Copyright:
    {
        AllRightsReserved: "TriSys Business Software"
    },

    //#region TriSysSDK.Grid
    // Grid Namespace
    Grid:
    {
        // This is the highly re-usable grid population method which uses a pre-defined SQL statement
        // which is then used to provide full dynamic paging, filtering, sorting and grouping with 
        // constant interaction with the web API to provide the most responsive and efficient 
        // mechanism based on dynamic SQL.
        //
        VirtualMode: function (gridLayout)
        {
            if (!gridLayout)
                return;

            if (TriSysAPI.Operators.isEmpty(gridLayout.SQL))
            {
                TriSysApex.UI.errorAlert("No SQL parameter.");
                return;
            }

            // KendoUI Grid May 2014
            var sJQueryGridDiv = '#' + gridLayout.Div;

            // Setup any additional run-time columns and edit controls for touch devices
            TriSysSDK.Grid.AddCheckBoxForMultiRowSelect(gridLayout);
            TriSysSDK.Grid.AddRowOpenButtonForTouchDeviceSupport(gridLayout);

            TriSysSDK.Grid.ClearCheckedRows();

            // Define our paging data source first
            var pagingDataSource = TriSysSDK.Grid.ServerPagingDataSource(gridLayout, sJQueryGridDiv);

            // Now hook this data source up to the grid
            TriSysSDK.Grid.InitialisePagingGridFromDataSource(pagingDataSource, gridLayout, sJQueryGridDiv);

            // Finally, we want the grid sized to fit the available space
            TriSysSDK.Grid.ResizingEventHandler(gridLayout, sJQueryGridDiv);
        },

        // Set up the paging/sortable/filterable data source for the grid
        ServerPagingDataSource: function (gridLayout, sJQueryGridDiv)
        {
            // The number of records per page is a user configurable option and affects both client side grid and server API
            var iNumberOfRecordsPerPage = TriSysApex.UserOptions.RecordsPerPage;

            // Create the data source
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options)
                    {
                        // The dynamic SQL statement which will form the basis of the filtering/sorting
                        var sSQL = gridLayout.SQL;

                        // Filtering - can have multiple levels of sorting
                        sSQL = TriSysSDK.Grid.ParseFilteringSQL(sSQL, gridLayout, options, sJQueryGridDiv);

                        // Sorting 
                        sSQL = TriSysSDK.Grid.ParseSortingSQL(sSQL, gridLayout, options);

                        TriSysApex.Logging.LogMessage(sSQL);

                        // Set up the data packet to contain this SQL statement
                        var dataPacketForSQL = {
                            'SQL': sSQL,
                            'PageNumber': options.data.page,
                            'RecordsPerPage': iNumberOfRecordsPerPage
                        };


                        // Now that the SQL statement is fully assembled, prepare the properties of the standard AJAX web API call
                        var payloadObject = {};
                        payloadObject.URL = 'SQLDatabase/SQL_Select';           // Our generic page oriented SQL data table 
                        payloadObject.OutboundDataPacket = dataPacketForSQL;    // The dynamic SQL statement and paging information
                        payloadObject.Asynchronous = true;                      // We are being called asynchronously by the grid data source request
                        payloadObject.InboundDataFunction = function (result)   // The callback function to populate the grid with the data
                        {
                            if (result)
                            {
                                var myDataTable = result.DataTable;

                                // notify the data source that the request succeeded
                                var successData = {
                                    dataTable: myDataTable,
                                    totalRecordCount: result.TotalRecordCount
                                };
                                options.success(successData);

                                // Grid controller may also require a callback
                                if (gridLayout.PostPopulationCallback)
                                    gridLayout.PostPopulationCallback(result.TotalRecordCount);
                            }
                        };

                        // Send this to the back end web API for data retrieval
                        TriSysAPI.Data.PostToWebAPI(payloadObject);
                    }
                },
                schema: {
                    model: {
                        id: gridLayout.KeyColumnName,
                        fields: gridLayout.Columns //gridLayout.Fields
                    },
                    total: "totalRecordCount",
                    data: "dataTable"
                },
                serverPaging: true,
                serverFiltering: true,
                pageSize: iNumberOfRecordsPerPage,
                serverSorting: true     //,
                //sort: { field: "Surname", dir: "asc" }        ' TODO: Default sort order
            });

            return dataSource;
        },

        ParseFilteringSQL: function (sSQL, gridLayout, options, sJQueryGridDiv)
        {
            var sWhere = '';
            var ctlGrid = $(sJQueryGridDiv).data("kendoGrid");
            if (typeof (options.data.filter) != "undefined")
            {
                if (options.data.filter)
                {
                    var myFilters = options.data.filter.filters;
                    var sInterFilterLogic = " AND ";
                    if (typeof (options.data.filter.logic) != "undefined")
                        sInterFilterLogic = " " + options.data.filter.logic + " ";

                    if (myFilters.length > 0)
                    {
                        for (var i = 0; i < myFilters.length; i++)
                        {
                            var sWhereOperation = '';
                            var filter = myFilters[i];
                            var sLogic = null;
                            if (typeof (filter.logic) != "undefined")
                                sLogic = filter.logic;

                            if (sLogic)
                            {
                                // two conditions separated by 'and' / 'or' 
                                var filter1 = filter.filters[0];
                                var filter2 = filter.filters[1];
                                sWhereOperation = "(" + TriSysSDK.Grid.Filtering.FieldOperatorValueToSQL(ctlGrid, filter1.field, filter1.operator, filter1.value) +
                                    ' ' + sLogic + ' ' + TriSysSDK.Grid.Filtering.FieldOperatorValueToSQL(ctlGrid, filter2.field, filter2.operator, filter2.value) + ")";
                            }
                            else
                            {
                                sWhereOperation = TriSysSDK.Grid.Filtering.FieldOperatorValueToSQL(ctlGrid, filter.field, filter.operator, filter.value);
                            }

                            // Add latest filter to the where clause
                            if (sWhere != '')
                                sWhere += sInterFilterLogic;

                            sWhere += sWhereOperation;
                        }
                    }
                }

                if (sWhere != '')
                {
                    var bExistingWhereClause = (sSQL.toLowerCase().indexOf("where") >= 0);
                    var sWhereAnd = (bExistingWhereClause ? " And " : " Where ");
                    sSQL += sWhereAnd + sWhere;
                }
            }

            return sSQL;
        },

        // Multiple column sorting supported!
        ParseSortingSQL: function (sSQL, gridLayout, options)
        {
            var sSort = gridLayout.OrderBy;     // Default order by clause
            if (options.data.sort)
            {
                var sort = options.data.sort;
                if (sort.length > 0)
                {
                    sSort = '';
                    for (var i = 0; i < sort.length; i++)
                    {
                        if (sSort != '')
                            sSort += ", ";

                        sSort += options.data.sort[i].field + ' ' + options.data.sort[i].dir;
                    }

                    sSort = " Order By " + sSort;
                }
            }

            sSQL += sSort;

            return sSQL;
        },

        InitialisePagingGridFromDataSource: function (pagingDataSource, gridLayout, sJQueryGridDiv)
        {
            var iVCRButtonCount = 20, bRefresh = true, bNumeric = true, bGroupable = true, bResizeable = true;
            var bColumnMenu = true;

            // Filters
            var jsonColumnFilters = false, bColumnFilters = true;
            if (!TriSysAPI.Operators.isEmpty(gridLayout.ColumnFilters))
                bColumnFilters = gridLayout.ColumnFilters;

            if (bColumnFilters)
            {
                jsonColumnFilters = {
                    operators: {
                        string: {
                            startswith: "Starts with",
                            contains: "Contains",
                            eq: "Is equal to",
                            endswith: "Ends with",
                            doesnotcontain: "Does not contain",
                            neq: "Is not equal to"
                        }
                    }
                };
            }

            // Grouping
            if (!TriSysAPI.Operators.isEmpty(gridLayout.Grouping))
                bGroupable = gridLayout.Grouping;


            // Mobile consideration
            if (TriSysApex.Pages.Index.MobileViewEnabled)
            {
                bRefresh = false;
                bNumeric = false;
                bGroupable = false;
                bColumnMenu = false;
            }


            // Setup the grid
            var ctlGrid = $(sJQueryGridDiv);

            try
            {
                ctlGrid.data().kendoGrid.destroy();
                ctlGrid.empty();
            }
            catch (err)
            {
                // Perhaps grid does not exist yet?
            }


            // Instantiate the grid
            ctlGrid.kendoGrid({
                //dataSource: pagingDataSource,
                columns: gridLayout.Columns,
                editable: false,
                //pageable: true,       Show numbers for each page
                pageable: {
                    //input: true,        Show a text box
                    buttonCount: iVCRButtonCount,
                    refresh: bRefresh,
                    numeric: bNumeric
                },
                //selectable: "multiple, row",        // If we turn this on, check box row selection is inconsistent
                sortable: {
                    mode: 'multiple',       // Multiple columns can be sorted
                    allowUnsort: true       // Users can cycle between Up, Down, Off
                },
                filterable: jsonColumnFilters,
                resizable: bResizeable,
                reorderable: true,
                groupable: bGroupable,

                // Mobile option to adaptively render
                //mobile: true,

                columnMenu: bColumnMenu,
                columnShow: function (e)
                {
                    TriSysSDK.Grid.Persistence.ColumnVisibility(true, e);
                },
                columnHide: function (e)
                {
                    TriSysSDK.Grid.Persistence.ColumnVisibility(false, e);
                },
                columnReorder: function (e)
                {
                    TriSysSDK.Grid.Persistence.ColumnReOrder(e);
                },
                columnResize: function (e)
                {
                    TriSysSDK.Grid.Persistence.ColumnResize(e);
                },
                dataBound: onDataBound

            });

            // Bind click event to the checkbox
            var kendoGridInstance = $(sJQueryGridDiv).data("kendoGrid");
            kendoGridInstance.table.on("click", ".check_row", OnSelectRowEvent);

            // Bind only after setting the row template
            var mobileRowTemplate = gridLayout.MobileRowTemplate;
            if (TriSysApex.Pages.Index.MobileViewEnabled && mobileRowTemplate)
            {
                //var sClassName = 'k-primary';   //'TriSys-Grid-Open-Button';
                var sClassName = 'k-info-colored';

                var sTemplateHTML = '<tr data-uid="#= uid #">';
                if (gridLayout.MultiRowSelect)
                    sTemplateHTML += '<td role="gridcell"><input class="check_row" type="checkbox"></td>';

                sTemplateHTML += '<td style="display:none" role="gridcell">#: ' + gridLayout.KeyColumnName + ' #</td>' +
                        mobileRowTemplate +
                        '<td role="gridcell"><a class="k-button k-button-icontext ' + sClassName + ' k-grid-Open">Open</a></td></tr>';

                mobileRowTemplate = kendo.template(sTemplateHTML);
                kendoGridInstance.rowTemplate = mobileRowTemplate;
                kendoGridInstance.altRowTemplate = mobileRowTemplate;

                // Hide header
                //$(sJQueryGridDiv + ' .k-grid-header').hide();
                kendoGridInstance.groupable = false;
            }

            // Bind to data source now
            kendoGridInstance.setDataSource(pagingDataSource);


            function onDataBound(e)
            {
                TriSysSDK.Grid.Resize(gridLayout, sJQueryGridDiv);

                // Restore previously selected rows
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++)
                {
                    var iRecordID = view[i].id;

                    var bChecked = TriSysSDK.Grid.CheckedRowIds[iRecordID];
                    if (bChecked)
                    {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".check_row")
                            .attr("checked", "checked");
                    }
                }

                //#region Grouped Column Hack
                // Make sure grouped columns do not show as regular columns
                //var g = $(sJQueryGridDiv).data("kendoGrid");
                //if (g.columns)
                //{
                //    for (var i = 0; i < g.columns.length; i++)
                //    {
                //        g.showColumn(i);
                //    }
                //}
                //$("div.k-group-indicator").each(function (i, v)
                //{
                //    g.hideColumn($(v).data("field"));
                //});
                //#endregion Grouped Column Hack
            };

            function OnSelectRowEvent()
            {
                var checked = this.checked,
                    row = $(this).closest("tr"),
                    grid = $(sJQueryGridDiv).data("kendoGrid"),
                    dataItem = grid.dataItem(row);

                TriSysSDK.Grid.CheckedRowIds[dataItem.id] = checked;
                if (checked)
                {
                    //-select the row
                    row.addClass("k-state-selected");
                }
                else
                {
                    //-remove selection
                    row.removeClass("k-state-selected");
                }
            }
        },

        //#region Check Box Related

        CheckedRowIds: {},
        ClearCheckedRows: function ()    // TriSysSDK.Grid.ClearCheckedRows
        {
            TriSysSDK.Grid.CheckedRowIds = {};
        },

        GetSelectedRowRecordIds: function ()
        {
            var checkedRows = [];
            for (var i in TriSysSDK.Grid.CheckedRowIds)
            {
                if (TriSysSDK.Grid.CheckedRowIds[i])
                    checkedRows.push(i);
            }

            return checkedRows;
        },

        // Select/de-select all rows in the current page i.e. not all rows in all other pages
        //
        SelectAllRows: function (sGridDiv, bAll)
        {
            TriSysSDK.Grid.ClearCheckedRows();

            var sJQueryGridDiv = '#' + sGridDiv;

            // Found elsewhere, but does not do what we require
            //$(sJQueryGridDiv + " tbody input:checkbox").attr("checked", bAll);
            //return;

            var grid = $(sJQueryGridDiv).data("kendoGrid");
            var view = grid.dataSource.view();

            for (var i = 0; i < view.length; i++)
            {
                var iRecordID = view[i].id;

                TriSysSDK.Grid.CheckedRowIds[iRecordID] = bAll;

                if (bAll)
                {
                    grid.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected")
                        .find(".check_row")
                        .attr("checked", "checked");
                }
                else
                {
                    grid.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .removeClass("k-state-selected")
                        .find(".check_row")
                        .removeAttr("checked");
                }
            }

            if (bAll)
            {
                // Have to refresh after selecting all because a clear all prevents checkboxes from being set
                grid.refresh();
            }
        },

        AddCheckBoxForMultiRowSelect: function (gridLayout)
        {
            if (gridLayout.MultiRowSelect)
            {
                var sCheckRowField = 'check_row';
                var bFieldExists = TriSysSDK.Grid.doesFieldExistAlreadyAfterLastInvocation(sCheckRowField, gridLayout.Columns);
                if (!bFieldExists)
                {
                    gridLayout.Columns.splice(0, 0,
                    {
                        field: sCheckRowField, title: " ", width: 30, filterable: false, sortable: false,
                        template: "<input class='" + sCheckRowField + "' type='checkbox' />"
                    });
                }
            }
        },

        //#endregion Check Box Related

        AddRowOpenButtonForTouchDeviceSupport: function (gridLayout)
        {
            if (gridLayout.DrillDownFunction)
            {
                //var sClassName = 'k-primary';   //'TriSys-Grid-Open-Button';
                var sClassName = 'k-info-colored';
                var bFieldExists = TriSysSDK.Grid.doesFieldExistAlreadyAfterLastInvocation(null, gridLayout.Columns, sClassName);

                if (!bFieldExists)
                {
                    // Add a drill down button to the right of the grid for touch devices
                    gridLayout.Columns.push({ command: { className: sClassName, text: "Open", click: openButtonClick }, title: " ", width: "80px" });
                }
            }

            function openButtonClick(e)
            {
                e.preventDefault();

                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                gridLayout.DrillDownFunction(dataItem);
            }

        },

        // For some yet unknown reason, some dynamic grid columns get left lying around in memory!
        doesFieldExistAlreadyAfterLastInvocation: function (sFieldName, columns, sCommandClassName)
        {
            for (var i = 0; i < columns.length; i++)
            {
                var col = columns[i];
                if (sFieldName)
                {
                    if (col.field == sFieldName)
                        return true;
                }
                if (sCommandClassName)
                {
                    var command = col.command;
                    if (command)
                    {
                        if (command.className == sCommandClassName)
                            return true;
                    }
                }
            }
            return false;
        },

        //#region TriSysSDK.Grid.Filtering
        Filtering:
        {
            FieldOperatorValueToSQL: function (ctlGrid, sFieldName, sGridOperator, objValue)     // TriSysSDK.Grid.Filtering.FieldOperatorValueToSQL
            {
                var sQuoter = '';
                var sFieldType = ctlGrid.dataSource.reader.model.fields[sFieldName].type;
                switch (sFieldType)
                {
                    case 'string':
                        sQuoter = "'";
                        break;

                    case 'date':
                        if (objValue)
                        {
                            objValue = moment(objValue).format('YYYY-MM-DD');
                            sQuoter = "'";
                        }
                        break;
                }

                switch (sGridOperator)
                {
                    case 'eq':
                        return sFieldName + " = " + sQuoter + objValue + sQuoter;

                    case 'neq':
                        return sFieldName + " <> " + sQuoter + objValue + sQuoter;

                    case 'contains':
                        return sFieldName + " like " + sQuoter + "%" + objValue + "%" + sQuoter;

                    case 'doesnotcontain':
                        return sFieldName + " not like " + sQuoter + "%" + objValue + "%" + sQuoter;

                    case 'startswith':
                        return sFieldName + " like " + sQuoter + objValue + "%" + sQuoter;

                    case 'endswith':
                        return sFieldName + " like " + sQuoter + "%" + objValue + sQuoter;

                    case 'gte':
                        return sFieldName + " >= " + sQuoter + objValue + sQuoter;

                    case 'gt':
                        return sFieldName + " > " + sQuoter + objValue + sQuoter;

                    case 'lte':
                        return sFieldName + " <= " + sQuoter + objValue + sQuoter;

                    case 'lt':
                        return sFieldName + " < " + sQuoter + objValue + sQuoter;
                }
            }
        },
        //#endregion TriSysSDK.Grid.Filtering

        //#region TriSysSDK.Grid.Persistence

        // Save/Restore the grid layout each time this grid is changed/loaded
        Persistence:
        {
            ColumnVisibility: function (bShow, e)
            {
                TriSysApex.Logging.LogMessage("Column " + (bShow ? "Show" : "Hide") + ": " + e.column.field);
            },

            ColumnReOrder: function (e)
            {
                TriSysApex.Logging.LogMessage("Column Reorder: " + e.column.field + " from " + e.oldIndex + " to " + e.newIndex);
            },

            ColumnResize: function (e)
            {
                TriSysApex.Logging.LogMessage("Column Resize: " + e.column.field + " from " + e.oldWidth + " to " + e.newWidth);
            },

            Filters: function (stuff)
            {
                // TODO: Filters
            },

            Sorting: function (stuff)
            {
                // TODO: Sorting
            }

        },
        //#endregion TriSysSDK.Grid.Persistence

        //#region Grid Resizing
        ResizingEventHandler: function (gridLayout, sJQueryGridDiv)
        {
            $(window).resize(function ()
            {
                TriSysSDK.Grid.Resize(gridLayout, sJQueryGridDiv);
            });
        },

        Resize: function (gridLayout, sJQueryGridDiv)
        {
            var bResizeAlgorithm = false;
            if (bResizeAlgorithm)
            {
                // Grid resize
                var divy = $(sJQueryGridDiv);
                if (divy.length && TriSysApex.Pages.Index.FormContentHeight > 0)
                {
                    //var pos = divy.offset();
                    var pos = divy.position();
                    var lTop = pos.top;
                    var lHeight = TriSysApex.Pages.Index.FormContentHeight - lTop;
                    TriSysApex.Logging.LogMessage("lTop=" + lTop + ', height=' + lHeight +
                            ', TriSysApex.Pages.Index.FormContentHeight=' + TriSysApex.Pages.Index.FormContentHeight);

                    //$(sJQueryDiv).height(lHeight);

                    var gridElement = $(sJQueryGridDiv),
                        dataArea = gridElement.find(".k-grid-content"),
                        gridHeight = gridElement.innerHeight(),
                        otherElements = gridElement.children().not(".k-grid-content"),
                        otherElementsHeight = 0;
                    otherElements.each(function ()
                    {
                        otherElementsHeight += $(this).outerHeight();
                    });
                    dataArea.height(lHeight - otherElementsHeight);
                }
            }

            TriSysSDK.Grid.ColumnVisibilityForMobileDevices(gridLayout, sJQueryGridDiv);
        },

        // If we switch between mobile and desktop, hide/show columns accordingly.
        // This needs further work to work correctly, but as we may use mobile widgets, we may not have to worry?
        ColumnVisibilityForMobileDevices: function (gridLayout, sJQueryGridDiv)
        {
            var mobileVisibleColumns = gridLayout.MobileVisibleColumns;
            if (mobileVisibleColumns)
            {
                var grid = $(sJQueryGridDiv).data("kendoGrid");
                var bMobile = TriSysApex.Pages.Index.MobileViewEnabled;

                try
                {
                    if (!grid.columns)
                        return;
                }
                catch (ex)
                {
                    return;
                }

                var iStartColumnIndex = (gridLayout.MultiRowSelect ? 1 : 0);
                var iEndColumnIndex = (gridLayout.DrillDownFunction ? grid.columns.length - 1 : grid.columns.length);
                for (var iAllColumns = iStartColumnIndex; iAllColumns < iEndColumnIndex; iAllColumns++)
                {
                    var sColumnName = gridLayout.Columns[iAllColumns].field;
                    var bHidden = gridLayout.Columns[iAllColumns].hidden;
                    var bFound = !bMobile;

                    if (bMobile)
                    {
                        for (var i = 0; i < mobileVisibleColumns.length; i++)
                        {
                            var sColumnField = mobileVisibleColumns[i].field;
                            var sColumnTitle = mobileVisibleColumns[i].title;

                            if (sColumnName == sColumnField)
                            {
                                bFound = true;
                                if (sColumnTitle)
                                {
                                    // Set explicitly in grid header
                                    $(sJQueryGridDiv + " thead [data-field=" + sColumnName + "] .k-link").html(sColumnTitle);
                                }
                            }
                        }
                    }

                    if (bHidden || !bFound)
                        grid.hideColumn(sColumnName);
                    else
                        grid.showColumn(sColumnName);
                }

                // Did attempt to get grid to change behaviour, but too difficult as KendoUI not mature enough!
                //#region Failed attempt at dynamic mobile columns
                return;
                var mobileRowTemplate = gridLayout.MobileRowTemplate;
                if (bMobile && mobileRowTemplate)
                {
                    //grid.options.rowTemplate = mobileRowTemplate;
                    grid.rowTemplate = mobileRowTemplate;
                    grid.altRowTemplate = mobileRowTemplate;

                    grid.thead.hide();
                    grid.groupable = false; // not workie

                    // Does not work:
                    //grid.dataSource.query({
                    //    page: 1,
                    //    skip: 0,
                    //    pageSize: TriSysApex.UserOptions.RecordsPerPage
                    //});

                    // Does not work
                    //grid.dataSource.page(1);

                    // Does work but leave spinner
                    //grid.dataSource.read();

                    //// hide loading indicator
                    //setTimeout(function ()
                    //{
                    //    //kendo.ui.progress(grid, false);
                    //}, 1000);


                    var tmpDataSource = grid.dataSource;

                    try
                    {
                        grid.data().kendoGrid.destroy();
                        grid.empty();
                    }
                    catch (err)
                    {
                        // Perhaps grid does not exist yet?
                    }

                    grid.setDataSource(tmpDataSource); //hell breaks loose

                    //grid.dataSource = tmpDataSource;

                    //grid.refresh();

                    //grid.options.mobile = true;
                }
                //#endregion Failed attempt at dynamic mobile columns
            }
        },

        //#endregion Grid Resizing

        //#region Grid Contents

        // We have to enumerate through the entire grid to calculate our grand total.
        // Could not find an easier KendoUI friendly way to do this!
        CalculatedColumnsGrandTotal: function (divTag, sCalculateColumnFieldPrefix)
        {
            var ctlGrid = $("#" + divTag).data("kendoGrid");
            if (ctlGrid)
            {
                var fTotal = 0;
                var gridData = ctlGrid.dataSource._data;
                var gridColumns = ctlGrid.columns;
                for (var iCol = 0; iCol < gridData.length; iCol++)
                {
                    var colData = gridData[iCol];

                    for (var iPeriodCol = 0; iPeriodCol < gridColumns.length; iPeriodCol++)
                    {
                        var column = gridColumns[iPeriodCol];
                        var sFieldName = column.field;
                        if (sFieldName.indexOf(sCalculateColumnFieldPrefix) == 0)   // Only interested in these columns
                        {
                            var fValue = colData[sFieldName];
                            fTotal += fValue;
                        }
                    }
                }
                return fTotal;
            }
        },

        // Get the data from the grid and let the caller decipher it for transmission back home.
        // Called by timesheet page when candidate saving their time.
        ReadGridContents: function (divTag)
        {
            var ctlGrid = $("#" + divTag).data("kendoGrid");
            if (ctlGrid)
            {
                var gridRows = ctlGrid.dataSource._data;
                var gridColumns = ctlGrid.columns;

                var gridDataObject = {
                    rows: gridRows,
                    columns: gridColumns
                };

                return gridDataObject;
            }
        }
        //#endregion Grid Contents

    }, // End of TriSysSDK.Grid namespace
    //#endregion TriSysSDK.Grid

    //#region TriSysSDK.CShowForm
    // Called CShowForm to identify it with the original design of the meta database classes
    CShowForm:
    {
        fieldDescriptions: function (sEntityName)
        {
            // The array of field descriptions from the cache
            var fieldDescriptionArray = [];

            // Get from the cache
            var cachedFieldDescriptions = TriSysApex.Cache.FieldDescriptions();
            for (var i = 0; i < cachedFieldDescriptions.length; i++)
            {
                var fd = cachedFieldDescriptions[i];
                if (fd.TableName == sEntityName || fd.TableName == sEntityName + 'ConfigFields')
                    fieldDescriptionArray.push(fd);
            }

            return fieldDescriptionArray;
        },

        // Get a list of field description values for the current set of
        // field descriptions and on-form fields which exist
        getFieldDescriptionValuesList: function (sEntityName)
        {
            // Get the field descriptions for this entity only
            var fieldDescriptions = TriSysSDK.CShowForm.fieldDescriptions(sEntityName);

            // Enumerate through the form to get fields which are available
            var fieldDescriptionValues = [];
            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fd = fieldDescriptions[i];
                var sFieldID = fd.TableName + "_" + fd.TableFieldName;
                var bFieldOnForm = TriSysApex.Forms.doesFieldExistInDOM(sFieldID);

                // Test: Override this to get all
                bFieldOnForm = true;
                // End of test

                if (bFieldOnForm)
                {
                    var fdValue = {
                        'FieldId': fd.FieldId,
                        'Value': fd.Value
                    };
                    fieldDescriptionValues.push(fdValue);
                }
            }

            return fieldDescriptionValues;
        },

        // Idea was to take a copy of the field descriptions, but now I think I should
        // simply keep them cached and overwrite each time?
        //
        copyFieldDescriptionValuesListToFieldDescriptions: function (sEntityName, fieldDescriptionValues)
        {
            var fieldDescriptions = TriSysSDK.CShowForm.fieldDescriptions(sEntityName);

            // Enumerate through the form to get fields which are available
            for (iField = 0; iField < fieldDescriptions.length; iField++)
            {
                var fd = fieldDescriptions[iField];

                for (iFieldValue = 0; iFieldValue < fieldDescriptionValues.length; iFieldValue++)
                {
                    var fdValue = fieldDescriptionValues[iFieldValue];
                    if (fdValue.FieldId == fd.FieldId)
                    {
                        fd.Value = fdValue.Value;
                        fd.DisplayValue = fdValue.DisplayValue;
                    }
                }
            }

            // Save a copy of these field descriptions for the current form
            TriSysApex.Cache.populateFormFieldDescriptions = fieldDescriptions;

            // Return all populated field descriptions
            return fieldDescriptions;
        },

        // Use the entity model to read data for the specified entity record
        // via an asynchronous web service.
        //
        readEntityFormDataFromServer: function (sEntityName, lRecordId, fnCallbackToPopulate)
        {
            // Save in memory for updates etc..
            TriSysSDK.CurrentRecord.EntityName = sEntityName;
            TriSysSDK.CurrentRecord.RecordId = lRecordId;

            // Create a data packet comprising only the fields we need
            var dataPacket = {
                'EntityName': sEntityName,
                'EntityId': lRecordId
            };

            var payloadObject = {};

            payloadObject.URL = 'Entity/ReadEntity';

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                TriSysApex.UI.HideWait();

                var JSONdataset = data;

                if (JSONdataset)
                {
                    if (JSONdataset.Success)
                    {
                        // Convert values only into full field descriptions so that callback
                        // function can quickly write to screen
                        var fieldDescriptions =
                            TriSysSDK.CShowForm.copyFieldDescriptionValuesListToFieldDescriptions(
                                sEntityName, JSONdataset.Entity.FieldDescriptionValues);

                        // Enumerate through all fields and write each field
                        if (fieldDescriptions)
                            TriSysSDK.CShowForm.populateFieldsFromFieldDescriptions(fieldDescriptions);

                        // Finally call back to the form so that it can do any tidying up or alerting
                        fnCallbackToPopulate(lRecordId, fieldDescriptions);
                    }
                    else
                        TriSysApex.UI.errorAlert(JSONdataset.ErrorMessage);
                }
                else
                {
                    sDisplay = "No records found.";
                }

                return true;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.ErrorHandlerRedirector('readEntityFormDataFromServer: ', request, status, error);
            };

            // Let user know we are at work...
            TriSysApex.UI.ShowWait(null, "Reading " + sEntityName + "...");

            // Do the asynchronous call
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        writeEntityFormDataToServer: function (sEntityName, lRecordId, fieldDescriptions, fnCallbackOnSave)
        {
            // Compile a smaller subset comprising only the field ids and values we require in
            // order to keep the transport small
            var fieldDescriptionValues = [];
            if (fieldDescriptions)
            {
                for (var i = 0; i < fieldDescriptions.length; i++)
                {
                    var fd = fieldDescriptions[i];
                    var fdValue =
                    {
                        'FieldId': fd.FieldId,
                        'Value': fd.Value
                    };
                    fieldDescriptionValues.push(fdValue);
                }
            }

            // Create a data packet comprising only the fields we need
            var dataPacket = {
                'EntityName': sEntityName,
                'EntityId': lRecordId,
                'FieldDescriptionValues': fieldDescriptionValues
            };

            var payloadObject = {};

            var sAPIFunction = (lRecordId > 0 ? 'UpdateEntity' : 'CreateEntity');
            payloadObject.URL = 'Entity/' + sAPIFunction;

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                TriSysApex.UI.HideWait();

                var CCreateReadUpdateDeleteOperation = data;
                var sDisplay = null;

                if (CCreateReadUpdateDeleteOperation)
                {
                    if (CCreateReadUpdateDeleteOperation.Success)
                    {
                        // Let user know that data updated.
                        sDisplay = "Successfuly updated " + sEntityName;

                        // Callback on save
                        if (fnCallbackOnSave)
                        {
                            fnCallbackOnSave(CCreateReadUpdateDeleteOperation.RecordId);
                            return;
                        }

                    } else
                        sDisplay = CCreateReadUpdateDeleteOperation.ErrorMessage;
                } else
                {
                    sDisplay = "No data returned.";
                }

                TriSysApex.UI.ShowMessage(sDisplay);

                return true;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.ErrorHandlerRedirector('TriSysSDK.CShowForm.writeEntityFormDataToServer: ', request, status, error);
            };

            // Let user know we are at work...
            TriSysApex.UI.ShowWait(null, "Saving " + sEntityName + "...");

            // Do the asynchronous call
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        // Enumerate through the field descriptions and simply write to screen
        populateFieldsFromFieldDescriptions: function (fieldDescriptions)
        {
            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fieldDescription = fieldDescriptions[i];
                var sFieldID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;

                if (TriSysApex.Forms.doesFieldExistInDOM(sFieldID))
                {
                    try
                    {
                        TriSysSDK.CShowForm.writeFieldDescriptionValue(fieldDescription);
                    }
                    catch (ex)
                    {
                        var sError = ex;
                        // TODO: Report error
                        alert(ex + ': ' + sFieldID);
                    }
                }
            }

            // Turn editing off on form load
            TriSysApex.Forms.InitialiseFormEditing();
        },

        // Return all field descriptions for this form which are present in the DOM
        getFieldDescriptionsInDOM: function (sEntityName)
        {
            // Get the field descriptions for this entity only
            var fieldDescriptions = TriSysSDK.CShowForm.fieldDescriptions(sEntityName);
            if (!fieldDescriptions)
                return null;

            var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

            return fieldDescriptionsInDOM;
        },

        // Return only field descriptions which are present in the DOM
        filterOnlyFieldDescriptionsInDOM: function (fieldDescriptions)
        {
            var fieldDescriptionsInDOM = [];

            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fieldDescription = fieldDescriptions[i];
                var sFieldID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;

                if (TriSysApex.Forms.doesFieldExistInDOM(sFieldID))
                {
                    try
                    {
                        var bFound = false;
                        $.each(fieldDescriptionsInDOM, function (index, fd)
                        {
                            if (fd.TableName == fieldDescription.TableName && fd.TableFieldName == fieldDescription.TableFieldName)
                                bFound = true;
                        });

                        if (!bFound)
                            fieldDescriptionsInDOM.push(fieldDescription);
                    }
                    catch (ex)
                    {
                        var sError = ex;
                        // TODO: Report error
                        alert(ex + ': ' + sFieldID);
                    }
                }
            }

            return fieldDescriptionsInDOM;
        },

        // Move a specified uploaded file to a suitable server in the cloud and return path to it for use e.g. display an image.
        MoveUploadedTemporaryFileReference: function (sEntityName, lRecordId, sTableFieldName, sServerFilePath, fnCallback)
        {
            var dataPacket = {
                'TableName': sEntityName,
                'TableFieldName': sTableFieldName,
                'RecordId': lRecordId,
                'TemporaryFilePath': sServerFilePath
            };

            var payloadObject = {};

            payloadObject.URL = 'Entity/MoveUploadedTemporaryFileReference';

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                var JSONdataset = data;

                if (JSONdataset)
                {
                    if (JSONdataset.Success)
                        fnCallback(JSONdataset.URL);
                }

                return true;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.ErrorHandlerRedirector('MoveUploadedTemporaryFileReference: ', request, status, error);
            };

            // Do the asynchronous call
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        // Called when the form is loaded for the first time and we are to KendoUI'se the field widgets.
        //
        InitialiseFields: function (sFormName)
        {
            var fieldDescriptions = TriSysSDK.CShowForm.getFieldDescriptionsInDOM(sFormName);
            if (!fieldDescriptions)
                return;

            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fieldDescription = fieldDescriptions[i];

                TriSysSDK.CShowForm.InitialiseFieldWidget(fieldDescription);
            }
        },

        InitialiseFieldWidget: function (fieldDescription)
        {
            var sFieldID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
            var sJQueryID = "#" + sFieldID;

            switch (fieldDescription.FieldTypeId)
            {
                case TriSysSDK.Controls.FieldTypes.Text: // Text
                    if (fieldDescription.Lookup && sFieldID != "Contact_Users")
                        TriSysSDK.CShowForm.skillCombo(sFieldID, fieldDescription.TableFieldName, null, {});
                    break;

                case TriSysSDK.Controls.FieldTypes.Date: // Date
                    // create DatePicker from input HTML element
                    TriSysSDK.CShowForm.datePicker(sFieldID);
                    break;

                case TriSysSDK.Controls.FieldTypes.List:    // List
                case TriSysSDK.Controls.FieldTypes.MultiSelectList:    // MultiSelectList
                    TriSysSDK.CShowForm.skillList(sFieldID, fieldDescription.TableFieldName);
                    break;

                case TriSysSDK.Controls.FieldTypes.CurrencyAmountPeriod:    // CurrencyAmountPeriod
                    TriSysSDK.Controls.CurrencyAmountPeriod.Load(sFieldID, fieldDescription);
                    break;

                case TriSysSDK.Controls.FieldTypes.FileReference:    // File Reference
                case TriSysSDK.Controls.FieldTypes.Image:            // Image (file ref)
                    TriSysSDK.Controls.FileReference.Load(sFieldID, fieldDescription);
                    break;

                case TriSysSDK.Controls.FieldTypes.JobTitle: // Job Title
                    TriSysSDK.CShowForm.jobTitleCombo(sFieldID, null, {});
                    break;

                case TriSysSDK.Controls.FieldTypes.Integer:
                case TriSysSDK.Controls.FieldTypes.Float:
                case TriSysSDK.Controls.FieldTypes.Currency:
                case TriSysSDK.Controls.FieldTypes.Percent:
                    TriSysSDK.Controls.NumericSpinner.Initialise(sFieldID, fieldDescription);
                    break;
            }

            // Entity Type Combo
            if (fieldDescription.TableFieldName == 'Type' && fieldDescription.FieldTypeId == TriSysSDK.Controls.FieldTypes.SkillCombo)
            {
                TriSysSDK.CShowForm.entityTypeCombo(fieldDescription.TableName, sFieldID, null, {});
            }

            switch (fieldDescription.TableName)
            {
                case 'Contact':
                    switch (fieldDescription.TableFieldName)
                    {
                        case 'Priority':
                            TriSysSDK.CShowForm.ContactPriorityCombo(sFieldID);
                            break;

                        case 'OwnerUser':
                            TriSysSDK.CShowForm.UserCombo(sFieldID);
                            break;

                        case 'Users':
                            TriSysSDK.CShowForm.MultiUserCombo(sFieldID);
                            break;
                    }
            }
        },

        // This function understands all field types and is called to display the value
        // of the field description into the form field on the page.
        //
        writeFieldDescriptionValue: function (fieldDescription)
        {
            var sFieldID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
            var sJQueryID = "#" + sFieldID;
            var htmlValue = TriSysSDK.CShowForm.getFieldDescriptionValue(fieldDescription);
            var dataValue = htmlValue;
            var bMandatory = true;
            var bEditable = true;
            var editableStruct = null;
            var bWriteToField = true;

            switch (fieldDescription.FieldTypeId)
            {
                case TriSysSDK.Controls.FieldTypes.Text: // Text
                    var bConvertTextToHTML = false;
                    if (htmlValue && bConvertTextToHTML)
                        htmlValue = htmlValue.replace(/\r\n/g, "<br />\r\n");
                    bMandatory = false;

                    if (fieldDescription.Lookup)
                    {
                        TriSysSDK.CShowForm.SetTextInCombo(sFieldID, htmlValue);
                        bWriteToField = false;
                    }
                    break;

                case TriSysSDK.Controls.FieldTypes.Date: // Date
                    TriSysSDK.CShowForm.setDatePickerValue(sFieldID, htmlValue);
                    bMandatory = false;
                    break;

                case TriSysSDK.Controls.FieldTypes.List:    // List
                case TriSysSDK.Controls.FieldTypes.MultiSelectList:    // MultiSelectList
                    TriSysSDK.CShowForm.SetSkillsInList(sFieldID, htmlValue);
                    break;

                case TriSysSDK.Controls.FieldTypes.CurrencyAmountPeriod:    // CurrencyAmountPeriod
                    TriSysSDK.Controls.CurrencyAmountPeriod.SetCurrencyAmountPeriod(sFieldID, htmlValue);
                    break;

                case TriSysSDK.Controls.FieldTypes.JobTitle: // Job Title
                    TriSysSDK.CShowForm.SetTextInCombo(sFieldID, htmlValue);
                    bWriteToField = false;
                    bMandatory = false;
                    break;

                case TriSysSDK.Controls.FieldTypes.FileReference:
                case TriSysSDK.Controls.FieldTypes.Image:
                    TriSysSDK.Controls.FileReference.SetFile(sFieldID, htmlValue);
                    break;

                case TriSysSDK.Controls.FieldTypes.Integer:
                case TriSysSDK.Controls.FieldTypes.Float:
                case TriSysSDK.Controls.FieldTypes.Currency:
                case TriSysSDK.Controls.FieldTypes.Percent:
                    TriSysSDK.Controls.NumericSpinner.SetValue(sFieldID, htmlValue);
                    break;

            };

            if (bWriteToField)
            {
                // Write value
                //$('#' + sFieldID).html(htmlValue);
                $(sJQueryID).val(htmlValue);
                //document.getElementById(sFieldID).setAttribute("data-value", dataValue);
            }

            // KendoUI fields

            // Entity Type Combo
            if (fieldDescription.TableFieldName == 'Type' && fieldDescription.FieldTypeId == TriSysSDK.Controls.FieldTypes.SkillCombo)
            {
                TriSysSDK.CShowForm.SetTextInCombo(sFieldID, htmlValue);
            }


            // Contact special fields
            switch (fieldDescription.TableName)
            {
                case 'Contact':
                    switch (fieldDescription.TableFieldName)
                    {
                        case 'ContactMale':
                            if (!editableStruct) editableStruct = {};
                            TriSysSDK.CShowForm.contactSexFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                            break;

                        case 'ContactTitle':
                            if (!editableStruct) editableStruct = {};
                            TriSysSDK.CShowForm.contactTitleFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                            break;

                        case 'ContactMarried':
                            if (!editableStruct) editableStruct = {};
                            TriSysSDK.CShowForm.contactMarriedFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                            break;
                    }
                    break;

                case 'ContactConfigFields':
                    switch (fieldDescription.TableFieldName)
                    {
                        //case 'TypeOfWorkRequired':
                        //    if (!editableStruct) editableStruct = {};
                        //    alert("I am a twerp!");
                        //    TriSysSDK.CShowForm.contactTypeOfWorkFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                        //    bMandatory = false;
                        //    break;
                    }
                    break;
            }


            // TODO: Editable and Bootstrap
            return;

            // Legacy metronic stuff

            // Bootstrap editing for each field
            if (bEditable)
            {
                // TriSys SDK Attributes/Tags added to each field as desired
                var sPlacement = $('#' + sFieldID).attr('trisys-placement');
                var sButtons = $('#' + sFieldID).attr('trisys-showbuttons');

                if (sPlacement)
                {
                    if (!editableStruct) editableStruct = {};
                    editableStruct.placement = sPlacement;
                }

                if (sButtons)
                {
                    if (!editableStruct) editableStruct = {};
                    editableStruct.showbuttons = sButtons;
                }

                // Entity Type Combo
                if (fieldDescription.TableFieldName == 'Type' && fieldDescription.FieldTypeId == TriSysSDK.Controls.FieldTypes.SkillCombo)
                {
                    TriSysSDK.CShowForm.entityTypeCombo(fieldDescription.TableName, sFieldID, htmlValue, {});
                }

                // Contact special fields
                switch (fieldDescription.TableName)
                {
                    case 'Contact':
                        switch (fieldDescription.TableFieldName)
                        {
                            case 'ContactMale':
                                if (!editableStruct) editableStruct = {};
                                TriSysSDK.CShowForm.contactSexFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                                break;

                            case 'ContactTitle':
                                if (!editableStruct) editableStruct = {};
                                TriSysSDK.CShowForm.contactTitleFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                                break;

                            case 'ContactMarried':
                                if (!editableStruct) editableStruct = {};
                                TriSysSDK.CShowForm.contactMarriedFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                                break;
                        }
                        break;

                    case 'ContactConfigFields':
                        switch (fieldDescription.TableFieldName)
                        {
                            case 'TypeOfWorkRequired':
                                if (!editableStruct) editableStruct = {};
                                TriSysSDK.CShowForm.contactTypeOfWorkFieldEditableProperties(sFieldID, htmlValue, editableStruct);
                                bMandatory = false;
                                break;
                        }
                        break;
                }

                // Mandatory fields force user to enter data or choose a drop down
                if (bMandatory)
                {
                    if (!editableStruct) editableStruct = {};

                    editableStruct.validate = function (value)
                    {
                        if ($.trim(value) == '') return 'This field is required';
                    };
                }



                // Set the editable properties of the field
                if (editableStruct)
                    $('#' + sFieldID).editable(editableStruct);
                else
                    $('#' + sFieldID).editable();
            }
        },

        // Each field is analysed to get the correct value or display value
        getFieldDescriptionFromCollection: function (fieldDescriptions, sTableName, sTableFieldName)
        {
            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fieldDescription = fieldDescriptions[i];
                if (fieldDescription.TableName == sTableName && fieldDescription.TableFieldName == sTableFieldName)
                    return fieldDescription;
            }
        },

        // Each field is analysed to get the correct value or display value
        getFieldDescriptionValue: function (fieldDescription)
        {
            var fieldNamesForDisplayValue = [
                "Contact_CompanyName",
                "Contact_CompanyAddress"
            ];

            var sFieldID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;

            for (var i = 0; i < fieldNamesForDisplayValue.length; i++)
            {
                var sFieldName = fieldNamesForDisplayValue[i];
                if (sFieldName == sFieldID)
                    return fieldDescription.DisplayValue;

                switch (fieldDescription.FieldTypeId)
                {
                    case TriSysSDK.Controls.FieldTypes.JobTitle:
                    case TriSysSDK.Controls.FieldTypes.CurrencyAmountPeriod:
                        return fieldDescription.DisplayValue;
                        break;
                }
            }

            return fieldDescription.Value;
        },

        entityTypeCombo: function (sEntityName, sFieldID, sType, editableStruct)
        {
            // Find the matching type in the cache
            var iTypeIndex = 0;
            var iEntityTypeIndex = -1;
            var sourceTypes = [];
            var entityTypes = TriSysApex.Cache.EntityTypes();
            for (var i = 0; i < entityTypes.length; i++)
            {
                var entityType = entityTypes[i];
                if (entityType.EntityName == sEntityName)
                {
                    iEntityTypeIndex += 1;

                    var sourceStruct = {
                        value: entityType.EntityTypeId,
                        text: entityType.Name
                    };
                    sourceTypes.push(sourceStruct);

                    if (entityType.Name == sType)
                        iTypeIndex = iEntityTypeIndex;
                }
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceTypes,
                index: iTypeIndex,
                change: onChange
            });

            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            try { dropdownlist.select(iTypeIndex); } catch (err) { }

            function onChange()
            {
                var value = $('#' + sFieldID).val();
                //TriSysApex.UI.ShowMessage(value);
            };

        },

        ContactPriorityCombo: function (sFieldID)
        {
            var sourcePriorities = [];
            var priorities = TriSysApex.Cache.ContactPriorities();
            for (var i = 0; i < priorities.length; i++)
            {
                var CContactPriorityColour = priorities[i];
                var sourceStruct = {
                    value: CContactPriorityColour.ContactPriorityId,
                    text: CContactPriorityColour.ContactPriority
                };
                sourcePriorities.push(sourceStruct);
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourcePriorities,
                index: 0
            });
        },

        UserCombo: function (sFieldID)
        {
            var sourceUsers = TriSysSDK.CShowForm.UserCacheDataSource();

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceUsers,
                index: 0
            });
        },

        UserCacheDataSource: function ()
        {
            var sourceUsers = [];
            $.each(TriSysApex.Cache.Users(), function (index, user)
            {
                var sourceStruct = {
                    value: user.UserId,
                    text: user.FullName
                };
                sourceUsers.push(sourceStruct);
            });

            return sourceUsers;
        },

        MultiUserCombo: function (sFieldID)
        {
            var sourceUsers = TriSysSDK.CShowForm.UserCacheDataSource();
            TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sFieldID, sourceUsers);
        },

        entityTypeMultiSelectCombo: function (sEntityName, sFieldID)
        {
            var sourceTypes = [];
            var entityTypes = TriSysApex.Cache.EntityTypes();
            for (var i = 0; i < entityTypes.length; i++)
            {
                var entityType = entityTypes[i];
                if (entityType.EntityName == sEntityName)
                {
                    var sourceStruct = {
                        value: entityType.EntityTypeId,
                        text: entityType.Name
                    };
                    sourceTypes.push(sourceStruct);
                }
            }

            TriSysSDK.CShowForm.populateMultiSelectComboFromDataSource(sFieldID, sourceTypes);
        },

        LoadCurrencyCombo: function (sJQueryDivTag)
        {
            var currencies = TriSysApex.Cache.Currencies();
            var sourceCurrencies = [];

            for (var i = 0; i < currencies.length; i++)
            {
                var currency = currencies[i];

                var sourceStruct = {
                    value: currency.Symbol,
                    text: currency.Symbol
                };
                sourceCurrencies.push(sourceStruct);
            }

            // Set this now
            $(sJQueryDivTag).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceCurrencies,
                index: 0
            });
        },

        LoadConsultancyPeriodCombo: function (sJQueryDivTag)
        {
            var periods = TriSysApex.Cache.ConsultancyPeriods();
            var sourcePeriods = [];

            for (var i = 0; i < periods.length; i++)
            {
                var period = periods[i];

                var sourceStruct = {
                    value: period.Name,
                    text: period.Name
                };
                sourcePeriods.push(sourceStruct);
            }

            // Set this now
            $(sJQueryDivTag).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourcePeriods,
                index: 0
            });
        },

        skillCombo: function (sFieldID, sCategory, sSkill, editableStruct)
        {
            // Find the matching skill in the cache
            var iSkillIndex = 0;
            var iSkillCategoryCounter = 0;
            var sourceSkills = [];
            var skills = TriSysApex.Cache.Skills();
            for (var i = 0; i < skills.length; i++)
            {
                var skill = skills[i];

                if (skill.Category == sCategory)
                {
                    iSkillCategoryCounter += 1;
                    var sourceStruct = {
                        value: skill.SkillId,
                        text: skill.Skill
                    };
                    sourceSkills.push(sourceStruct);

                    if (skill.Skill == sSkill)
                        iSkillIndex = iSkillCategoryCounter - 1;
                }
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceSkills,
                index: iSkillIndex,
                change: onChange
            });

            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            try { dropdownlist.select(iSkillIndex); } catch (ex) { }

            function onChange()
            {
                var value = $('#' + sFieldID).val();
                //TriSysApex.UI.ShowMessage(value);
            };

        },

        datePicker: function (sFieldId)
        {
            var sJQueryID = "#" + sFieldId;
            $(sJQueryID).kendoDatePicker({
                format: "dd MMMM yyyy",
                min: new Date(1900, 1, 1) // sets max date to Jan 1st, 2011
            });
        },

        setDatePickerValue: function (sFieldId, dtValue)
        {
            var sJQueryID = "#" + sFieldId;
            var datepicker = $(sJQueryID).data("kendoDatePicker");
            if (datepicker)
                datepicker.value(dtValue);
        },

        getDatePickerValue: function (sFieldId)
        {
            var sJQueryID = "#" + sFieldId;
            var datepicker = $(sJQueryID).data("kendoDatePicker");
            if (datepicker)
            {
                var sValue = datepicker.value();
                if (sValue)
                {
                    sValue = moment(sValue).format("YYYY/MM/DD");
                    return sValue;
                }
            }
        },

        populateComboFromListOfString: function (sDiv, lstOfString, sSelectedItem)
        {
            if (lstOfString)
            {
                var dataSource = [];

                for (var i = 0; i < lstOfString.length; i++)
                {
                    var sItem = lstOfString[i];
                    var item = { text: sItem, value: sItem };
                    dataSource.push(item);
                }

                if (dataSource.length > 0)
                {
                    TriSysSDK.CShowForm.populateComboFromDataSource(sDiv, dataSource, 0);

                    if (sSelectedItem)
                        TriSysSDK.CShowForm.SetTextInCombo(sDiv, sSelectedItem);
                }
            }
        },

        populateComboFromDataSource: function (sDiv, sourceData, iIndex)
        {
            if (!iIndex)
                iIndex = 0;

            $('#' + sDiv).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceData,
                index: iIndex
            });
        },

        populateMultiSelectComboFromDataSource: function (sDiv, sourceData)
        {
            $('#' + sDiv).kendoMultiSelect({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceData
            });
        },

        //#region Skill Lists
        skillList: function (sFieldID, sCategory)
        {
            // Find the matching skill in the cache
            var sourceSkills = [];
            var skills = TriSysApex.Cache.Skills();
            for (var i = 0; i < skills.length; i++)
            {
                var skill = skills[i];

                if (skill.Category == sCategory)
                {
                    var sourceStruct = {
                        text: skill.Skill,
                        value: skill.SkillId
                    };
                    sourceSkills.push(sourceStruct);
                }
            }

            $('#' + sFieldID).kendoMultiSelect({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceSkills
            });

        },

        SetValueInCombo: function (sFieldID, sValue)
        {
            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            if (dropdownlist)
            {
                var ds = dropdownlist.dataSource.options.data;
                if (ds)
                {
                    dropdownlist.value(sValue);
                }
            }
        },

        SetTextInCombo: function (sFieldID, sValue)
        {
            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            if (dropdownlist)
            {
                var ds = dropdownlist.dataSource.options.data;
                if (ds)
                {
                    dropdownlist.value(null);
                    if (sValue)
                    {
                        // Find it in data source
                        for (var j = 0; j < ds.length; j++)
                            if (sValue == ds[j].text)
                            {
                                dropdownlist.value(ds[j].value);
                                break;
                            }
                    }
                }
            }
        },

        SetSkillsInList: function (sFieldID, sValue)
        {
            var skillListWidget = $('#' + sFieldID).data("kendoMultiSelect");
            if (skillListWidget)
            {
                var ds = skillListWidget.dataSource.options.data;
                if (ds)
                {
                    // Loop through data source to find text matches with data source
                    skillListWidget.value(null);
                    if (sValue)
                    {
                        var values = [];
                        var sTextArray = sValue.split(", ");
                        for (var i = 0; i < sTextArray.length; i++)
                        {
                            var sText = sTextArray[i];

                            // Find it in data source
                            for (var j = 0; j < ds.length; j++)
                            {
                                if (sText == ds[j].text)
                                    values.push(ds[j].value);
                            }
                        }

                        skillListWidget.value(values);
                    }
                }
            }
        },

        SetValuesArrayInList: function (sFieldID, textArray)
        {
            var skillListWidget = $('#' + sFieldID).data("kendoMultiSelect");
            if (skillListWidget)
            {
                var ds = skillListWidget.dataSource.options.data;
                if (ds)
                {
                    // Loop through data source to find text matches with data source
                    skillListWidget.value(null);
                    if (textArray)
                    {
                        var values = [];
                        for (var i = 0; i < textArray.length; i++)
                        {
                            var sText = textArray[i];

                            // Find it in data source
                            for (var j = 0; j < ds.length; j++)
                            {
                                if (sText == ds[j].text)
                                    values.push(ds[j].value);
                            }
                        }

                        skillListWidget.value(values);
                    }
                }
            }
        },

        GetSelectedSkillsFromList: function (sFieldID)
        {
            // Loop through data source to find text matches with data source
            var valuesArray = TriSysSDK.CShowForm.GetSelectedSkillsFromListAsArray(sFieldID);

            if (valuesArray)
            {
                var sValues = '';
                for (var i = 0; i < valuesArray.length; i++)
                {
                    var sSkillText = valuesArray[i];

                    if (sValues)
                        sValues += ", ";
                    sValues += sSkillText;
                }

                return sValues;
            }
        },

        GetSelectedSkillsFromListAsArray: function (sFieldID)
        {
            var skillListWidget = $('#' + sFieldID).data("kendoMultiSelect");
            if (skillListWidget)
            {
                var ds = skillListWidget.dataSource.options.data;
                if (ds)
                {
                    var valuesArray = skillListWidget.value();
                    //return valuesArray;
                    if (valuesArray)
                    {
                        var textArray = [];
                        for (var i = 0; i < valuesArray.length; i++)
                        {
                            var iValue = valuesArray[i];
                            for (var j = 0; j < ds.length; j++)
                            {
                                if (ds[j].value == iValue)
                                {
                                    textArray.push(ds[j].text);
                                    break;
                                }
                            }
                        }
                        return textArray;
                    }
                }
            }
        },

        addCallbackOnComboSelect: function (sFieldID, fnCallback)
        {
            function dropdownlist_change(e)
            {
                var value = this.value();
                var text = this.text();
                fnCallback(value, text);
            }
            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            dropdownlist.bind("change", dropdownlist_change);
        },
        //#endregion Skill Lists

        jobTitleCombo: function (sFieldID, sJobTitle, editableStruct)
        {
            // Find the matching job title in the cache
            var iJobTitleIndex = 0;
            var bJobTitleId = $.isNumeric(sJobTitle);
            var sourceJobTitles = [];
            var jobTitles = TriSysApex.Cache.JobTitles();
            var iMax = jobTitles.length;
            for (var i = 0; i < iMax; i++)
            {
                var jobTitle = jobTitles[i];
                var sourceStruct = {
                    value: jobTitle.JobTitleId,
                    text: jobTitle.JobTitle
                };
                sourceJobTitles.push(sourceStruct);

                if (bJobTitleId)
                {
                    if (jobTitle.JobTitleId == sJobTitle)
                        iJobTitleIndex = i;
                }
                else if (jobTitle.JobTitle == sJobTitle)
                    iJobTitleIndex = i;
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceJobTitles,
                index: iJobTitleIndex,
                change: onChange
            });

            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            try { dropdownlist.select(iJobTitleIndex); } catch(ex) { }

            function onChange()
            {
                var value = $('#' + sFieldID).val();
                //TriSysApex.UI.ShowMessage(value);
            };
            return;

            // Set this now
            $('#' + sFieldID).html(sJobTitle);
            document.getElementById(sFieldID).setAttribute("data-value", iJobTitleId);

            editableStruct.showbuttons = false;
            editableStruct.inputclass = 'm-wrap';
            editableStruct.source = sourceJobTitles;
        },

        contactTitleFieldEditableProperties: function (sFieldID, sTitle, editableStruct)
        {
            var iContactTitleIndex = 0;
            var sourceTitles = [];
            var contactTitles = TriSysApex.Cache.ContactTitles();
            for (var i = 0; i < contactTitles.length; i++)
            {
                var contactTitle = contactTitles[i];
                var sourceStruct = {
                    value: contactTitle.ContactTitleId,
                    text: contactTitle.Name
                };
                sourceTitles.push(sourceStruct);

                if (contactTitle.Name == sTitle)
                    iContactTitleIndex = i;
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceTitles,
                index: iContactTitleIndex,
                change: onChange
            });

            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            try { dropdownlist.select(iContactTitleIndex); } catch (ex) { }

            function onChange()
            {
                var value = $('#' + sFieldID).val();
                var sText = TriSysSDK.CShowForm.GetTextFromCombo(sFieldID);
                //TriSysApex.UI.ShowMessage("contactTitleFieldEditableProperties.onChange(): " + value + ', ' + sText);
            };
            return;

            // OLD
            $('#' + sFieldID).html(sTitle);
            document.getElementById(sFieldID).setAttribute("data-value", iContactTitleId);

            editableStruct.showbuttons = false;
            editableStruct.inputclass = 'm-wrap';
            editableStruct.source = sourceTitles;
        },

        contactTypeOfWorkFieldEditableProperties: function (sFieldID, sValues, editableStruct)
        {
            // Get all values from the cache
            var sourceTypes = [];
            var entityTypes = TriSysApex.Cache.EntityTypes();
            for (var i = 0; i < entityTypes.length; i++)
            {
                var entityType = entityTypes[i];
                if (entityType.EntityName == 'Requirement')
                {
                    var sourceStruct = {
                        value: entityType.EntityTypeId,
                        text: entityType.Name
                    };
                    sourceTypes.push(sourceStruct);

                    if (entityType.Name == sType)
                        iTypeId = entityType.EntityTypeId;
                }
            }

            // Split contact values
            var sTypes = null;
            if (sValues)
                sTypes = sValues.split(", ");

            // Create an array of values data structure with ID's
            var arrTypes = [];
            if (sTypes)
            {
                for (var i = 0; i < sTypes.length; i++)
                {
                    var sType = sTypes[i];
                    var iID = 0;

                    // Find the matching type
                    for (var k = 0; k < sourceTypes.length; k++)
                    {
                        var sourceStruct = sourceTypes[k];
                        if (sourceStruct.text == sType)
                            iID = sourceStruct.value;
                    }

                    // Save our values for later
                    var typeObject = {
                        type: sType,
                        Id: iID
                    };
                    arrTypes.push(typeObject);
                }
            }

            // Create the string representation
            var sDataValue = '';
            for (var i = 0; i < arrTypes.length; i++)
            {
                var typeObject = arrTypes[i];
                if (sDataValue)
                    sDataValue += ',';
                sDataValue += typeObject.Id;

            }

            // Set this now
            $('#' + sFieldID).html(sValues);
            document.getElementById(sFieldID).setAttribute("data-value", sDataValue);

            editableStruct.pk = 1;
            editableStruct.limit = 3; //sourceTypes.length;
            editableStruct.source = sourceTypes;


            //$('#chkTypeOfWorkRequired').editable({
            //    pk: 1,
            //    limit: 3,
            //    source: [{
            //        value: 1,
            //        text: 'Permanent'
            //    }, {
            //        value: 2,
            //        text: 'Contract'
            //    }, {
            //        value: 3,
            //        text: 'Temporary'
            //    }
            //    ]
            //});

            $('#' + sFieldID).on('shown', function (e, reason)
            {
                App.initUniform();
            });
        },

        contactMarriedFieldEditableProperties: function (sFieldID, bMarried, editableStruct)
        {
            var data = [{
                value: 0,
                text: 'Single'
            }, {
                value: 1,
                text: 'Married'
            }];
            var iMarried = (bMarried ? 1 : 0);
            TriSysSDK.CShowForm.dropDownComboFieldEditableProperties(sFieldID, iMarried, data, editableStruct);
        },

        countriesCombo: function (sFieldID, sDefaultCountry)
        {
            var countriesData = TriSysSDK.Countries.Load();
            if (countriesData)
            {
                var data = [];
                for (var i = 0; i < countriesData.length; i++)
                {
                    var country = countriesData[i];
                    var struct = { value: country.name, text: country.name };
                    data.push(struct);
                }

                TriSysSDK.CShowForm.dropDownComboFieldEditableProperties(sFieldID, sDefaultCountry, data, null);
            }
        },

        // Generic drop down combo: See contactMarriedFieldEditableProperties() for example of how to use
        dropDownComboFieldEditableProperties: function (sFieldID, objSelectedValue, sourceData, editableStruct)
        {
            var sText = '';
            var iValueIndex = 0;
            for (var i = 0; i < sourceData.length; i++)
            {
                var sourceDataItem = sourceData[i];
                if (sourceDataItem.value == objSelectedValue)
                    iValueIndex = i;
            }

            // Set this now
            $('#' + sFieldID).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sourceData,
                index: iValueIndex,
                change: onChange
            });

            var dropdownlist = $('#' + sFieldID).data("kendoDropDownList");
            try { dropdownlist.select(iValueIndex); } catch (err) { }

            function onChange()
            {
                var value = $('#' + sFieldID).val();
                //TriSysApex.UI.ShowMessage(value);
            };
        },

        contactSexFieldEditableProperties: function (sFieldID, objMale, editableStruct)
        {
            var iSex = -1;

            var data = [{
                value: -1,
                text: ' '
            }, {
                value: 1,
                text: 'Male'
            }, {
                value: 0,
                text: 'Female'
            }];

            if (!TriSysAPI.Operators.isEmpty(objMale))
            {
                var bMale = objMale; //TriSysAPI.Operators.stringToBoolean(objMale);
                iSex = (bMale ? 1 : 0);
            }

            TriSysSDK.CShowForm.dropDownComboFieldEditableProperties(sFieldID, iSex, data, editableStruct);
            return;

            editableStruct.prepend = "not selected";
            editableStruct.inputclass = 'm-wrap';
            editableStruct.source = [{
                value: 1,
                text: 'Male'
            }, {
                value: 2,
                text: 'Female'
            }];
            editableStruct.display = function (value, sourceData)
            {
                var colors = {
                    '': "gray",
                    1: "blue",
                    2: "pink"
                },
                    elem = $.grep(sourceData, function (o)
                    {
                        return o.value == value;
                    });

                if (elem.length)
                {
                    $(this).text(elem[0].text).css("color", colors[value]);
                } else
                {
                    $(this).empty();
                }
            };

            if (sSex && iSex)
            {
                document.getElementById(sFieldID).setAttribute("data-value", iSex);
                $('#' + sFieldID).html(sSex);
            }
        },

        EntityIdFromFieldDescriptionValues: function (sEntityName, fieldDescriptions)
        {
            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fd = fieldDescriptions[i];
                if (fd.TableName == sEntityName && fd.TableFieldName == sEntityName + "Id")
                {
                    var lRecordId = fd.Value;
                    return lRecordId;
                }
            }
        },

        // The file reference is a complex field because the value is a server g:\ path
        GetFileReference: function (sDivID)
        {
            var bCVFileRef = (sDivID.indexOf("CVFileRef") >= 0);
            if (bCVFileRef)
            {
                var cvPath = TriSysSDK.Controls.CVManager.GetCVPath(sDivID);
                if (cvPath)
                {
                    // This is a CV file manager control
                    return cvPath.DocumentPath;
                }
            }
            else
            {
                // This is a generic file reference field
                var sDocumentPath = TriSysSDK.Controls.FileReference.GetFile(sDivID);
                return sDocumentPath;
            }
        },

        // Get all field descriptions values from HTML page widgets
        //
        ReadFieldDescriptionValues: function (sEntity, bIncludeNullFields, bInEditingMode)
        {
            var fieldDescriptions = TriSysSDK.CShowForm.fieldDescriptions(sEntity);

            // Get all on-screen values for all fields
            var populatedFieldDescriptions = [];
            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fd = fieldDescriptions[i];
                var sValue = TriSysSDK.CShowForm.FieldWidgetValue(fd);

                if (sValue || bIncludeNullFields)
                {
                    if (bInEditingMode && sValue == "Empty")
                        sValue = null;

                    fd.Value = sValue;
                    fd.DisplayValue = sValue;

                    // Add to the array of data
                    populatedFieldDescriptions.push(fd);
                }
            }

            return populatedFieldDescriptions;
        },

        FieldWidgetValue: function (fieldDescription)
        {
            var sID = fieldDescription.TableName + '_' + fieldDescription.TableFieldName;
            var sJQueryDivTag = "#" + sID;

            // If DIV does not exist, return null
            if ($(sJQueryDivTag).length == 0)
                return null;

            // Get default value
            var sValue = $(sJQueryDivTag).val();

            // We want the HTML value to include <br> and other HTML characters
            //var sValue = $(sJQueryDivTag).html();

            // Bootstrap
            //var sValue = $(sJQueryDivTag).text();


            // Get from widget
            switch (fieldDescription.FieldTypeId)
            {
                case TriSysSDK.Controls.FieldTypes.Text:     // Text 
                    if (fieldDescription.Lookup)
                        sValue = TriSysSDK.CShowForm.GetTextFromCombo(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.YesNo:     // Check Box
                    //sValue = TriSysForms.ShowForm.CheckBoxValue(sID, true);
                    break;

                case TriSysSDK.Controls.FieldTypes.Date:     // Date
                    sValue = TriSysSDK.CShowForm.getDatePickerValue(sID);
                    bMandatory = false;
                    break;

                case TriSysSDK.Controls.FieldTypes.FileReference:    // File Reference
                case TriSysSDK.Controls.FieldTypes.Image:            // Image (file ref)
                    sValue = TriSysSDK.CShowForm.GetFileReference(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.List:    // List
                case TriSysSDK.Controls.FieldTypes.MultiSelectList:    // Multi Select List
                    sValue = TriSysSDK.CShowForm.GetSelectedSkillsFromList(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.CurrencyAmountPeriod:    // Currency Amount Period
                    sValue = TriSysSDK.Controls.CurrencyAmountPeriod.GetCurrencyAmountPeriod(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.JobTitle:    // Job Title
                    sValue = TriSysSDK.CShowForm.GetTextFromCombo(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.SkillCombo:    // Skill Combo
                    sValue = TriSysSDK.CShowForm.GetTextFromCombo(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.TextDropDown:
                    sValue = TriSysSDK.Controls.TextBoxDropDown.GetText(sID);
                    break;

                case TriSysSDK.Controls.FieldTypes.Integer:
                case TriSysSDK.Controls.FieldTypes.Float:
                case TriSysSDK.Controls.FieldTypes.Currency:
                case TriSysSDK.Controls.FieldTypes.Percent:
                    sValue = TriSysSDK.Controls.NumericSpinner.GetValue(sID);
                    break;

                    // More ToDo...

                default: // Everything else is a text box
                    break;
            }


            // Some fields are 'special' so need treating differently
            switch (sID)
            {
                case "Contact_ContactMarried":
                    sValue = TriSysSDK.CShowForm.GetTextFromCombo(sID);
                    sValue = (sValue == 'Married') ? true : false;
                    break;

                case "Contact_ContactMale":
                    sValue = TriSysSDK.CShowForm.GetTextFromCombo(sID);
                    sValue = (sValue == 'Female') ? false : true;
                    break;
            }

            // Return it
            return sValue;
        },

        GetTextFromCombo: function (sID)        // TriSysSDK.CShowForm.GetTextFromCombo
        {
            var sJQueryDivTag = "#" + sID;
            var dropdownlist = $(sJQueryDivTag).data("kendoDropDownList");
            sValue = dropdownlist.text();
            return sValue;
        },

        GetValueFromCombo: function (sID)        // TriSysSDK.CShowForm.GetValueFromCombo
        {
            var sJQueryDivTag = "#" + sID;
            var dropdownlist = $(sJQueryDivTag).data("kendoDropDownList");
            sValue = dropdownlist.value();
            return sValue;
        },

        ComboEnabled: function (sID, bEnabled)
        {
            var sJQueryDivTag = "#" + sID;
            $(sJQueryDivTag).kendoDropDownList({
                enable: bEnabled
            });
        },

        // Called after populating forms where certain fields require special treatment i.e. the 1% caveat conditions.
        // An example is showing vacancies to a candidate where they only need to know the city address.
        // Execute asyncronous SQL queries for each field description and populate upon return.
        //
        PopulateAsyncFieldDescriptionsWithSingletonSQLStatement: function (fieldDescriptions)
        {
            // The callback function with the 
            var fnPopulateField = function (CSQLDatabaseSearchResults, fieldDescription)
            {
                if (CSQLDatabaseSearchResults)
                {
                    if (CSQLDatabaseSearchResults.DataTable)
                    {
                        var sFieldName = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                        var sValue = TriSysSDK.Database.GetSnapshotFieldFromDataTable(CSQLDatabaseSearchResults.DataTable, sFieldName);
                        fieldDescription.Value = sValue;
                        fieldDescription.DisplayValue = sValue;
                        TriSysSDK.CShowForm.writeFieldDescriptionValue(fieldDescription);
                    }
                }
            };

            for (var i = 0; i < fieldDescriptions.length; i++)
            {
                var fd = fieldDescriptions[i];
                var sSQL = fd.SQL;

                TriSysSDK.Database.GetDataSet(sSQL, fnPopulateField, fd);
            }
        }


    }, // End of TriSysSDK.CShowForm
    //#endregion TriSysSDK.CShowForm

    //#region TriSysSDK.CurrentRecord
    CurrentRecord: {
        EntityName: null,
        RecordId: 0

    }, // End of TriSysSDK.CurrentRecord
    //#endregion TriSysSDK.CurrentRecord

    //#region TriSysSDK.Controls
    Controls:
    {
        General:
        {
            ReplaceDivTagsOfDynamicallyLoadedUserControl: function (divTag)
            {
                var sPrefix = divTag + '-';

                // Get all nodes recursively beneath this DIV and change their ID to be unique by prefixing with this DIV name
                var nodesArray = TriSysSDK.DocumentObjectModel.GetAllNodesBeneathUniqueDivID(divTag);

                if (nodesArray)
                {
                    for (var i = 0; i < nodesArray.length; i++)
                    {
                        var node = nodesArray[i];
                        if (node.id)
                        {
                            var sID = node.id;
                            if (sID.length > 0 && sID != divTag)
                                node.id = sPrefix + sID;       // e.g. ContactConfigFields_CVFileRef-CVManager_DeleteCVButton
                        }
                    }
                }
            }
        },

        // Direct mapping of field types in Version 10 code base with additional Apex-Only field types
        FieldTypes:         // TriSysSDK.Controls.FieldTypes
        {
            // Select * From FieldTypes
            Text: 1,
            Integer: 2,
            Float: 3,
            Percent: 4,
            Currency: 5,
            YesNo: 6,
            Date: 7,
            Panel: 8,
            Grid: 9,
            FileReference: 11,
            List: 13,
            Image: 14,
            EMail: 15,
            Internet: 16,
            Telephone: 17,
            User: 18,
            Contact: 19,
            Company: 20,
            CurrencyLink: 21,
            WebPage: 22,
            Label: 23,
            Graph: 24,
            RichTextBox: 25,
            Schedule: 26,
            WordProcessor: 27,
            MultiSelectList: 28,
            ProgressBar: 29,
            GridAlpha: 30,
            Application: 31,
            DocumentPreview: 32,
            Video: 33,
            ChartFX: 34,
            CurrencyAmount: 35,
            SkillTree: 36,
            PanelAlpha: 37,
            Address: 38,
            CurrencyAmountPeriod: 39,
            EntitySearchCriteria: 40,
            ActivityTypeCombo: 41,
            FolderReference: 42,
            ListView: 43,
            TreeView: 44,
            GridGamma: 45,
            JobTitle: 46,
            ChartFXASync: 47,
            FormUserControl: 48,
            SkillCombo: 49,
            Password: 50,
            DocumentEditor: 51,

            // Apex-Only Field Types
            TextDropDown: 1001
        },

        HelpHint:
        {
            Load: function (divTag, sHintText)
            {
                var sHTML = "<div class='TriSys-ctrlHelpHint'>" +
                    "<p class='TriSys-ctrlHelpHint-Text'>" + sHintText + "</p></div>";
                $('#' + divTag).html(sHTML);
            }
        },

        //#region TriSysSDK.Controls.CVManager
        CVManager:  // TriSysSDK.Controls.CVManager
        {
            // Load the specified CV manager control.
            // There are up to 5 CV managers on a contact form at any one time. (original, formatted #1, ... #4)
            //
            Load: function (divTag, bCVFileRef, iIndex)
            {
                var sPath = TriSysApex.Constants.UserControlFolder + 'ctrlCVManager.html';

                TriSysApex.FormsManager.loadPageIntoDiv(divTag, sPath,
                    function (response, status, xhr)
                    {
                        TriSysSDK.Controls.CVManager.AfterLoadEvent(divTag, bCVFileRef, iIndex);
                    });
            },

            // After the CV manager has loaded, we need to change the ID's of all DIV's
            // which are in sub nodes only, to be prefixed by the CV field name.
            AfterLoadEvent: function (divTag, bCVFileRef, iIndex)
            {
                // Get all nodes recursively beneath this DIV and change their ID to be unique by prefixing with this DIV name
                TriSysSDK.Controls.General.ReplaceDivTagsOfDynamicallyLoadedUserControl(divTag);

                var sPrefix = divTag + '-';

                var sCaption = (bCVFileRef ? "Original Candidate CV File Reference" : "Formatted CV File Reference #" + iIndex);
                var sJQueryDiv = "#" + sPrefix + "CVManager_Title";
                $(sJQueryDiv).text(sCaption);


                // Now make these widgets come alive
                sJQueryDiv = "#" + sPrefix + "CVManager_Menu";
                $(sJQueryDiv).kendoMenu({
                    openOnClick: true
                });

                var sJQueryDivFind = "#" + sPrefix + "CVManager_FindCVButton";
                $(sJQueryDivFind).kendoButton({
                    imageUrl: "images/trisys/16x16/search.ico"
                });
                $(sJQueryDivFind).click(function ()
                {
                    findCV(sJQueryDivFind);
                });

                var sJQueryDivOpen = "#" + sPrefix + "CVManager_OpenCVButton";
                $(sJQueryDivOpen).kendoButton({
                    imageUrl: "images/trisys/16x16/Folder Open File.ico"
                });
                $(sJQueryDivOpen).click(function ()
                {
                    openCV(sPrefix);
                });

                var sJQueryDivDelete = "#" + sPrefix + "CVManager_DeleteCVButton";
                $(sJQueryDivDelete).kendoButton({
                    imageUrl: "images/trisys/16x16/Remove.ico"
                });
                $(sJQueryDivDelete).click(function ()
                {
                    deleteCV(divTag);
                });

                var sJQueryDivCopy = "#" + sPrefix + "CVManager_CopyCV";
                if (bCVFileRef)
                {
                    $(sJQueryDivCopy).kendoMenu({
                        openOnClick: true,
                        dataSource:
                            [{
                                text: "Copy CV",
                                imageUrl: "images/trisys/16x16/CVAR.ico",
                                items:
                                    [{
                                        text: "Formatted CV File Reference #1",
                                        imageUrl: "images/trisys/16x16/CV Copy.ico",
                                        value: sPrefix + "CopyCV"
                                    }]
                            }],
                        select: onCVManagerCopyCVMenuSelect
                    });
                }
                else
                    $('#sJQueryDivCopy').hide();


                function openCV(sCVFieldPrefix)
                {
                    // The server path of the CV
                    var sPath = $('#' + sCVFieldPrefix + 'CVManager_FilePath').val();
                    var sName = $('#' + sCVFieldPrefix + 'CVManager_FileName').val();

                    if (!sPath)
                    {
                        TriSysApex.UI.ShowMessage("No CV specified.</br />Use the Find CV button to upload a CV.");
                        return;
                    }

                    // Get a URL of this file which we can view via an asynchronous call
                    TriSysSDK.Controls.FileManager.GetOpenFileURLFromWebService("TriSys CV Viewer", sPath, sName)
                }

                function findCV(sJQueryDiv)
                {
                    TriSysSDK.Controls.FileManager.FindFileUpload("Find CV", function (sFile) { assignUploadedCVToContactCVField(sJQueryDiv, sFile); });
                }

                // Called after user has uploaded a local file and we now wish to assign it to the
                // specified contact CV file ref field.
                //
                function assignUploadedCVToContactCVField(sJQueryDiv, sRelativeUploadFilePath)
                {
                    // Debug only
                    //alert("assignUploadedCVToContactCVField(" + sJQueryDiv + "): You uploaded and saved: " + sRelativeUploadFilePath +
                    //        " for contactId=" + TriSysSDK.CurrentRecord.RecordId + "?");

                    if (TriSysSDK.CurrentRecord.RecordId > 0)
                    {
                        // Call the Web API to copy the specified server path to the field
                        var objParts = { CVField: null, Operation: null };
                        splitFieldOperationParts(sJQueryDiv, objParts);
                        var sParts = objParts.CVField.split("_");
                        var sTableName = sParts[0];
                        var sTableFieldName = sParts[1];

                        // Call the Web API now to copy the uploaded server file for the specified contact for the specified field.
                        // Upon completion, update the on-screen field
                        var payloadObject = {};

                        payloadObject.URL = "ContactCV/MoveUploadedCVFileReference";

                        payloadObject.OutboundDataPacket = {
                            UploadedFilePath: sRelativeUploadFilePath,
                            ContactId: TriSysSDK.CurrentRecord.RecordId,
                            TableFieldName: sTableFieldName
                        };

                        payloadObject.InboundDataFunction = function (data)
                        {
                            TriSysApex.UI.HideWait();
                            var JSONdataset = data;

                            if (JSONdataset)
                            {
                                //sDisplay = JSON.stringify(JSONdataset);
                                //alert(sDisplay);
                                //return;

                                if (JSONdataset.Success)
                                {
                                    var sCVFilePath = JSONdataset.CVFolderFilePath

                                    var sDiv = objParts.CVField;
                                    TriSysSDK.Controls.CVManager.SetCVPath(sDiv, sCVFilePath);
                                    TriSysSDK.Controls.CVManager.ShowCVPath(sDiv);
                                }
                                else
                                {
                                    TriSysApex.UI.errorAlert(JSONdataset.ErrorMessage);
                                }
                            } else
                            {
                                // Something else has gone wrong!
                            }
                        };

                        payloadObject.ErrorHandlerFunction = function (request, status, error)
                        {
                            TriSysApex.UI.HideWait();
                            TriSysApex.UI.ErrorHandlerRedirector('assignUploadedCVToContactCVField: ', request, status, error);
                        };

                        TriSysApex.UI.ShowWait(null, "Renaming CV...");
                        TriSysAPI.Data.PostToWebAPI(payloadObject);
                    }
                }

                function deleteCV(sJQueryDiv)
                {
                    //TriSysApex.UI.ShowMessage("Delete CV: " + sJQueryDiv);
                    TriSysSDK.Controls.CVManager.SetCVPath(sJQueryDiv, '');
                    TriSysSDK.Controls.CVManager.ShowCVPath(sJQueryDiv);
                }

                function onCVManagerCopyCVMenuSelect(e)
                {
                    var selectedItem = TriSysApex.MobileToolbarMenu.KendoUIMenuSelectEvent(this, e);
                    if (selectedItem)
                    {
                        var sMenuItem = selectedItem.value;
                        var sToField = selectedItem.text;

                        var objParts = { CVField: null, Operation: null };
                        splitFieldOperationParts(sMenuItem, objParts);

                        switch (objParts.Operation)
                        {
                            case "CopyCV":
                                var cvPath = TriSysSDK.Controls.CVManager.GetCVPath(objParts.CVField);
                                if (cvPath)
                                {
                                    //TriSysApex.UI.ShowMessage("Copy CV " + cvPath.DocumentPath + " from " + objParts.CVField + " to " + sToField);
                                    // OK, so now we are at the sharp end of this functionality
                                    // Definately:  a. Call web service to copy file on NAS server
                                    // Then either: 
                                    //              b. Open browser .DOCX editor 
                                    //      or
                                    //              c. Convert from .DOCX to .HTML and open WYSIWYG editor
                                    //      or
                                    //              d. Use cloud storage exclusively e.g. Google Drive or Microsoft Sky Drive
                                    //      or
                                    //              e. Download CV and force local word to open. Provide the TriSys Add-In with ability to save back to Web API.
                                    //      or
                                    //              f. Open using Microsoft Office Viewer and allow download to edit and re-upload after save.
                                    //
                                    // Chose option f. because it is the easiest for us to develop and allows us to add e. when needed.

                                    var sFormattedCVField = "ContactConfigFields_FormattedCVFileRef1";
                                    var payloadObject = {};

                                    payloadObject.URL = "ContactCV/CreateFormattedCVFileReference";

                                    payloadObject.OutboundDataPacket = {
                                        OriginalCVPath: cvPath.DocumentPath,
                                        ContactId: TriSysSDK.CurrentRecord.RecordId,
                                        TableFieldName: sFormattedCVField
                                    };

                                    payloadObject.InboundDataFunction = function (data)
                                    {
                                        TriSysApex.UI.HideWait();
                                        var JSONdataset = data;

                                        if (JSONdataset)
                                        {
                                            if (JSONdataset.Success)
                                            {
                                                var sCVFilePath = JSONdataset.FormattedCVFilePath;

                                                var sDiv = objParts.CVField;
                                                TriSysSDK.Controls.CVManager.SetCVPath(sFormattedCVField, sCVFilePath);
                                                TriSysSDK.Controls.CVManager.ShowCVPath(sFormattedCVField);
                                            }
                                            else
                                            {
                                                TriSysApex.UI.errorAlert(JSONdataset.ErrorMessage);
                                            }
                                        } else
                                        {
                                            // Something else has gone wrong!
                                        }
                                    };

                                    payloadObject.ErrorHandlerFunction = function (request, status, error)
                                    {
                                        TriSysApex.UI.HideWait();
                                        TriSysApex.UI.ErrorHandlerRedirector('onCVManagerCopyCVMenuSelect: ', request, status, error);
                                    };

                                    TriSysApex.UI.ShowWait(null, "Formatting CV...");
                                    TriSysAPI.Data.PostToWebAPI(payloadObject);
                                }
                                break;
                        }
                    }
                }

                function splitFieldOperationParts(sMenuItem, objParts)
                {
                    sMenuItem = sMenuItem.replace('#', '');
                    var sParts = sMenuItem.split("-");
                    objParts.CVField = sParts[0];
                    objParts.Operation = sParts[1];
                }
            },

            SetCVPath: function (sDivID, objValue)
            {
                var cvPath = TriSysSDK.Controls.CVManager.GetCVPath(sDivID);
                if (cvPath)
                {
                    cvPath.DocumentPath = objValue;
                    return;
                }

                var newCVPath = new TriSysSDK.Controls.CVManager.CVPath(sDivID, objValue);
                TriSysSDK.Controls.CVManager.CVPaths.push(newCVPath);
            },

            GetCVPath: function (sDivID)
            {
                for (var i = 0; i < TriSysSDK.Controls.CVManager.CVPaths.length; i++)
                {
                    var cvPath = TriSysSDK.Controls.CVManager.CVPaths[i];
                    if (cvPath.FieldName == sDivID)
                        return cvPath;
                }
                return null;
            },

            ShowCVPath: function (sDivID)
            {
                // Set the path in the named DIV for this instance of this control
                var sPath = null, sName = null;
                var cvPath = TriSysSDK.Controls.CVManager.GetCVPath(sDivID);
                if (cvPath)
                {
                    sPath = cvPath.DocumentPath;
                    if (sPath)
                    {
                        var sParts = sPath.split('\\');
                        sName = sParts[sParts.length - 1];
                    }
                }
                $('#' + sDivID + '-CVManager_FileName').val(sName);
                $('#' + sDivID + '-CVManager_FilePath').val(sPath);
            },

            CVPaths: [],
            CVPath: function (sFieldName, sDocumentPath)     // Note how we use the 'new' operator in the SetCVPath() function above
            {
                this.FieldName = sFieldName;
                this.DocumentPath = sDocumentPath;
            }
        },
        //#endregion TriSysSDK.Controls.CVManager


        //#region TriSysSDK.Controls.FileManager)
        FileManager:
        {
            FindFileUpload: function (sFunction, fnCallback)
            {
                // Standard properties
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sFunction;

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlUploadFile.html';
                parametersObject.Height = 150;
                parametersObject.Resizable = false;
                parametersObject.Maximize = false;
                parametersObject.Image = 'upload';

                parametersObject.Layer = 2;     // Will appear as a popup on-top of another modal

                // File specific properties
                TriSysSDK.Controls.FileManager.SaveCallbackFunction = fnCallback;

                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonLeftText = "Save";
                parametersObject.ButtonLeftFunction = function ()
                {
                    if (!TriSysSDK.Controls.FileManager._UploadedFileRelativePath)
                    {
                        TriSysApex.UI.Notifications.Error("No File", "Please choose a local file before uploading it.");
                        return;
                    }

                    //TriSysApex.UI.CloseTopmostModalPopup();
                    TriSysSDK.Controls.FileManager.SaveCallbackFunction(TriSysSDK.Controls.FileManager._UploadedFileRelativePath);
                };

                parametersObject.ButtonRightVisible = true;
                parametersObject.ButtonRightText = "Cancel";
                parametersObject.ButtonRightFunction = function ()
                {
                    // It should be closed by default
                };

                parametersObject.OnLoadCallback = function ()
                {
                    TriSysSDK.Controls.FileManager.InitialiseFileUploadModalDialogue();
                }

                // Open the popup dialogue
                TriSysApex.UI.PopupMessage(parametersObject);
            },

            InitialiseFileUploadModalDialogue: function ()
            {
                $("#TriSysUploadFiles").kendoUpload({
                    async: {
                        //saveUrl: "https://api.trisys.co.uk/files/uploadfile",
                        //removeUrl: "remove",
                        autoUpload: false,
                        withCredentials: true
                    },
                    multiple: false,
                    localization: {
                        select: "Select a Local File",
                        uploadSelectedFiles: "Upload"
                    },
                    select: onSelect,
                    remove: onRemove,
                    progress: onProgress,
                    complete: onComplete,
                    upload: onUpload,
                    success: onSuccess,
                    error: onError
                });

                function onRemove(e)
                {
                    TriSysSDK.Controls.FileManager._UploadedFileRelativePath = null;
                }

                function onSelect(e)
                {
                    TriSysApex.Logging.LogMessage("Select :: " + getFileInfo(e));

                    var formData = new FormData();
                    var trisysFile = $('#TriSysUploadFiles')[0];
                    formData.append("trisysFile", trisysFile.files[0]);

                    TriSysApex.UI.ShowWait(null, "Uploading File...");

                    $.ajax({
                        type: "POST",
                        url: "https://api.trisys.co.uk/files/uploadfile",
                        contentType: false,
                        processData: false,
                        data: formData,
                        cache: false,
                        crossDomain: true,
                        beforeSend: function (xhr)
                        {
                            xhr.setRequestHeader('SiteKey', TriSysAPI.Session.DeveloperSiteKey());

                            var sDataServicesKey = TriSysAPI.Session.DataServicesKey();
                            if (!TriSysAPI.Operators.isEmpty(sDataServicesKey))
                                xhr.setRequestHeader('DataServicesKey', sDataServicesKey);
                        },
                        success: function (result)
                        {
                            TriSysSDK.Controls.FileManager._UploadedFileRelativePath = result[0];
                            TriSysApex.UI.HideWait();
                        },

                        error: function (request, status, error)
                        {
                            TriSysApex.UI.HideWait();
                            TriSysApex.UI.ErrorHandlerRedirector("?", request, status, error);
                        }
                    });
                }
                function onComplete(e)
                {
                    TriSysApex.Logging.LogMessage("Complete");
                }
                function onProgress(e)
                {
                    TriSysApex.Logging.LogMessage("Upload progress :: " + e.percentComplete + "% :: " + getFileInfo(e));
                }
                function onSuccess(e)
                {
                    TriSysApex.Logging.LogMessage("Success (" + e.operation + ") :: " + getFileInfo(e));
                }
                function onError(e)
                {
                    TriSysApex.Logging.LogMessage("Error (" + e.operation + ") :: " + getFileInfo(e));
                }

                function getFileInfo(e)
                {
                    return $.map(e.files, function (file)
                    {
                        var info = file.name;

                        // File size is not available in all browsers
                        if (file.size > 0)
                        {
                            info += " (" + Math.ceil(file.size / 1024) + " KB)";
                        }
                        return info;
                    }).join(", ");
                }

                function onUpload(e)
                {
                    TriSysApex.Logging.LogMessage('onUpload');

                    // Array with information about the uploaded files
                    var files = e.files;

                    // Check the extension of each file and abort the upload if it is not .jpg
                    //$.each(files, function ()
                    //{
                    //    if (this.extension.toLowerCase() != ".jpg")
                    //    {
                    //        alert("Only .jpg files can be uploaded")
                    //        e.preventDefault();
                    //    }
                    //});

                    var xhr = e.XMLHttpRequest;
                    if (xhr)
                    {
                        //State  Description
                        //0      The request is not initialized
                        //1      The request has been set up
                        //2      The request has been sent
                        //3      The request is in process
                        //4      The request is complete
                        xhr.addEventListener("readystatechange", function (e)
                        {
                            TriSysApex.Logging.LogMessage('xhr.addEventListener("readystatechange", function (e) xhr.readyState=' + xhr.readyState);

                            if (xhr.readyState == 1 /* OPENED */)
                            {
                                TriSysApex.Logging.LogMessage("xhr.readyState == 1 /* OPENED */");

                                xhr.setRequestHeader('SiteKey', TriSysAPI.Session.DeveloperSiteKey());

                                var sDataServicesKey = TriSysAPI.Session.DataServicesKey();
                                if (!TriSysAPI.Operators.isEmpty(sDataServicesKey))
                                    xhr.setRequestHeader('DataServicesKey', sDataServicesKey);

                                TriSysApex.Logging.LogMessage("Security headers completed.");
                            }
                        });
                    }
                }
            },

            SaveCallbackFunction: null,

            // We have the value of the file reference field which is a fully qualified path (e.g. G:\CVData\... )
            // which is not accessible here on a client browser.
            // We need therefore to get the file (or contents) as a URL so that we can use the most convenient method of viewing it.
            GetOpenFileURLFromWebService: function (sTitle, sFile, sName)
            {
                var payloadObject = {};

                payloadObject.URL = "Files/GetFileURL";

                payloadObject.OutboundDataPacket = { FilePath: sFile };

                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.HideWait();

                    if (data.Success)
                    {
                        var sURL = data.URL;
                        var sName = data.FileName;
                        TriSysSDK.Controls.FileManager.OpenDocumentViewer(sTitle, sURL, sName);
                    }
                    else
                        TriSysApex.UI.errorAlert(data.ErrorMessage);

                    return true;
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.HideWait();
                    TriSysApex.UI.errorAlert('GetOpenFileURLFromWebService(' + sFile + ') : ' + status + ": " + error + ". responseText: " + request.responseText);
                };

                TriSysApex.UI.ShowWait(null, "Reading " + sName);
                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            OpenDocumentViewer: function (sTitle, sDocumentPath, sName)
            {
                TriSysSDK.Controls.FileManager.DocumentPath = sDocumentPath;

                //TriSysApex.UI.ShowMessage(sDocumentPath);
                //return;


                if (TriSysSDK.Controls.FileManager.isImageFile(sDocumentPath))
                {
                    // Use FancyBox
                    $.fancybox({ href: sDocumentPath, title: sName });
                    return;
                }


                // Use our standard modal dialogues
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = sTitle + ": " + sName;
                parametersObject.Image = 'document';

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlDocumentViewer.html';     // Our own document viewer control

                parametersObject.Height = document.body.offsetHeight - 100;
                parametersObject.Width = document.body.offsetWidth - 100;

                TriSysApex.UI.PopupMessage(parametersObject);
            },

            DocumentPath: null,      // The current document being viewed which will be a 'hidden' public URL

            isImageFile: function (sPath)
            {
                var bImage = false;
                var sSuffix = TriSysApex.Pages.FileManagerForm.FileSuffix(sPath);
                switch (sSuffix.toLowerCase())
                {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'bmp':
                    case 'gif':
                    case 'tif':
                    case 'tiff':
                    case 'pdf':
                        bImage = true;
                        break;
                }

                return bImage;
            }
        },
        //#endregion TriSysSDK.Controls.FileManager


        //#region TriSysSDK.Controls.UserProfileEditor
        //
        // Popup dialogue for user to edit their own login profile i.e. preferences, avatar, payment details etc..
        //
        UserProfileEditor:
        {
            Open: function (fnCallback)
            {
                var parametersObject = new TriSysApex.UI.ShowMessageParameters();
                parametersObject.Title = "Edit Login Profile";
                parametersObject.Image = 'userprofile';

                parametersObject.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlUserProfileEditor.html';

                ////HACK
                //var openWindow = $("#divTriSysMessageBoxWindow").data("kendoWindow");
                //if (openWindow)
                //    openWindow.close();

                //var actions = ["Close"];
                //var win = $("#divTriSysMessageBoxWindow").kendoWindow({
                //    height: 275,
                //    width: 500,
                //    minHeight: 275,
                //    minWidth: 500,
                //    title: "Skenken",
                //    visible: false,
                //    modal: true,
                //    resizable: false,
                //    maximize: false,
                //    content: "http://www.trisys.co.uk/mobilesite",
                //    close: function () { $("#divTriSysMessageBoxWindow").empty(); },
                //    actions: actions
                //});

                //// Fix the KendoUI window icon
                //$(".k-window-title").prepend('<div id="divWindowIconKludge" class="TriSys-PopupWindow-Icon-Question"></div>');

                //// Center and Open
                //var dialog = $("#divTriSysMessageBoxWindow").data("kendoWindow");
                //dialog.center();
                ////win.center();
                //dialog.open();
                //return;
                //// END OF HACK

                parametersObject.Height = 275;
                //parametersObject.Width = document.body.offsetWidth - 100;
                parametersObject.Resizable = false;
                parametersObject.Maximize = false;

                parametersObject.ButtonLeftVisible = true;
                parametersObject.ButtonRightVisible = true;

                parametersObject.OnLoadCallback = function ()
                {
                    TriSysApex.UserOptions.showUserProfileSettings();
                };

                TriSysApex.UI.PopupMessage(parametersObject);
            }
        },
        //#endregion TriSysSDK.Controls.UserProfileEditor


        //#region TriSysSDK.Controls.CurrencyAmountPeriod
        CurrencyAmountPeriod:
        {
            // Numerous currency amount period fields on a form.
            // Load it dynamically and rename the DIV tags
            Load: function (sFieldDivTag, fieldDescription, fnCallbackAfterLoad)
            {
                var sPath = TriSysApex.Constants.ThemeFolder+TriSysApex.Constants.UserControlFolder + 'ctrlCurrencyAmountPeriod.html';

                //alert(sPath);

                TriSysApex.FormsManager.loadPageIntoDiv(sFieldDivTag, sPath,
                    function (response, status, xhr)
                    {
                        TriSysSDK.Controls.CurrencyAmountPeriod.AfterLoadEvent(sFieldDivTag, fieldDescription);
                        if (fnCallbackAfterLoad)
                            fnCallbackAfterLoad();
                    });
            },

            // After the currency amount period has loaded, we need to change the ID's of all DIV's
            // which are in sub nodes only, to be prefixed by the field name.
            AfterLoadEvent: function (divTag, fieldDescription)
            {
                var sPrefix = divTag + '-';

                // Get all nodes recursively beneath this DIV and change their ID to be unique by prefixing with this DIV name
                TriSysSDK.Controls.General.ReplaceDivTagsOfDynamicallyLoadedUserControl(divTag);

                // Populate the lookups
                var sJQueryCurrencyDiv = "#" + sPrefix + "CurrencyAmountPeriod_Currency";
                TriSysSDK.CShowForm.LoadCurrencyCombo(sJQueryCurrencyDiv);

                var sJQueryPeriodDiv = "#" + sPrefix + "CurrencyAmountPeriod_Period";
                TriSysSDK.CShowForm.LoadConsultancyPeriodCombo(sJQueryPeriodDiv);

                // Set the max/min/step of the numeric text box
                var fMin = 0;
                if (fieldDescription.MinValue)
                    fMin = fieldDescription.MinValue;

                var fMax = 999999999;
                if (fieldDescription.MaxValue)
                    fMax = fieldDescription.MaxValue;

                var fIncrement = 1;
                if (fieldDescription.SpinIncrement)
                    if (fieldDescription.SpinIncrement > 0)
                        fIncrement = fieldDescription.SpinIncrement;

                var sJQueryPeriodDiv = "#" + sPrefix + "CurrencyAmountPeriod_Amount";
                $(sJQueryPeriodDiv).kendoNumericTextBox({
                    min: fMin,
                    max: fMax,
                    step: fIncrement
                });
            },

            // £100,000 per Annum
            // £90 per Hour
            SetCurrencyAmountPeriod: function (sFieldID, htmlValue)
            {
                //TriSysApex.Logging.LogMessage(sFieldID + '=' + htmlValue);

                var sCurrency = '', fAmount = 0, sPeriod = '';
                if (htmlValue)
                {
                    var sParts = htmlValue.split(' ');
                    var sCurrencyPlusAmount = sParts[0];
                    var sPer = sParts[1];
                    var sPeriod = sParts[2];

                    // Parse currency and amount
                    var currencies = TriSysApex.Cache.Currencies();
                    for (var i = 0; i < currencies.length; i++)
                    {
                        var currency = currencies[i];
                        var iIndex = sCurrencyPlusAmount.indexOf(currency.Symbol);
                        if (iIndex >= 0)
                        {
                            var sNumber = sCurrencyPlusAmount.substring(iIndex + currency.Symbol.length);
                            sNumber = sNumber.replace(/,/g, '');
                            if ($.isNumeric(sNumber))
                            {
                                sCurrency = currency.Symbol;
                                fAmount = sNumber;
                                break;
                            }
                        }
                    }
                }

                // Write to widget now
                var sPrefix = sFieldID + '-';
                TriSysSDK.CShowForm.SetValueInCombo(sPrefix + "CurrencyAmountPeriod_Currency", sCurrency);
                TriSysSDK.CShowForm.SetValueInCombo(sPrefix + "CurrencyAmountPeriod_Period", sPeriod);

                var numerictextbox = $("#" + sPrefix + "CurrencyAmountPeriod_Amount").data("kendoNumericTextBox");
                numerictextbox.value(fAmount);
            },

            // Get the full display version of the currency amount period - used for CRUD operations
            GetCurrencyAmountPeriod: function (sFieldID)
            {
                var sPrefix = sFieldID + '-';
                var sCurrency = TriSysSDK.CShowForm.GetTextFromCombo(sPrefix + "CurrencyAmountPeriod_Currency");
                var sPeriod = TriSysSDK.CShowForm.GetTextFromCombo(sPrefix + "CurrencyAmountPeriod_Period");

                var fAmount = $("#" + sPrefix + "CurrencyAmountPeriod_Amount").val();

                var sDisplay = sCurrency + fAmount + " per " + sPeriod;
                return sDisplay;
            },

            // Get the numerical value of the currency amount per hour - used for searching
            // See V10 Code Base: TriSys Business Forms.ctrlCurrencyAmountPeriod
            GetCurrencyAmountPeriodPerHour: function (sFieldID)
            {
                var sPrefix = sFieldID + '-';
                var sCurrency = TriSysSDK.CShowForm.GetTextFromCombo(sPrefix + "CurrencyAmountPeriod_Currency");
                var sPeriod = TriSysSDK.CShowForm.GetTextFromCombo(sPrefix + "CurrencyAmountPeriod_Period");

                var fAmount = $("#" + sPrefix + "CurrencyAmountPeriod_Amount").val();

                switch (sPeriod)
                {
                    case 'Day':
                        fAmount = fAmount / 7.5;
                        break;

                    case 'Week':
                        fAmount = fAmount / 37.5;
                        break;

                    case 'Month':
                        fAmount = fAmount / (7.5 * 20);
                        break;

                    case 'Annum':
                        fAmount = fAmount / (7.5 * 20 * 12);
                        break;
                }

                return fAmount;
            }
        },
        //#endregion TriSysSDK.Controls.CurrencyAmountPeriod

        //#region TriSysSDK.Controls.NumericSpinner
        NumericSpinner:
        {
            Initialise: function (sFieldID, fieldDescription)
            {
                // Set the max/min/step of the numeric text box
                var fMin = 0;
                if (fieldDescription.MinValue)
                    fMin = fieldDescription.MinValue;

                var fMax = 999999999;
                if (fieldDescription.MaxValue)
                    fMax = fieldDescription.MaxValue;

                var fIncrement = 1;
                if (fieldDescription.SpinIncrement)
                    if (fieldDescription.SpinIncrement > 0)
                        fIncrement = fieldDescription.SpinIncrement;

                var sJQueryPeriodDiv = "#" + sFieldID;
                $(sJQueryPeriodDiv).kendoNumericTextBox({
                    min: fMin,
                    max: fMax,
                    step: fIncrement
                });
            },

            SetValue: function (sFieldID, fValue)
            {
                var sJQueryDiv = "#" + sFieldID;
                var numerictextbox = $(sJQueryDiv).data("kendoNumericTextBox");
                numerictextbox.value(fValue);
            },

            GetValue: function (sFieldID)
            {
                var sJQueryDiv = "#" + sFieldID;
                var fValue = $(sJQueryDiv).val();
                return fValue;
            }
        },
        //#endregion TriSysSDK.Controls.NumericSpinner


        //#region TriSysSDK.Controls.TextBoxDropDown
        TextBoxDropDown:
        {
            // Drop down text box to allow large text to take up less space on screen.
            Load: function (sFieldDivTag)
            {
                var sPath = TriSysApex.Constants.UserControlFolder + 'ctrlTextDropDown.html';

                TriSysApex.FormsManager.loadPageIntoDiv(sFieldDivTag, sPath,
                    function (response, status, xhr)
                    {
                        TriSysSDK.Controls.TextBoxDropDown.AfterLoadEvent(sFieldDivTag);
                    });
            },

            // After the control has loaded, we need to change the ID's of all DIV's
            // which are in sub nodes only, to be prefixed by the field name.
            AfterLoadEvent: function (divTag)
            {
                var sPrefix = divTag + '-';

                // Get all nodes recursively beneath this DIV and change their ID to be unique by prefixing with this DIV name
                TriSysSDK.Controls.General.ReplaceDivTagsOfDynamicallyLoadedUserControl(divTag);

                // Add events to buttons to update the control
                var sJQueryDiv = "#" + sPrefix + "TextDropDown_Menu";
                $(sJQueryDiv).kendoMenu({
                    openOnClick: true,
                    closeOnClick: false
                });

                var sJQueryDivOK = "#" + sPrefix + "TextDropDown_OKButton";
                $(sJQueryDivOK).kendoButton();
                $(sJQueryDivOK).click(function ()
                {
                    SaveTextEditToSummary(divTag);
                });

                var sJQueryDivCancel = "#" + sPrefix + "TextDropDown_CancelButton";
                $(sJQueryDivCancel).kendoButton();
                $(sJQueryDivCancel).click(function ()
                {
                    CloseDropDown(divTag);
                });

                function SaveTextEditToSummary(divTag)
                {
                    var sEditedText = TriSysSDK.Controls.TextBoxDropDown.GetText(divTag);
                    TriSysSDK.Controls.TextBoxDropDown.SetText(divTag, sEditedText);
                    CloseDropDown(divTag);
                }

                function CloseDropDown(divTag)
                {
                    var sPrefix = divTag + '-';
                    var menu = $('#' + sPrefix + "TextDropDown_Menu").data("kendoMenu");
                    menu.close();
                }
            },

            SetText: function (divTag, sText)
            {
                var sPrefix = divTag + '-';
                $('#' + sPrefix + "TextDropDown_Summary").val(sText);
                $('#' + sPrefix + "TextDropDown_Text").val(sText);
            },

            GetText: function (divTag)
            {
                var sPrefix = divTag + '-';
                var sEditedText = $('#' + sPrefix + "TextDropDown_Text").val();
                return sEditedText;
            }
        },
        //#endregion TriSysSDK.Controls.TextBoxDropDown

        //#region TriSysSDK.Controls.FileReference
        FileReference:
        {
            // Generic file reference composite control
            Load: function (sFieldDivTag, fieldDescription, fnCallback)
            {
                if (TriSysAPI.Operators.isEmpty(fieldDescription))
                {
                    fieldDescription = {};
                    fieldDescription.TableName = "General";
                    fieldDescription.TableFieldName = sFieldDivTag;
                }

                var sPath = TriSysApex.Constants.UserControlFolder + 'ctrlFileReference.html';

                TriSysApex.FormsManager.loadPageIntoDiv(sFieldDivTag, sPath,
                    function (response, status, xhr)
                    {
                        TriSysSDK.Controls.FileReference.AfterLoadEvent(sFieldDivTag, fieldDescription, fnCallback);
                    });
            },

            // After the control has loaded, we need to change the ID's of all DIV's
            // which are in sub nodes only, to be prefixed by the field name.
            AfterLoadEvent: function (divTag, fieldDescription, fnCallback)
            {
                var sPrefix = divTag + '-';

                // Get all nodes recursively beneath this DIV and change their ID to be unique by prefixing with this DIV name
                TriSysSDK.Controls.General.ReplaceDivTagsOfDynamicallyLoadedUserControl(divTag);

                // Add events to buttons to update the control

                var sJQueryDivFind = "#" + sPrefix + "FileReference_FindButton";
                $(sJQueryDivFind).kendoButton({
                    imageUrl: "images/trisys/16x16/search.ico"
                });
                $(sJQueryDivFind).click(function ()
                {
                    FindFile(divTag, fieldDescription, fnCallback);
                });

                var sJQueryDivOpen = "#" + sPrefix + "FileReference_OpenButton";
                $(sJQueryDivOpen).kendoButton({
                    imageUrl: "images/trisys/16x16/Folder Open File.ico"
                });
                $(sJQueryDivOpen).click(function ()
                {
                    OpenFile(divTag, fieldDescription);
                });

                var sJQueryDivDelete = "#" + sPrefix + "FileReference_DeleteButton";
                $(sJQueryDivDelete).kendoButton({
                    imageUrl: "images/trisys/16x16/Remove.ico"
                });
                $(sJQueryDivDelete).click(function ()
                {
                    DeleteFile(divTag, fnCallback);
                });

                function FindFile(divTag, fieldDescription, fnCallback)
                {
                    //TriSysApex.UI.ShowMessage("Find: " + divTag);
                    TriSysSDK.Controls.FileManager.FindFileUpload("Find " + fieldDescription.TableFieldName,
                        function (sFile)
                        {
                            assignUploadedDocumentToFormField(divTag, sFile, fieldDescription, fnCallback);
                        });
                }

                function OpenFile(divTag, fieldDescription)
                {
                    var sFilePath = TriSysSDK.Controls.FileReference.GetFile(divTag);
                    if (sFilePath)
                    {
                        var sTitle = "Open " + fieldDescription.TableFieldName;

                        if (sFilePath.indexOf("https://") == 0)
                        {
                            // HTTPS Path
                            var sParts = sFilePath.split('/');
                            var sName = sParts[sParts.length - 1];
                            TriSysSDK.Controls.FileManager.OpenDocumentViewer(sTitle, sFilePath, sName);
                        }
                        else
                        {
                            // UNC Path
                            var sParts = sFilePath.split('\\');
                            var sName = sParts[sParts.length - 1];
                            TriSysSDK.Controls.FileManager.GetOpenFileURLFromWebService(sTitle, sFilePath, sName);
                        }
                    }
                }

                function DeleteFile(divTag, fnCallback)
                {
                    TriSysSDK.Controls.FileReference.SetFile(divTag, '');
                    if (fnCallback)
                        fnCallback('delete', divTag, null);
                }

                // Called after user has uploaded a local file and we now wish to assign it to the
                // specified file ref field.
                //
                function assignUploadedDocumentToFormField(sJQueryDiv, sRelativeUploadFilePath, fieldDescription, fnCallback)
                {
                    // Debug only
                    //alert("assignUploadedDocumentToFormField(" + sJQueryDiv + "): You uploaded and saved: " + sRelativeUploadFilePath +
                    //        " for fieldDescription=" + fieldDescription.TableName + '.' + fieldDescription.TableFieldName + "?");

                    // Call the Web API now to copy the uploaded server file for the specified field.
                    // Upon completion, update the on-screen field
                    var payloadObject = {};

                    payloadObject.URL = "Files/MoveUploadedFileReferenceDocument";

                    payloadObject.OutboundDataPacket = {
                        UploadedFilePath: sRelativeUploadFilePath,
                        TableName: fieldDescription.TableName,
                        TableFieldName: fieldDescription.TableFieldName
                    };

                    payloadObject.InboundDataFunction = function (data)
                    {
                        TriSysApex.UI.HideWait();
                        var JSONdataset = data;

                        if (JSONdataset)
                        {
                            //sDisplay = JSON.stringify(JSONdataset);
                            //alert(sDisplay);
                            //return;

                            if (JSONdataset.Success)
                            {
                                var sFilePath = JSONdataset.FolderFilePath;

                                TriSysSDK.Controls.FileReference.SetFile(sJQueryDiv, sFilePath);

                                if (fnCallback)
                                    fnCallback('find', divTag, sFilePath);
                            }
                            else
                            {
                                TriSysApex.UI.errorAlert(JSONdataset.ErrorMessage);
                            }
                        } else
                        {
                            // Something else has gone wrong!
                        }
                    };

                    payloadObject.ErrorHandlerFunction = function (request, status, error)
                    {
                        TriSysApex.UI.HideWait();
                        TriSysApex.UI.ErrorHandlerRedirector('assignUploadedDocumentToFormField: ', request, status, error);
                    };

                    TriSysApex.UI.ShowWait(null, "Renaming " + fieldDescription.TableFieldName + "...");
                    TriSysAPI.Data.PostToWebAPI(payloadObject);
                }
            },

            SetFile: function (divTag, sFilePath)
            {
                var sFileName = '';
                if (sFilePath)
                {
                    if (sFilePath.indexOf(':') > 0)
                    {
                        var sParts = sFilePath.split('\\');
                        sFileName = sParts[sParts.length - 1];
                    }
                }
                var sPrefix = divTag + '-';
                $('#' + sPrefix + "FileReference_FileName").val(sFileName);
                $('#' + sPrefix + "FileReference_FilePath").text(sFilePath);
            },

            GetFile: function (divTag)
            {
                var sPrefix = divTag + '-';
                var sFilePath = $('#' + sPrefix + "FileReference_FilePath").text();
                if (sFilePath)
                    if (sFilePath.indexOf(':') > 0)
                        return sFilePath;
            },

            FindButtonVisible: function (divTag, bVisible)
            {
                var sPrefix = divTag + '-';
                var sJQueryDivFind = "#" + sPrefix + "FileReference_FindButton";
                if (bVisible)
                    $(sJQueryDivFind).show();
                else
                    $(sJQueryDivFind).hide();
            },

            DeleteButtonVisible: function (divTag, bVisible)
            {
                var sPrefix = divTag + '-';
                var sJQueryDivDelete = "#" + sPrefix + "FileReference_DeleteButton";
                if (bVisible)
                    $(sJQueryDivDelete).show();
                else
                    $(sJQueryDivDelete).hide();
            },

            OpenButtonVisible: function (divTag, bVisible)
            {
                var sPrefix = divTag + '-';
                var sJQueryDivFind = "#" + sPrefix + "FileReference_OpenButton";
                if (bVisible)
                    $(sJQueryDivFind).show();
                else
                    $(sJQueryDivFind).hide();
            }
        },
        //#endregion TriSysSDK.Controls.FileReference

        //#region TriSysSDK.Controls.Animations
        Animations:
        {
            StartWaitingSpinner: function (sDiv, sMessage)
            {
                if (!sMessage)
                    sMessage = "Loading";

                // OLD: Replaced div contents - bad!
                var sHTML = '<img src="images/wait-spinner.gif" >' +
                            ' &nbsp; ' + sMessage + '...';
                //$('#' + sDiv).html(sHTML);

                // NEW: Insert before the specified div and make original invisible until data retrieved
                var sTempDiv = "Waiting-" + sDiv;
                sHTML = '<div id="' + sTempDiv + '">' + sHTML + '</div>';
                $('#' + sDiv).before(sHTML);
                $('#' + sDiv).hide();
            },

            StopWaitingSpinner: function (sDiv)
            {
                var sTempDiv = "Waiting-" + sDiv;
                $('#' + sTempDiv).remove();
                $('#' + sDiv).show();
            },

            AccordionGroupClick: function (element, sDivTag, fnCallbackOnOpen)
            {
                var sAttributeName = 'AccordionGroupClickOpenStatus';
                var bOpen = false;
                if (element.hasAttribute(sAttributeName))
                {
                    var sStatus = element.getAttribute(sAttributeName);
                    bOpen = TriSysAPI.Operators.stringToBoolean(sStatus);
                }

                bOpen = !bOpen;
                element.setAttribute(sAttributeName, bOpen);

                if (bOpen && fnCallbackOnOpen)
                {
                    // Callback the function to start the asynchronous data retrieval
                    fnCallbackOnOpen(sDivTag);
                }
            }
        }
        //#endregion TriSysSDK.Controls.Animations

    },  // End of TriSysSDK.Controls
    //#endregion TriSysSDK.Controls

    //#region TriSysSDK.EntityFormTabs
    //
    // These are the tabs which resize on the entity form - in fact any page can use these techniques
    EntityFormTabs:
    {
        DrawTabs: function (sDivTagOfTabControl, sFormName)
        {
            var dataSourceTabs = TriSysSDK.EntityFormTabs.GetTabDataSource(sFormName);

            if (dataSourceTabs)
            {
                $("#" + sDivTagOfTabControl).kendoTabStrip(
                {
                    animation: { open: { effects: "fadeIn" } },
                    dataTextField: "text",
                    dataImageUrlField: "imageUrl",
                    dataContentField: "content",
                    dataSource: dataSourceTabs

                }).data("kendoTabStrip").select(0);
            }
            else
            {
                // Static tabs drawn by developer/admin/designer
                $("#" + sDivTagOfTabControl).kendoTabStrip(
                {
                    animation: { open: { effects: "fadeIn" } }

                }).data("kendoTabStrip").select(0);
            }
        },

        ResizeEventManagement: function (sDivTagOfTabControl, fnResizeCallback, bFullPage)
        {
            $(window).resize(function ()
            {
                if (bFullPage)
                    resizePageEvent();
                else
                    resizeTabEvent();
            });

            var resizePageEvent = function ()
            {
                // The whole page has been resized
                var iTODOCalculateCaptionAndLogoHeight = 55;
                var iDocumentHeight = $(document).height() - iTODOCalculateCaptionAndLogoHeight;
                fnResizeCallback(iDocumentHeight);
                return;
            };

            var resizeTabEvent = function ()
            {
                try
                {
                    var tabStripElement = $("#" + sDivTagOfTabControl).kendoTabStrip();
                    tabStrip = tabStripElement.data("kendoTabStrip");

                    var pos = tabStripElement.position();
                    var lTop = pos.top;
                    var iFudgeFactor = 4;
                    var lHeight = TriSysApex.Pages.Index.FormContentHeight - lTop - iFudgeFactor;

                    tabStripElement.height(lHeight);

                    // Also resize the content of each tab page
                    // These objects cannot be reliably CSS sized 100% height on all browsers so do it manually
                    var iObjectHeight = lHeight - 32;
                    fnResizeCallback(iObjectHeight);
                }
                catch (err)
                {
                    // We get this sometimes!                   
                }

                var expandContentDivs = function (divs)
                {
                    var iFudgeKludge = 5;
                    divs.height(tabStripElement.innerHeight() - tabStripElement.children(".k-tabstrip-items").outerHeight() - iFudgeKludge);
                };

                var resizeAllTabs = function ()
                {
                    expandContentDivs(tabStripElement.children(".k-content"));
                };

                resizeAllTabs();
            };

            if (bFullPage)
                resizePageEvent();
            else
                resizeTabEvent();
        },

        //#region TriSysSDK.EntityFormTabs.GetTabDataSource
        GetTabDataSource: function (sEntityName)
        {
            switch (sEntityName)
            {
                case "Dashboard":
                    return DashboardTabDataSource();

                case "Contact":
                    return null;    //ContactTabDataSource();

                default:
                    return EmptyTabDataSource();
            }

            function EmptyTabDataSource()
            {
                return null;
                return [{ text: "Not Found", content: "The tabs for this form have not been programmed correctly" }];
            }

            function DashboardTabDataSource()
            {
                var ds = [
                        {
                            text: "Metrics",
                            content: '<div id="divTestCharts" style="border-width: 0; height: 20px"></div>' +
                                        '<div id="chartTest" style="min-width: 310px; height: 400px; margin: 0 auto"></div>'
                        },
                        {
                            text: "Recruitment Activity Summary",
                            content: "A summary of all your recruiting activity allowing drill down to important events and tasks."
                        },
                        {
                            text: "Active Searching",
                            content: "TriSys continually matches candidates with requirements. alerting you for each match found, allowing you to save valuable time and increase efficiency."
                        },
                        {
                            text: "My E-Mail",
                            content: "Your incoming and outgoing e-mail messages which could not be synchronised with contacts in your database."
                        },
                        {
                            text: "Help Guide",
                            content: '<div id="TriSysHelpGuide" ></div>'
                        },
                        {
                            text: "Support Information",
                            content: '<div id="TriSysSupportInformation"></div>'
                        },
                        {
                            text: "TriSys Web Jobs",
                            content: '<div id="TriSysWebJobs"></div>'
                        }
                ];

                return ds;
            }

            // Example only - we do not do the contact tabs this way!
            function ContactTabDataSource()
            {
                var ds = [
                        {
                            text: "Personal",
                            content: 'Personal details',
                            imageUrl: 'Images/TriSys/16x16/cvar.ico'
                        },
                        {
                            text: "Employment",
                            content: "A summary of all your recruiting activity allowing drill down to important events and tasks."
                        },
                        {
                            text: "Attributes",
                            content: "TriSys continually matches candidates with requirements. alerting you for each match found, allowing you to save valuable time and increase efficiency."
                        },
                        {
                            text: "Social Networks",
                            content: "Your incoming and outgoing e-mail messages which could not be synchronised with contacts in your database."
                        },
                        {
                            text: "Back Office",
                            content: '<div id="TriSysHelpGuide" ></div>'
                        },
                        {
                            text: "Requirements",
                            content: '<div id="TriSysSupportInformation"></div>'
                        },
                        {
                            text: "Placements",
                            content: '<div id="TriSysWebJobs"></div>'
                        },
                        {
                            text: "Notes/History",
                            content: '<div id="TriSysWebJobs"></div>'
                        },
                        {
                            text: "Scheduled Tasks",
                            content: '<div id="TriSysWebJobs"></div>'
                        }
                ];

                return ds;
            }
        }
        //#endregion TriSysSDK.EntityFormTabs.GetTabDataSource

    },
    //#endregion TriSysSDK.EntityFormTabs

    //#region TriSysSDK.DocumentObjectModel

    // Our own DOM functions which are easy to use without knowing the ins and outs of web browser DOM's
    DocumentObjectModel:
    {
        // Used for getting a list of nodes beneath the specified node with this ID.
        //
        GetAllNodesBeneathUniqueDivID: function (sDivID)
        {
            var el = document.getElementById(sDivID);
            var nodes = [];
            htmlTree(el, nodes);

            function htmlTree(el, nodesArray)
            {
                if (el)
                {
                    nodesArray.push(el);

                    if (el.hasChildNodes())
                    {
                        var child = el.firstChild;
                        while (child)
                        {
                            if (child.nodeType === 1 && child.nodeName != 'SCRIPT')
                                htmlTree(child, nodesArray);

                            child = child.nextSibling;
                        }
                    }
                }
            }

            return nodes;
        }
    },
    //#endregion TriSysSDK.DocumentObjectModel

    //#region TriSysSDK.Database

    // We have a lot of SQL on the client simply to make it easier to configure grids and client-side logic for specific customers
    Database:
    {
        // Single quote the value
        SQLQuoterNull: function (sValue)
        {
            if (sValue)
                return sValue.replace(/'/g, "''");
            else
                return "null";
        },

        SQLDateFormat: function (sValue)
        {
            if (sValue)
                return "'" + TriSysSDK.Database.SQLQuoterNull(sValue) + "'";
            else
                return "null";
        },

        // Used for small data sets only e.g. get a list for a combo.
        // NOT used for grids - they have their own paged mechanisms.
        GetDataSet: function (sSQL, fnCallback, objCallerDefinedData)
        {
            var iMaxRecords = 1024;

            // Set up the data packet to contain this SQL statement
            var dataPacketForSQL = {
                'SQL': sSQL,
                'PageNumber': 1,
                'RecordsPerPage': iMaxRecords
            };

            // Now that the SQL statement is fully assembled, prepare the properties of the standard AJAX web API call
            var payloadObject = {};
            payloadObject.URL = 'SQLDatabase/SQL_Select';           // Our generic page oriented SQL data table 
            payloadObject.OutboundDataPacket = dataPacketForSQL;    // The dynamic SQL statement and paging information
            payloadObject.Asynchronous = true;                      // We are being called asynchronously 
            payloadObject.InboundDataFunction = function (result)   // The callback function to populate the widget with data
            {
                var CSQLDatabaseSearchResults = result;
                fnCallback(CSQLDatabaseSearchResults, objCallerDefinedData);
            };

            // Send this to the back end web API for data retrieval
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        // Convert a data table from the API into a data source for a kendoUI control
        DataTableToDataSource: function (dt, sFieldName)
        {
            var sourceData = [];
            if (dt)
            {
                for (var i = 0; i < dt.length; i++)
                {
                    var dr = dt[i];
                    var sValue = dr[sFieldName];
                    var sourceStruct = {
                        text: sValue,
                        value: sValue
                    };
                    sourceData.push(sourceStruct);
                }
            }
            return sourceData;
        },

        // Expecting a single row with a single field
        GetSnapshotFieldFromDataTable: function (dt, sFieldName)
        {
            try
            {
                if (dt)
                {
                    var dr = dt[0];
                    var sValue = dr[sFieldName];
                    return sValue;
                }
            }
            catch (err)
            {
                // Programmer has not tested
            }
        },

        // Get selected comma separated list and amalgamate into a SQL clause
        SelectedMultiSelectComboSQLClause: function (sDiv, sFieldName)
        {
            var sWhereSQL = '';
            var sValuesFromCombo = TriSysSDK.CShowForm.GetSelectedSkillsFromList(sDiv);
            if (sValuesFromCombo)
            {
                var sValues = sValuesFromCombo.split(', ');
                if (sValues.length > 0)
                {
                    sWhereSQL += sFieldName + " in (";
                    for (var i = 0; i < sValues.length; i++)
                    {
                        var sValue = sValues[i];
                        if (i > 0)
                            sWhereSQL += ',';
                        sWhereSQL += "'" + TriSysSDK.Database.SQLQuoterNull(sValue) + "'";
                    }
                    sWhereSQL += ")";
                }
            }

            return sWhereSQL;
        },

        //#region TriSysSDK.Database.SQL

        // Mechanism of allowing customised SQL queries to be modified on a per installation basis
        // independent of the .js files. 
        //

        SQL:
        {
            Query: function (sID)
            {
                var allQueries = TriSysSDK.Database.SQL.Load();
                if (allQueries)
                {
                    for (var i = 0; i < allQueries.length; i++)
                    {
                        var resource = allQueries[i];
                        if (resource.ID == sID)
                        {
                            // SQL Queries can span multiple lines for legibility
                            var sQuery = resource.SQL.join('\n');
                            return sQuery;
                        }
                    }
                }
            },

            Load: function ()
            {
                var SQLQueriesData = null;
                var sFilePath = TriSysLoad.Utility.FilePathPrefix() + 'data/sql.json';
                $.ajax({
                    type: 'GET',
                    url: sFilePath + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache(),
                    dataType: 'json',
                    success: function (data)
                    {
                        SQLQueriesData = data;
                    },
                    error: function (request, status, error)
                    {
                        TriSysApex.Logging.LogMessage("Error finding " + sFilePath + ": " + status + ": " + error + ". responseText: " + request.responseText);
                    },
                    async: false
                });

                return SQLQueriesData;
            }
        }
        //#endregion TriSysSDK.Database.SQL
    },
    //#endregion TriSysSDK.Database

    //#region TriSysSDK.Countries)
    Countries:
    {
        // Need Date/countries.json folder in your project
        Load: function ()
        {
            var sFile = TriSysLoad.Utility.FilePathPrefix() + 'data/countries.json' + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache();
            var countriesData = null;
            $.ajax({
                type: 'GET',
                url: sFile,
                dataType: 'json',
                success: function (data)
                {
                    countriesData = data;
                },
                error: function (request, status, error)
                {
                    alert("Error finding data/countries.json: " + status + ": " + error + ". responseText: " + request.responseText);
                },
                async: false
            });

            return countriesData;
        },

        // Use the IP address to take a guess at the users' current country name
        // See http://jsfiddle.net/zK5FN/2/
        CurrentCountryName: function (fnCallback)
        {
            $.get("http://ipinfo.io", function (response)
            {
                var sCountryCode = response.country;
                if (sCountryCode)
                {
                    // Now have to convert 2 letter country code into associated name
                    var sCountryName = null;
                    var countriesData = TriSysSDK.Countries.Load();
                    if (countriesData)
                    {
                        var data = [];
                        for (var i = 0; i < countriesData.length; i++)
                        {
                            var country = countriesData[i];
                            if (country.cca2 == sCountryCode)
                            {
                                sCountryName = country.name;
                                break;
                            }
                        }

                        // Now callback with these details
                        fnCallback(sCountryName);
                    }
                }
            }, "jsonp");
        },

        // Use the IP address to take a guess at the users' current town/city name
        CurrentCityName: function (fnCallback)
        {
            $.get("http://ipinfo.io", function (response)
            {
                var sCity = response.city;
                fnCallback(sCity);
            }, "jsonp");
        }
    },
    //#endregion TriSysSDK.Countries

    //#region TriSysSDK.Resources

    // Mechanism of allowing centralised international resources per site without having to have
    // multiple .js script files.
    // Each site and/or international implementation (via a sub-folder) will utilise the appropriate
    // international language.
    // It is expected that it will be done in the documentReady() call by setting the appropriate
    // TriSysSDK.Resources.Language setting below.
    // This will load the correct international resource file for the site language.
    // All other resources changes would be applied to the folder .html files as expected.
    //

    Resources:
    {
        Language: 'en',
        InsertNewline: false,

        Message: function (sID)
        {
            var allResources = TriSysSDK.Resources.Load();
            if (allResources)
            {
                for (var i = 0; i < allResources.length; i++)
                {
                    var resource = allResources[i];
                    if (resource.ID == sID)
                    {
                        var sJoinSeparator = '';
                        if (TriSysSDK.Resources.InsertNewline)
                            sJoinSeparator = '\n';

                        var sHTML = resource.HTML.join(sJoinSeparator);
                        return sHTML;
                    }
                }
            }

            return sID;     // Return the ID if not found in the resources file
        },

        Load: function ()
        {
            var resourcesData = null;
            var sFilePath = 'data/resource-' + TriSysSDK.Resources.Language + '.json';
            sFilePath = TriSysLoad.Utility.FilePathPrefix() + sFilePath;
            $.ajax({
                type: 'GET',
                url: sFilePath + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache(),
                dataType: 'json',
                success: function (data)
                {
                    resourcesData = data;
                },
                error: function (request, status, error)
                {
                    alert("Error finding " + sFilePath + ": " + status + ": " + error + ". responseText: " + request.responseText);
                },
                async: false
            });

            return resourcesData;
        }
    },
    //#endregion TriSysSDK.Resources

    //#region TriSysSDK.PostCode
    PostCode:
    {
        Lookup: function (sPostCodeDiv, sStreetDiv, sCityDiv, sCountyDiv, sCountryDiv)
        {
            var sPostCode = $('#' + sPostCodeDiv).val();
            if (!sPostCode)
                return;

            TriSysApex.UI.ShowWait(null, "Post Code Lookup...");

            var dataPacket = {
                PostCode: sPostCode
            };
            var payloadObject = {};
            payloadObject.URL = "PostCode/Lookup";
            payloadObject.OutboundDataPacket = dataPacket;
            payloadObject.InboundDataFunction = function (data)
            {
                TriSysApex.UI.HideWait();

                var CPostCodeLookupResponse = data;
                if (CPostCodeLookupResponse)
                {
                    if (CPostCodeLookupResponse.Success)
                    {
                        var address = CPostCodeLookupResponse.Address;
                        var sStreet = address.Street;
                        var sCity = address.City;
                        var sCounty = address.County;
                        sPostCode = address.PostCode;
                        var sCountry = address.Country;

                        $('#' + sStreetDiv).val(sStreet);
                        $('#' + sCityDiv).val(sCity);
                        $('#' + sCountyDiv).val(sCounty);
                        $('#' + sPostCodeDiv).val(sPostCode);
                        if (sCountry)
                            TriSysSDK.CShowForm.SetTextInCombo(sCountryDiv, sCountry);
                    }
                    else
                        TriSysApex.UI.errorAlert(CPostCodeLookupResponse.ErrorMessage, TriSysWeb.Constants.ApplicationName);
                }

                return true;
            };
            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.ErrorHandlerRedirector('TriSysSDK.PostCode.Lookup: ', request, status, error);
            };
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        }
    },
    //#endregion TriSysSDK.PostCode

    //#region TriSysSDK.Numbers
    Numbers:
    {
        Pad: function (
              a, // the number to convert 
              b // number of resulting characters
            )
        {
            return (
            1e15 + a + // combine with large number
            "" // convert to string
            ).slice(-b); // cut leading "1"
        },

        Format: function (num, decimalPlaces)
        {
            var p = num.toFixed(decimalPlaces).split(".");
            return p[0].split("").reduceRight(function (acc, num, i, orig)
            {
                if ("-" === num && 0 === i)
                {
                    return num + acc;
                }
                var pos = orig.length - i - 1;
                return num + (pos && !(pos % 3) ? "," : "") + acc;
            }, "") + (p[1] ? "." + p[1] : "");
        }
    },
    //#endregion TriSysSDK.Numbers

    //#region TriSysSDK.Tables
    Tables:
    {
        ColumnVisibilityByIndex: function (sTableDiv, iColumnIndex, bVisible)
        {
            var stl;
            if (bVisible) stl = 'block';
            else stl = 'none';

            var tbl = document.getElementById(sTableDiv);
            var rows = tbl.getElementsByTagName('tr');

            for (var row = 0; row < rows.length; row++)
            {
                var cells = rows[row].getElementsByTagName('td');
                if (!bVisible)
                    cells[iColumnIndex].style.display = stl;
            }
        }
    },
    //#endregion TriSysSDK.Tables

    //#region TriSysSDK.Scheduler
    // See DiaryManager.html - this is a layer over the KendoUI scheduler in which we map our own Tasks to it.
    // Tried asynchronous data binding like in the grid, but the KendoUI scheduler does not support that - timezone fuck up
    Scheduler:
    {
        Instantiate: function (sDivTag, dataSourceArray)
        {
            var TimezoneGMT = "Europe/London"; // Use the London timezone
            var currentDate = new Date();
            var startTime = moment(currentDate).format("YYYY-MMM-DD") + " 07:00";
            $("#" + sDivTag).kendoScheduler({
                date: currentDate,
                startTime: new Date(startTime),
                //height: 400,
                timezone: TimezoneGMT,
                currentTimeMarker: {
                    useLocalTimezone: true
                },
                dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'ddd')# #=kendo.toString(date, 'dd')# #=kendo.toString(date, 'MMM')#</strong>"),
                views: [
                    "day",
                    { type: "week", selected: true },
                    "month",
                    "agenda"
                ],
                showWorkHours: true,
                selectable: true,
                //dataBinding: scheduler_dataBinding,
                //dataBound: scheduler_dataBound,
                save: TriSysSDK.Scheduler.Save,
                remove: TriSysSDK.Scheduler.Delete,
                edit: TriSysSDK.Scheduler.Edit,
                //add: scheduler_add,
                //cancel: scheduler_cancel,
                //change: scheduler_change,
                moveStart: TriSysSDK.Scheduler.MoveStart,
                move: TriSysSDK.Scheduler.Move,
                moveEnd: TriSysSDK.Scheduler.MoveEnd,
                //resizeStart: scheduler_resizeStart,
                //resize: scheduler_resize,
                //resizeEnd: scheduler_resizeEnd,
                navigate: TriSysSDK.Scheduler.Navigate,
                editable: {
                    confirmation: false
                },
                dataSource: dataSourceArray,
                //sync: function ()
                //{
                //    this.read();
                //},
                //dataSource: {
                //    batch: true,
                //    transport:
                //    {
                //        // Original pulled from Telerik test
                //        //read: {
                //        //    url: "http://demos.telerik.com/kendo-ui/service/tasks",
                //        //    dataType: "jsonp"
                //        //},


                //        // Clone of TriSysSDK.ServerPagingDataSource()
                //        read: function (options)
                //        {                      
                //            var successData = {
                //                dataTable: [
                //                    { "TaskID": 4, "OwnerID": 2, "Title": "Bowling tournament", "Description": "", "Start": new Date("2014/11/19 08:00"), "End": new Date("2014/11/19 09:00"), "RecurrenceRule": null, "RecurrenceID": null, "RecurrenceException": null, "IsAllDay": false }, //, "StartTimezone": TimezoneGMT, "EndTimezone": TimezoneGMT },
                //                    { "TaskID": 5, "OwnerID": 2, "Title": "Take the dog to the vet", "Description": "", "Start": new Date("2014/11/19 12:00"), "End": new Date("2014/11/19 15:00"), "RecurrenceRule": null, "RecurrenceID": null, "RecurrenceException": null, "IsAllDay": false } //, "StartTimezone": TimezoneGMT, "EndTimezone": TimezoneGMT }
                //                ],
                //                totalRecordCount: 2
                //            };
                //            options.success(successData);
                //        },


                //        update: {
                //            url: "http://demos.telerik.com/kendo-ui/service/tasks/update",
                //            dataType: "jsonp"
                //        },
                //        create: {
                //            url: "http://demos.telerik.com/kendo-ui/service/tasks/create",
                //            dataType: "jsonp"
                //        },
                //        destroy: {
                //            url: "http://demos.telerik.com/kendo-ui/service/tasks/destroy",
                //            dataType: "jsonp"
                //        },
                //        parameterMap: function(options, operation) {
                //            if (operation !== "read" && options.models) {
                //                return {models: kendo.stringify(options.models)};
                //            }
                //        }
                //    },
                //    schema: {
                //        model: {
                //            id: "taskID",
                //            fields: {
                //                taskID: { from: "TaskID", type: "number" },
                //                title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                //                start: { type: "date", from: "Start" },
                //                end: { type: "date", from: "End" },
                //                //startTimezone: { from: "StartTimezone" },
                //                //endTimezone: { from: "EndTimezone" },
                //                description: { from: "Description" },
                //                recurrenceId: { from: "RecurrenceID" },
                //                recurrenceRule: { from: "RecurrenceRule" },
                //                recurrenceException: { from: "RecurrenceException" },
                //                ownerId: { from: "OwnerID", defaultValue: 1 },
                //                isAllDay: { type: "boolean", from: "IsAllDay" }
                //            }
                //        }
                //    }
                //}

                resources: [
                    {
                        field: "taskTypeId",
                        dataSource: [
                            { value: 1, text: "Telephone Call", color: "#6699ff" },
                            { value: 2, text: "Meeting", color: "#ff99ff" },
                            { value: 3, text: "Note", color: "#ff9933" }
                        ],
                        title: "TaskTypeId"
                    }
                ]
            });
        },

        TasksTable: function ()
        {
            var myDataTable = [
                {
                    id: 1,
                    start: new Date("2014/11/25 08:00"),
                    end: new Date("2014/11/25 09:00"),
                    title: "Interview: interview with Miss Evans and her friend Wadkins",
                    description: "This is an interview with Miss Evans and her friend Wadkins",
                    taskTypeId: 1
                },
                {
                    id: 2,
                    start: new Date("2014/11/25 12:00"),
                    end: new Date("2014/11/25 14:00"),
                    title: "Meeting: meeting with Miss Evans and her friend Wadkins",
                    description: "This is a meeting with Miss Evans and her friend Wadkins",
                    taskTypeId: 2
                },
                {
                    id: 3,
                    start: new Date("2014/11/26 11:00"),
                    end: new Date("2014/11/26 14:00"),
                    title: "Note: catch-up with Miss Evans about her friend Wadkins",
                    description: "This is a catch-up with Miss Evans about her friend Wadkins",
                    taskTypeId: 3
                }
            ];

            return myDataTable;
        },

        Populate: function (sDivTag)
        {
            //http://docs.telerik.com/kendo-ui/api/javascript/ui/scheduler#methods-setDataSource
            var scheduler = $("#" + sDivTag).data("kendoScheduler");

            // Create a demo data source
            var dataSource = new kendo.data.SchedulerDataSource({
                data: TriSysSDK.Scheduler.TasksTable(),
                batch: true,
                serverAggregates: true,
                serverFiltering: true,
                serverGrouping: true,
                serverPaging: true,
                serverSorting: true
            });

            scheduler.setDataSource(dataSource);
        },

        Navigate: function (e)
        {
            TriSysApex.Logging.LogMessage(kendo.format("navigate:: action:{0}; view:{1}; date:{2:d};", e.action, e.view, e.date));
        },
        Edit: function (e)
        {
            e.preventDefault();
            TriSysApex.UI.ShowMessage("TriSysSDK.Scheduler.Edit(" + e.event.id + ")");

        },
        Move: function (e)
        {
            TriSysApex.Logging.LogMessage("TriSysSDK.Scheduler.Move");
        },
        MoveStart: function (e)
        {
            TriSysApex.Logging.LogMessage("TriSysSDK.Scheduler.MoveStart");
        },
        MoveEnd: function (e)
        {
            TriSysApex.Logging.LogMessage("TriSysSDK.Scheduler.MoveEnd");
        },
        Save: function (e)
        {
            TriSysApex.Logging.LogMessage("TriSysSDK.Scheduler.Save(" + e.event.id + ")");
        },
        Delete: function (e)
        {
            TriSysApex.Logging.LogMessage("TriSysSDK.Scheduler.Delete(" + e.event.id + ")");

            // Disable
            e.preventDefault();
        },

        // Testing only: DO NOT USE THIS AS IT HANGS THE BROWSER
        NextTaskId: 10,
        Add: function (lTaskId, sTitle, lTaskTypeId, dtStart, dtEnd)
        {
            return;
            var newEvent = {
                id: lTaskId,
                start: dtStart,
                end: dtEnd,
                title: sTitle,
                taskTypeId: lTaskTypeId
            };
            var scheduler = $("#scheduler").data("kendoScheduler");

            // does not work
            //scheduler.addEvent(newEvent);

            var dataSource = scheduler.dataSource;
            dataSource.options.data.push(newEvent);
            scheduler.dataSource = dataSource;
            scheduler.dataSource.sync();
        }
    },
    //#endregion TriSysSDK.Scheduler

    //#region TriSysSDK.Browser
    Browser:
    {
        // Our workaround for the CORS problem cannot do synchronous API calls.
        SupportsSynchronousAPI: function ()
        {
            var iVersion = TriSysSDK.Browser.InternetExplorerVersion();
            if (iVersion > 0 && iVersion < 10)
                return false;
            else
                return true;
        },

        CORSProxyKludgeApplied: false,
        CORSProxyKludge: function ()
        {
            if (TriSysSDK.Browser.CORSProxyKludgeApplied)
                return;

            var bKludgeRequired = false;
            var iVersion = TriSysSDK.Browser.InternetExplorerVersion();
            if (iVersion >= 8 && iVersion <= 9)
                bKludgeRequired = true;

            if (bKludgeRequired)
            {
                $.corsProxy.register({
                    url: 'https://api.trisys.co.uk/cors-proxy.html'
                });
            }

            TriSysSDK.Browser.CORSProxyKludgeApplied = true;
        },

        // Centralised dynamic javascript loader for legacy browser CORS support
        CORSProxyJavascriptLoad: function ()
        {
            var iVersion = TriSysSDK.Browser.InternetExplorerVersion();
            if (iVersion >= 8 && iVersion <= 9)
            {
                var sFile = "https://api.trisys.co.uk/javascript/cors-proxy.js";
                $.getScript(sFile, function (data, status, jqxhr)
                {
                    //  do something now that the script is loaded and code has been executed
                });
            }
        },

        // Internet Explorer pre 8 is NOT supported i.e. 6 & 7
        Supported: function ()
        {
            var iVersion = TriSysSDK.Browser.InternetExplorerVersion();
            if (iVersion > 0 && iVersion < 8)
                return false;
            else
                return true;
        },

        InternetExplorerVersion: function ()
        {
            var sVersion = TriSysSDK.Browser.Version();
            if (sVersion.indexOf("msie") === 0)
            {
                var sParts = sVersion.split(' ');
                var iVersion = parseInt(sParts[1], 10);
                return iVersion;
            }

            return -1;
        },

        // http://stackoverflow.com/questions/5916900/detect-version-of-browser
        Version: function ()
        {
            var uagent = navigator.userAgent.toLowerCase(), match = '';

            var _browser = {};
            _browser.chrome = /webkit/.test(uagent) && /chrome/.test(uagent);
            _browser.firefox = /mozilla/.test(uagent) && /firefox/.test(uagent);
            _browser.msie = /msie/.test(uagent) || /trident/.test(uagent);
            _browser.safari = /safari/.test(uagent) && /applewebkit/.test(uagent) && !/chrome/.test(uagent);
            _browser.opr = /mozilla/.test(uagent) && /applewebkit/.test(uagent) && /chrome/.test(uagent) && /safari/.test(uagent) && /opr/.test(uagent);
            _browser.version = '';

            for (x in _browser)
            {
                if (_browser[x])
                {
                    match = uagent.match(new RegExp("(" + x + ")( |/)([0-9]+)"));
                    if (match)
                    {
                        _browser.version = match[3];
                    }
                    else
                    {
                        match = uagent.match(new RegExp("rv:([0-9]+)"));
                        if (match)
                        {
                            _browser.version = match[1];
                        }
                    }

                    var sDescription = (x === "opr" ? "Opera" : x) + " " + _browser.version;
                    return sDescription;
                }
            }
        }
    }
    //#endregion TriSysSDK.Browser

}; // End of TriSysSDK namespace