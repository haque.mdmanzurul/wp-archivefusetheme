﻿var TriSysCMS =
{
    // Copyright Notice
    Copyright:      // TriSysCMS.Copyright
    {
        AllRightsReserved: "TriSys Business Software",
        LongProductDescription: "Content Management System for TriSys Web Technologies",
        ShortProductName: "TriSysCMS",
        HomePage: "www.trisys.co.uk"
    },

    //#region TriSysCMS.Security
    Security:
    {
        // Is the current user permissioned to make edits on the current live web page?
        ValidateLogin: function ()
        {
            // Page only allowed to be edited if the user (recruitment consultant working for the agency) has logged in
            var bValidated = TriSysApex.LoginCredentials.areUserCredentialsValid();
            bValidated = true;  // TESTING
            return bValidated;
        }
    },
    //#endregion TriSysCMS.Security

    //#region TriSysCMS.PageManager
    PageManager:
    {
        PageName: function ()
        {
            var sURL = window.location.href;
            var sPageName = TriSysApex.URL.Parser(sURL).getPath();
            return sPageName;
        },

        LoadContent: function (bPageInvocation)
        {
            // Read all customised content for this user from the Web API
            var arrMatches = TriSysCMS.PageManager.GetTags('trisys-cms-');
            if (!arrMatches)
            {
                // No CMS tags found, so get out of here
                return;
            }

            // Read content from cache
            var pageContentTags = TriSysCMS.Data.Cache.Read();

            // Enumerate through all segments and write to appropriate DIV tag
            TriSysCMS.PageManager.WriteContentToTags(arrMatches, pageContentTags);

            if (bPageInvocation)
            {
                // Asynchronously and silently retrieve the updated content from the Web API
                // and compare to the cache. If any difference, call us again only once in 
                // order to display the updated content.
                // TODO
                var asyncContent = TriSysCMS.Data.ReadCachedData();
                if (asyncContent)
                {
                    TriSysCMS.Data.Cache.Write(asyncContent)

                    TriSysCMS.PageManager.LoadContent(false);
                }
            }
        },

        WriteContentToTags: function (arrMatches, pageContentTags)
        {
            if (!arrMatches || !pageContentTags)
                return;

            for (var i = 0; i < arrMatches.length; i++)
            {
                var element = arrMatches[i];

                for (var k = 0; k < pageContentTags.length; k++)
                {
                    var pageContentTag = pageContentTags[k];

                    if (pageContentTag.ID == element.id)
                    {
                        element.innerHTML = pageContentTag.InnerHTML;
                        break;
                    }
                }
            }
        },

        GetTags: function (sStartTagName)
        {
            var matchingTags = [];

            // get all divs
            var divs = document.getElementsByTagName("*");

            // iterate over them to find matches
            for (var i = 0; i < divs.length; i++)
            {
                var divTag = divs[i];
                if (divTag.nodeType === 1)
                {
                    if (divTag.id.indexOf(sStartTagName) == 0)
                        matchingTags.push(divTag);
                }
            }

            return matchingTags;
        }
    },
    //#endregion TriSysCMS.PageManager

    //#region TriSysCMS.Data
    Data:
    {
        ReadCachedData: function ()
        {
            // TEST DATA
            //var asynContent = [];
            //var contentTag1 = {
            //    ID: "trisys-cms-opuslaboris-index-welcome",
            //    InnerHTML: 'Hello World 3!'
            //};
            //asynContent.push(contentTag1);
            //var contentTag2 = {
            //    ID: "trisys-cms-opuslaboris-index-clients",
            //    InnerHTML: 'Lorem Ipsum 3...'
            //};
            //asynContent.push(contentTag2);

            //return asynContent;

            return TriSysCMS.Data.Cache.Read();
        },

        WriteCachedData: function (editorObject)
        {
            var contentTag = {
                ID: editorObject.ID,
                InnerHTML: editorObject.InnerHTML
            };

            // Get cache
            var cachedData = TriSysCMS.Data.Cache.Read();

            // Find this entry and delete it
            if (cachedData)
            {
                for (var i = 0; i < cachedData.length; i++)
                {
                    var cachedElement = cachedData[i];
                    if (cachedElement.ID == contentTag.ID)
                    {
                        cachedData.splice(i, 1);
                        break;
                    }
                }
            }
            else
                cachedData = [];

            // Add to array of tags
            cachedData.push(contentTag);

            // Store
            TriSysCMS.Data.Cache.Write(cachedData);
        },

        Cache:
        {
            // Stored as cookies per page
            Read: function ()
            {
                var sCookieName = TriSysCMS.Constants.PageContent + '.' + TriSysCMS.PageManager.PageName();
                TriSysAPI.Cookies.CookiePrefix = TriSysCMS.Constants.CookieName;
                var sStringifiedJSON = TriSysAPI.Cookies.getCookie(sCookieName);
                if (sStringifiedJSON)
                {
                    var pageContentTags = JSON.parse(sStringifiedJSON);
                    return pageContentTags;
                }
            },

            Write: function (pageContent)
            {
                var sCookieName = TriSysCMS.Constants.PageContent + '.' + TriSysCMS.PageManager.PageName();
                var sStringifiedJSON = JSON.stringify(pageContent);
                TriSysAPI.Cookies.CookiePrefix = TriSysCMS.Constants.CookieName;
                TriSysAPI.Cookies.setCookie(sCookieName, sStringifiedJSON);
            }
        }
    },
    //#endregion TriSysCMS.Data

    //#region TriSysCMS.Editor
    Editor:
    {
        Open: function (this_divTag)
        {
            // The editable content must have an ID in order to be edited, persisted and restored
            if (!this_divTag)
                return;

            var sID = this_divTag.id;
            if (!sID)
                return;

            // User must also be logged in as the correct type
            if (!TriSysCMS.Security.ValidateLogin())
            {
                TriSysApex.UI.ShowMessage("Please login as a user to edit content");
                return;
            }

            var sHTML = this_divTag.innerHTML;
            var iTop = this_divTag.offsetTop;
            var iLeft = this_divTag.offsetLeft;

            //TriSysApex.UI.ShowMessage('TriSysCMS.Editor.Open(' + sID + '): ' + sHTML);

            // Setup the in-memory parameters for this browser session
            var editorObject = {
                ID: sID,
                InnerHTML: sHTML,
                Height: 700,
                Width: 900,
                EventType: ''
            };

            TriSysCMS.Editor.SetEditObject(editorObject);

            // Add a listener event to catch when the user updated the content so that we can refresh for each save, rather than when the dialogue closes
            window.addEventListener('storage', TriSysCMS.Editor.EditorEventHandler, false);

            // Open the editor dialogue now
            TriSysCMS.Editor.OpenModalDialogue("Edit Web Page Content", editorObject.Height, editorObject.Width);
        },

        SetEditObject: function (editorObject)
        {
            TriSysAPI.Cookies.CookiePrefix = TriSysCMS.Constants.CookieName;
            var sStringifiedJSON = JSON.stringify(editorObject);
            TriSysAPI.Cookies.setCookie(TriSysCMS.Constants.EditObject, sStringifiedJSON);
        },

        GetEditObject: function ()
        {
            TriSysAPI.Cookies.CookiePrefix = TriSysCMS.Constants.CookieName;
            var sStringifiedJSON = TriSysAPI.Cookies.getCookie(TriSysCMS.Constants.EditObject);
            if (!sStringifiedJSON)
                return;
            var editorObject = JSON.parse(sStringifiedJSON);
            return editorObject;
        },

        OpenModalDialogue: function (sTitle, lHeight, lWidth)
        {
            var fnCancelSend = function ()
            {
                TriSysApex.UI.CloseTopmostModalPopup();
            };

            // Show the dialogue now
            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.Image = 'information';
            parametersObject.Height = lHeight;
            parametersObject.Width = lWidth + 2;
            parametersObject.Resizable = false;

            parametersObject.ContentURL = 'http://www.trisys.co.uk/development/test/cms/editor.aspx';

            TriSysApex.UI.PopupMessage(parametersObject);
            return;
        },

        SendSaveEventToPageBeingEdited: function (editorObjectContent, sEditedText)
        {
            editorObjectContent.InnerHTML = sEditedText
            editorObjectContent.EventType = 'Edit';

            // Send back to host page so that it gets picked up to reflect changes immediately
            // Use session storage for this as it works on browsers which we expect to be used for designing pages
            var sJSON = JSON.stringify(editorObjectContent);
            sessionStorage.setItem('storage', sJSON);
        },

        CloseEditorEvent: function (editorObjectContent)
        {
            editorObjectContent.EventType = 'Close';

            // Send back to host page using the event driven session storage
            var sJSON = JSON.stringify(editorObjectContent);
            sessionStorage.setItem('storage', sJSON);
        },

        EditorEventHandler: function (event)
        {
            //alert('event fired');
            var sJSON = event.newValue;
            if (sJSON)
            {
                var editorObject = JSON.parse(sJSON);
                if (editorObject)
                {
                    var sEventType = editorObject.EventType;
                    switch (sEventType)
                    {
                        case 'Edit':
                            document.getElementById(editorObject.ID).innerHTML = editorObject.InnerHTML;
                            TriSysCMS.Editor.SetEditObject(editorObject);

                            // TODO: Save to API asynchronously using lazy writer mechanism (to design also ;-)
                            TriSysCMS.Data.WriteCachedData(editorObject);
                            break;

                        case 'Close':
                            TriSysApex.UI.CloseTopmostModalPopup();
                            break;
                    }
                }
            }
        }

    },
    //#endregion TriSysCMS.Editor

    Constants:
    {
        CookieName: "TriSysCMS.Editor",
        EditObject: "EditObject",
        PageContent: "PageContent"
    }
};
