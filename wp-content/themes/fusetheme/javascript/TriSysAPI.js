﻿/* TriSys Web API Specific Javascript Code
*
* (c) 2013 TriSys Business Software
*
*/

// The TriSysAPI namespace
var TriSysAPI =
{
    // Copyright Notice
    Copyright:
    {
        AllRightsReserved: "TriSys Business Software"
    },

    //#region TriSysAPI.Data

    Data:
    {
        // Wrapper for all JQuery/Ajax calls so that we have centralised control.
        // We have found that POST is the most versatile HTTP verb.
        PostToWebAPI: function (payloadObject)
        {
            var bAsynchronous = true;
            var sURL = null;
            var sAPIurl = TriSysAPI.Data.BaseAddress();
            var dataPacketOutbound = null;
            var fDataPacketInboundFunction = null;  // function (data) { so something with data; }
            var fErrorHandlerFunction = null;       // function (request, status, error) { so something with data; }
            var sHTTPVerb = "POST";
            var sFunction = "TriSysAPI.Data.PostToWebAPI(): ";
            var bShowErrorAlert = true;

            if (payloadObject)
            {
                try
                {
                    if (!TriSysAPI.Operators.isEmpty(payloadObject.URL))
                    {
                        sURL = sAPIurl + payloadObject.URL;
                        //alert('API: ' + sURL);
                    }

                    if (!TriSysAPI.Operators.isEmpty(payloadObject.OutboundDataPacket))
                        dataPacketOutbound = payloadObject.OutboundDataPacket;

                    if (!TriSysAPI.Operators.isEmpty(payloadObject.InboundDataFunction))
                        fDataPacketInboundFunction = payloadObject.InboundDataFunction;

                    if (!TriSysAPI.Operators.isEmpty(payloadObject.ErrorHandlerFunction))
                    {
                        fErrorHandlerFunction = payloadObject.ErrorHandlerFunction;
                        bShowErrorAlert = false;
                    }

                    if (!TriSysAPI.Operators.isEmpty(payloadObject.Asynchronous))
                    {
                        bAsynchronous = payloadObject.Asynchronous;
                    }

                }
                catch (err)
                {
                    alert(sFunction + err.message);
                }
            }

            if (!sURL)
            {
                alert(sFunction + 'missing URL parameter.');
                return;
            }

            if (!fDataPacketInboundFunction)
            {
                alert(sFunction + 'missing inbound data function.');
                return;
            }

            // Browser compatibility to support legacy Internet Explorer
            TriSysSDK.Browser.CORSProxyKludge();

            if (!bAsynchronous)
            {
                if (TriSysSDK.Browser.SupportsSynchronousAPI())
                {
                    // Turn off asynchronicity where calls must be synchronised
                    $.ajaxSetup({ async: false });
                }
                else
                {
                    // If we are unable to perform synchronous API calls (IE 6, 7, 8, 9), then return
                    fDataPacketInboundFunction("");
                    return;
                }
            }

            //alert('About to post $.ajax() to ' + sURL);

            $.ajaxSetup({
                accepts: {
                    xml: "application/xml, text/xml",
                    html: "text/html",
                    script: "text/javascript, application/javascript",
                    json: "application/json, text/javascript",
                    text: "text/plain",
                    // Hack for Chrome/Safari
                    _default: ""
                }
            });

            $.ajax({
                url: sURL,
                data: dataPacketOutbound,
                type: sHTTPVerb,
                dataType: 'json',
                //contentType: "application/json",      F****NG EVIL LINE BREAKS POST PARAMETERS

                crossDomain: true,
                cache: false,
                beforeSend: function (xhr)
                {
                    var sSiteKey = TriSysAPI.Session.DeveloperSiteKey();
                    xhr.setRequestHeader('SiteKey', sSiteKey);

                    var sDataServicesKey = TriSysAPI.Session.DataServicesKey();
                    if (!TriSysAPI.Operators.isEmpty(sDataServicesKey))
                        xhr.setRequestHeader('DataServicesKey', sDataServicesKey);
                },
                success: function (data)
                {
                    fDataPacketInboundFunction(data);
                },

                error: function (request, status, error)
                {
                    if (fErrorHandlerFunction)
                        fErrorHandlerFunction(request, status, error);

                    if (bShowErrorAlert)
                        TriSysApex.UI.ErrorHandlerRedirector(sFunction, request, status, error);
                }
            });


            if (!bAsynchronous)
            {
                // Turn on asynchronicity as this should be the default for all Ajax calls
                $.ajaxSetup({ async: true });
            }
        },

        BaseAddress: function ()
        {
            var sAPIurl = TriSysAPI.Constants.SecureURL;    // Note: Change to http://localhost:61333/ for testing API source code

            var bDebuggingAPIServerCode = false;

            if (bDebuggingAPIServerCode)
            {
                var sThisURL = window.location.href;
                try
                {
                    if (sThisURL.indexOf("http://localhost") === 0)
                        sAPIurl = '/';
                }
                catch (ex)
                {
                    alert('BaseAddress exception: ' + ex.message);
                }
            }

            return sAPIurl;
        },

        Version: function ()
        {
            var payloadObject = {};
            payloadObject.URL = "Start/VersionNumber";
            payloadObject.Asynchronous = false;

            var sVersion = null;

            payloadObject.InboundDataFunction = function (data)
            {
                sVersion = data;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                alert('TriSysAPI.Data.Version: ' + status + ": " + error + ". responseText: " + request.responseText);
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);

            return sVersion;
        },

        IPAddress: function (bSupressError)
        {
            var payloadObject = {};
            payloadObject.URL = "Start/IPAddress";
            payloadObject.Asynchronous = false;

            var sIPAddress = null;

            payloadObject.InboundDataFunction = function (data)
            {
                sIPAddress = data;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                if (TriSysAPI.Operators.isEmpty(bSupressError))
                    alert('TriSysAPI.Data.IPAddress: ' + status + ": " + error + ". responseText: " + request.responseText);
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);

            return sIPAddress;
        },

        VerifyDataServicesKey: function ()
        {
            if (!TriSysSDK.Browser.SupportsSynchronousAPI())
            {
                var sDataServicesKey = TriSysAPI.Session.DataServicesKey();
                if (!TriSysAPI.Operators.isEmpty(sDataServicesKey))
                    return true;
                else
                    return false;
            }

            var payloadObject = {};
            payloadObject.URL = "Security/VerifyDataServicesKey";
            payloadObject.Asynchronous = false;

            var bVerified = false;

            payloadObject.InboundDataFunction = function (data)
            {
                if (data)
                    bVerified = data.Success;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                //alert('TriSysAPI.Data.VerifyDataServicesKey: ' + status + ": " + error + ". responseText: " + request.responseText);
                // We are not connected or something has gone wrong
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);

            return bVerified;
        },

        DrawTable: function (sDivTag, sEntityName, entityCollection, columnCollection)
        {
            var sTable = '<table width="100%" border="1">' +
                        ' <tr>' +
                        TriSysAPI.Data.TableHeader(sEntityName, columnCollection) +
                        ' </tr>  \r\n';
            var sRow = null;
            var entityObject = null;

            switch (sEntityName)
            {
                case 'Counters':
                    entityObject = entityCollection;

                    sRow = TriSysAPI.Data.TableColumn("Contact") +
                                    TriSysAPI.Data.TableColumn(entityObject.Contacts);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("Company") +
                                    TriSysAPI.Data.TableColumn(entityObject.Companies);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("Requirement") +
                                    TriSysAPI.Data.TableColumn(entityObject.Requirements);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("Placement") +
                                    TriSysAPI.Data.TableColumn(entityObject.Placements);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("Timesheet") +
                                    TriSysAPI.Data.TableColumn(entityObject.Timesheets);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("Task") +
                                    TriSysAPI.Data.TableColumn(entityObject.Tasks);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                    sRow = TriSysAPI.Data.TableColumn("User") +
                                    TriSysAPI.Data.TableColumn(entityObject.Users);
                    sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";

                    entityCollection = null;
                    break;
            }

            // Enumerate through data collection
            if (entityCollection)
            {

                for (i = 0; i < entityCollection.length; i++)
                {

                    entityObject = entityCollection[i];
                    var sHyperlink = null;
                    sRow = null;

                    switch (sEntityName)
                    {

                        case 'Contact':
                            sHyperlink = TriSysAPI.Data.HyperlinkColumn("Contact.aspx?ContactId=",
                                                            entityObject.ContactId, entityObject.ContactId);
                            sRow = sHyperlink +
                                    TriSysAPI.Data.TableColumn(entityObject.FullName) +
                                    TriSysAPI.Data.TableColumn(entityObject.CompanyName) +
                                    TriSysAPI.Data.TableColumn(entityObject.JobTitle);
                            break;

                        case 'User':
                            sHyperlink = TriSysAPI.Data.HyperlinkColumn("User.aspx?UserId=",
                                                            entityObject.UserId, entityObject.UserId);
                            sRow = sHyperlink +
                                    TriSysAPI.Data.TableColumn(entityObject.FullName) +
                                    TriSysAPI.Data.TableColumn(entityObject.LoginName);
                            break;

                        case 'FieldDescription':
                        case 'DataSet':
                            for (iFD = 1; iFD <= columnCollection.length; iFD++)
                            {
                                var sColumnName = columnCollection[iFD - 1];
                                var objValues = entityObject.Values;

                                var sValue = null;
                                for (jFD = 0; jFD < objValues.length; jFD++)
                                {
                                    if (objValues[jFD].ColumnName == sColumnName)
                                    {
                                        sValue = objValues[jFD].ColumnValue;
                                        break;
                                    }
                                }

                                if (!sRow)
                                    sRow = '';

                                sRow = sRow + TriSysAPI.Data.TableColumn(sValue);
                            }
                            break;
                    }

                    if (sRow)
                        sTable = sTable + "<tr>" + sRow + "</tr>  \r\n";
                }
            }

            sTable = sTable + "</table>";

            $(sDivTag).html(sTable);
        },

        DrawTableFooter: function (sDivTag, jsonServerPackage, dataPacket,
                                                    sPagingComboID, functionCallback)
        {

            var sPages = '';
            for (i = 1; i <= jsonServerPackage.TotalPageCount; i++)
            {

                if (i != jsonServerPackage.PageNumber)
                {
                    var sFunction = "javascript:trisysGridPager(" + i + ");";

                    var sHyperlink = '<a href="javascript:void(0)" onclick=' + sFunction + '>' + i + '</a>';
                    //var sHyperlink = '<a href="javascript:void(0)" onclick="javascript:' + sFunction + '">' + i + '</a>';
                    sPages = sPages + sHyperlink;
                }
                else
                    sPages = sPages + i;

                sPages = sPages + " &nbsp; ";

                if (i == 50)
                    break;
            }

            var sTableFooter = '<table width="100%" border="1">' +
                                '<tr>' +
                                '   <td>' + sPages +
                                '   </td>' +
                                '</tr>' +
                                '</table>';

            sTableFooter = sTableFooter + "<script>" +
                                "function trisysGridPager(iPage) " +
                                " { " +
                                "   $('" + sPagingComboID + "').val(iPage);" +
                                "   " + functionCallback + ";" +
                                " }" +
                                "</script>";

            $(sDivTag).append(sTableFooter);
        },

        TableHeader: function (sEntityName, columnCollection)
        {

            var sHTML = null;

            switch (sEntityName)
            {

                case 'Contact':

                    sHTML = TriSysAPI.Data.TableColumn("ContactId", true) +
                            TriSysAPI.Data.TableColumn("Full Name", true) +
                            TriSysAPI.Data.TableColumn("Company Name", true) +
                            TriSysAPI.Data.TableColumn("job Title", true);
                    break;

                case 'User':

                    sHTML = TriSysAPI.Data.TableColumn("UserId", true) +
                            TriSysAPI.Data.TableColumn("Full Name", true) +
                            TriSysAPI.Data.TableColumn("Login Name", true);
                    break;

                case 'Counters':

                    sHTML = TriSysAPI.Data.TableColumn("Table/Entity", true) +
                            TriSysAPI.Data.TableColumn("Record Count", true);
                    break;

                case 'FieldDescription':
                case 'DataSet':

                    for (i = 1; i <= columnCollection.length; i++)
                    {
                        if (!sHTML) sHTML = '';
                        var sColumnName = columnCollection[i - 1];
                        sHTML = sHTML + TriSysAPI.Data.TableColumn(sColumnName, true);
                    }
                    break;
            }

            return sHTML;
        },

        TableColumn: function (sText, bHeader)
        {
            if (bHeader)
                sText = '<b>' + sText + '</b>';

            var sHTML = "<td>" + sText + "</td>";
            return sHTML;
        },

        HyperlinkColumn: function (sPageURL, lRecordId, sDisplayText)
        {

            var sText = '<a href="' + sPageURL + lRecordId + '">' + sDisplayText + '</a>';

            var sHTML = TriSysAPI.Data.TableColumn(sText);
            return sHTML;
        }
    },
    //#endregion TriSysAPI.Data

    //#region TriSysAPI.Session

    Session:
    {
        // These are held in memory only
        DeveloperSiteKey: function ()
        {
            var s = TriSysAPI.Session.Memory.Get("DeveloperSiteKey");
            return s;
        },

        SetDeveloperSiteKey: function (sKey)
        {
            TriSysAPI.Session.Memory.Set("DeveloperSiteKey", sKey, 1);
        },

        SetDataServicesKey: function (sKey)
        {
            TriSysAPI.Session.Memory.Set("DataServicesKey", sKey, 1);
        },

        DataServicesKey: function ()
        {
            try
            {
                var s = TriSysAPI.Session.Memory.Get("DataServicesKey");
                return s;
            }
            catch (ex)
            {
                // Ignore 'not found'
                return null;
            }
        },

        GetLastAppRefreshDateTime: function ()
        {
            var dt = TriSysAPI.Session.Memory.Get('LastAppRefreshDateTime');
            return dt;
        },
        SetLastAppRefreshDateTime: function ()
        {
            var dt = new Date();
            TriSysAPI.Session.Memory.Set('LastAppRefreshDateTime', dt);
        },


        // These are held in cookies on the machine
        UserName: function ()
        {
            var s = TriSysAPI.Cookies.getCookie('UserName');
            return s;
        },
        SetUserName: function (s)
        {
            TriSysAPI.Cookies.setCookie('UserName', s, 1);
        },
        CompanyName: function ()
        {
            var s = TriSysAPI.Cookies.getCookie('CompanyName');
            return s;
        },
        SetCompanyName: function (s)
        {
            TriSysAPI.Cookies.setCookie('CompanyName', s, 1);
        },
        Password: function ()
        {
            // Stored in cookies
            var s = TriSysAPI.Cookies.getCookie('Password');
            return s;
        },
        SetPassword: function (s)
        {
            TriSysAPI.Cookies.setCookie('Password', s, 1);
        },
        SaveLoginCredentials: function ()
        {
            // Stored in cookies
            var s = TriSysAPI.Cookies.getCookie('SaveLoginCredentials');
            var b = TriSysAPI.Operators.stringToBoolean(s, false);
            return b;
        },
        SetSaveLoginCredentials: function (b)
        {
            // Store in cookies
            TriSysAPI.Cookies.setCookie('SaveLoginCredentials', b, 1);
        },

        // 22 Sep 2014: Are we connected to service API?
        isConnected: function ()
        {
            if (TriSysSDK.Browser.SupportsSynchronousAPI())
            {
                var sIPAddress = TriSysAPI.Data.IPAddress();
                if (!TriSysAPI.Operators.isEmpty(sIPAddress))
                {
                    if (sIPAddress.length >= 7)     // 1.1.1.1
                        return true;
                }
            }
            else
            {
                // Have to assume another mechanism - check for a data services key
                var sDataServicesKey = TriSysAPI.Session.DataServicesKey();
                if (!TriSysAPI.Operators.isEmpty(sDataServicesKey))
                    return true;
            }
            return false;
        },


        // 23 July 2014: Code lifted from http://www.sitepoint.com/blogs/2009/09/03/javascript-session-variable-library
        // to provide us with proper session based storage rather than persistent.
        // This is much more secure and harder to crack.
        // Note how I have used additional brackets on function declaration to allow using code to NOT have to use ()?
        /*
         * Usage:
         *
         * // store a session value/object
         * TriSysAPI.Session.Memory.Set(name, object);
         *
         * // retreive a session value/object
         * TriSysAPI.Session.Memory.Get(name);
         *
         * // clear all session data
         * TriSysAPI.Session.Memory.Clear();
         *
         * // dump session data
         * TriSysAPI.Session.Memory.Dump();
         */

        Memory: (function ()      // TriSysAPI.Session.Memory
        {
            // window object
            var win = null;

            try
            {
                win = window.top || window;
            }
            catch (err)
            {
                // Cross-domain problem
                alert("Cross domain bug #1");
                return null;
            }


            // session store
            var store = null;

            try
            {
                store = (win.name ? JSON.parse(win.name) : {});
            }
            catch (err)
            {
                // Cross-domain problem
                alert("Cross domain bug #2");
                //return null;
            }

            // save store on page unload
            function Save()
            {
                win.name = JSON.stringify(store);
            };

            // page unload event
            if (window.addEventListener) window.addEventListener("unload", Save, false);
            else if (window.attachEvent) window.attachEvent("onunload", Save);
            else window.onunload = Save;

            // public methods
            return {

                // set a session variable
                Set: function (name, value)
                {
                    store[name] = value;
                },

                // get a session value
                Get: function (name)
                {
                    return (store[name] ? store[name] : null);
                },

                // clear session
                Clear: function () { store = {}; },

                // dump session data
                Dump: function () { return JSON.stringify(store); }
            };
        })()

    },
    //#endregion TriSysAPI.Session

    //#region TriSysAPI.Cookies

    Cookies:
    {
        //CookiePrefix: 'TriSysAPI_',

        setCookie: function (c_name, value, exdays)
        {

            //c_name = TriSysAPI.Cookies.CookiePrefix + c_name;

            try
            {
                // PhoneGap Compatible!
                window.localStorage.setItem(c_name, value);
                return;

                // Browser cookies
                var exdate = new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                document.cookie = c_name + "=" + c_value;
            }
            catch (err)
            {
                alert('setCookie error: ' + err.message);
            }
        },

        getCookie: function (c_name)
        {

            //c_name = TriSysAPI.Cookies.CookiePrefix + c_name;

            //alert('getCookie: ' + c_name);

            try
            {
                // PhoneGap Compatible!
                var value = window.localStorage.getItem(c_name);
                return value;

                // Browser cookies
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1)
                {
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1)
                {
                    c_value = null;
                }
                else
                {
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1)
                    {
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start, c_end));
                }

                var sDisplay = c_value;
                if (sDisplay)
                    sDisplay = sDisplay.substring(0, 50) + '...';

                alert('getCookie(' + c_name + ') = \n ' + sDisplay);
                return c_value;
            }
            catch (err)
            {
                alert('getCookie error: ' + err.message);
            }

        }
    },

    //#endregion TriSysAPI.Cookies

    //#region TriSysAPI.Persistence
    Persistence:
    {
        Read: function (sObjectName)
        {
            var sObjectString = TriSysAPI.Cookies.getCookie(sObjectName);
            if (sObjectString)
            {
                // Convert from a string to the native JSON object
                return JSON.parse(sObjectString);
            }
        },

        Write: function (sObjectName, object)
        {
            // Cache as a cookie
            var sObjectString = JSON.stringify(object);
            TriSysAPI.Cookies.setCookie(sObjectName, sObjectString);
        }
    },
    //#endregion TriSysAPI.Persistence

    //#region TriSysAPI.Operators

    Operators:
    {
        isEmpty: function (obj)
        {

            try
            {
                if (
                    obj === "" ||
                    obj === null ||
                    obj === "NULL" ||
                    obj === undefined //|| obj === false || obj === 0 || obj === "0" ||
                )
                {
                    return true;
                }

                if (typeof (obj) === 'object')
                {
                    var i = 0;
                    for (key in obj)
                    {
                        i++;
                    }
                    if (i === 0) { return true; }
                }
                return false;
            }
            catch (err)
            {
            }

            return true;
        },

        stringToBoolean: function (str, bDefault)
        {
            if (str)
            {
                switch (str.toLowerCase())
                {
                    case "true": case "yes": case "1": return true;
                    case "false": case "no": case "0": case null: return false;
                    default: return Boolean(str);
                }
            }
            else
            {
                return bDefault;
            }
        },

        booleanToString: function (b)
        {
            try
            {
                if (b)
                    return 'true';
                else
                    return 'false';
            }
            catch (ex)
            {
                return false;
            }
        }

    },
    //#endregion TriSysAPI.Operators

    //#region TriSysAPI.Encryption
    // Only intended for client-side encryption per application so that they can have different ciphers
    // Used to store e-mail addresses/passwords etc.. as cookies
    Encryption:
    {
        legal_characters: "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_´abcdefghijklmnopqrstuvwxyz{|}~€_‚£€©",

        encrypt: function (cookie_value, encrypt_key)
        {
            try
            {
                var cookie_character;
                var character_location;
                var encrypted_location;
                var encrypted_character;

                // This variable holds the encrypted cookie characters
                var encrypted_string = "";

                // Run through each character in the cookie value
                for (var counter = 0; counter < cookie_value.length; counter++)
                {
                    // Get the current cookie character
                    cookie_character = cookie_value.substring(counter, counter + 1);

                    // Get the character's location in the string of legal characters
                    character_location = TriSysAPI.Encryption.legal_characters.indexOf(cookie_character);

                    // XOR the character location with the encrypt_key
                    encrypted_location = character_location ^ encrypt_key;

                    // Use the encrypted location to specify the encrypted 
                    // character within the string of legal characters
                    encrypted_character = TriSysAPI.Encryption.legal_characters.substring(encrypted_location, encrypted_location + 1);

                    // Add the encrypted character to the string
                    encrypted_string += encrypted_character;

                }
                return encrypted_string;
            }
            catch (err)
            {
                return cookie_value;
            }
        },

        decrypt: function (encrypted_string, encrypt_key)
        {
            try
            {
                var cookie_character;
                var character_location;
                var encrypted_location;
                var encrypted_character;

                // This variable holds the decrypted cookie value
                var cookie_value = "";

                // Run through each character in the encrypted string
                for (var counter = 0; counter < encrypted_string.length; counter++)
                {
                    // Get the current encrypted character
                    encrypted_character = encrypted_string.substring(counter, counter + 1);

                    // Get the character's location in the string of legal characters
                    encrypted_location = TriSysAPI.Encryption.legal_characters.indexOf(encrypted_character);

                    // XOR the character location with the encrypt_key
                    character_location = encrypted_location ^ encrypt_key;

                    // Use the character location to specify the plain text 
                    // character within the string of legal characters
                    cookie_character = TriSysAPI.Encryption.legal_characters.substring(character_location, character_location + 1);

                    // Add the plain text character to the string
                    cookie_value += cookie_character;
                }
                return cookie_value;
            }
            catch (err)
            {
                return encrypted_string;
            }
        }
    },
    //#endregion TriSysAPI.Encryption

    //#region TriSysAPI.Constants
    Constants:
    {
        SecureURL: 'https://api.trisys.co.uk/'    //    SECURE VERSION
    }
    //#endregion TriSysAPI.Constants
};
