﻿/* TriSys Apex Javascript Code
 * The Apex framework is the operational behaviour of the application.
 * It takes care of layout, events, session state, login mechanism,
 * forms, paging, and all other application framework stuff.
 *
 * (c) 2013-2014 TriSys Business Software
 *
 */

// The TriSysApex Namespace
var TriSysApex =
{
    // Copyright Notice
    Copyright:      // TriSysApex.Copyright
    {
        AllRightsReserved: "TriSys Business Software",
        LongProductDescription: "Customer Relationship Management System for Recruitment Agencies",
        ShortProductName: "TriSys",
        HomePage: "www.trisys.co.uk"
    },

    //#region TriSysApex.UI

    // User Interface Namespace: TriSysApex.UI
    UI:
    {
        iTriSysMessageBoxWidth: 500,

        ShowMessage: function (sMessage, sTitle, fnButton)
        {
            if (!sTitle)
                sTitle = TriSysApex.Copyright.ShortProductName + " Message";

            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.MessageText = sMessage;
            parametersObject.Image = 'information';

            parametersObject.ButtonCentreVisible = true;
            parametersObject.ButtonCentreText = "OK";
            if (!fnButton)
                fnButton = function () { return true };

            parametersObject.ButtonCentreFunction = fnButton;

            TriSysApex.UI.PopupMessage(parametersObject);
        },

        ShowError: function (catchErrorObject, sTitle, fOKButtonFunction)
        {
            if (!sTitle)
                sTitle = TriSysApex.Copyright.ShortProductName + " Error";

            var sMessage = null;
            try
            {
                sMessage = catchErrorObject;
                var sObjectMessage = catchErrorObject.message;
                if (typeof (element) != 'undefined' && element != null)
                    sMessage = sObjectMessage;
            }
            catch (err)
            {
                // Caller screwed up
            }

            if (!fOKButtonFunction)
                fOKButtonFunction = function () { return true };

            TriSysApex.UI.errorAlert(sMessage, sTitle, fOKButtonFunction);
        },

        ShowWait: function (sCaption, sMessage)
        {
            if (!sCaption)
                sCaption = "Please Wait";

            var lHeight = 200;      // Fixed!
            $('#modal-position').height(lHeight);

            $('#modal-message').html(sMessage);
            $('#pleaseWaitDiv').modal();
        },

        HideWait: function ()
        {
            $('#pleaseWaitDiv').modal('hide');
        },

        ShowMessageParameters: function ()
        {
            this.Title = TriSysApex.Copyright.ShortProductName + ' Message';
            this.MessageText = '';
            this.ButtonLeftVisible = false;
            this.ButtonLeftText = 'OK';
            this.ButtonLeftWidth = 50;
            this.ButtonLeftFunction = null;
            this.ButtonCentreVisible = false;
            this.ButtonCentreText = '?';
            this.ButtonCentreWidth = 50;
            this.ButtonCentreFunction = null;
            this.ButtonRightVisible = false;
            this.ButtonRightText = 'Cancel';
            this.ButtonRightWidth = 50;
            this.ButtonRightFunction = null;
            this.Height = 300;
            this.Width = 500; //TriSysApex.UI.iTriSysMessageBoxWidth;
            this.Resizable = true;
            this.Maximize = true;
            this.Image = 'information';
            this.Layer = 1;
            this.ContentURL = TriSysApex.Constants.UserControlFolder + 'ctrlTriSysMessageBox.html';

            // Generic Modal Dialogue
            this.ModalDialogueContainer = 'ctrlModalDialogue.html';
            this.UserControlFolder = TriSysApex.Constants.UserControlFolder;
            this.ModalControl = null;       // e.g. ctrlDoSomethingModally.html
            this.OnLoadCallback = null;     // Called after loading the above user control
        },

        CurrentShowMessageParameters: null,     // The current show message instance of ShowMessageParameters() above


        // New Oct 2014 PopupMessage
        BootstrapPopupMessage: function (parametersObject)
        {
            var typeOfDialogue = BootstrapDialog.TYPE_PRIMARY;
            switch (parametersObject.Image)
            {
                case 'error':
                    typeOfDialogue = BootstrapDialog.TYPE_DANGER;
                    break;
                case 'question':
                    typeOfDialogue = BootstrapDialog.TYPE_SUCCESS;
                    break;
            }

            var eSize = BootstrapDialog.SIZE_NORMAL;

            var fnMessage = parametersObject.MessageText;
            var dataSource = null;
            if (parametersObject.ContentURL.indexOf("ctrlTriSysMessageBox.html") < 0)
            {
                // The URL should always be local .html files, but we do not want cacheing
                var sURLContent = parametersObject.ContentURL + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache(parametersObject.ContentURL);
                dataSource = {
                    'pageToLoad': sURLContent
                };
                fnMessage = function (dialog)
                {
                    var $message = $('<div></div>');
                    var pageToLoad = dialog.getData('pageToLoad');
                    $message.load(pageToLoad);

                    return $message;
                };
            }

            var buttonsArray = [];
            if (parametersObject.ButtonLeftText && parametersObject.ButtonLeftVisible)
            {
                buttonsArray.push({
                    label: parametersObject.ButtonLeftText,
                    cssClass: 'btn-danger',
                    action: function (dialogItself)
                    {
                        if (parametersObject.ButtonLeftFunction)
                            if (!parametersObject.ButtonLeftFunction())
                                return;
                        dialogItself.close();
                    }
                });
            }
            if (parametersObject.ButtonCentreText && parametersObject.ButtonCentreVisible)
            {
                buttonsArray.push({
                    label: parametersObject.ButtonCentreText,
                    cssClass: 'btn-success',
                    action: function (dialogItself)
                    {
                        if (parametersObject.ButtonCentreFunction)
                            if (!parametersObject.ButtonCentreFunction())
                                return;
                        dialogItself.close();
                    }
                });
            }
            if (parametersObject.ButtonRightText && parametersObject.ButtonRightVisible)
            {
                buttonsArray.push({
                    label: parametersObject.ButtonRightText,
                    cssClass: 'btn-warning',
                    action: function (dialogItself)
                    {
                        if (parametersObject.ButtonRightFunction)
                            if (!parametersObject.ButtonRightFunction())
                                return;
                        dialogItself.close();
                    }
                });
            }

            var fnOnShown = null;
            if (parametersObject.OnLoadCallback)
            {
                fnOnShown = function ()
                {
                    parametersObject.OnLoadCallback();
                };
            }

            BootstrapDialog.show({
                title: parametersObject.Title,
                message: fnMessage,
                data: dataSource,
                type: typeOfDialogue,
                size: eSize,
                draggable: true,
                closable: true,
                closeByBackdrop: false,
                closeByKeyboard: false,
                buttons: buttonsArray,
                onshown: fnOnShown
            });
        },

        // Popup a message explicitly telling the user 
        PopupMessage: function (parametersObject)
        {
            // New Oct 2014 PopupMessage
            TriSysApex.UI.BootstrapPopupMessage(parametersObject);
            return;

            function FixupWindowIcon(sImage)
            {
                var sClass = 'TriSys-PopupWindow-Icon-Information';
                switch (sImage)
                {
                    case 'error':
                        sClass = 'TriSys-PopupWindow-Icon-Error';
                        break;
                    case 'question':
                        sClass = 'TriSys-PopupWindow-Icon-Question';
                        break;
                    case 'document':
                        sClass = 'TriSys-PopupWindow-Icon-Document';
                        break;
                    case 'upload':
                        sClass = 'TriSys-PopupWindow-Icon-Upload';
                        break;
                    case 'userprofile':
                        sClass = 'TriSys-PopupWindow-Icon-UserProfile';
                        break;
                        //default:
                        //    sClass = 'TriSys-PopupWindow-Icon-' + sImage;
                        //    break;
                }

                // Fix the KendoUI window icon
                $(".k-window-title").prepend('<div id="divWindowIconKludge" class="' + sClass + '"></div>');
            }

            function setWindowPopupSize(win, iHeight, iWidth)
            {
                var iDeviceHeight = document.body.offsetHeight;
                var iDeviceWidth = document.body.offsetWidth;
                var iPadding = 16;

                if (iHeight > iDeviceHeight)
                    iHeight = iDeviceHeight - iPadding;

                if (iWidth > iDeviceWidth)
                    iWidth = iDeviceWidth - iPadding;

                win.setOptions({
                    height: iHeight,
                    width: iWidth
                });
            }

            var iMinHeight = 200, iMinWidth = 200;
            var actions = ["Close"];
            if (!parametersObject.Resizable)
            {
                iMinHeight = parametersObject.Height;
                iMinWidth = parametersObject.Width;
            }
            else
                actions.unshift("Maximize");

            // Modal dialogue support
            if (parametersObject.ModalControl)
                parametersObject.ContentURL = parametersObject.UserControlFolder + parametersObject.ModalDialogueContainer;


            // The URL should always be local .html files, but we do not want cacheing
            var sURLContent = parametersObject.ContentURL + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache(parametersObject.ContentURL);

            // Set up the global message parameters object: See messageBoxReady()
            TriSysApex.UI.CurrentShowMessageParameters = parametersObject;

            var sJQueryDivTag = TriSysApex.Constants.ModalPopupJQueryDiv;
            if (parametersObject.Layer > 1)
                sJQueryDivTag += parametersObject.Layer;

            // Close last instance
            TriSysApex.UI.ClosePopupMessage(sJQueryDivTag);

            // Open the KendoUI window instance
            var win = $(sJQueryDivTag).kendoWindow({
                height: parametersObject.Height,
                width: parametersObject.Width,
                minHeight: iMinHeight,
                minWidth: iMinWidth,
                title: parametersObject.Title,
                visible: false,
                modal: true,
                resizable: parametersObject.Resizable,
                maximize: parametersObject.Maximize,
                content: sURLContent,
                close: function () { $(sJQueryDivTag).empty(); },
                actions: actions
            });

            if (!win)
            {
                // Problem with the div tag not in the hosting page
                alert('Page is missing the ' + sJQueryDivTag + ' html tag.');
                return;
            }

            var dialog = $(sJQueryDivTag).data("kendoWindow");

            // Calculate height and width
            setWindowPopupSize(dialog, parametersObject.Height, parametersObject.Width);

            // Do icon
            if (parametersObject.Layer <= 1)
                FixupWindowIcon(parametersObject.Image);

            // Center and Open
            dialog.center();
            dialog.open();

            //$('#trisys-messagebox-text').html(sMessageText);

        }, // End of TriSysApex.UI.PopupMessage

        CloseTopmostModalPopup: function ()
        {
            var parametersObject = TriSysApex.UI.CurrentShowMessageParameters;
            var sJQueryDivTag = TriSysApex.Constants.ModalPopupJQueryDiv;

            if (parametersObject)
            {
                if (parametersObject.Layer > 1)
                    sJQueryDivTag += parametersObject.Layer;
            }

            TriSysApex.UI.ClosePopupMessage(sJQueryDivTag);
        },

        ClosePopupMessage: function (sJQueryDivTag)
        {
            var win = $(sJQueryDivTag).data("kendoWindow");
            if (win)
                win.close();
        },

        // Dynamic CSS: http://stackoverflow.com/questions/566203/changing-css-values-with-javascript
        //
        cssUpdate: function (selector, property, value)
        {
            for (var i = 0; i < document.styleSheets.length; i++)
            {//Loop through all styles
                //Try add rule
                try
                {
                    document.styleSheets[i].insertRule(selector + ' {' + property + ':' + value + '}',
                        document.styleSheets[i].cssRules.length);
                } catch (err)
                {
                    try
                    {
                        document.styleSheets[i].addRule(selector, property + ':' + value);
                    }
                    catch (err) { }
                }//IE
            }
        },

        messageBoxReady: function (parametersObject, bLoadModalControl)
        {
            if (!parametersObject)
            {
                alert("Programmer error: messageBoxReady()");
                return;
            }

            // Assign properties to buttons
            $('#trisys-messagebox-text').html(parametersObject.MessageText);

            $('#buttonLeft').text(parametersObject.ButtonLeftText);
            $('#buttonLeft').prop('title', parametersObject.ButtonLeftText);
            $('#buttonLeft').width(parametersObject.ButtonLeftWidth)
            $('#buttonCentre').text(parametersObject.ButtonCentreText);
            $('#buttonCentre').prop('title', parametersObject.ButtonCentreText);
            $('#buttonCentre').width(parametersObject.ButtonCentreWidth);
            $('#buttonRight').text(parametersObject.ButtonRightText);
            $('#buttonRight').prop('title', parametersObject.ButtonRightText);
            $('#buttonRight').width(parametersObject.ButtonRightWidth);

            // Hide/Show buttons
            if (!parametersObject.ButtonLeftVisible)
                $('#buttonLeft').hide();
            else
                $('#buttonLeft').show();

            if (!parametersObject.ButtonCentreVisible)
                $('#buttonCentre').hide();
            else
                $('#buttonCentre').show();

            if (!parametersObject.ButtonRightVisible)
                $('#buttonRight').hide();
            else
                $('#buttonRight').show();

            // Support for modal dialogue - a control within a control!
            if (bLoadModalControl)
            {
                var sURLContent = parametersObject.UserControlFolder + parametersObject.ModalControl;
                TriSysApex.FormsManager.loadPageIntoDiv('ctrlModalDialogue_Container', sURLContent);
            }
        },

        alert: function (sMessage, sTitle)
        {

            if (!sTitle)
                sTitle = "TriSys Alert";

            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.MessageText = sMessage;
            parametersObject.Image = 'information';

            TriSysApex.UI.PopupMessage(parametersObject);
        },

        errorAlert: function (sMessage, sTitle, fOKButtonFunction)
        {

            if (!sTitle)
                sTitle = "TriSys Error";

            TriSysApex.Logging.LogMessage(sTitle + ": " + sMessage);

            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.MessageText = sMessage;
            parametersObject.Image = 'error';

            parametersObject.ButtonCentreFunction = fOKButtonFunction;
            parametersObject.ButtonCentreVisible = true;
            parametersObject.ButtonCentreText = "OK";

            TriSysApex.UI.PopupMessage(parametersObject);
        },

        questionYesNo: function (sMessage, sTitle, sYesCaption, fnYesFunction, sNoCaption, fnNoFunction)
        {
            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.MessageText = sMessage;
            parametersObject.Image = 'question';
            parametersObject.Height = 250;
            parametersObject.ButtonLeftText = sYesCaption;
            parametersObject.ButtonLeftFunction = fnYesFunction;
            parametersObject.ButtonLeftVisible = true;
            parametersObject.ButtonRightVisible = true;
            parametersObject.ButtonRightText = sNoCaption;
            parametersObject.ButtonRightFunction = fnNoFunction;
            TriSysApex.UI.PopupMessage(parametersObject);
        },

        questionYesNoCancel: function (sMessage, sTitle, sYesCaption, fnYesFunction,
                                                         sNoCaption, fnNoFunction,
                                                         sCancelCaption, fnCancelFunction)
        {
            var parametersObject = new TriSysApex.UI.ShowMessageParameters();
            parametersObject.Title = sTitle;
            parametersObject.MessageText = sMessage;
            parametersObject.Image = 'question';
            parametersObject.Height = 250;
            parametersObject.ButtonLeftText = sYesCaption;
            parametersObject.ButtonLeftFunction = fnYesFunction;
            parametersObject.ButtonLeftVisible = true;
            parametersObject.ButtonCentreVisible = true;
            parametersObject.ButtonCentreText = sNoCaption;
            parametersObject.ButtonCentreFunction = fnNoFunction;
            parametersObject.ButtonRightVisible = true;
            parametersObject.ButtonRightText = sCancelCaption;
            parametersObject.ButtonRightFunction = fnCancelFunction;
            TriSysApex.UI.PopupMessage(parametersObject);
        },

        CheckBoxFieldValueToBoolean: function (sField)
        {
            var sValue = $('input[name=' + sField + ']').is(':checked');

            switch (sValue)
            {

                case "on", "true", "1", true:
                    return true;

                default:
                    return false;
            }

        },

        SetCheckBoxFieldValue: function (sField, bChecked)
        {
            // This works but not in JQuery ready()
            var sID = "#" + sField;

            // Works fine - but not in this framework when form is loading!
            $(sID).prop('checked', bChecked);

            // We have instead to do this - get the <SPAN> of the parent and set the class
            try
            {
                var spanObject = $(sID).parent("span");
                if (bChecked && spanObject)
                {
                    // Set the span class to checked
                    spanObject.attr('class', "checked");
                }
            } catch (ex) { }
        },

        setThemeColoursInSDKWidgets: function ()
        {
            // Set our themes throughout
            //$('.portlet box').switchClass("blue", "purple");
            //$(".portlet box blue").switchClass("portlet box blue", "portlet box purple", 'fast');
            //return;

            var cusid_ele = document.getElementsByClassName('portlet box');
            for (var i = 0; i < cusid_ele.length; ++i)
            {
                var item = cusid_ele[i];
                item.className = "portlet box blue";
            }
            return;

            // Almost worked
            $(".portlet").each(function (index)
            {
                var sClass = $(this).attr("class");
                //alert(index + ": " + sClass);

                if (sClass.indexOf("portlet box") == 0)
                {
                    // Does not work
                    //$(this).switchClass(sClass, "portlet box blue");

                    // Does not work either
                    //$(this).addClass('portlet box blue');
                    //$(this).removeClass(sClass);

                    document.getElementById('skenkenDoobry').className = "portlet box blue";
                }
            });
        },

        // Should be the error handler for all API calls so that we can perform redirection
        // to login page or error page depending upon error type.
        //
        ErrorHandlerRedirector: function (sFunction, request, status, error, bDisplayAlert)
        {
            var sResponseError = request.responseText;
            var sError = sFunction + "Error: " + status + ": " + error + ". responseText: " + sResponseError;
            TriSysApex.Logging.LogMessage(sError);

            switch (sResponseError)
            {
                case 'Expired DataServicesKey':
                    // Session has expired, so log user out of everything and return back to the Ready screen
                    TriSysApex.LoginCredentials.Logoff(false);
                    break;

                default:
                    if (TriSysAPI.Operators.isEmpty(bDisplayAlert))
                        bDisplayAlert = true;

                    if (bDisplayAlert)
                    {
                        // Display error
                        alert(sError);
                    }
                    break;
            }
        },

        Notifications:
        {
            _notificationWindow: null,

            Initialise: function ()
            {
                if (!TriSysApex.UI.Notifications._notificationWindow)
                {
                    TriSysApex.UI.Notifications._notificationWindow = $("#TriSysNotification").kendoNotification({
                        position: {
                            pinned: true,
                            top: 30,
                            right: 30
                        },
                        autoHideAfter: 5000,
                        stacking: "down",
                        templates: [{
                            type: "info",
                            template: $("#emailTemplate").html()
                        }, {
                            type: "error",
                            template: $("#errorTemplate").html()
                        }, {
                            type: "upload-success",
                            template: $("#successTemplate").html()
                        }]

                    }).data("kendoNotification");
                }
            },

            Error: function (sTitle, sMessage)
            {
                TriSysApex.UI.Notifications.Show(sTitle, sMessage, "error");
            },
            Info: function (sTitle, sMessage)
            {
                TriSysApex.UI.Notifications.Show(sTitle, sMessage, "info");
            },
            UploadSuccess: function (sTitle, sMessage)
            {
                TriSysApex.UI.Notifications.Show(sTitle, sMessage, "upload-success");
            },

            Show: function (sTitle, sMessage, sType)
            {
                TriSysApex.UI.Notifications.Initialise();

                TriSysApex.UI.Notifications._notificationWindow.show({
                    title: sTitle,
                    message: sMessage
                }, sType);

            }

        }

    }, // End of TriSysApex.UI Namespace

    //#endregion TriSysApex.UI

    //#region TriSysApex.Forms
    Forms:
    {       // TriSysApex.Forms

        AboutTriSys: function ()
        {
            var d = new Date();
            var iYear = d.getFullYear();
            var sProductNameHTML = "<b>" + TriSysApex.Copyright.ShortProductName + "</b>&nbsp;" + TriSysApex.Copyright.LongProductDescription;
            var sContactUsHTML = '&copy;' + ' 1994-' + iYear + ' ' + TriSysApex.Copyright.AllRightsReserved;
            var sWebLinkHTML = '<a href="http://' + TriSysApex.Copyright.HomePage + '" target="_blank" alt="TriSys Home Page" title="' +
                                    TriSysApex.Copyright.HomePage + '">' + TriSysApex.Copyright.HomePage + '</a>';
            var sVersionString = sProductNameHTML + "<br />" +
                                "<br />Apex Version: " + TriSysApex.Constants.Version +
                                "<br />Web API Version: " + TriSysAPI.Data.Version() +
                                "<br /><br />" +
                                sContactUsHTML + "<br />" +
                                sWebLinkHTML;
            TriSysApex.UI.ShowMessage(sVersionString);
        },

        Login:      // TriSysApex.Forms.Login
        {
            Ready: function (sLoginDiv, sPasswordDiv, sCompanyDiv, sRememberDiv)
            {
                // Refresh the browser now before login or after logoff so that we always pick up the latest version from Apex web server
                TriSysApex.Environment.RefreshCachedApplication();

                // Get the saved login credentials from the browser storage/cookies
                TriSysApex.LoginCredentials.ReadPersistedLoginCredentials(sLoginDiv, sCompanyDiv, sRememberDiv);

                if (TriSysApex.Forms.Login.areLoginNameAndCompanyNameFieldsCompleted(sLoginDiv, sCompanyDiv))
                {
                    // The callback function for auto-login
                    var loginClicker = function ()
                    {
                        TriSysApex.Forms.Login.LoginButtonClick(sLoginDiv, sPasswordDiv, sCompanyDiv, sRememberDiv);
                    };

                    // If we are in dev mode, then allow auto-login
                    if (TriSysApex.Environment.isLocalhost() || TriSysApex.Environment.isDeployedInTestMode())
                        TriSysApex.UI.questionYesNo("Auto Login?", "TriSys Developer", "Yes",
                                            function ()
                                            {
                                                TriSysApex.LoginCredentials.isProjectInDevelopment(sPasswordDiv, loginClicker);
                                            }, "No");
                }
            },

            // Used purely to determine whether we should auto-login or not
            areLoginNameAndCompanyNameFieldsCompleted: function (sLoginDiv, sCompanyDiv)
            {
                var sLoginName = $('#' + sLoginDiv).val();
                var sCompany = $('#' + sCompanyDiv).val();
                var bNotNull = (sLoginName && sCompany);
                return (bNotNull);
            },

            LoginButtonClick: function (sLoginDiv, sPasswordDiv, sCompanyDiv, sRememberDiv)
            {
                var sLoginName = $('#' + sLoginDiv).val();
                var sCompany = $('#' + sCompanyDiv).val();
                var sPassword = $('#' + sPasswordDiv).val();
                var bRememberMe = TriSysApex.UI.CheckBoxFieldValueToBoolean(sRememberDiv);

                var bValidate = TriSysApex.LoginCredentials.Validate(sLoginName, sCompany, sPassword, bRememberMe,
                                        TriSysApex.Forms.Login.ValidatedLoginCredentialsCallback);
            },

            ValidatedLoginCredentialsCallback: function ()
            {
                TriSysApex.UI.ShowWait(null, "Loading forms...");

                // Go back to get the cached user settings to make life easier to draw her own
                // highly customised application layout/functions...
                TriSysApex.LoginCredentials.ReadLoggedInUserSettings(TriSysApex.Forms.Login.AfterReadingLoggedInUserSettings);
            },

            AfterReadingLoggedInUserSettings: function (CDataConnectionKey)
            {
                // Go back to get the cached standing data settings and meta database
                TriSysApex.LoginCredentials.CacheStandingData(CDataConnectionKey, TriSysApex.Forms.Login.Authenticated);
            },

            Authenticated: function (CDataConnectionKey, CCachedStandingDataWithBOLObjects)
            {
                TriSysApex.Pages.Index.PostLoginVisuals(true);

                // Close the login form and display the first form
                TriSysApex.FormsManager.CloseAllForms(true, true);

                TriSysApex.UI.HideWait();

                TriSysApex.FormsManager.OpenForm("Dashboard", 0);
            },

            ForgottenPasswordButtonClick: function ()
            {
                TriSysApex.FormsManager.OpenForm("ForgottenPassword", 0);
            },

            SignUpButtonClick: function ()
            {
                TriSysApex.FormsManager.OpenForm("SignUp", 0);
            }
        },

        ForgottenPassword: // TriSysApex.Forms.ForgottenPassword
        {
            ButtonClick: function (sEMailDiv)
            {
                var sTitle = "Forgotten Password";
                var sEMail = $('#' + sEMailDiv).val();

                if (!TriSysApex.LoginCredentials.validateEmail(sEMail))
                {
                    TriSysApex.UI.ShowMessage("Please enter a valid e-mail address.", sTitle);
                    return;
                }

                var fnForgottenPasswordComplete = function ()
                {
                    TriSysApex.UI.ShowMessage("Your password has been requested by e-mail.", sTitle);
                };

                // Make call to web service to validate this e-mail address via the appropriate method
                TriSysApex.LoginCredentials.ForgottenPasswordRequest(sEMail, fnForgottenPasswordComplete);
            }

        },

        SignUp:             // TriSysApex.Forms.SignUp
        {
            ButtonClick: function (sTypeOfRequest)      // subscription or trial
            {
                var sTitle = "Sign-Up";

                var sFullName = $('#txtSignUp_FullName').val();
                var sCompany = $('#txtSignUp_Company').val();
                var sEMail = $('#txtSignUp_EMail').val();
                var sTelNo = $('#txtSignUp_TelNo').val();
                var sStreet = $('#txtSignUp_Street').val();
                var sCity = $('#txtSignUp_City').val();
                var sCounty = $('#txtSignUp_County').val();
                var sPostCode = $('#txtSignUp_PostCode').val();
                var sCountry = $('#txtSignUp_Country').val();
                var sPassword = $('#txtSignUp_Password').val();
                var sVerifyPassword = $('#txtSignUp_VerifyPassword').val();
                var bAgree = TriSysApex.UI.CheckBoxFieldValueToBoolean('chkSignUp_Agree');

                var bValidate = TriSysApex.LoginCredentials.ValidateSignUp(sFullName, sCompany, sEMail,
                                            sTelNo, sStreet, sCity, sCounty, sPostCode, sCountry, sPassword, sVerifyPassword, bAgree);

                if (!bValidate)
                    return;

                // Passed minimum credentials, so start sign-up...

                // Enough Web API fields to create contact, company and address
                var dataPacket = {
                    'FullName': sFullName,
                    'Company': sCompany,
                    'EMail': sEMail,
                    'TelNo': sTelNo,
                    'Street': sStreet,
                    'City': sCity,
                    'County': sCounty,
                    'PostCode': sPostCode,
                    'Country': sCountry,
                    'Password': sPassword,
                    'TypeOfRequest': sTypeOfRequest
                };

                var fnSuccess = function ()
                {
                    var sThankYou = "Thank you for signing-up to the " + sTypeOfRequest + " service.<br />" +
                        "You have been sent an automated e-mail confirming your request.<br />";

                    if (sTypeOfRequest = 'Trial')
                    {
                        // Trial Service - can log in to demo database now
                        var fnAutoLogin = function ()
                        {
                            // TODO: Login as a trial user
                        };
                        var fnReturnToLoginForm = function ()
                        {
                            // Close all forms and return to login
                            var bForceClose = true;
                            TriSysApex.FormsManager.CloseAllForms(bForceClose);
                        };

                        TriSysApex.UI.questionYesNo(sThankYou +
                            "You will be able to use the link in this e-mail to login to the trial at any time.<br />" +
                            "Would you like to login now?", "Sign-Up to TriSys Trial", "Yes", fnAutoLogin, "No", fnReturnToLoginForm);
                    }
                    else
                    {
                        // Subscription Service - will take minutes to provision
                        TriSysApex.UI.ShowMessage(sThankYou +
                            "You will then have the opportunity to pay the monthly subscription in order to " +
                            "provision your private and secure cloud database.");
                    }
                };

                // Make call to web service to harvest the details and return when complete
                TriSysApex.LoginCredentials.SignUpRequest(dataPacket, fnSuccess);
            }
        },

        //#region TriSysApex.Forms.Configuration
        // Forms Menu Icons, Captions etc..
        Configuration:      // TriSysApex.Forms.Configuration
        {
            Pages: null,
            Initialise: function ()
            {
                TriSysApex.Forms.Configuration.Pages = TriSysApex.Forms.Configuration.PagesFile();
            },

            PagesFile: function ()
            {
                var sFile = TriSysLoad.Utility.FilePathPrefix() + 'configuration/forms.json';
                var jsonData = null;
                $.ajax({
                    type: 'GET',
                    url: sFile,
                    dataType: 'json',
                    success: function (data)
                    {
                        jsonData = data;
                    },
                    error: function (request, status, error)
                    {
                        alert("Error: " + status + ": " + error + ". responseText: " + request.responseText);
                    },
                    async: false
                });
                return jsonData;
            },

            GetPage: function (sFormName)
            {
                for (var i = 0; i < TriSysApex.Forms.Configuration.Pages.length; i++)
                {
                    var page = TriSysApex.Forms.Configuration.Pages[i];
                    if (page.FormName == sFormName)
                        return page;
                }
            },

            // Ribbon/Menu callback to identify if a known configuration form/page/action
            GetPageFromMenuInvocationId: function (sId)
            {
                for (var i = 0; i < TriSysApex.Forms.Configuration.Pages.length; i++)
                {
                    var page = TriSysApex.Forms.Configuration.Pages[i];
                    if (page.MenuInvocationId == sId)
                        return page;
                }
            }
        },

        EntityForm:     // TriSysApex.Forms.EntityForm
        {
            SaveEvent: function (sEntityName, lRecordId, fnBeforeSaveFieldDescriptions, fnAfterSaveRecord)
            {
                // Finally in January 2014, we have a full CRUD based Web API which we can now use to save this
                // data in our database, so let's do it!

                //TriSysApex.UI.ShowMessage("SAVE: " + sEntityName);

                var fieldDescriptions = TriSysSDK.CShowForm.ReadFieldDescriptionValues(sEntityName, true, TriSysApex.Forms.bEditingForm);
                if (!fieldDescriptions || fieldDescriptions.length == 0)
                {
                    TriSysApex.UI.ShowError('Form: Save: No field descriptions.');
                    return;
                }

                // After reading screen values, callback to get any adjustments based on business logic rules
                if (fnBeforeSaveFieldDescriptions)
                    fieldDescriptions = fnBeforeSaveFieldDescriptions(fieldDescriptions);

                //#region EntityForm.SaveEvent Diagnostics
                // Display as diagnostics
                //var sDiagnostics = "";
                //if (fieldDescriptions.length > 0)
                //{
                //    for (var i = 0; i < fieldDescriptions.length; i++)
                //    {
                //        var fieldDescription = fieldDescriptions[i];
                //        if (sDiagnostics.length > 0)
                //            sDiagnostics = sDiagnostics + ", ";
                //        sDiagnostics = sDiagnostics + fieldDescription.TableName + "." + fieldDescription.TableFieldName + "=" + fieldDescription.Value;
                //    }
                //}

                //TriSysApex.UI.alert('Form: Save: bLoadNew=' + bLoadNew + ", " + sDiagnostics);
                // End of diagnostics only
                //#endregion EntityForm.SaveEvent Diagnostics

                // Make the asynchronous update call now
                TriSysSDK.CShowForm.writeEntityFormDataToServer(sEntityName, lRecordId, fieldDescriptions, fnAfterSaveRecord);
            }
        },

        // OLD CODE: Metronic Specific - is gradually being eaten by new 2014 Apex!

        LogoHyperlink: function ()
        {
            TriSysApex.Forms.loadPageProgrammatically("About.aspx", "menuHelp_About");
        },

        buttonGoBack: function ()
        {
            var queue = TriSysApex.Forms.formQueue;
            if (queue.length > 0 && TriSysApex.Forms.currentFormIndex > 0)
            {
                TriSysApex.Forms.currentFormIndex = TriSysApex.Forms.currentFormIndex - 1;
                var item = queue[TriSysApex.Forms.currentFormIndex];
                if (item)
                {
                    if (item.PageURL)
                        App.loadDynamicPageIntoContainer(null, null, item.PageURL);
                }
            }
        },

        buttonGoForward: function ()
        {
            var queue = TriSysApex.Forms.formQueue;
            if (queue.length > 0 && TriSysApex.Forms.currentFormIndex < (queue.length - 1))
            {
                TriSysApex.Forms.currentFormIndex = TriSysApex.Forms.currentFormIndex + 1;
                var item = queue[TriSysApex.Forms.currentFormIndex];
                if (item)
                {
                    if (item.PageURL)
                        App.loadDynamicPageIntoContainer(null, null, item.PageURL);
                }
            }
        },

        enableBackForwardButton: function (sDivTag, sDirection, bEnable)
        {
            var sClass = "m-icon-big-swap" + sDirection + " m-icon-" + (bEnable ? "white" : "grey");
            document.getElementById(sDivTag).className = sClass;
        },

        ForwardBackButtonAvailabilityUpdate: function ()
        {
            var bBackButtonEnabled = false;
            var bForwardButtonEnabled = false;
            var queue = TriSysApex.Forms.formQueue;

            if (queue.length > 0)
            {
                bBackButtonEnabled = (TriSysApex.Forms.currentFormIndex > 0);
                bForwardButtonEnabled = (TriSysApex.Forms.currentFormIndex < (queue.length - 1));
            }

            TriSysApex.Forms.enableBackForwardButton('NavBar_BackButton', 'left', bBackButtonEnabled);
            TriSysApex.Forms.enableBackForwardButton('NavBar_ForwardButton', 'right', bForwardButtonEnabled);
        },

        pushPageOntoFormQueue: function (sPageURL)
        {
            var queue = TriSysApex.Forms.formQueue;
            var item = null;

            // Remove blank forms only i.e. the first time!
            for (i = queue.length - 1; i >= 0; i--)
            {
                item = queue[i];
                if (item.PageURL == '')
                    queue.splice(i, 1);
            }

            // Is this in the queue already?
            var iIndexOfItemInQueue = -1;
            for (i = queue.length - 1; i >= 0; i--)
            {
                item = queue[i];
                if (item.PageURL == sPageURL)
                {
                    iIndexOfItemInQueue = i;
                    break;
                }
            }

            if (iIndexOfItemInQueue < 0)
            {
                // Add to the end of the stack/queue
                item = {
                    PageURL: sPageURL
                };
                queue.push(item);

                // Always point this last item for now even if a duplicate! - WRITING CODE GARRY!
                TriSysApex.Forms.currentFormIndex = queue.length - 1;
            } else
            {
                // Point at this item in the queue
                TriSysApex.Forms.currentFormIndex = iIndexOfItemInQueue;
            }

            // Calculate the status of the forward/back buttons
            TriSysApex.Forms.ForwardBackButtonAvailabilityUpdate();
        },

        formQueue: [{
            PageURL: ''
        }],

        currentFormIndex: 0,

        entityCountersForMainMenu: function ()
        {
            var payloadObject = {};
            payloadObject.URL = "Counters/EntityCount";
            payloadObject.OutboundDataPacket = null;
            payloadObject.InboundDataFunction = function (data)
            {
                var entityObject = data;
                if (entityObject)
                {
                    $('#lblContactsCounter').html(entityObject.Contacts);
                    $('#lblCompaniesCounter').html(entityObject.Companies);
                    $('#lblRequirementsCounter').html(entityObject.Requirements);
                    $('#lblPlacementsCounter').html(entityObject.Placements);
                    $('#lblTimesheetsCounter').html(entityObject.Timesheets);
                }

                return true;
            };
            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.ErrorHandlerRedirector('entityCountersForMainMenu: ', request, status, error);
            };
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        FormCloseEvent: function ()
        {
            var sMessage = "Are you sure that you wish to close this form?";
            TriSysApex.UI.questionYesNo(sMessage, "Close Form", "Yes",
                function ()
                {
                    TriSysApex.Forms.loadPageProgrammatically('Dashboard.aspx', 'menuHome_Dashboard');
                },
                "No");

            return false;
        },

        FormNewEvent: function ()
        {
            if (TriSysApex.Forms.bEditingForm)
            {
                var sMessage = "You are currently editing this form. Save changes? ";
                TriSysApex.UI.questionYesNo(sMessage, "New Record", "Yes",
                    function ()
                    {
                        var bLoadNew = true;
                        TriSysApex.Forms.FormSaveEvent(bLoadNew);
                    },
                    "No");
            }
        },

        FormSaveAsEvent: function ()
        {
            TriSysApex.UI.alert('Form: SaveAs');
        },

        FormDeleteEvent: function ()
        {
            TriSysApex.UI.alert('Form: Delete');
        },

        // After form is loaded, initialise the editors
        InitialiseFormEditing: function ()
        {
            return;

            // Metronic Specific
            //global settings 
            $.fn.editable.defaults.inputclass = 'form-control';
            $.fn.editable.defaults.url = '/post';
            $.fn.editableform.buttons = '<button type="submit" class="btn green editable-submit"><i class="fa fa-check"></i></button>';
            $.fn.editableform.buttons += '<button type="button" class="btn editable-cancel"><i class="fa fa-times"></i></button>';

            // Turn editing off on form load
            $('#dataEntryForm .editable').editable('toggleDisabled');
            $('#dataEntryFormTabs .editable').editable('toggleDisabled');

        },

        doesFieldExistInDOM: function (sFieldID)
        {
            var element = document.getElementById(sFieldID);
            if (typeof (element) != 'undefined' && element != null)
            {
                return true; // exists.
            }

            return false;
        },

        bEditingForm: false,

        EditMode: function (bEditMode, sMenuTag)
        {
            // Toggle editing
            $('#dataEntryForm .editable').editable('toggleDisabled');
            $('#dataEntryFormTabs .editable').editable('toggleDisabled');

            // Record state
            TriSysApex.Forms.bEditingForm = bEditMode;

            // Hide the calling menu
            if (bEditMode)
            {
                $('#formToolbarMenu_EditModeOn').hide();
                $('#formToolbarMenu_EditModeOff').show();
            } else
            {
                $('#formToolbarMenu_EditModeOn').show();
                $('#formToolbarMenu_EditModeOff').hide();
            }

        },

        FormDesigner: function ()
        {
            TriSysApex.UI.alert('Form Designer in development.');
        },

        // Forms Menu Icons, Captions etc..
        ConfigurationOldMetronic: {
            Pages:
            // Page Name, Icon, Nav Bar Menu 
            [{
                PageName: 'Dashboard.aspx',
                CaptionIcon: 'fa fa-dashboard',
                CaptionText: 'Dashboard',
                HideMenu: true
            }, {
                PageName: 'Contacts.aspx',
                CaptionIcon: 'fa fa-male',
                CaptionText: 'Contact Lookup'
            }, {
                PageName: 'Contact.aspx',
                CaptionIcon: 'fa fa-male',
                CaptionText: 'Contact'
            }, {
                PageName: 'Companies.aspx',
                CaptionIcon: 'fa fa-sitemap',
                CaptionText: 'Company Lookup'
            }, {
                PageName: 'Company.aspx',
                CaptionIcon: 'fa fa-sitemap',
                CaptionText: 'Company'
            }, {
                PageName: 'Requirements.aspx',
                CaptionIcon: 'fa fa-ok-sign',
                CaptionText: 'Requirements'
            }, {
                PageName: 'Requirement.aspx',
                CaptionIcon: 'fa fa-ok-sign',
                CaptionText: 'Requirement'
            }, {
                PageName: 'Placements.aspx',
                CaptionIcon: 'fa fa-gbp',
                CaptionText: 'Placements'
            }, {
                PageName: 'Placement.aspx',
                CaptionIcon: 'fa fa-gbp',
                CaptionText: 'Placement'
            }, {
                PageName: 'TaskSearch.aspx',
                CaptionIcon: 'fa fa-wrench',
                CaptionText: 'Task Search'
            }, {
                PageName: 'AdministratorConsole.aspx',
                CaptionIcon: 'fa fa-warning-sign',
                CaptionText: 'Administrator Console',
                HideMenu: true
            }, {
                PageName: 'FileManager.aspx',
                CaptionIcon: 'fa fa-folder-open',
                CaptionText: 'Administrator Console'
            }, {
                PageName: 'UserOptions.aspx',
                CaptionIcon: 'fa fa-user',
                CaptionText: 'User Options',
                HideMenu: true
            }, {
                PageName: 'About.aspx',
                CaptionIcon: 'fa fa-tumblr',
                CaptionText: 'About Us',
                HideMenu: true
            }, {
                PageName: 'WebForm.aspx',
                CaptionIcon: 'fa fa-cloud',
                CaptionText: 'Web Form Placeholder',
                HideMenu: true
            }],

            // Called by App.loadDynamicPageIntoContainer() 
            // We set the caption, icon, toolbar etc..
            FormLoading: function (sPage)
            {
                // Strip off parameters ?, & etc..
                var iQuestionMarkIndex = sPage.indexOf("?");
                if (iQuestionMarkIndex > 0)
                    sPage = sPage.substring(0, iQuestionMarkIndex);

                // Find this form in our config
                var pages = TriSysApex.Forms.Configuration.Pages;
                for (var i = 0; i < pages.length; i++)
                {
                    var page = pages[i];
                    if (page.PageName.toLowerCase() == sPage.toLowerCase())
                    {
                        // Form Icon
                        if (page.CaptionIcon)
                        {
                            $('#FormIcon').removeClass("fa-cog").addClass(page.CaptionIcon);
                            //document.getElementById('FormIcon').className = sIcon;
                        }

                        // Form Caption
                        if (page.CaptionText)
                            $('#FormCaption').html(page.CaptionText);

                        // Menu
                        if (page.HideMenu)
                            $('#FormToolbar').hide();
                    }
                }

                // Add this to our queue to help user quickly navigate between opened forms
                TriSysApex.Forms.pushPageOntoFormQueue(sPage);
            }
        } // End of TriSysApex.Forms.Configuration

        //#endregion TriSysApex.Forms.Configuration

    }, // End of TriSysApex.Forms
    //#endregion TriSysApex.Forms

    //#region TriSysApex.Actions
    Actions:
    {
        testActionFromGrid: function (sGridDivTag)
        {
            // Now display this for debugging
            var sDisplay = '';

            // Find existing array element for this grid
            var item = TriSysSDK.Grid.getSelectedGridRowKeys(sGridDivTag);
            if (!item)
                return;

            for (var i = 0; i < item.SelectedRows.length; i++)
            {
                var iRowIndex = item.SelectedRows[i];
                sDisplay += iRowIndex + " ";

            }

            //sDisplay += "\n" + "Grid Contents:\n" + TriSysSDK.Grid.readGridContents(sGridDivTag);

            TriSysApex.UI.alert('testActionFromGrid: ' + sGridDivTag + ": " + sDisplay);

        }

    }, // End of TriSysApex.Actions
    //#endregion TriSysApex.Actions

    //#region TriSysApex.LoginCredentials

    // Login Credentials Namespace
    LoginCredentials:       // TriSysApex.LoginCredentials
    {
        ReadPersistedLoginCredentials: function (sLoginNameID, sCompanyID, sRememberMeID)
        {
            // Read from storage
            var bRememberMe = TriSysAPI.Session.SaveLoginCredentials();
            var sLoginName = TriSysAPI.Session.UserName();
            var sCompanyName = TriSysAPI.Session.CompanyName();

            // Write to widgets
            if (bRememberMe)
            {
                $('#' + sLoginNameID).val(sLoginName);
                $('#' + sCompanyID).val(sCompanyName);
            }
            TriSysApex.UI.SetCheckBoxFieldValue(sRememberMeID, bRememberMe);
        },

        // To save having to type test password every time in VS2012
        isProjectInDevelopment: function (sPasswordID, fnLoginButtonFunction)
        {
            var bDebuggingMode = true;

            if (bDebuggingMode && (TriSysApex.Environment.isLocalhost() || TriSysApex.Environment.isDeployedInTestMode()))
            {
                try
                {
                    var sPassword = $('#' + sPasswordID).val();

                    if (fnLoginButtonFunction)
                    {
                        sPassword = TriSysAPI.Session.Password();
                        if (sPassword)
                        {
                            $('#' + sPasswordID).val(sPassword);
                            fnLoginButtonFunction();
                        }
                    }
                }
                catch (ex)
                {
                    alert('isProjectInDevelopment exception: ' + ex.message);
                }
            }
        },

        ForgottenPasswordRequest: function (sEMail, callbackFunction)
        {
            // Connect to Web Service to request password reset
            var dataPacket = {
                'EMail': sEMail
            };

            var payloadObject = {};

            payloadObject.URL = "Security/ForgottenPasswordRequest";

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                TriSysApex.UI.HideWait();

                var objCompleted = data;

                var sDisplay = JSON.stringify(data);
                //alert(sDisplay);

                if (objCompleted)
                {
                    if (objCompleted.Success)
                    {
                        // Let user know
                        TriSysApex.UI.ShowMessage('You have been sent an e-mail to: ' + sEMail + ' with instructions on how to recover/reset your password.');

                        // Call back into forms engine to continue login process
                        callbackFunction();
                        return;
                    }
                }

                TriSysApex.UI.ShowMessage('Unrecognised e-mail address: ' + sEMail + TriSysApex.LoginCredentials.EMailSupportMessageHTML());
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.errorAlert('TriSysApex.ForgottenPasswordRequest: ' + status + ": " + error + ". responseText: " + request.responseText);
            };

            TriSysApex.UI.ShowWait(null, "Requesting password reset...");
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        ValidationMessageTitle: "TriSys Login Credentials",

        Validate: function (sLoginName, sCompany, sPassword, bRememberMe, validateCallbackFunction)
        {
            var sMessageTitle = TriSysApex.LoginCredentials.ValidationMessageTitle;

            // Simple textual validation rules
            var sError = null;
            var iMinLoginNameChars = 3;
            var iMinCompanyNameChars = 3;
            var iMinPasswordChars = 6;
            if (!sLoginName || sLoginName.length < iMinLoginNameChars)
                sError = 'Full name must be at least ' + iMinLoginNameChars + ' characters.';
            else if (!sCompany || sCompany.length < iMinCompanyNameChars)
                sError = 'Company name must be at least ' + iMinLoginNameChars + ' characters.';
            else if (!sPassword || sPassword.length < iMinPasswordChars)
                sError = 'Password must be at least ' + iMinPasswordChars + ' characters.';

            if (sError)
            {
                TriSysApex.UI.ShowMessage(sError, sMessageTitle);
                return false;
            }

            // Connect to Web Service to obtain data services key
            var dataPacket = {
                'FullName': sLoginName,
                'CompanyName': sCompany,
                'Password': sPassword
            };

            TriSysApex.LoginCredentials.ValidateViaWebService(dataPacket, bRememberMe, validateCallbackFunction);

            return true;
        },

        // Connect to Web Service to obtain data services key
        ValidateViaWebService: function (dataPacket, bRememberMe, validateCallbackFunction, fnIncorrectLogin)
        {
            var sMessageTitle = TriSysApex.LoginCredentials.ValidationMessageTitle;

            var payloadObject = {};

            payloadObject.URL = "Security/AuthenticateTriSysCRMCredentials";

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                var sDataServicesKey = data;

                TriSysApex.UI.HideWait();

                if (sDataServicesKey && sDataServicesKey.length > 0)
                {
                    // Record in session state
                    TriSysAPI.Session.SetDataServicesKey(sDataServicesKey);
                    TriSysAPI.Session.SetUserName(dataPacket.FullName);
                    TriSysAPI.Session.SetCompanyName(dataPacket.CompanyName);
                    TriSysAPI.Session.SetSaveLoginCredentials(bRememberMe);

                    if (TriSysApex.Environment.isLocalhost() || TriSysApex.Environment.isDeployedInTestMode())
                        TriSysAPI.Session.SetPassword(dataPacket.Password);


                    // Call back into forms engine to continue login process
                    validateCallbackFunction();

                    return true;
                }
                else
                {
                    if (fnIncorrectLogin)
                        fnIncorrectLogin();
                    else
                    {
                        var sIncorrectMessage = 'Incorrect login credentials.' + TriSysApex.LoginCredentials.EMailSupportMessageHTML();
                        TriSysApex.UI.ShowMessage(sIncorrectMessage, sMessageTitle);
                    }
                    return false;
                }
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.errorAlert('TriSysApex.LoginCredentials.ValidateViaWebService: ' + status + ": " + error + ". responseText: " + request.responseText);
            };

            TriSysApex.UI.ShowWait(null, "Authenticating Credentials...");
            TriSysAPI.Data.PostToWebAPI(payloadObject);

            return true;
        },

        ValidateSignUp: function (sFullName, sCompany, sEMail,
                                            sTelNo, sStreet, sCity, sCounty, sPostCode, sCountry, sPassword, sVerifyPassword, bAgree)
        {
            // Simple textual validation rules
            var sError = null;
            var iMinFullNameChars = 5;
            var iMinCompanyNameChars = 3;
            var iMinPasswordChars = 6;
            if (!sFullName || sFullName.length < iMinFullNameChars)
                sError = 'Full name must be at least ' + iMinFullNameChars + ' characters.';
            else if (!sCompany || sCompany.length < iMinCompanyNameChars)
                sError = 'Company name must be at least ' + iMinCompanyNameChars + ' characters.';
            else if (!TriSysApex.LoginCredentials.validateEmail(sEMail))
                sError = 'E-Mail address must be completed.';
            else if (!sTelNo)
                sError = 'Telephone number must be completed.';
            else if (!sStreet)
                sError = 'Street address is required.';
            else if (!sCity)
                sError = 'City/Town is required.';
            else if (!sCounty)
                sError = 'County/State is required.';
            else if (!sPostCode)
                sError = 'Post/Zip Code is required.';
            else if (!sCountry)
                sError = 'Country is required.';
            else if (!sPassword || sPassword.length < iMinPasswordChars)
                sError = 'Password must be at least ' + iMinPasswordChars + ' characters.';
            else if (sPassword != sVerifyPassword)
                sError = 'Passwords must match.';
            else if (!bAgree)
                sError = 'You must agree to our terms of service and privacy policy.';

            if (sError)
            {
                TriSysApex.UI.ShowMessage(sError, "TriSys Sign-Up");
                return false;
            }

            return true;
        },

        SignUpRequest: function (dataPacket, callbackFunction)
        {
            var sEMail = dataPacket.EMail;

            var payloadObject = {};

            payloadObject.URL = "Security/RegisterAccount";

            payloadObject.OutboundDataPacket = dataPacket;

            payloadObject.InboundDataFunction = function (data)
            {
                TriSysApex.UI.HideWait();

                var objCompleted = data;

                var sDisplay = JSON.stringify(data);
                //alert(sDisplay);

                if (objCompleted)
                {
                    if (objCompleted.Success)
                    {
                        // Let user know
                        TriSysApex.UI.alert('You have been sent an e-mail to: ' + sEMail +
                            ' with instructions on how to pay for the service or use a trial account.');

                        // Call back into forms engine to continue login process
                        callbackFunction();
                        return;
                    } else
                    {
                        TriSysApex.UI.errorAlert(objCompleted.ErrorMessage);
                        return;
                    }
                }

                TriSysApex.UI.errorAlert(payloadObject.URL + " API Error.");
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.HideWait();
                TriSysApex.UI.errorAlert('TriSysApex.SignUpRequest: ' + status + ": " + error + ". responseText: " + request.responseText);
            };

            TriSysApex.UI.ShowWait(null, "Signing Up. Please wait...");
            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        validateEmail: function (email)
        {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        },

        ReadLoggedInUserSettings: function (fnOnCompletion)
        {
            var payloadObject = {};

            payloadObject.URL = "Security/PostLoginCRMCredentials";

            payloadObject.OutboundDataPacket = null;

            payloadObject.InboundDataFunction = function (data)
            {
                var CDataConnectionKey = data;

                if (CDataConnectionKey)
                {
                    /* DEBUG
                    sDisplay = JSON.stringify(CDataConnectionKey);
                    TriSysApex.UI.alert(sDisplay);
                    return;
                       END DEBUG */

                    // Cache the user settings
                    TriSysApex.LoginCredentials.UserCredentials(CDataConnectionKey);

                    // Call back to continue login orchestration
                    fnOnCompletion(CDataConnectionKey);
                }
                else
                {
                    // Something has gone wrong!
                }

                return true;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.ErrorHandlerRedirector('ReadLoggedInUserSettings: ', request, status, error);
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        CacheStandingData: function (CDataConnectionKey, fnOnCompletion)
        {
            var payloadObject = {};

            payloadObject.URL = "Cache/StandingData";

            payloadObject.OutboundDataPacket = null;

            payloadObject.InboundDataFunction = function (data)
            {
                var CCachedStandingDataWithBOLObjects = data;

                if (CCachedStandingDataWithBOLObjects)
                {
                    // Cache the standing data
                    TriSysApex.Cache.SetStandingData(CCachedStandingDataWithBOLObjects);

                    // Call back to continue login orchestration
                    fnOnCompletion(CDataConnectionKey, CCachedStandingDataWithBOLObjects);
                }
                else
                {
                    // Something has gone wrong!
                }

                return true;
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                TriSysApex.UI.ErrorHandlerRedirector('CacheStandingData: ', request, status, error);
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        Logoff: function (bPromptUser)
        {
            if (!TriSysApex.LoginCredentials.areUserCredentialsValid())
            {
                // Use this opportunity to refresh the application cache
                if (!TriSysApex.Environment.RefreshCachedApplication())
                {
                    // User is not logged in so let them know
                    TriSysApex.UI.ShowMessage("You are not yet logged in to the application.");
                    return;
                }
            }

            var fnLogoffCallback = function ()
            {
                //TriSysApex.UI.alert('You clicked function yes?');

                // Remove all session state about the logged in user
                TriSysApex.LoginCredentials.ClearCachedCredentials();
                TriSysApex.Cache.Clear();

                TriSysApex.Pages.Index.PostLoginVisuals(false);

                // Remove any counters/queues etc.. to start afresh
                TriSysApex.Forms.formQueue.length = 0;

                var bForceClose = true;
                TriSysApex.FormsManager.CloseAllForms(bForceClose);

                // Fire a web service call to physically log the session off the service and mark it so
                TriSysApex.LoginCredentials.LogoffWebService();
            };

            if (!bPromptUser)
            {
                fnLogoffCallback();
                return;
            }

            // Prompt the user for confirmation
            var sLogoffMessage = "Are you sure that you wish to log out of the application?" +
                "<br /><br />" +
                "All your open forms will be closed and you will return to the login form if you select the 'Yes' button." +
                " If you click the 'No' button, you will remain logged in.";
            TriSysApex.UI.questionYesNo(sLogoffMessage, "Logoff TriSys", "Yes", fnLogoffCallback, "No");
        },

        LogoffWebService: function ()
        {
            var fnClearKeysAndShowLogin = function ()
            {
                TriSysApex.UI.HideWait();

                // Clear the data services key as it is no longer valid
                TriSysAPI.Session.SetDataServicesKey('');

                // Now re-direct to the login page but not using browser history
                TriSysApex.Pages.Controller.OpenLoginForm();
            };

            TriSysApex.UI.ShowWait(null, "Logging Off...");
            TriSysApex.LoginCredentials.LogoffWebServiceWithCallback(fnClearKeysAndShowLogin);
        },

        LogoffWebServiceWithCallback: function (fnCallback)
        {
            var payloadObject = {};

            payloadObject.URL = "Security/Logoff";

            payloadObject.OutboundDataPacket = null;

            payloadObject.InboundDataFunction = function (data)
            {
                fnCallback();
            };

            payloadObject.ErrorHandlerFunction = function (request, status, error)
            {
                fnCallback();
            };

            TriSysAPI.Data.PostToWebAPI(payloadObject);
        },

        // Cached from TriSys Web API: CDataConnectionKey
        // Used to transfer from login.aspx to default.aspx
        //
        UserCredentials: function (optionalSetObject)
        {
            var cookieName = 'UserCredentials';
            var sStringifiedJSON = null;

            if (optionalSetObject)
            {
                // Set property
                sStringifiedJSON = JSON.stringify(optionalSetObject);
                TriSysAPI.Cookies.setCookie(cookieName, sStringifiedJSON, 1);
            }
            else
            {
                // Get property
                if (TriSysApex.LoginCredentials.userCredentialsCache)
                    return TriSysApex.LoginCredentials.userCredentialsCache;

                // Get from storage, then delete to prevent re-use
                sStringifiedJSON = TriSysAPI.Cookies.getCookie(cookieName);
                if (sStringifiedJSON)
                {
                    TriSysAPI.Cookies.setCookie(cookieName, null, 1);

                    TriSysApex.LoginCredentials.userCredentialsCache = JSON.parse(sStringifiedJSON);
                    return TriSysApex.LoginCredentials.userCredentialsCache;
                }
            }
        },

        ClearCachedCredentials: function ()
        {
            TriSysApex.LoginCredentials.userCredentialsCache = null;
        },

        // Local page cache
        userCredentialsCache: null,

        areUserCredentialsValid: function ()
        {
            var userCredentials = TriSysApex.LoginCredentials.UserCredentials();

            if (userCredentials)
            {
                if (userCredentials.LoggedInUser.UserId > 0)
                {
                    return true;
                }
            }

            return false;
        },

        EMailSupportMessageHTML: function ()     // TriSysApex.LoginCredentials.EMailSupportMessageHTML
        {
            var callbackURL = "http://www.trisys.co.uk/Support/CallbackRequest.aspx?Enquiry=Support&Message=Apex%20Login%20Problem";
            var projectIssueURL = "http://www.trisys.co.uk/Support/ProjectIssue.aspx";
            var supportEMail = "support@trisys.co.uk";

            var msg = "<br /><br />" +
                'Please e-mail <a href="mailto:' + supportEMail + '">' + supportEMail + '</a>' +
                ', or <a href="' + projectIssueURL + '" target="_blank">log a support issue</a>' +
                ', or <a href="' + callbackURL + '" target="_blank">request a callback</a>' +
                ' if you need help connecting to our service.';
            return msg;
        }

    }, // End of TriSysApex.LoginCredentials

    //#endregion TriSysApex.LoginCredentials

    //#region TriSysApex.UserOptions

    UserOptions:        // TriSysApex.UserOptions
    {
        showOption: function (sID)
        {
            var allTabs = ['option_FormsBehaviour', 'option_LoginCredentials', 'option_SystemSettings',
                'option_GridManagement', 'option_Reports', 'option_EMail'
            ];
            for (var i = 0; i < allTabs.length; i++)
                $('#' + allTabs[i]).hide();

            $('#' + sID).show();
        },

        SystemSettings: function ()
        {
            var sVersion = TriSysAPI.Data.Version();
            var sIPAddress = TriSysAPI.Data.IPAddress();
            var fnPart = TriSysApex.UserOptions.SystemSettingTablePart;

            var sText = '<table style="width:100%" border="0">' +
                fnPart(true, "Apex Version:") + fnPart(false, TriSysApex.Constants.Version) +
                fnPart(true, "Web API Version:") + fnPart(false, sVersion) +
                fnPart(true, "IP Address:") + fnPart(false, sIPAddress);

            var userCredentials = TriSysApex.LoginCredentials.UserCredentials();
            if (userCredentials)
            {
                sText +=
                    fnPart(true, 'SQL Server:') + fnPart(false, userCredentials.SQLServer) +
                    fnPart(true, 'Database:') + fnPart(false, userCredentials.Database) +
                    fnPart(true, 'G:\ Drive Path:') + fnPart(false, userCredentials.GDrivePath) +
                    fnPart(true, 'User Id:') + fnPart(false, userCredentials.LoggedInUser.UserId) +
                    fnPart(true, 'Login Name:') + fnPart(false, userCredentials.LoggedInUser.LoginName) +
                    fnPart(true, 'Full Name:') + fnPart(false, userCredentials.LoggedInUser.FullName) +
                    fnPart(true, 'Company Name:') + fnPart(false, userCredentials.LoggedInUser.CompanyName) +
                    fnPart(true, 'Contact Id:') + fnPart(false, userCredentials.LoggedInUser.ContactId);

                var contact = userCredentials.LoggedInUser.Contact;
                if (contact)
                {
                    sText +=
                        fnPart(true, 'FullName:') + fnPart(false, contact.FullName) +
                        fnPart(true, 'Forenames:') + fnPart(false, contact.Forenames) +
                        fnPart(true, 'Surname:') + fnPart(false, contact.Surname) +
                        fnPart(true, 'CompanyName:') + fnPart(false, contact.CompanyName) +
                        fnPart(true, 'CompanyAddressStreet:') + fnPart(false, contact.CompanyAddressStreet) +
                        fnPart(true, 'CompanyAddressCity:') + fnPart(false, contact.CompanyAddressCity) +
                        fnPart(true, 'Title:') + fnPart(false, contact.Title) +
                        fnPart(true, 'WorkTelNo:') + fnPart(false, contact.WorkTelNo) +
                        fnPart(true, 'MobileTelNo:') + fnPart(false, contact.MobileTelNo) +
                        fnPart(true, 'JobTitle:') + fnPart(false, contact.JobTitle) +
                        fnPart(true, 'ContactType:') + fnPart(false, contact.ContactType) +
                        fnPart(true, 'Priority:') + fnPart(false, contact.Priority) +
                        fnPart(true, 'HomeAddressStreet:') + fnPart(false, contact.HomeAddressStreet) +
                        fnPart(true, 'HomeAddressCity:') + fnPart(false, contact.HomeAddressCity) +
                        fnPart(true, 'HomeAddressTelNo:') + fnPart(false, contact.HomeAddressTelNo) +
                        fnPart(true, 'WorkEMail:') + fnPart(false, contact.WorkEMail) +
                        fnPart(true, 'PersonalEMail:') + fnPart(false, contact.PersonalEMail) +
                        fnPart(true, 'ContactPhoto:') + fnPart(false, contact.ContactPhoto);
                }
            }

            sText += '</table>';

            return sText;
        },

        SystemSettingTablePart: function (bLabel, sValue)
        {
            var sTableText = '';
            if (bLabel)
                sTableText = '<tr><td style="width:175px">' + sValue + '</td>';
            else
                sTableText = '<td>' + sValue + '</td></tr>';

            return sTableText;
        },

        UpdateUserPhoto: function (sServerImagePath, sDivTag)
        {
            var lContactId = TriSysApex.LoginCredentials.UserCredentials().LoggedInUser.ContactId;
            TriSysApex.UserOptions.UpdateUserPhotoByContactId(sServerImagePath, sDivTag, lContactId);
        },

        UpdateUserPhotoByContactId: function (sServerImagePath, sDivTag, lContactId)
        {
            //alert("UpdateUserPhotoByContactId: " + sServerImagePath + ", " + sDivTag)
            TriSysSDK.CShowForm.MoveUploadedTemporaryFileReference("ContactConfigFields", lContactId, "ContactPhoto", sServerImagePath,
                function (sURL)
                {
                    TriSysApex.UserOptions.RefreshUserPhotosAfterChange(sDivTag, sURL);
                });
        },

        UpdateContactPhoto: function (sServerImagePath, sDivTag, sAvatarImageDiv, lContactId)
        {
            TriSysSDK.CShowForm.MoveUploadedTemporaryFileReference("ContactConfigFields", lContactId, "ContactPhoto", sServerImagePath,
                function (sURL)
                {
                    TriSysApex.Pages.Index.ShowImageThumbnail(sURL, sDivTag, TriSysApex.Constants.ProfileImageSize, TriSysApex.Constants.ProfileImageSize);
                    TriSysSDK.Controls.FileReference.SetFile(sAvatarImageDiv, sContactPhoto);
                });
        },

        DeleteContactPhoto: function (sDivTag, sAvatarImageDiv, lContactId)
        {
            TriSysApex.Pages.Index.ShowImageThumbnail(null, sDivTag, TriSysApex.Constants.ProfileImageSize, TriSysApex.Constants.ProfileImageSize);
            TriSysSDK.Controls.FileReference.SetFile(sAvatarImageDiv, null);
        },

        DeleteUserPhoto: function (sDivTag)
        {
            var lContactId = TriSysApex.LoginCredentials.UserCredentials().LoggedInUser.ContactId;
            TriSysApex.UserOptions.DeleteUserPhotoByContactId(sDivTag, lContactId);
        },

        DeleteUserPhotoByContactId: function (sDivTag, lContactId)
        {
            //alert("DeleteUserPhotoByContactId: " + sDivTag)
            TriSysSDK.CShowForm.MoveUploadedTemporaryFileReference("ContactConfigFields", lContactId, "ContactPhoto", null,
                function (sURL)
                {
                    TriSysApex.UserOptions.RefreshUserPhotosAfterChange(sDivTag, null);
                });
        },

        RefreshUserPhotosAfterChange: function (sDivTag, sImage)
        {
            var userCredentials = TriSysApex.LoginCredentials.UserCredentials();
            if (userCredentials)
            {
                var contact = userCredentials.LoggedInUser.Contact;
                if (contact)
                    contact.ContactPhoto = sImage;

                TriSysApex.Pages.Index.ShowImageThumbnail(sImage, sDivTag, TriSysApex.Constants.ProfileImageSize, TriSysApex.Constants.ProfileImageSize);
            }

            TriSysApex.Pages.Index.PostLoginVisuals(true);
        },

        SQLTopNRecords: 1024,
        RecordsPerPage: 15

    }, // End of TriSysApex.UserOptions
    //#endregion TriSysApex.UserOptions

    //#region TriSysApex.Environment

    Environment:        // TriSysApex.Environment
    {
        isLocalhost: function ()
        {
            var sThisURL = window.location.href;
            return (sThisURL.indexOf("http://localhost") == 0);
        },

        isDeployedInTestMode: function ()
        {
            var sThisURL = window.location.href;
            return (sThisURL.indexOf("http://www.trisys.co.uk/development/test/") == 0);
        },

        // Refresh the browser so that we always pick up the latest version from Apex web server
        RefreshCachedApplication: function ()
        {
            // When did we last refresh?
            var dtLastRefresh = TriSysAPI.Session.GetLastAppRefreshDateTime();
            TriSysApex.Logging.LogMessage("RefreshCachedApplication: dtLastRefresh=" + dtLastRefresh);

            if (dtLastRefresh)
            {
                var iTimeBetweenRefreshesInSeconds = 15;    // Should take less than this many seconds to refresh from server cache
                var dtNow = new Date();
                TriSysApex.Logging.LogMessage("RefreshCachedApplication: dtNow=" + dtNow);
                var iSecondsDifference = moment(dtNow).diff(moment(dtLastRefresh), 'seconds');
                TriSysApex.Logging.LogMessage("RefreshCachedApplication: iSecondsDifference=" + iSecondsDifference);

                if (iSecondsDifference < iTimeBetweenRefreshesInSeconds)
                    return false;     // We just refreshed so do not need to do it again
            }

            // Record the current date/time as the date last refreshed
            TriSysAPI.Session.SetLastAppRefreshDateTime();

            // Force refresh now
            var bForceReCacheFromServer = true;
            location.reload(bForceReCacheFromServer);

            return true;
        }

    }, // End of TriSysApex.Environment Namespace
    //#endregion TriSysApex.Environment

    //#region TriSysApex.Cache

    Cache:
    {
        cookieName: 'CachedStandingData',
        cachedStandingDataInMemory: null,

        isCached: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            var bStandingDataExists = !TriSysAPI.Operators.isEmpty(standingData);
            return bStandingDataExists;
        },

        Clear: function ()
        {
            TriSysApex.Cache.cachedStandingDataInMemory = null;
            TriSysApex.Cache.populateFormFieldDescriptions = null;
        },

        SetStandingData: function (setObject)
        {
            var sStringifiedJSON = JSON.stringify(setObject);
            TriSysAPI.Cookies.setCookie(TriSysApex.Cache.cookieName, sStringifiedJSON, 1);
        },

        GetStandingData: function ()
        {
            if (TriSysApex.Cache.cachedStandingDataInMemory)
                return TriSysApex.Cache.cachedStandingDataInMemory;

            // Get from storage as cache may be large and we only need it ocassionally
            var sStringifiedJSON = TriSysAPI.Cookies.getCookie(TriSysApex.Cache.cookieName);
            if (sStringifiedJSON)
            {
                var standingDataCache = JSON.parse(sStringifiedJSON);
                TriSysApex.Cache.cachedStandingDataInMemory = standingDataCache;
                return standingDataCache;
            }
        },

        Users: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.Users;
        },

        Skills: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.Skills;
        },

        SystemSettings: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.SystemSettings;
        },

        EntityTypes: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.EntityTypes;
        },

        JobTitles: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.JobTitles;
        },

        ContactTitles: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.ContactTitles;
        },

        Currencies: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.Currencies;
        },

        ConsultancyPeriods: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.ConsultancyPeriods;
        },

        FieldDescriptions: function ()
        {
            var standingData = TriSysApex.Cache.GetStandingData();
            if (standingData)
                return standingData.FieldDescriptions;
        },

        populateFormFieldDescriptions: null

    }, // End of Cache Namespace
    //#endregion TriSysApex.Cache

    //#region TriSysApex.Pages

    Pages:
    {        // TriSysApex.Pages

        //#region TriSysApex.Pages.Controller
        Controller:     // TriSysApex.Pages.Controller
        {
            OpenLoginForm: function ()
            {
                setTimeout(function ()
                {
                    TriSysApex.FormsManager.OpenForm("Login");  // The login form
                }, 250);
            },

            EnsureDeveloperOnlyAccess: function ()
            {
                var bInDev = (TriSysApex.Environment.isLocalhost() || TriSysApex.Environment.isDeployedInTestMode());
                var bPrivateBeta = true;

                if (bInDev || bPrivateBeta)
                {
                    // Only certain IP addresses are allowed to use the non-obfuscated product 
                    var bDeveloperAccessGranted = TriSysApex.Pages.Controller.IsClientADeveloperIPAddress();
                    if (bDeveloperAccessGranted)
                        return true;
                }
                else
                    if (!bPrivateBeta)
                        return true;

                // We are going to redirect, but do this after cacheing the page in case our logic changes to determine
                // who has access e.g. adding a trusted IP address, or moving out of a private beta
                TriSysApex.Environment.RefreshCachedApplication();

                // When in private beta and not a development machine, redirect to allow user to request access
                window.location = "http://www.trisys.co.uk/Support/CallbackRequest.aspx?Enquiry=Support&Message=Apex%20Development%20Access";
                return false;
            },

            IsClientADeveloperIPAddress: function ()
            {
                return true;
                //                                     6HA              Wellington House     Bedlington
                var validTriSysDeveloperIPAddresses = ["86.27.188.178", "90.152.120.82", "81.156.101.194"];
                var sIPAddress = TriSysAPI.Data.IPAddress();
                var bFound = ($.inArray(sIPAddress, validTriSysDeveloperIPAddresses) >= 0);
                return bFound;
            },

            ResizeManagerForHelpPanelVisibility: function (sDiv)     // TriSysApex.Pages.Controller.ResizeManagerForHelpPanelVisibility
            {
                // Handle resize event
                $(window).resize(function ()
                {
                    ShowHideHelpPanel();
                });

                function ShowHideHelpPanel()
                {
                    var sJQueryTag = '#' + sDiv;
                    // Show/Hide help
                    if (TriSysApex.Pages.Index.MobileViewEnabled)
                        $(sJQueryTag).hide();
                    else
                        $(sJQueryTag).show();
                }

                // Resize now also
                ShowHideHelpPanel();
            }

        },      // End of TriSysApex.UI.Pages.Controller
        //#endregion TriSysApex.Pages.Controller

        //#region TriSysApex.Pages.Index
        //
        // This is the entry point into the application.
        // It is called from index.html.
        //
        Index:  // TriSysApex.Pages.Index
        {
            // See Default.aspx: jQuery(document).ready(function ()
            documentReady: function (sMobileMenuDiv)
            {
                // Setup global error handling
                TriSysApex.ErrorHandling.CatchAll();

                // Set the API key to identify all server traffic requests
                var sSiteKey = TriSysApex.Constants.DeveloperSiteKey;
                TriSysAPI.Session.SetDeveloperSiteKey(sSiteKey);

                if (!TriSysApex.Pages.Controller.EnsureDeveloperOnlyAccess())
                    return;

                TriSysApex.Forms.Configuration.Initialise();
                TriSysApex.MobileToolbarMenu.DrawMobileMenuBar(sMobileMenuDiv);        // Populate mobile menu bar after login
                TriSysApex.Pages.Controller.OpenLoginForm();

                // Ribbon and Layout
                TriSysApex.Ribbon.DrawRibbon();               // Populate ribbon after login

                // Hide all contextual ribbon forms
                TriSysApex.FormsManager.ShowContextualRibbonTab(null);

                // Set up window resize events
                TriSysApex.Pages.Index.WindowsResizeEvents();

                // Prevent user going back
                TriSysApex.Pages.Index.PreventBrowserBackButton();
            },

            // Do not allow incompatible web browsers to proceed
            BrowserCompatibilityCheck: function ()
            {
                var bCompatible = TriSysSDK.Browser.Supported();
                if (!bCompatible)
                {
                    TriSysApex.UI.errorAlert("Your web browser is not supported. Please upgrade to a modern browser.");
                }

                return bCompatible;
            },
            PostLoginVisuals: function (bLoggedIn)      // TriSysApex.Pages.Index.PostLoginVisuals
            {
                var sUserName = '', sCompanyName = '', sImage = TriSysApex.Constants.DefaultLoggedInImage;
                if (bLoggedIn)
                {
                    // Display logged in users credentials
                    var userCredentials = TriSysApex.LoginCredentials.UserCredentials();
                    if (userCredentials)
                    {
                        sCompanyName = userCredentials.LoggedInUser.CompanyName;
                        sUserName = userCredentials.LoggedInUser.FullName;

                        var contact = userCredentials.LoggedInUser.Contact;
                        if (contact)
                        {
                            sCompanyName = contact.CompanyName;
                            sUserName = contact.FullName;
                            if (contact.ContactPhoto)
                                sImage = contact.ContactPhoto;
                        }
                    }
                }

                $('#lblLoggedInUserCompanyName').text(sCompanyName);
                $('#lblLoggedInUserFullName').text(sUserName);

                TriSysApex.Pages.Index.ShowLoggedInAvatar(sImage);
            },

            // Image can be any size as we proportionately resize it to fit
            ShowLoggedInAvatar: function (sImage)
            {
                if (sImage)
                {
                    $('#thLoggedInUserImage').show();
                    TriSysApex.Pages.Index.ShowImageThumbnail(sImage, 'imgLoggedInUserImage', TriSysApex.Constants.ProfileImageSize, TriSysApex.Constants.ProfileImageSize);
                }
                else
                {
                    $('#thLoggedInUserImage').hide();
                }
            },

            ShowImageThumbnail: function (sImage, sDivTag, iHeight, iWidth)
            {
                $('#' + sDivTag).attr("src", sImage);

                $('#' + sDivTag).jqthumb({
                    classname: 'jqthumb',
                    width: iWidth,
                    height: iHeight,
                    showoncomplete: true
                });
            },

            ChangeLoggedInAvatar: function ()
            {
                //TriSysApex.UI.ShowMessage("Change Avatar and/or login profile...");
                TriSysSDK.Controls.UserProfileEditor.Open(function ()
                {
                    TriSysApex.Pages.Index.PostLoginVisuals(true);
                });
            },

            PreventBrowserBackButton: function ()
            {
                var noBackButton = '@';
                history.pushState({ page: 1 }, "title 1", "#" + noBackButton);
                window.onhashchange = function (event)
                {
                    window.location.hash = noBackButton;

                };
            },

            WindowsResizeEvents: function ()
            {
                // Fire this event when the window resizes
                window.onresize = function (event)
                {
                    TriSysApex.Pages.Index.onWindowResize();
                };

                // Fire it now after starting application
                TriSysApex.Pages.Index.onWindowResize();
            },

            onWindowResize: function ()
            {
                var iWidth = document.body.offsetWidth;

                if (TriSysApex.Pages.Index.isScreenWidthSuitableForMobile())
                    TriSysApex.Pages.Index.changeMobileOrDesktopViews(true);
                else
                    TriSysApex.Pages.Index.changeMobileOrDesktopViews(false);

                var iMinAppTitle = 636;
                if (iWidth < iMinAppTitle)
                    $('#app-caption').hide();
                else
                    $('#app-caption').show();

            },

            isScreenWidthSuitableForMobile: function ()        // TriSysApex.Pages.Index.isScreenWidthSuitableForMobile
            {
                // These are set to zero when form is loading i.e. before it is shown
                var iHeight = document.body.offsetHeight;
                var iWidth = document.body.offsetWidth;

                var iMinRibbonWidth = 1010;
                if (iWidth < iMinRibbonWidth)
                    return true;
                else
                    return false;
            },

            // Called when the user collapses or expands the ribbon
            ribbonExpander: function (bExpanded)
            {
                var bCollapsed = !bExpanded;
                TriSysApex.Logging.LogMessage("ribbon is " + (bCollapsed ? "collapsed" : "expanded"));

                var sDivTag = "#divRibbonRow";
                var obj = $(sDivTag);
                var iRibbonHeight = (bCollapsed ? 53 : 146);
                obj.css('height', iRibbonHeight);

                var iHeaderHeight = 45;
                var iTopPosition = iRibbonHeight + iHeaderHeight;
                TriSysApex.Pages.Index.setTopPositionInDiv("divFormCaptionRow", iTopPosition);

                var iFormCaptionHeight = 23;
                iTopPosition = iTopPosition + iFormCaptionHeight + 2;
                TriSysApex.Pages.Index.setTopPositionInDiv("divFormContentRow", iTopPosition);
            },

            FormContentBottom: 0,
            FormContentHeight: 0,
            MobileViewEnabled: false,

            changeMobileOrDesktopViews: function (bMobile)      // TriSysApex.Pages.Index.changeMobileOrDesktopViews
            {
                var iAppCaptionHeight = 45;
                var iRibbonHeight = 146, iMenuHeight = 26;
                var iRibbonRowHeight = (bMobile ? iMenuHeight : iRibbonHeight);
                var iFormCaptionHeight = 23;
                var iFormTabsHeight = 28;
                var iStatusBarHeight = 20;
                var iFormCaptionTop, iFormContentTop, iFormTabsBottom = 0;

                // Record our current view style to render forms correctly for device preference
                TriSysApex.Pages.Index.MobileViewEnabled = bMobile;

                if (bMobile)
                {
                    // Mobile view
                    $("#desktop-ribbon").hide();    // Hide the ribbon
                    $("#menu-CRM").show();          // Show the drop down menu
                    $('#divFooterRow').hide();      // Hide status bar
                    $('#divFormMenu').hide();       // Hide form caption menu

                    // Move other controls up
                    var iFormCaptionTop = iAppCaptionHeight + iMenuHeight;
                    TriSysApex.Pages.Index.FormContentBottom = iFormTabsHeight;

                    // Set default size of message box
                    TriSysApex.UI.iTriSysMessageBoxWidth = 300;
                }
                else
                {
                    // Desktop view
                    $("#desktop-ribbon").show();    // Show the ribbon
                    $("#menu-CRM").hide();          // Hide the drop down menu
                    $('#divFooterRow').show();      // Show status bar
                    $('#divFormMenu').show();       // Show form caption menu

                    // Move other controls up
                    var iFormCaptionTop = iAppCaptionHeight + iRibbonHeight;
                    iFormTabsBottom = iStatusBarHeight;
                    TriSysApex.Pages.Index.FormContentBottom = iFormTabsHeight + iStatusBarHeight;

                    // Set default size of message box
                    TriSysApex.UI.iTriSysMessageBoxWidth = 500;
                }

                var iFormContentTop = iFormCaptionTop + iFormCaptionHeight + 2;

                TriSysApex.Pages.Index.setTopPositionInDiv("divFormCaptionRow", iFormCaptionTop);
                TriSysApex.Pages.Index.setTopPositionInDiv("divFormContentRow", iFormContentTop);

                // Position form tabs at bottom for easy navigation
                var formTabs = $("#divFormTabsRow");
                formTabs.css('bottom', iFormTabsBottom);

                // Make maximum room for content
                var obj = $("#divFormContentRow");
                obj.css('bottom', TriSysApex.Pages.Index.FormContentBottom);

                TriSysApex.Pages.Index.FormContentHeight = obj.height();

                // Ribbon/Toolbar/Menu section
                var obj = $("#divRibbonRow");
                obj.css('height', iRibbonRowHeight);

                // Set random CSS property to see how easy it is to do!
                //cssUpdate('#top-application-caption-pane', 'background-color', 'Red');
            },

            moveDivPosition: function (sDivTag, bRibbonVisible, iRibbonHeight)
            {
                sDivTag = "#" + sDivTag;
                var obj = $(sDivTag);
                var position = obj.position();
                var lNewPosition = position.top - iRibbonHeight;
                if (bRibbonVisible)
                    lNewPosition = position.top + iRibbonHeight;

                obj.css('top', lNewPosition);
            },

            setTopPositionInDiv: function (sDivTag, iTop)
            {
                sDivTag = "#" + sDivTag;
                var obj = $(sDivTag);
                obj.css('top', iTop);
            }

        },  // End of TriSysApex.Pages.Index

        //#endregion TriSysApex.Pages.Index

        //#region TriSysApex.Pages.DashboardForm
        DashboardForm:      // TriSysApex.Pages.DashboardForm
        {
            populateTestCharts: function (sDivTag)
            {
                $('#' + sDivTag).html("<b>Populating charts...</b>");
                $('#' + sDivTag).hide();
            },

        },
        //#endregion TriSysApex.Pages.DashboardForm

        ContactsForm:      // TriSysApex.Pages.ContactsForm
        {
            SearchCriteria: {
                Forenames: '',
                Surname: '',
                Company: '',
                Wildcard: ''
            },
            isEmptySearchCriteria: function ()
            {
                var criteria = TriSysApex.Pages.ContactsForm.SearchCriteria;
                return (criteria.Forenames == '' && criteria.Surname == '' && criteria.Company == '' && criteria.Wildcard == '');
            },

            // New sophisticated SQL:
            SQL: 'Select ' +
                 '  Contact_ContactId,   Contact_Christian, Contact_Surname,   Contact_WorkTelNo,   ' +
                 '       Contact_MobileTelNo, Contact_EMail,   Contact_DateOfBirth, Contact_Priority,   ' +
                 '       Contact_Title, Contact_OwnerUser,   Contact_Company,   Contact_CompanyAddressStreet,   ' +
                 '       Contact_CompanyAddressCity,   Contact_CompanyAddressCounty,   Contact_CompanyAddressPostCode,  ' +
                 '       Contact_CompanyAddressCountry,   Contact_HomeAddressStreet,   Contact_HomeAddressCity,   ' +
                 '       Contact_HomeAddressCounty,   Contact_HomeAddressPostCode,   Contact_HomeAddressCountry,   ' +
                 '       Contact_HomeAddressTelNo,   Contact_ContactType, Contact_JobTitle,   Contact_DateFirstRegistered, ' +
                 '       Contact_DateLastUpdated,   Contact_AddedByName,   Contact_AvailabilityDate,   Contact_ContactSource,   ' +
                 '       Contact_LimitedCompany,   Contact_Status, Contact_WorkMobile, Contact_HomeEmail ' +
                 '   from v_Contact_AllFields',

            OrderBy: ' Order by Contact_Surname, Contact_Christian, Contact_Company',

            Lookup: function ()
            {
                var sWhereSQL = ' ';

                //#region Commented out Criteria
                var criteria = TriSysApex.Pages.ContactsForm.SearchCriteria;
                //criteria.Forenames = $('#txtForenames').val();
                //criteria.Surname = $('#txtSurname').val();
                //criteria.Company = $('#txtCompany').val();
                //criteria.Wildcard = $('#txtWildcard').val();


                //if (criteria.Forenames)
                //    sWhereSQL += " And Christian like '" + criteria.Forenames + "%'";

                //if (criteria.Surname)
                //    sWhereSQL += " And Surname like '" + criteria.Surname + "%'";

                //if (criteria.Company)
                //    sWhereSQL += " And comp.Name like '" + criteria.Company + "%'";

                //if (criteria.Wildcard) {
                //    sWhereSQL += " And ( Christian like '" + criteria.Wildcard + "%'" +
                //        "      or Surname like '" + criteria.Wildcard + "%'" +
                //        "      or comp.Name like '" + criteria.Wildcard + "%'" +
                //        "     )";
                //}
                //#endregion Commented out Criteria

                TriSysSDK.Grid.VirtualMode({
                    Div: "divContactsGrid",
                    ID: "grdContacts",
                    Title: "Contacts",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.ContactsForm.SQL +
                        sWhereSQL,
                    OrderBy: TriSysApex.Pages.ContactsForm.OrderBy,
                    Columns: [
                        { field: "Contact_ContactId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Contact_Christian", title: "Forename(s)", type: "string" },
                        { field: "Contact_Surname", title: "Surname", type: "string" },
                        { field: "Contact_Company", title: "Company", type: "string" },
                        { field: "Contact_EMail", title: "Work E-Mail", type: "string" },
                        { field: "Contact_HomeEmail", title: "Home E-Mail", type: "string" },
                        { field: "Contact_WorkTelNo", title: "Work Tel. No", type: "string" },
                        { field: "Contact_MobileTelNo", title: "Personal Mobile", type: "string" },
                        { field: "Contact_WorkMobile", title: "Work Mobile", type: "string", hidden: true },
                        { field: "Contact_HomeAddressTelNo", title: "Home Tel. No", type: "string" },
                        { field: "Contact_DateOfBirth", title: "Date of Birth", hidden: true, type: "date", format: "{0:dd MMM yyyy }" },
                        { field: "Contact_Title", title: "Title", type: "string", hidden: true },
                        { field: "Contact_OwnerUser", title: "Owner User", type: "string", hidden: true },
                        { field: "Contact_CompanyAddressStreet", title: "Company Street Address", type: "string", hidden: true },
                        { field: "Contact_CompanyAddressCity", title: "Company Street City", type: "string", hidden: true },
                        { field: "Contact_CompanyAddressCounty", title: "Company Street County", type: "string", hidden: true },
                        { field: "Contact_CompanyAddressPostCode", title: "Company Street Post/ZIP Code", type: "string", hidden: true },
                        { field: "Contact_CompanyAddressCountry", title: "Company Street Country", type: "string", hidden: true },
                        { field: "Contact_HomeAddressStreet", title: "Home Street Address", type: "string", hidden: true },
                        { field: "Contact_HomeAddressCity", title: "Home Street City", type: "string", hidden: true },
                        { field: "Contact_HomeAddressCounty", title: "Home Street County", type: "string", hidden: true },
                        { field: "Contact_HomeAddressPostCode", title: "Home Street Post/ZIP Code", type: "string", hidden: true },
                        { field: "Contact_HomeAddressCountry", title: "Home Street Country", type: "string", hidden: true },
                        { field: "Contact_ContactType", title: "Contact Type", type: "string", hidden: true },
                        { field: "Contact_JobTitle", title: "Job Title", type: "string", hidden: true },
                        { field: "Contact_AvailabilityDate", title: "Availability Date", hidden: true, type: "date", format: "{0:dd MMM yyyy }" },
                        { field: "Contact_ContactSource", title: "Source", type: "string", hidden: true },
                        { field: "Contact_Status", title: "Status", type: "string", hidden: true }
                    ],
                    MobileVisibleColumns: [
                        { field: "Contact_Christian" },
                        { field: "Contact_Surname" },
                        { field: "Contact_Company" }
                    ],

                    KeyColumnName: "Contact_ContactId",
                    //DrillDownFunction: "TriSysApex.Pages.ContactForm.contactRecordDrillDown"
                    DrillDownFunction: function (rowData)
                    {
                        // Load Contact Form with specified ID
                        //TriSysApex.UI.ShowMessage("You drilled down into row: " + rowData.Contact_ContactId + ', ' + rowData.Contact_Christian + ' ' + rowData.Contact_Surname + '?');
                        TriSysApex.FormsManager.OpenForm("Contact", rowData.Contact_ContactId);
                    },
                    MultiRowSelect: true
                });
            }
        },  // End of TriSysApex.Pages.ContactsForm

        ContactForm:      // TriSysApex.Pages.ContactForm
        {
            LoadRecord: function (lContactId)
            {
                // Put in a very small delay purely to show popup message during read
                setTimeout(function ()
                {
                    TriSysSDK.CShowForm.readEntityFormDataFromServer('Contact', lContactId,
                                                TriSysApex.Pages.ContactForm.AfterFormDataLoaded);

                }, TriSysApex.Constants.FormLoadDataDelay);
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lContactId, fieldDescriptions)
            {
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "ContactConfigFields_CVFileRef":
                        case "ContactConfigFields_FormattedCVFileRef1":
                        case "ContactConfigFields_FormattedCVFileRef2":
                        case "ContactConfigFields_FormattedCVFileRef3":
                        case "ContactConfigFields_FormattedCVFileRef4":
                            // CV Manager 
                            TriSysSDK.Controls.CVManager.SetCVPath(sDivID, objValue);
                            TriSysSDK.Controls.CVManager.ShowCVPath(sDivID);
                            break;

                        case "ContactConfigFields_IdentificationComments":
                            TriSysSDK.Controls.TextBoxDropDown.SetText(sDivID, objValue);
                            fieldDescription.FieldTypeId = TriSysSDK.Controls.FieldTypes.TextDropDown;
                            TriSysApex.Cache.SetStandingData();
                            break;
                    }
                }
            }
        },

        CompaniesForm:      // TriSysApex.Pages.CompaniesForm
        {
            SearchCriteria: {
                Name: '',
                Industry: '',
                WebPage: '',
                TelNo: ''
            },
            isEmptySearchCriteria: function ()
            {
                var criteria = TriSysApex.Pages.CompaniesForm.SearchCriteria;
                return (criteria.Name == '' && criteria.Industry == '' && criteria.WebPage == '' && criteria.TelNo == '');
            },
            SQL: 'Select Company_CompanyId, Company_Name,' +
                '       Company_WebPage, Company_MainTelNo,' +
                '       Company_Industry, Company_Comments' +
                ' From v_Company_AllFields' +
                ' Where Company_CompanyId > 0',
            OrderBy: ' Order by Company_Name',

            Lookup: function ()
            {
                var criteria = TriSysApex.Pages.CompaniesForm.SearchCriteria;
                //criteria.Name = '';
                //criteria.Name = $('#txtName').val();
                //criteria.Industry = $('#txtIndustry').val();
                //criteria.WebPage = $('#txtWebPage').val();
                //criteria.TelNo = $('#txtTelNo').val();

                var sWhereSQL = ' ';

                if (criteria.Name)
                    sWhereSQL += " And Company_Name like '" + criteria.Name + "%'";

                if (criteria.WebPage)
                    sWhereSQL += " And Company_WebPage like '" + criteria.WebPage + "%'";

                if (criteria.TelNo)
                    sWhereSQL += " And Company_MainTelNo like '" + criteria.TelNo + "%'";

                if (criteria.Industry)
                    sWhereSQL += " And Company_Industry like '" + criteria.Industry + "%'";


                TriSysSDK.Grid.VirtualMode({
                    Div: "divCompaniesGrid",
                    ID: "grdCompanies",
                    Title: "Companies",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.CompaniesForm.SQL,
                    OrderBy: TriSysApex.Pages.CompaniesForm.OrderBy,
                    Columns: [
                        { field: "Company_CompanyId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Company_Name", title: "Company Name", type: "string" },
                        { field: "Company_WebPage", title: "Web Page", type: "string" },
                        { field: "Company_MainTelNo", title: "Main Tel No", type: "string" },
                        { field: "Company_Industry", title: "Industry", type: "string" },
                        { field: "Company_Comments", title: "Comments", type: "string" }
                    ],
                    MobileVisibleColumns: [
                        { field: "Company_Name" },
                        { field: "Company_MainTelNo" }
                    ],

                    KeyColumnName: "Company_CompanyId",
                    DrillDownFunction: function (rowData)
                    {
                        // Load Company Form with specified ID
                        //TriSysApex.UI.ShowMessage("You drilled down into row: " + rowData.Company_CompanyId + ', ' + rowData.Company_Name + '?');
                        TriSysApex.FormsManager.OpenForm("Company", rowData.Company_CompanyId);
                    },
                    MultiRowSelect: true
                });
            }

        },  // End of TriSysApex.Pages.CompaniesForm

        CompanyForm:      // TriSysApex.Pages.CompanyForm
        {
            LoadRecord: function (lCompanyId)
            {
                // Put in a very small delay purely to show popup message during read
                setTimeout(function ()
                {
                    TriSysSDK.CShowForm.readEntityFormDataFromServer('Company', lCompanyId,
                                                TriSysApex.Pages.CompanyForm.AfterFormDataLoaded);

                }, TriSysApex.Constants.FormLoadDataDelay);
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lCompanyId, fieldDescriptions)
            {
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "a company field":

                            break;
                    }
                }
            }
        },   // End of TriSysApex.Pages.CompanyForm

        RequirementsForm: {
            SearchCriteria: {
                Reference: '',
                Type: '',
                Company: '',
                JobTitle: '',
                ClientContact: '',
                User: ''
            },
            isEmptySearchCriteria: function ()
            {
                var criteria = TriSysApex.Pages.RequirementsForm.SearchCriteria;
                return (criteria.Reference == '' && criteria.Type == '' && criteria.Company == '' && criteria.JobTitle == '' && criteria.ClientContact == '' && criteria.User == '');
            },
            SQL: 'Select Requirement_RequirementId, Requirement_RequirementReference, Requirement_RequirementEntityType, ' +
                '  Requirement_JobTitle, RequirementCompany_Name, RequirementContact_FullName, ' +
                '  Requirement_RequirementStatus, Requirement_DateInitiated, ' +
                '  Requirement_EarliestStartDate, Requirement_Fee, ' +
                '  Requirement_SiteAddress, Requirement_UserName, Requirement_Comments' +
                ' from v_Requirement_AllFields',
            OrderBy: ' Order by RequirementCompany_Name, RequirementContact_Surname, Requirement_RequirementReference',
            Columns: [
                        { field: "Requirement_RequirementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Requirement_RequirementReference", title: "Reference", type: "string" },
                        { field: "Requirement_RequirementEntityType", title: "Type", type: "string" },
                        { field: "Requirement_JobTitle", title: "Job Title", type: "string" },
                        { field: "RequirementCompany_Name", title: "Company", type: "string" },
                        { field: "RequirementContact_FullName", title: "Contact", type: "string" },
                        { field: "Requirement_RequirementStatus", title: "Status", type: "string" },
                        { field: "Requirement_DateInitiated", title: "Initiated", type: "date", format: "{0:dd MMM yyyy }" },
                        { field: "Requirement_EarliestStartDate", title: "Earliest Start", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Requirement_Fee", title: "Fee", hidden: true, type: "string" },
                        { field: "Requirement_SiteAddress", title: "Site Address", hidden: true, type: "string" },
                        { field: "Requirement_UserName", title: "Consultant", hidden: true, type: "string" },
                        { field: "Requirement_Comments", title: "Comments", hidden: true, type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Requirement_RequirementReference" },
                { field: "Requirement_JobTitle" },
                { field: "RequirementCompany_Name" }
            ],

            Lookup: function ()
            {
                var criteria = TriSysApex.Pages.RequirementsForm.SearchCriteria;
                //criteria.Reference = $('#txtReference').val();
                //criteria.Type = $('#txtType').val();
                //criteria.Company = $('#txtCompanyName').val();
                //criteria.JobTitle = $('#txtJobTitle').val();
                //criteria.ClientContact = $('#txtClientContact').val();
                //criteria.User = $('#txtUser').val();

                var sWhereSQL = ' ';

                if (criteria.Reference)
                    sWhereSQL += " And Requirement_RequirementReference like '" + criteria.Reference + "%'";

                if (criteria.Type)
                    sWhereSQL += " And Requirement_RequirementEntityType like '" + criteria.Type + "%'";

                if (criteria.Company)
                    sWhereSQL += " And RequirementCompany_Name like '" + criteria.Company + "%'";

                if (criteria.JobTitle)
                    sWhereSQL += " And Requirement_JobTitle like '" + criteria.JobTitle + "%'";

                if (criteria.ClientContact)
                    sWhereSQL += " And RequirementContact_FullName like '" + criteria.ClientContact + "%'";

                if (criteria.User)
                    sWhereSQL += " And Requirement_UserName like '" + criteria.User + "%'";

                TriSysSDK.Grid.VirtualMode({
                    Div: "divRequirementsGrid",
                    ID: "grdRequirements",
                    Title: "Requirements",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.RequirementsForm.SQL,
                    OrderBy: TriSysApex.Pages.RequirementsForm.OrderBy,
                    Columns: TriSysApex.Pages.RequirementsForm.Columns,
                    MobileVisibleColumns: TriSysApex.Pages.RequirementsForm.MobileVisibleColumns,

                    KeyColumnName: "Requirement_RequirementId",
                    //DrillDownFunction: "TriSysApex.Pages.RequirementForm.requirementRecordDrillDown"
                    DrillDownFunction: function (rowData)
                    {
                        // TODO: Load Company Form with specified ID
                        //TriSysApex.UI.ShowMessage("You drilled down into row: " + rowData.Requirement_RequirementReference + ', ' + rowData.Requirement_JobTitle + ' ' + rowData.RequirementCompany_Name + '?');
                        TriSysApex.FormsManager.OpenForm("Requirement", rowData.Requirement_RequirementId);
                    },
                    MultiRowSelect: true
                });
            }
        },

        RequirementForm:      // TriSysApex.Pages.RequirementForm
        {
            LoadRecord: function (lRequirementId)
            {
                // Put in a very small delay purely to show popup message during read
                setTimeout(function ()
                {
                    TriSysSDK.CShowForm.readEntityFormDataFromServer('Requirement', lRequirementId,
                                                TriSysApex.Pages.RequirementForm.AfterFormDataLoaded);

                }, TriSysApex.Constants.FormLoadDataDelay);
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lRequirementId, fieldDescriptions)
            {
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "a Requirement field":

                            break;
                    }
                }
            }
        },   // End of TriSysApex.Pages.RequirementForm

        PlacementsForm: {
            SearchCriteria: {
                Reference: '',
                Type: '',
                Company: '',
                JobTitle: '',
                ClientContact: '',
                Candidate: '',
                Status: '',
                User: ''
            },
            isEmptySearchCriteria: function ()
            {
                var criteria = TriSysApex.Pages.PlacementsForm.SearchCriteria;
                return (criteria.Reference == '' && criteria.Type == '' && criteria.Company == '' && criteria.JobTitle == '' && criteria.ClientContact == '' && criteria.Candidate == '' && criteria.Status == '' && criteria.User == '');
            },
            SQL: 'Select Placement_PlacementId, Placement_Reference, Placement_PlacementEntityType, ' +
                '  Placement_JobTitle, CandidateContact_FullName, Placement_PlacementStatus, ' +
                '  PlacementCompany_Name, PlacementContact_FullName, ' +
                '  Placement_DateInitiated, ' +
                '  Placement_StartDate, Placement_EndDate, Placement_Fee, Placement_Salary, ' +
                '   Placement_UserName' +
                ' from v_Placement_AllFields' +
                ' Where Placement_PlacementId > 0 ',
            OrderBy: ' Order by PlacementCompany_Name, PlacementContact_FullName, Placement_Reference',
            Columns: [
                        { field: "Placement_PlacementId", title: "ID", type: "number", width: 70, hidden: true },
                        { field: "Placement_Reference", title: "Reference", type: "string" },
                        { field: "Placement_PlacementEntityType", title: "Type", type: "string" },
                        { field: "Placement_JobTitle", title: "Job Title", type: "string" },
                        { field: "CandidateContact_FullName", title: "Candidate", type: "string" },
                        { field: "Placement_PlacementStatus", title: "Status", type: "string" },
                        { field: "PlacementCompany_Name", title: "Company", type: "string" },
                        { field: "PlacementContact_FullName", title: "Client Contact", type: "string" },
                        { field: "Placement_DateInitiated", title: "Initiated", type: "date", format: "{0:dd MMM yyyy }" },
                        { field: "Placement_StartDate", title: "Start Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Placement_EndDate", title: "End Date", type: "date", format: "{0:dd MMM yyyy}" },
                        { field: "Placement_Fee", title: "Fee", hidden: true, type: "string" },
                        { field: "Placement_Salary", title: "Salary", hidden: true, type: "string" },
                        { field: "Placement_UserName", title: "Consultant", hidden: true, type: "string" }
            ],
            MobileVisibleColumns: [
                { field: "Placement_Reference" },
                { field: "Placement_JobTitle" },
                { field: "CandidateContact_FullName" }
            ],

            Lookup: function ()
            {
                var criteria = TriSysApex.Pages.PlacementsForm.SearchCriteria;
                //criteria.Reference = $('#txtReference').val();
                //criteria.Type = $('#txtType').val();
                //criteria.Company = $('#txtCompanyName').val();
                //criteria.JobTitle = $('#txtJobTitle').val();
                //criteria.ClientContact = $('#txtClientContact').val();
                //criteria.Candidate = $('#txtCandidate').val();
                //criteria.Status = $('#txtStatus').val();
                //criteria.User = $('#txtUser').val();

                var sWhereSQL = ' ';

                if (criteria.Reference)
                    sWhereSQL += " And Placement_Reference like '" + criteria.Reference + "%'";

                if (criteria.Type)
                    sWhereSQL += " And Placement_PlacementEntityType like '" + criteria.Type + "%'";

                if (criteria.Company)
                    sWhereSQL += " And PlacementCompany_Name like '" + criteria.Company + "%'";

                if (criteria.JobTitle)
                    sWhereSQL += " And Placement_JobTitle like '" + criteria.JobTitle + "%'";

                if (criteria.ClientContact)
                    sWhereSQL += " And PlacementContact_FullName like '" + criteria.ClientContact + "%'";

                if (criteria.Candidate)
                    sWhereSQL += " And CandidateContact_FullName like '" + criteria.Candidate + "%'";

                if (criteria.Status)
                    sWhereSQL += " And Placement_PlacementStatus like '" + criteria.Status + "%'";

                if (criteria.User)
                    sWhereSQL += " And Placement_UserName like '" + criteria.User + "%'";

                TriSysSDK.Grid.VirtualMode({
                    Div: "divPlacementsGrid",
                    ID: "grdPlacements",
                    Title: "Placements",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.PlacementsForm.SQL,
                    OrderBy: TriSysApex.Pages.PlacementsForm.OrderBy,
                    Columns: TriSysApex.Pages.PlacementsForm.Columns,
                    MobileVisibleColumns: TriSysApex.Pages.PlacementsForm.MobileVisibleColumns,

                    KeyColumnName: "Placement_PlacementId",
                    //DrillDownFunction: "TriSysApex.Pages.PlacementForm.placementRecordDrillDown"
                    DrillDownFunction: function (rowData)
                    {
                        // TODO: Load Company Form with specified ID
                        //TriSysApex.UI.ShowMessage("You drilled down into row: " + rowData.Placement_Reference + ', ' + rowData.Placement_JobTitle + ', ' + rowData.CandidateContact_FullName + '?');
                        TriSysApex.FormsManager.OpenForm("Placement", rowData.Placement_PlacementId);
                    },
                    MultiRowSelect: true
                });
            }
        },

        PlacementForm:      // TriSysApex.Pages.PlacementForm
        {
            LoadRecord: function (lPlacementId)
            {
                // Put in a very small delay purely to show popup message during read
                setTimeout(function ()
                {
                    TriSysSDK.CShowForm.readEntityFormDataFromServer('Placement', lPlacementId,
                                                TriSysApex.Pages.PlacementForm.AfterFormDataLoaded);

                }, TriSysApex.Constants.FormLoadDataDelay);
            },

            // When this is called, we have populated 90% of the standard fields on the form.
            // All we have to do is to deal with the caveats/exceptions such as special fields
            // which have their own complex implementation.
            //
            AfterFormDataLoaded: function (lPlacementId, fieldDescriptions)
            {
                var fieldDescriptionsInDOM = TriSysSDK.CShowForm.filterOnlyFieldDescriptionsInDOM(fieldDescriptions);

                for (var i = 0; i < fieldDescriptionsInDOM.length; i++)
                {
                    var fieldDescription = fieldDescriptionsInDOM[i];
                    var sDivID = fieldDescription.TableName + "_" + fieldDescription.TableFieldName;
                    var objValue = fieldDescription.Value;

                    switch (sDivID)
                    {
                        case "a Placement field":

                            break;
                    }
                }
            }
        },   // End of TriSysApex.Pages.PlacementForm

        TaskSearchForm: {
            SearchCriteria: {
                TaskType: '',
                Scheduled: false,
                Contact: '',
                User: '',
                StartDate: '',
                EndDate: ''
            },
            isEmptySearchCriteria: function ()
            {
                var criteria = TriSysApex.Pages.TaskSearchForm.SearchCriteria;
                return (criteria.TaskType == '' && !criteria.Scheduled && criteria.Contact == '' && criteria.User == '' && criteria.StartDate == '' && criteria.EndDate == '');
            },
            SQL: 'Select Top #TopN# Task.TaskId, TaskType,  Task.StartDate,  Task.EndDate,  Task.Scheduled, ' +
                '  Task.DisplayDescription, LoginName, TaskContacts, TaskUsers ' +
                ' from v_Task_AllFields v, Task' +
                ' Where v.TaskId = Task.TaskId ',
            OrderBy: ' Order by Task.StartDate desc',
            Columns: [{
                Caption: '',
                Checkable: true
            }, {
                Caption: 'TaskId',
                Visible: false
            }, {
                Caption: 'Task Type'
            }, {
                Caption: 'Start Date'
            }, {
                Caption: 'End Date'
            }, {
                Caption: 'Scheduled'
            }, {
                Caption: 'Description'
            }, {
                Caption: 'Owner'
            }, {
                Caption: 'Contacts',
                Visible: false
            }, {
                Caption: 'Users',
                Visible: false
            }],

            Lookup: function ()
            {
                var criteria = TriSysApex.Pages.TaskSearchForm.SearchCriteria;
                criteria.TaskType = $('#txtTaskType').val();
                criteria.Scheduled = $('#chkScheduled').val();
                criteria.Contact = $('#txtContact').val();
                criteria.User = $('#txtUser').val();
                criteria.StartDate = $('#txtStartDate').val();
                criteria.EndDate = $('#txtEndDate').val();

                var sWhereSQL = ' ';

                if (criteria.TaskType)
                    sWhereSQL += " And TaskType like '" + criteria.TaskType + "%'";

                if (criteria.Scheduled)
                    sWhereSQL += " And Task.Scheduled = " + (criteria.Scheduled ? 1 : 0);

                if (criteria.Contact)
                    sWhereSQL += " And TaskContacts like '%" + criteria.Contact + "%'";

                if (criteria.User)
                    sWhereSQL += " And TaskUsers like '%" + criteria.User + "%'";

                if (criteria.StartDate)
                    sWhereSQL += " And Task.StartDate >= '" + criteria.StartDate + "'";

                if (criteria.EndDate)
                    sWhereSQL += " And Task.StartDate <= '" + criteria.EndDate + "'";

                TriSysSDK.Grid.Initialise({
                    Div: "divTaskGrid",
                    ID: "grdTasks",
                    Title: "Tasks",
                    RecordsPerPage: TriSysApex.UserOptions.TaskSearchForm,
                    SQL: TriSysApex.Pages.TaskSearchForm.SQL +
                        sWhereSQL +
                        TriSysApex.Pages.TaskSearchForm.OrderBy,
                    Columns: TriSysApex.Pages.TaskSearchForm.Columns,

                    KeyColumnName: "TaskId",
                    DrillDownFunction: "TriSysApex.Pages.TaskSearchForm.taskRecordDrillDown"
                });
            },

            taskRecordDrillDown: function (lTaskId)
            {
                // Debug
                TriSysApex.UI.alert('TriSysApex.Pages.TaskSearchForm.taskRecordDrillDown(' + lTaskId + ')');
            }
        },

        ContactFormOldMetronic: {
            ContactId: 0,

            contactRecordDrillDown: function (lContactId)
            {
                // Debug
                //TriSysApex.UI.alert('TriSysApex.Pages.ContactForm.contactRecordDrillDown(' + lContactId + ')');

                // Assign this contact to an in-memory variable
                TriSysApex.Pages.ContactForm.ContactId = lContactId;

                // Clear memory of any grids being populated
                TriSysApex.Pages.ContactForm.populatedGrids.length = 0;

                // Open the form 
                TriSysApex.Forms.loadPageProgrammatically('Contact.aspx', 'menuContactManagement_Contacts');
            },

            // Called after data for page has been read from the server and
            // all fields are populated.
            // This function can carry out any custom logic per form post load.
            //
            afterFormDataLoaded: function (lRecordId, fieldDescriptions)
            {
                return;
                // Hard coded stuff
                //$('#lblContactName').html(CContact.Forenames + " " + CContact.Surname);

                //setValueInField('dtDateOfBirth', '29 April 1963');
                //$('#dtDateOfBirth').html('29 April 1963');

                //$('#dtDateOfBirth').editable({
                //    rtl: App.isRTL()
                //});

                // TODO: This is the SDK code which is to be relocated into TriSysSDK.js
                // once we have the framework operating as expected.

                // Stop contact comments popup squashing on top
                $('#Contact_Comments').editable({
                    placement: 'left',
                    showbuttons: 'bottom'
                });

                //$('#Contact_Surname').editable();

                var iSex = 1;
                var sSex = "Female";
                document.getElementById('cmbSex').setAttribute("data-value", iSex);
                $('#cmbSex').html(sSex);

                $('#cmbSex').editable({
                    prepend: "not selected",
                    inputclass: 'form-control',
                    source: [{
                        value: 0,
                        text: 'Male'
                    }, {
                        value: 1,
                        text: 'Female'
                    }],
                    display: function (value, sourceData)
                    {
                        var colors = {
                            "": "gray",
                            0: "green",
                            1: "blue"
                        },
                            elem = $.grep(sourceData, function (o)
                            {
                                return o.value == value;
                            });

                        if (elem.length)
                        {
                            $(this).text(elem[0].text).css("color", colors[value]);
                        } else
                        {
                            $(this).empty();
                        }
                    }
                });


                $('#cmbContactType').html("Client");
                document.getElementById('cmbContactType').setAttribute("data-value", 1);

                $('#cmbContactType').editable({
                    showbuttons: false,
                    inputclass: 'form-control',
                    source: [{
                        value: 0,
                        text: 'Candidate'
                    }, {
                        value: 1,
                        text: 'Client'
                    }, {
                        value: 2,
                        text: 'ClientCandidate'
                    }, {
                        value: 3,
                        text: 'Referee'
                    }, {
                        value: 4,
                        text: 'User'
                    }],
                    display: function (value, sourceData)
                    {
                        var colors = {
                            0: "red",
                            1: "black",
                            2: "blue",
                            3: "orange",
                            4: "pink"
                        },
                            elem = $.grep(sourceData, function (o)
                            {
                                return o.value == value;
                            });

                        if (elem.length)
                        {
                            $(this).text(elem[0].text).css("color", colors[value]);
                        } else
                        {
                            $(this).empty();
                        }
                    }
                });

                $('#chkTypeOfWorkRequired').editable({
                    pk: 1,
                    limit: 3,
                    source: [{
                        value: 1,
                        text: 'Permanent'
                    }, {
                        value: 2,
                        text: 'Contract'
                    }, {
                        value: 3,
                        text: 'Temporary'
                    }]
                });

                $('#chkTypeOfWorkRequired').on('shown', function (e, reason)
                {
                    App.initUniform();
                });


                // Turn editing off on form load
                TriSysApex.Forms.InitialiseFormEditing();
            },

            populatedGrids: [{
                GridName: "test"
            }],
            isGridPopulated: function (sGridName)
            {
                var populatedGrids = TriSysApex.Pages.ContactForm.populatedGrids;
                var gridItem = null;
                for (var i = 0; i < populatedGrids.length; i++)
                {
                    gridItem = populatedGrids[i];
                    if (gridItem.GridName == sGridName)
                    {
                        // Already populated
                        return true;
                    }
                }

                // Populate the grid so record in the array
                gridItem = {
                    GridName: sGridName
                };
                populatedGrids.push(gridItem);

                return false;
            },

            RequirementsGrid: function (sDivTag)
            {
                var sGridName = 'grdRequirements';
                if (TriSysApex.Pages.ContactForm.isGridPopulated(sGridName))
                    return;

                var sWhereSQL = " And RequirementContact_ContactId = " + TriSysApex.Pages.ContactForm.ContactId;

                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Requirements",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.RequirementsForm.SQL +
                        sWhereSQL +
                        TriSysApex.Pages.RequirementsForm.OrderBy,
                    Columns: TriSysApex.Pages.RequirementsForm.Columns,

                    KeyColumnName: "Requirement_RequirementId",
                    DrillDownFunction: "TriSysApex.Pages.RequirementForm.requirementRecordDrillDown"
                });
            },

            PlacementsGrid: function (sDivTag)
            {
                var sGridName = 'grdPlacements';
                if (TriSysApex.Pages.ContactForm.isGridPopulated(sGridName))
                    return;

                var sWhereSQL = " And Placement_PlacementId in " +
                    "(Select PlacementId From PlacementContact Where ContactId = " + TriSysApex.Pages.ContactForm.ContactId +
                    "    UNION" +
                    " Select EntityId From PlacementConfigFields Where Candidate = " + TriSysApex.Pages.ContactForm.ContactId +
                    ")";

                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Placements",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.PlacementsForm.SQL +
                        sWhereSQL +
                        TriSysApex.Pages.PlacementsForm.OrderBy,
                    Columns: TriSysApex.Pages.PlacementsForm.Columns,

                    KeyColumnName: "Placement_PlacementId",
                    DrillDownFunction: "TriSysApex.Pages.PlacementForm.placementRecordDrillDown"
                });
            },

            TaskGrid: function (sDivTag, bScheduled)
            {
                var sGridName = sDivTag.replace("div", "grd");
                if (TriSysApex.Pages.ContactForm.isGridPopulated(sGridName))
                    return;

                var sWhereSQL = " And Task.TaskId in " +
                    "(Select TaskId From TaskContacts Where ContactId = " + TriSysApex.Pages.ContactForm.ContactId + ")";

                if (bScheduled)
                    sWhereSQL += " And ( Task.Scheduled = 1 AND Task.Clear =  0	)";
                else
                    sWhereSQL += " And ( Task.Scheduled = 0 OR Task.Clear =  1 )";


                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Tasks",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: TriSysApex.Pages.TaskSearchForm.SQL +
                        sWhereSQL +
                        TriSysApex.Pages.TaskSearchForm.OrderBy,
                    Columns: TriSysApex.Pages.TaskSearchForm.Columns,

                    KeyColumnName: "TaskId",
                    DrillDownFunction: "TriSysApex.Pages.TaskSearchForm.taskRecordDrillDown"
                });
            },

            // Show all field descriptions and their values
            PopulatedFieldsGrid: function (sDivTag)
            {
                var sGridName = sDivTag.replace("div", "grd");
                if (TriSysApex.Pages.ContactForm.isGridPopulated(sGridName))
                    return;

                // Get all field description values
                var fieldDescriptions = TriSysApex.Cache.populateFormFieldDescriptions;

                // Eliminate the null ones
                var nonEmptyFieldDescriptions = [];
                for (var i = 0; i < fieldDescriptions.length; i++)
                {
                    var fd = fieldDescriptions[i];
                    if (fd.Value || fd.DisplayValue)
                        nonEmptyFieldDescriptions.push(fd);
                }

                // Show in the grid
                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Populated Field Descriptions",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: nonEmptyFieldDescriptions,
                    Columns: [{
                        Caption: 'FieldId',
                        ColumnName: "FieldId"
                    }, {
                        Caption: 'Table Name',
                        ColumnName: "TableName"
                    }, {
                        Caption: 'Table Field Name',
                        ColumnName: "TableFieldName"
                    }, {
                        Caption: 'Field Type',
                        ColumnName: "FieldTypeId"
                    }, {
                        Caption: 'Value',
                        ColumnName: "Value"
                    }, {
                        Caption: 'Display Value',
                        ColumnName: "DisplayValue"
                    }],
                    AddDrillDownButtonColumn: false
                });
            }
        },

        FileManagerForm: {
            startFolderPopulation: function (sTreeViewDiv)
            {
                var bRecursive = true;

                var dataPacket = {
                    'RootFolder': null,
                    'Recursion': bRecursive
                };

                var payloadObject = {};

                payloadObject.URL = 'Folders/SubFolders';

                payloadObject.OutboundDataPacket = dataPacket;

                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.hideLoadingImage();

                    var JSONdataset = data;

                    TriSysApex.Pages.FileManagerForm.populateFoldersAfterWebServiceCall(sTreeViewDiv, JSONdataset);

                    return true;
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.hideLoadingImage();
                    TriSysApex.UI.ErrorHandlerRedirector('FileManagerForm.startFolderPopulation: ', request, status, error);
                };

                TriSysApex.UI.waitingPopup("Traversing file system...");

                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            populateFoldersAfterWebServiceCall: function (sTreeViewDiv, folderHierarchy)
            {
                // Convert our hierarchy into KendoUI
                var treeData = TriSysApex.Pages.FileManagerForm.recurseTreeSubFolders(folderHierarchy);

                // Populate KendoUI tree view
                $("#" + sTreeViewDiv).kendoTreeView({
                    dataSource: treeData,
                    select: TriSysApex.Pages.FileManagerForm.onFolderSelect
                });

            },

            recurseTreeSubFolders: function (subFolders)
            {
                if (subFolders)
                {
                    var treeData = [];

                    for (var i = 0; i < subFolders.Folders.length; i++)
                    {
                        var folder = subFolders.Folders[i];
                        var folderItems = TriSysApex.Pages.FileManagerForm.recurseTreeSubFolders(folder.SubFolders);
                        var item = {
                            text: folder.Name,
                            expanded: false,
                            spriteCssClass: "folder",
                            FullPath: folder.FullPath,
                            items: folderItems
                        };
                        treeData.push(item);
                    }

                    return treeData;
                }
            },

            onFolderSelect: function (e)
            {
                var dataItem = this.dataItem(e.node);
                var sFullPath = dataItem.FullPath;

                // Display the full path and files in the selected folder
                var sCaption = "Files: " + sFullPath;
                $('#divFileManagerFilesCaption').text(sCaption);

                // Start the asynchronous retrieval of the list of files from the server
                TriSysApex.Pages.FileManagerForm.startFolderFilesPopulation(sFullPath);
            },

            startFolderFilesPopulation: function (sFullPath)
            {
                var dataPacket = {
                    'Folder': sFullPath
                };

                var payloadObject = {};

                payloadObject.URL = 'Files/Files';

                payloadObject.OutboundDataPacket = dataPacket;

                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.hideLoadingImage();

                    var JSONdataset = data;

                    TriSysApex.Pages.FileManagerForm.PopulateFilesGrid('divFilesGrid', JSONdataset.Files);

                    return true;
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.hideLoadingImage();
                    TriSysApex.UI.ErrorHandlerRedirector('FileManagerForm.startFolderFilesPopulation: ', request, status, error);
                };

                TriSysApex.UI.waitingPopup("Listing files...");

                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            PopulateFilesGrid: function (sDivTag, Files)
            {
                var sGridName = "grdFiles";
                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Files",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: Files,
                    Columns: [{
                        Caption: '',
                        Checkable: true
                    }, {
                        Caption: 'File Name',
                        ColumnName: "Name"
                    }, {
                        Caption: 'Size',
                        ColumnName: "Size"
                    }, {
                        Caption: 'Date Modified',
                        ColumnName: "DateModified"
                    }],

                    KeyColumnName: "FullPath",
                    DrillDownFunction: "TriSysApex.Pages.FileManagerForm.FileOpenDrillDown"
                });
            },

            FileOpenDrillDown: function (sFullPath)
            {
                // Show it
                //TriSysApex.UI.alert(sFullPath);

                // Either download and view or get a URL and open it in
                // a cloud document viewer
                var payloadObject = {};

                payloadObject.URL = 'Files/GetFileURL';

                payloadObject.OutboundDataPacket = {
                    FilePath: sFullPath
                };

                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.hideLoadingImage();
                    if (data.Success)
                    {
                        var sURL = data.URL;
                        var sName = data.FileName;
                        TriSysApex.Pages.FileManagerForm.OpenDocumentViewer(sURL, sName);
                    }

                    return true;
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.hideLoadingImage();
                    TriSysApex.UI.ErrorHandlerRedirector('FileManagerForm.FileOpenDrillDown: ', request, status, error);
                };

                TriSysApex.UI.waitingPopup('Opening ' + sFullPath + '...');

                TriSysAPI.Data.PostToWebAPI(payloadObject);
            },

            OpenDocumentViewer: function (sURL, sName)
            {
                var sHowToOpen = 'Google';
                var sSuffix = TriSysApex.Pages.FileManagerForm.FileSuffix(sName);
                switch (sSuffix.toLowerCase())
                {
                    case 'doc':
                    case 'docx':
                    case 'xls':
                    case 'xlsx':
                        sHowToOpen = 'Microsoft';
                        break;
                }

                var strWindowFeatures = "menubar=no,location=yes,resizable=yes,scrollbars=yes,status=no";

                if (sHowToOpen == 'Google')
                {
                    sURL = 'http://docs.google.com/viewer?url=' + encodeURIComponent(sURL);
                } else if (sHowToOpen == 'Microsoft')
                {
                    sURL = 'http://view.officeapps.live.com/op/view.aspx?src=' + encodeURIComponent(sURL);
                }

                var windowObjectReference = window.open(sURL, sName, strWindowFeatures);
            },

            FileSuffix: function (sFile)
            {
                if (!sFile)
                    return;

                var n = sFile.split(".");
                if (n)
                {
                    if (n.length > 0)
                    {
                        var sSuffix = n[n.length - 1];
                        return sSuffix;
                    }
                }
            }
        },

        AdminConsoleForm: {
            populatedGrids: [{
                GridName: "test"
            }],
            isGridPopulated: function (sGridName)
            {
                var populatedGrids = TriSysApex.Pages.AdminConsoleForm.populatedGrids;
                var gridItem = null;
                for (var i = 0; i < populatedGrids.length; i++)
                {
                    gridItem = populatedGrids[i];
                    if (gridItem.GridName == sGridName)
                    {
                        // Already populated
                        return true;
                    }
                }

                // Populate the grid so record in the array
                gridItem = {
                    GridName: sGridName
                };
                populatedGrids.push(gridItem);

                return false;
            },

            // Populate the specified grid div tag from the specified cached users
            PopulateUsersGrid: function (sDivTag, Users)
            {
                var sGridName = "grdUsers";
                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Users",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: Users,
                    Columns: [{
                        Caption: '',
                        Checkable: true
                    }, {
                        Caption: 'UserId',
                        ColumnName: "UserId"
                    }, {
                        Caption: 'Full Name',
                        ColumnName: "FullName"
                    }, {
                        Caption: 'Login Name',
                        ColumnName: "LoginName"
                    }],

                    KeyColumnName: "UserId",
                    DrillDownFunction: "TriSysApex.Pages.UserForm.userRecordDrillDown"
                });
            },

            PopulateSkillsGrid: function (sDivTag, Skills)
            {
                var sGridName = "grdSkills";

                if (TriSysApex.Pages.AdminConsoleForm.isGridPopulated(sGridName))
                    return;

                var Skills = TriSysApex.Cache.Skills();

                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Attributes/Lookups",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: Skills,
                    Columns: [{
                        Caption: '',
                        Checkable: true
                    }, {
                        Caption: 'SkillId',
                        ColumnName: "SkillId"
                    }, {
                        Caption: 'Attribute/Skill Name',
                        ColumnName: "Skill"
                    }, {
                        Caption: 'Description',
                        ColumnName: "Description"
                    }, {
                        Caption: 'Category',
                        ColumnName: "Category"
                    }],

                    KeyColumnName: "SkillId",
                    DrillDownFunction: "TriSysApex.Pages.SkillForm.skillRecordDrillDown"
                });
            },

            PopulateSystemSettingsGrid: function (sDivTag, SystemSettings)
            {
                var sGridName = "grdSystemSettings";

                if (TriSysApex.Pages.AdminConsoleForm.isGridPopulated(sGridName))
                    return;

                var SystemSettings = TriSysApex.Cache.SystemSettings();

                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "System Settings",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: SystemSettings,
                    Columns: [{
                        Caption: '',
                        Checkable: true
                    }, {
                        Caption: 'SystemSettingId',
                        ColumnName: "SystemSettingId",
                        Visible: false
                    }, {
                        Caption: 'Name',
                        ColumnName: "Name"
                    }, {
                        Caption: 'Value',
                        ColumnName: "Value"
                    }, {
                        Caption: 'Description',
                        ColumnName: "Description"
                    }],

                    KeyColumnName: "SystemSettingsId",
                    DrillDownFunction: "TriSysApex.Pages.SystemSettingsForm.systemSettingRecordDrillDown"
                });
            },

            PopulateFieldDescriptionsGrid: function (sDivTag, FieldDescriptions)
            {
                var sGridName = "grdFieldDescriptions";

                if (TriSysApex.Pages.AdminConsoleForm.isGridPopulated(sGridName))
                    return;

                var FieldDescriptions = TriSysApex.Cache.FieldDescriptions();

                TriSysSDK.Grid.Initialise({
                    Div: sDivTag,
                    ID: sGridName,
                    Title: "Field Descriptions",
                    RecordsPerPage: TriSysApex.UserOptions.RecordsPerPage,
                    SQL: null,
                    PopulationByObject: FieldDescriptions,
                    Columns: [{
                        Caption: '',
                        Checkable: true
                    }, {
                        Caption: 'FieldId',
                        ColumnName: "FieldId",
                        Visible: false
                    }, {
                        Caption: 'Table Name',
                        ColumnName: "TableName"
                    }, {
                        Caption: 'Table Field Name',
                        ColumnName: "TableFieldName"
                    }, {
                        Caption: 'Field Type',
                        ColumnName: "FieldTypeId"
                    }, {
                        Caption: 'Lookup',
                        ColumnName: "Lookup"
                    }],

                    KeyColumnName: "FieldId",
                    DrillDownFunction: "TriSysApex.Pages.FieldDescriptionsForm.fieldDescriptionRecordDrillDown"
                });
            },

            ExecuteSQL: function (sSQLDivTag, sGridDivTag)
            {
                var sSQLStatement = $('#' + sSQLDivTag).val();
                if (!sSQLStatement)
                {
                    TriSysApex.UI.alert('Please enter a SQL statement.');
                    return;
                }

                // Connect to Web Service to obtain data

                var dataPacket = {
                    'SQL': sSQLStatement,
                    'PageNumber': 1,
                    'RecordsPerPage': TriSysApex.UserOptions.SQLTopNRecords
                };

                var payloadObject = {};

                payloadObject.URL = 'SQLDatabase/SQL_Select';

                payloadObject.OutboundDataPacket = dataPacket;

                payloadObject.InboundDataFunction = function (data)
                {
                    TriSysApex.UI.hideLoadingImage();

                    var JSONdataset = data;
                    var sDisplay = null;

                    if (JSONdataset)
                    {
                        sDisplay = "TotalRecordCount:" + JSONdataset.TotalRecordCount +
                            ", TotalPageCount:" + JSONdataset.TotalPageCount;

                        // Just show raw object
                        sDisplay = JSON.stringify(JSONdataset);
                    } else
                    {
                        sDisplay = "No records found.";
                    }

                    // Show table
                    TriSysSDK.Grid.DynamicDataSet(sGridDivTag, sSQLStatement, 'DataSet', JSONdataset, JSONdataset.Columns);

                    return true;
                };

                payloadObject.ErrorHandlerFunction = function (request, status, error)
                {
                    TriSysApex.UI.hideLoadingImage();
                    TriSysApex.UI.ErrorHandlerRedirector('AdminConsoleForm.ExcuteSQL: ', request, status, error);
                };

                TriSysApex.UI.waitingPopup("Reading dataset...");

                TriSysAPI.Data.PostToWebAPI(payloadObject);
            }

        } // End of Admin Console Namespace

    }, // End of Pages Namespace
    //#endregion TriSysApex.Pages

    //#region TriSysApex.FormsManager

    // Manage all forms, including captions and tabs in the framework
    FormsManager:   // TriSysApex.FormsManager
    {
        // Code written by GL in May 2014. Algorithm ported from V10 Forms Manager.
        OpenForm: function (sFormName, lRecordId)
        {
            // 1.a If form already open, then focus on this and hide all others. Set caption and tab.
            // 1.b Else, open the form, instantiate it. Set caption and tab.
            // 2.  Load data associated with the specified record id if it exists

            // Get the configured page parameters first of all, as a failure here is a programmer/configuration error
            var formTabs = TriSysApex.FormsManager.FormTabs;
            var pageConfig = TriSysApex.Forms.Configuration.GetPage(sFormName);
            if (!pageConfig)
            {
                // Programmer error
                alert('Unable to find form: ' + sFormName + ' in the application configuration.');
                return;
            }

            // Does form instance exist?
            var formInstance = TriSysApex.FormsManager.FormInstances.GetForm(sFormName);
            var bDoesFormExist = (formInstance ? true : false);
            if (bDoesFormExist)
            {
                // Focus on this form now

                // Manually do the form
                TriSysApex.FormsManager.loadPageIntoContentDiv(formInstance.PageConfiguration.PageName, formInstance.PageConfiguration.FormName);

                // Now do the tab
                TriSysApex.Logging.LogMessage('Form "' + sFormName + '" is already open in the framework, so focus on it and its tab.');
            }
            else
            {
                // Create a new form instance and a corresponding tab

                // Load this form by creating its own div wrapper and adding it to the list of div's in the container.
                // This way forms can be re-opened simply by clicking on the tab form which makes it visible
                if (pageConfig.MustBeLoggedIn)
                {
                    // Page only allowed to be loaded if the user has logged in
                    if (!TriSysApex.LoginCredentials.areUserCredentialsValid())
                    {
                        // User is not logged in so redirect to login page
                        TriSysApex.LoginCredentials.Logoff(false);
                        return;
                    }
                }

                // Add the form to the collection of live instances
                TriSysApex.FormsManager.FormInstances.AddForm(pageConfig, lRecordId);

                // Load the page now
                TriSysApex.FormsManager.loadPageIntoContentDiv(pageConfig.PageName, pageConfig.FormName);

                // Tabs
                formTabs.AddTab(pageConfig.ImagePath, pageConfig.Caption, pageConfig.FormName);

                // For the first tab added, initialise it
                formTabs.InitialiseTabs();
            }

            // Set the caption also
            TriSysApex.FormsManager.Captions.SetCaption(pageConfig.Caption, pageConfig.ImagePath);

            // Select this form tab
            formTabs.SelectTab(pageConfig.FormName);

            // Show the correct ribbon tab
            TriSysApex.FormsManager.ShowContextualRibbonTab(pageConfig.ContextualRibbonTab);

            // Must resize the page after making ribbon visibility changes!
            TriSysApex.Pages.Index.onWindowResize();

            // Load data associated with the form
            if (lRecordId)
            {
                // This will read the data from the Web API and populate the form fields
                TriSysApex.FormsManager.OpenRecord(pageConfig, lRecordId);
            }
        },

        ShowContextualRibbonTab: function (iContextualRibbonTabIndex)
        {
            var iStartContextualRibbonTabIndex = 6;
            var iFinalContextualRibbonTabIndex = 9;

            // Hide all contextual ribbon tabs
            for (var i = iStartContextualRibbonTabIndex; i <= iFinalContextualRibbonTabIndex; i++)
                TriSysApex.Ribbon.HideRibbonTab(i);

            if (!iContextualRibbonTabIndex)
                iContextualRibbonTabIndex = 0;        // Focus on "Home" if no contextual ribbon tab for this form

            TriSysApex.Ribbon.ShowRibbonTab(iContextualRibbonTabIndex);
        },

        // Called by TriSysApex.FormsManager.OpenForm()
        OpenRecord: function (pageConfig, lRecordId)
        {
            // Use the dynamically specified namespace path to the drill down function
            if (pageConfig.LoadRecord)
            {
                // Find the function associated with this drill down
                var fn = TriSysApex.DynamicClasses.GetObject(pageConfig.LoadRecord);

                if (fn)
                {
                    // Ensure that it is a valid function, and execute it
                    if (typeof fn === "function")
                    {
                        // Make the call now even though the dynamically loaded page may not yet be fully loaded.
                        fn(lRecordId);
                        // The called function will probably hit the web service asynchronously giving the above page time to load

                        // Record the ID of this record against the page config
                        pageConfig.RecordId = lRecordId;
                    }
                }
            }
        },

        // Called to close the specified form
        CloseForm: function (sPageName, bTabAlreadyClosed)
        {
            // Delete from our list of live instances
            TriSysApex.FormsManager.FormInstances.DeleteForm(sPageName);

            // Delete from the collection of div containers
            var sFormDivID = TriSysApex.FormsManager.ContentDivPrefix + sPageName;
            var existingDiv = document.getElementById(sFormDivID);
            if (existingDiv)
            {
                // Remove the div, thus removing the contents i.e. the form HTML
                existingDiv.parentNode.removeChild(existingDiv);

                // Mark the form as removed
                var pageConfig = TriSysApex.Forms.Configuration.GetPage(sPageName);
                pageConfig.RecordId = 0;

                if (!bTabAlreadyClosed)
                {
                    // Remove the tab also
                    TriSysApex.FormsManager.FormTabs.CloseTab(sPageName);
                }
            }
        },

        CloseCurrentForm: function ()
        {
            var sFormName = TriSysApex.FormsManager.FormTabs.SelectedFormName;
            if (sFormName != "")
            {
                var formInstance = TriSysApex.FormsManager.FormInstances.GetForm(sFormName);
                if (formInstance)
                {
                    if (TriSysApex.FormsManager.FormInstances.CanFormInstanceBeClosed(formInstance))
                    {
                        TriSysApex.FormsManager.CloseForm(sFormName, false);
                        TriSysApex.FormsManager.OpenAppropriateOpenFormInstanceFollowingFormClose();
                    }
                }
            }
        },

        // Called from ribbon to close all open forms
        CloseAllForms: function (bForceClose, bCloseLoginForm)
        {
            for (var i = TriSysApex.FormsManager.FormInstances.FormArray.length - 1; i >= 0; i--)
            {
                var formInstance = TriSysApex.FormsManager.FormInstances.FormArray[i];
                if (formInstance.PageConfiguration.CloseWhenCloseAll || bForceClose)
                {
                    if (TriSysApex.FormsManager.FormInstances.CanFormInstanceBeClosed(formInstance) || bCloseLoginForm)
                    {
                        // Close form+tab
                        var bTabAlreadyClosed = false;
                        TriSysApex.FormsManager.CloseForm(formInstance.PageConfiguration.FormName, bTabAlreadyClosed);
                    }
                }
            }

            if (!bCloseLoginForm)
                TriSysApex.FormsManager.OpenAppropriateOpenFormInstanceFollowingFormClose();
        },

        OpenAppropriateOpenFormInstanceFollowingFormClose: function ()
        {
            // Prepare if no forms to be retained
            TriSysApex.FormsManager.Captions.SetCaption(null, null);

            // Open remaining tab
            var firstFormInstance = TriSysApex.FormsManager.FormInstances.GetFirstInstance();
            if (firstFormInstance)
                TriSysApex.FormsManager.OpenForm(firstFormInstance.PageConfiguration.FormName);
        },

        // Load the page into the content div.
        // Allow multiple live DIV's so that data is preserved between tab clicks.
        // divFormContentRow            - a single current form visible like its selected tab
        // divHiddenFormContentDivArray - a collection of hidden forms for all non-selected tabs
        sPageLoading: null,
        ContentDivPrefix: 'div-apex-content-',

        loadPageIntoContentDiv: function (sPageFileName, sPageName)
        {
            // Move visible DIV into invisible DOM tree
            var visibleContentRow = document.getElementById('divFormContentRow');
            var hiddenContent = document.getElementById('divHiddenFormContentDivArray');

            // Enumerate through all div's in container div and make invisible
            var children = document.getElementById('divFormContentRow').childNodes;
            if (children)
            {
                for (var i = 0; i < children.length; i++)
                {
                    var containerDiv = children[i];
                    hiddenContent.appendChild(containerDiv);
                }
            }

            // Identify form/page div and if it exists, make it visible
            var sFormDivID = TriSysApex.FormsManager.ContentDivPrefix + sPageName;
            var existingDiv = document.getElementById(sFormDivID);
            if (existingDiv)
            {
                // Make this visible
                visibleContentRow.appendChild(existingDiv);
            }
            else
            {
                // Add the div to the parent and make visible
                var newDiv = document.createElement('div');
                newDiv.setAttribute('id', sFormDivID);
                newDiv.innerHTML = "Opening form...";
                visibleContentRow.appendChild(newDiv);

                $.ajaxSetup({
                    // Disable caching of AJAX responses
                    cache: false
                });

                // Load the page into this div
                sPageLoading = sPageFileName;
                var sPagePath = 'Forms/' + sPageFileName;
                TriSysApex.FormsManager.loadPageIntoDiv(sFormDivID, sPagePath);
            }
        },

        loadPageIntoDiv: function (sDivTag, sPagePath, fnCallback)       // TriSysApex.FormsManager.loadPageIntoDiv()
        {
            if (!fnCallback)
                fnCallback = TriSysApex.FormsManager.pageLoaded;

            $("#" + sDivTag).load(sPagePath + TriSysApex.FormsManager.pageURLTimestampArgumentsToForceNoCache(), fnCallback);
        },

        pageURLTimestampArgumentsToForceNoCache: function (sURL)
        {
            var sArgSeparator = '?';
            if (sURL)
            {
                if (sURL.indexOf('?') > 0)
                    sArgSeparator = '&';
            }

            var dt = new Date();
            var sArgsToForceLoad = sArgSeparator + 'datetime=' + dt.getFullYear() + dt.getMonth() + dt.getDay() +
                    dt.getHours() + dt.getMinutes() + dt.getSeconds() + dt.getMilliseconds();
            return sArgsToForceLoad;
        },

        pageLoaded: function (response, status, xhr)
        {
            if (status == "error")
            {
                var msg = "Error loading " + sPageLoading + "<br />";
                TriSysApex.UI.errorAlert(msg + xhr.status + " " + xhr.statusText);
            }
        },

        FormInstances:  // TriSysApex.FormsManager.FormInstances
        {
            FormArray: [],

            Count: function ()
            {
                return TriSysApex.FormsManager.FormInstances.FormArray.length;
            },

            AddForm: function (pageConfig, lRecordId)
            {
                var formInstance = {
                    RecordId: lRecordId,
                    PageConfiguration: pageConfig       // See TriSysApex.Forms.Configuration.Pages
                };

                TriSysApex.FormsManager.FormInstances.FormArray.push(formInstance);
            },

            DeleteForm: function (sFormName)
            {
                for (var i = 0; i < TriSysApex.FormsManager.FormInstances.FormArray.length; i++)
                {
                    var formInstance = TriSysApex.FormsManager.FormInstances.FormArray[i];
                    if (formInstance.PageConfiguration.FormName == sFormName)
                    {
                        TriSysApex.FormsManager.FormInstances.FormArray.splice(i, 1);
                        return;
                    }
                }
            },

            GetForm: function (sFormName)
            {
                for (var i = 0; i < TriSysApex.FormsManager.FormInstances.FormArray.length; i++)
                {
                    var formInstance = TriSysApex.FormsManager.FormInstances.FormArray[i];
                    if (formInstance.PageConfiguration.FormName == sFormName)
                        return formInstance;
                }
            },

            GetFirstInstance: function ()
            {
                if (TriSysApex.FormsManager.FormInstances.FormArray.length > 0)
                    return TriSysApex.FormsManager.FormInstances.FormArray[0];
            },

            CanFormInstanceBeClosed: function (formInstance)
            {
                // Does form instance allow closing?
                if (!formInstance.PageConfiguration.CanClose)
                    return false;

                // Call the form instance callback script to see whether it has 'dirty' data
                // TODO

                return true;
            }
        },

        Captions:      // TriSysApex.FormsManager.Captions
        {
            SetCaption: function (sCaption, sImage)
            {
                if (!sImage)
                    sImage = "http://www.trisys.co.uk/images/trisys/16x16/TriSys-16x16.ico";

                $('#TriSysFormCaptionText').html(sCaption);
                $('#TriSysFormCaptionIcon').attr("src", sImage);
            }

        },  // End of TriSysApex.FormsManager.Captions

        FormTabs:      // TriSysApex.FormsManager.FormTabs
        {
            // Code written by Madan Gopal in May 2014
            SelectedFormName: "",
            mySlider: null,
            bInitialised: false,
            InitialiseTabs: function ()
            {
                var formTabs = TriSysApex.FormsManager.FormTabs;
                if (formTabs.bInitialised)
                {
                    formTabs.mySlider.reloadSlider();
                    //formTabs.callDynamicNextClick();
                    return;
                }

                formTabs.mySlider = $('.bxslider').bxSlider({
                    infiniteLoop: false,
                    hideControlOnEnd: true
                });
                $("#tabuser > li:First-Child").removeClass('tabuser').addClass('onselectTabClass');
                formTabs.SelectedFormName = $("#tabuser > li:First-Child").attr('id');

                formTabs.bInitialised = true;
            },

            SetFirstTabAsSelectedIfNotSet: function ()
            {
                var formTabs = TriSysApex.FormsManager.FormTabs;
                if (formTabs.SelectedFormName == "")
                    formTabs.SelectedFormName = $("#tabuser > li:First-Child").attr('id');
            },

            AddTab: function (sImagePath, sCaption, sFormName)
            {
                var htmltab = "<li id='" + sFormName + "'><div id='" + sFormName + "'><img src='" + sImagePath + "' class='icon'><span>" +
                            sCaption + "</span><a href='javascript:;' id='closetab" + sFormName +
                            "'><img alt='Close tab' src='images/close-icon.png' alt='Close tab'></a></div></li>";

                if ($("#" + sFormName).length == 0)
                {
                    $("#tabuser").append(htmltab);
                }

                $("#closetab" + sFormName).click(function ()
                {
                    var bUserTabClick = true;
                    TriSysApex.FormsManager.FormTabs.onTabClose(sCaption, sFormName, bUserTabClick);
                });
                $("#" + sFormName).click(function ()
                {
                    var bUserTabClick = true;
                    TriSysApex.FormsManager.FormTabs.onTabSelect(sCaption, sFormName, bUserTabClick);
                });
                htmltab = "";
            },

            onTabSelect: function (sTabCaption, sFormName, bUserTabClick)
            {
                $("#tabuser > li").removeClass('onselectTabClass').addClass('tabuser');
                $("#" + sFormName).removeClass('tabuser').addClass('onselectTabClass');

                if ($("#" + sFormName).next().attr('id') == null)
                {
                    $(".bx-next").addClass('disabled');
                } else
                {
                    $(".bx-next").removeClass('disabled');
                }
                if ($("#" + sFormName).prev().attr('id') == null)
                {
                    $(".bx-prev").addClass('disabled');
                } else
                {
                    $(".bx-prev").removeClass('disabled');
                }
                TriSysApex.FormsManager.FormTabs.SelectedFormName = sFormName;

                TriSysApex.Logging.LogMessage('onTabSelect(' + sTabCaption + ',' + sFormName);

                if (bUserTabClick)
                {
                    // Make this form visible as it must be already open in our framework
                    TriSysApex.FormsManager.OpenForm(sFormName, null);
                }
            },

            onTabClose: function (sTabCaption, sFormName, bUserTabClick)
            {
                if (bUserTabClick)
                {
                    // Can this form/tab be deleted?
                    var formInstance = TriSysApex.FormsManager.FormInstances.GetForm(sFormName);
                    if (!TriSysApex.FormsManager.FormInstances.CanFormInstanceBeClosed(formInstance))
                    {
                        TriSysApex.UI.ShowMessage("Form: " + sFormName + " cannot be closed.");
                        return;
                    }
                }

                var formTabs = TriSysApex.FormsManager.FormTabs;

                // Go ahead and remove the tab
                if ($("#" + sFormName).next().attr('id') != null)
                {
                    formTabs.SelectedFormName = $("#" + sFormName).next().attr('id');

                }
                else if ($("#" + sFormName).prev().attr('id') != null)
                {
                    formTabs.SelectedFormName = $("#" + sFormName).prev().attr('id');

                }
                else
                {
                    formTabs.SelectedFormName = "";
                }

                $("#" + sFormName).remove();

                TriSysApex.Logging.LogMessage('onTabClose(' + sTabCaption + ',' + sFormName);

                // Remove the DIV containing the form if it exists
                bTabAlreadyClosed = true;
                TriSysApex.FormsManager.CloseForm(sFormName, bTabAlreadyClosed);

                // Re-open the next/previous form if necessary
                if (formTabs.SelectedFormName != "")
                {
                    // Now open the form of the next/last tab now selected
                    TriSysApex.FormsManager.OpenForm(formTabs.SelectedFormName, 0);
                }
                else
                {
                    // There are no more forms to open, hence set the form caption to empty too
                    TriSysApex.FormsManager.Captions.SetCaption(null, null);
                }
            },

            SelectTab: function (sFormName)
            {
                var formTabs = TriSysApex.FormsManager.FormTabs;
                formTabs.onTabSelect('', sFormName);
                //formTabs.SetFirstTabAsSelectedIfNotSet();
            },

            CloseTab: function (sFormName)
            {
                $("#" + sFormName).remove();
                TriSysApex.Logging.LogMessage('CloseTab(' + sFormName + ')');
            },

            callDynamicNextClick: function ()
            {
                var formTabs = TriSysApex.FormsManager.FormTabs;
                var leftmargin;
                var window_width = $(window).width();
                var postion_onscreen = $("#" + formTabs.SelectedFormName).offset().left + $("#" + formTabs.SelectedFormName).width();
                if (postion_onscreen > (window_width - 50))
                {
                    leftmargin = (leftmargin - 10);
                    var currentwidth = $("#" + formTabs.SelectedFormName).width() + 20;
                    leftmargin = (leftmargin + currentwidth);
                    window.alert = function () { };
                    $("#tabuser").animate({ left: "-=" + leftmargin });
                }
            }

        }   // End of TriSysApex.FormsManager.FormTabs

    },  // End of TriSysApex.FormsManager
    //#endregion TriSysApex.FormsManager

    //#region TriSysApex.Logging
    Logging:
    {
        Instantiated: false,
        LoggingEnabled: false,

        LogMessage: function (sMessage)
        {
            if (!TriSysApex.Logging.Instantiated)
            {
                TriSysApex.Logging.LoggingEnabled = TriSysApex.Pages.Controller.IsClientADeveloperIPAddress();
                TriSysApex.Logging.Instantiated = true;
            }

            if (TriSysApex.Logging.LoggingEnabled)
            {
                var sPrefix = 'TriSysApex: ';
                try
                {
                    console.log(sPrefix + sMessage);
                }
                catch (e)
                {
                    if (!TriSysSDK.Browser.InternetExplorerVersion() > 9)
                        alert(sPrefix + sMessage);
                };
            }
        }

    },
    //#endregion TriSysApex.Logging

    ErrorHandling:
    {
        CatchAll: function ()
        {
            window.onerror = function (errorMsg, url, lineNumber, column, errorObj)
            {
                var sError = 'Error: ' + errorMsg +
                    '\n\n Script: ' + url +
                    '\n\n Line: ' + lineNumber +
                    '\n\n Column: ' + column +
                    '\n\n StackTrace: ' + errorObj;

                try
                {
                    // Log it
                    TriSysApex.Logging.LogMessage(sError);
                }
                catch (err)
                {
                    // Probably an error in our own script
                }

                // Use standard alert in case our code is broken!
                alert(sError);

                var bSuppressErrorAlert = true;
                // If you return true, then error alerts (like in older versions of 
                // Internet Explorer) will be suppressed.
                return bSuppressErrorAlert;
            };
        }
    },

    //#region TriSysApex.DynamicClasses
    //
    // This class enumerates through namespace string paths to return the actual instance of
    // the specificed object.
    // It allows us to have dynamic menus, drilldowns etc.. without hard coding functions/objects.
    // For example: TriSysApex.DynamicClasses.GetObject("TriSysApex.Pages.ContactForm.LoadRecord")
    // returns the function LoadRecord in the specified namespace from where the caller can just execute it.
    //
    DynamicClasses:
    {
        GetObject: function (sFullNamespacePathOfObject)
        {
            // Split the path into an array of strings
            var sParts = sFullNamespacePathOfObject.split(".");
            if (sParts.length == 0)
                return null;

            var obj = null;
            for (var i = 0; i < sParts.length; i++)
            {
                var sPart = sParts[i];
                if (i == 0)
                    obj = window[sPart];
                else
                    obj = obj[sPart];
            }

            return obj;
        }
    },
    //#endregion TriSysApex.DynamicClasses

    //#region TriSysApex.URL
    URL:
    {
        // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
        // Example usage:
        // var sURL = window.location.href;     e.g. http://stuff.html?param1=value1
        // alert('param=' + TriSysApex.URL.Parser(sURL).getParam("param1") );
        //
        Parser: function (u)    // TriSysApex.URL.Parser
        {
            var path = "", query = "", hash = "", params;
            if (u.indexOf("#") > 0)
            {
                hash = u.substr(u.indexOf("#") + 1);
                u = u.substr(0, u.indexOf("#"));
            }
            if (u.indexOf("?") > 0)
            {
                path = u.substr(0, u.indexOf("?"));
                query = u.substr(u.indexOf("?") + 1);
                params = query.split('&');
            } else
                path = u;
            return {
                getHost: function ()
                {
                    var hostexp = /\/\/([\w.-]*)/;
                    var match = hostexp.exec(path);
                    if (match != null && match.length > 1)
                        return match[1];
                    return "";
                },
                getPath: function ()
                {
                    var pathexp = /\/\/[\w.-]*(?:\/([^?]*))/;
                    var match = pathexp.exec(path);
                    if (match != null && match.length > 1)
                        return match[1];
                    return "";
                },
                getHash: function ()
                {
                    return hash;
                },
                getParams: function ()
                {
                    return params;
                },
                getQuery: function ()
                {
                    return query;
                },
                setHash: function (value)
                {
                    if (query.length > 0)
                        query = "?" + query;
                    if (value.length > 0)
                        query = query + "#" + value;
                    return path + query;
                },
                setParam: function (name, value)
                {
                    if (!params)
                    {
                        params = new Array();
                    }
                    params.push(name + '=' + value);
                    for (var i = 0; i < params.length; i++)
                    {
                        if (query.length > 0)
                            query += "&";
                        query += params[i];
                    }
                    if (query.length > 0)
                        query = "?" + query;
                    if (hash.length > 0)
                        query = query + "#" + hash;
                    return path + query;
                },
                getParam: function (name)       //TriSysApex.URL.Parser.getParam
                {
                    if (params)
                    {
                        for (var i = 0; i < params.length; i++)
                        {
                            var pair = params[i].split('=');
                            if (decodeURIComponent(pair[0]) == name)
                                return decodeURIComponent(pair[1]);
                        }
                    }
                    console.log('Query variable %s not found', name);
                },
                hasParam: function (name)
                {
                    if (params)
                    {
                        for (var i = 0; i < params.length; i++)
                        {
                            var pair = params[i].split('=');
                            if (decodeURIComponent(pair[0]) == name)
                                return true;
                        }
                    }
                    console.log('Query variable %s not found', name);
                },
                removeParam: function (name)
                {
                    query = "";
                    if (params)
                    {
                        var newparams = new Array();
                        for (var i = 0; i < params.length; i++)
                        {
                            var pair = params[i].split('=');
                            if (decodeURIComponent(pair[0]) != name)
                                newparams.push(params[i]);
                        }
                        params = newparams;
                        for (var i = 0; i < params.length; i++)
                        {
                            if (query.length > 0)
                                query += "&";
                            query += params[i];
                        }
                    }
                    if (query.length > 0)
                        query = "?" + query;
                    if (hash.length > 0)
                        query = query + "#" + hash;
                    return path + query;
                }
            };
        }
    },
    //#endregion TriSysApex.URL

    //#region TriSysApex.Toasters
    Toasters:
    {
        SetOptions: function ()
        {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        },

        Info: function (sMessage)
        {
            TriSysApex.Toasters.SetOptions();
            toastr.info(sMessage);
        },
        Success: function (sMessage)
        {
            TriSysApex.Toasters.SetOptions();
            toastr.success(sMessage);
        },
        Warning: function (sMessage)
        {
            TriSysApex.Toasters.SetOptions();
            toastr.warning(sMessage);
        },
        Error: function (sMessage)
        {
            TriSysApex.Toasters.SetOptions();
            toastr.error(sMessage);
        }
    },
    //#endregion TriSysApex.Toasters

    //#region TriSysApex.Constants
    Constants:
    {
        Version: '2014.08.08.1517',                                 // YYYY.MM.DD.hhmm
        DeveloperSiteKey: '20131222-ba09-4e4c-8bcb-apex.web.app',   // Dedicated API key for testing
        iMillisecondsInSecond: 1000,                                // Hate literals!
        iMinimumPasswordLength: 8,                                  // User must supply a password >= 8 characters
        Cookie_LoginName: 'TriSysApex_LoginForm_EMail',             // Where the cookies are stored in browser
        VATPercentage: 20.0,                                        // Current UK VAT Rate
        ModalPopupJQueryDiv: "#divTriSysMessageBoxWindow",          // The first modal window name. Subsequent have .Level appended e.g. divTriSysMessageBoxWindow2 etc..
        FormLoadDataDelay: 5,                                       // Number of milliseconds in setTimeout to allow BlockUI to show after page loaded
        UserControlFolder: 'usercontrols/',                         // Allow hosted sites to have their own user controls to prevent cross-site problems
        ProfileImageSize: 32,                                       // Number of pixels for both height and width for avatar/profile of loggedin user
        DefaultLoggedInImage: "images/profiles/LoggedInImage.png"   // If User not linked to a contact with a photo, show this image

    }
    //#endregion TriSysApex.Constants

}; // End of TriSysApex Namespace
