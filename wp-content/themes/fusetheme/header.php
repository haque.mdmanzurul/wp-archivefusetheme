<!DOCTYPE html>

<html <?php language_attributes(); ?>>

  

    <head>

        <meta charset="<?php bloginfo('charset'); ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="description" content="">

        <meta name="author" content="">

        <title>

            <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>

        </title>

        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/icons/favicon.ico" />
		<script type="text/javascript">
        var exPrefix = "/wp-content/themes/fusetheme/";
		var basePath = "<?php bloginfo( 'url' ); ?>";
        </script>
        <!-- Javascript Libraries -->

        <script src="<?php echo get_template_directory_uri(); ?>/javascript/TriSysLoad.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/javascript/jquery.min.js" type="text/javascript"></script>
		



		<?php wp_head(); ?>

    </head>

  

<body>

<?php //include('usercontrols/header.php'); ?>
    <div id="TriSysWeb-Header"></div>

    

<!-- start of header.html -->

<!-- end of header.html -->