﻿<?php  
/* 
* Template name: Login Page
*/
get_header(); ?>

    <div class="jumbotron">

        <div class="container">

            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-8">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>

                <div class="col-md-4" style="text-align:right">
                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/Login.png" width="240"><?php } ?>    

                </div>

            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>

            <p style="text-align: center">

            </p>

        </div>

    </div>

    <div class="container">

        <div class="panel panel-warning">

            <div class="panel-heading">

                <h3 class="panel-title">

                    Your Details

                </h3>

            </div>

            <div class="panel-body">



                <div class="form-group">

                    <label>

                        E-Mail Address

                    </label>

                    <input type="email" placeholder="E-Mail" id="txtLoginEMail" maxlength="100" class="form-control" style="min-width:100px;" />

                </div>

                <div class="form-group">

                    <label>

                        Password

                    </label>

                    <input type="password" placeholder="Password" id="txtLoginPassword" maxlength="50" class="form-control" style="min-width:100px" />

                </div>



                <div class="form-group">

                    <hr class="panel-warning" />    <!-- Line Separator -->

                </div>



                <div class="form-group">

                    <input id="cmbLoginPersistence" style="width:100%" />

                </div>

                <div class="form-group">

                    <hr class="panel-warning" />    <!-- Line Separator -->

                </div>



                <div class="btn-group">

                    <a class="btn btn-primary" id="btnLogin" onclick="TriSysWeb.Security.LoginButtonClick()">Login</a>

                </div>

                <div class="btn-group">

                    <a class="btn btn-danger" id="btnForgottenPassword" href="/forgottenpassword">Forgotten Password</a>

                </div>

                &nbsp; &nbsp;



                <div class="btn-group">

                    <a class="btn btn-success" id="btnRegisterClient" href="/registerclient">Register as a Client</a>

                </div>

                <div class="btn-group">

                    <a class="btn btn-info" id="btnRegisterCandidate" href="/register">Register as a Candidate</a>

                </div>

            </div>

        </div>

    </div>


    <!-- /container -->



    <script type="text/javascript">

        $(document).ready(function ()

        {

            TriSysLoad.ApexFramework.LoadDynamically(function ()

            {

                var documentReadyObject = {

                    ClientLogin: false,

                    CandidateLogin: false,

                    UserLogin: false,

                    SQLLogin: false,

                    PageName: "login",

                    CallbackFunction: TriSysWeb.Pages.Login.Load

                };

                TriSysWeb.Pages.DocumentReady(documentReadyObject);

            });

        });



    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->