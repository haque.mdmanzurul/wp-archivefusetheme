﻿<?php  
/* 
* Template name: Timesheets Page
*/
get_header(); ?>
  
<body style="display:none">
    <div id="TriSysWeb-Header"></div>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        Timesheets
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/Timesheet.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                As a logged in candidate, you may view all of your current and historic timesheets
                and submit new timesheets for authorisation by your line manager.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <!-- Search Box -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheet Lookup
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Placement
                    </label>
                    <select id="cmbPlacement" style="width:auto;"></select>
                </div>
                <hr />
                <div class="btn-group">
                    <a class="btn btn-default" id="btnSearch" onClick="TriSysWeb.Pages.CandidateTimesheets.Search('divTimesheetGrid')">Search</a>
                </div>
            </div>
        </div>
    </div>   <!-- End of Search Box -->


    <div class="container">
        <div class="panel panel-default" id="pnlTimesheets">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheets
                </h3>
            </div>
            <div class="panel-body">
                <!-- The timesheet grid populated by JS -->
                <div id="divTimesheetGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>


    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: true,
                    SQLLogin: true,
                    PageName: "timesheets",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysWeb.Pages.CandidateTimesheets.PopulatePlacementLookup("cmbPlacement");

                        // Show all timesheets after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.CandidateTimesheets.SearchPersistence.ReloadLastSearchCriteriaAndReSearch, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->