<?php

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'fusetheme' ),
) );

add_theme_support( 'post-thumbnails' );
include('themeoptions/themeoptions.php');
?>