﻿<?php  
/* 
* Template name: ClientTimesheets Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-8">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/Timesheet.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a client, you can conduct an ad-hoc search for suitable candidates within our database.
                You can search their skills profile and CV and compile a list of favourite candidates
                whos CV you may wish to read and potentially interview for a role within your organisation.' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <!-- Search Box -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheet Lookup
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <select id="cmbReference" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <select id="cmbJobTitle" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <select id="cmbType" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Candidate
                    </label>
                    <select id="cmbCandidate" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <select id="cmbLocation" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <select id="cmbStartDate" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        End Date
                    </label>
                    <select id="cmbEndDate" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <select id="cmbStatus" style="width:auto;"></select>
                </div>
                <hr />
                <div class="btn-group">
                    <a class="btn btn-default" id="btnSearch" onclick="TriSysWeb.Pages.ClientTimesheets.Search('divTimesheetGrid')">Search</a>
                </div>
            </div>
        </div>
    </div>   <!-- End of Search Box -->


    <div class="container">
        <div class="panel panel-default" id="pnlTimesheets">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheets
                </h3>
            </div>
            <div class="panel-body">
                <!-- The timesheet grid populated by JS -->
                <div id="divTimesheetGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: true,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "clienttimesheets",
                    ParentMenuName: "client",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysWeb.Pages.ClientPlacements.PopulateReferenceLookup("cmbReference");
                        TriSysWeb.Pages.ClientPlacements.PopulateJobTitleLookup("cmbJobTitle");
                        TriSysWeb.Pages.ClientPlacements.PopulateTypeLookup("cmbType");
                        TriSysWeb.Pages.ClientPlacements.PopulateCandidateLookup("cmbCandidate");
                        TriSysWeb.Pages.ClientPlacements.PopulateLocationLookup("cmbLocation");
                        TriSysWeb.Pages.ClientPlacements.PopulateStatusLookup("cmbStatus");
                        TriSysWeb.Pages.ClientPlacements.PopulateStartDateLookup("cmbStartDate");
                        TriSysWeb.Pages.ClientPlacements.PopulateEndDateLookup("cmbEndDate");

                        // Show all timesheets after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.ClientTimesheets.SearchPersistence.ReloadLastSearchCriteriaAndReSearch, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->
