﻿<?php  
/* 
* Template name: Interviews Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/Interview.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default" id="pnlInterviews">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Interviews
                </h3>
            </div>
            <div class="panel-body">

                <p>Grid of Interviews: TODO</p>
            </div>
        </div>

    </div>


    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "interviews",
                    ParentMenuName: "candidate"
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->