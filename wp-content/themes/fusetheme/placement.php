﻿<?php  
/* 
* Template name: Placement Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        Placement
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/Placement.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                As a logged in candidate, you may view any of your placement details.
                As a logged in client, you may view any of your current and historic candidate placements.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Placement
                </h3>
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <input id="Reference" readonly="readonly" style="width:100%" />
                    <div id="PlacementId" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>
                        Company
                    </label>
                    <input id="CompanyName" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <input id="JobTitle" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <input id="Type" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Candidate
                    </label>
                    <input id="CandidateName" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Location/Site Address
                    </label>
                    <textarea class="form-control" name="Location" id="Location" rows="5" style="resize: none" readonly></textarea>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <input id="StartDate" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group" id="divEndDate">
                    <label>
                        End Date
                    </label>
                    <input id="EndDate" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <input id="Status" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Company Contact / Hiring Manager
                    </label>
                    <input id="ClientName" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group" id="divChargeRates" style="display:none">
                    <label>
                        Charge Rates
                    </label>
                    <textarea class="form-control" name="ChargeRate" id="ChargeRate" rows="3" style="resize: none" readonly></textarea>
                </div>
                <div class="form-group" id="divPayRates" style="display:none">
                    <label>
                        Pay Rates
                    </label>
                    <textarea class="form-control" name="PayRate" id="PayRate" rows="3" style="resize: none" readonly></textarea>
                </div>
                <div class="form-group" id="divSalary" style="display:none">
                    <label>
                        Salary
                    </label>
                    <input id="Salary" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group" id="divBonusValue" style="display:none">
                    <label>
                        Bonus Value
                    </label>
                    <input id="BonusValue" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group" id="divBenefits" style="display:none">
                    <label>
                        Benefits
                    </label>
                    <input id="Benefits" readonly="readonly" style="width:100%" />
                </div>
                <div class="form-group" id="divBenefitsValue" style="display:none">
                    <label>
                        Benefits Value
                    </label>
                    <input id="BenefitsValue" readonly="readonly" style="width:100%" />
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="divTimesheets">
        <div class="panel panel-default" id="pnlPlacements">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheets
                </h3>
            </div>
            <div class="panel-body">
                <!-- The timesheet grid populated by JS -->
                <div id="divTimesheetGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

    <!-- Form Actions -->
    <div class="container" id="containerPlacementActions" style="display: none">
        <div class="panel panel-default" id="pnlPlacementActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Form Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-primary" id="btnSaveTimesheet" onclick="TriSysWeb.Pages.Placement.NewTimesheet()">New Timesheet</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Form Actions -->

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientOrCandidateLogin: true,
                    SQLLogin: true,
                    PageName: "placement",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Show placement details after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.Placement.Load, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->