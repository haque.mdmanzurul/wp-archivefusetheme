﻿<?php get_header(); ?>



    <div class="jumbotron">
        <div class="container">
            <div class="row" id="trisys-cms-opuslaboris-index-headertext" ondblclick="TriSysCMS.Editor.Open(this);">
                <div class="col-md-9">
                    <h1>
                        <?php bloginfo("name");  ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/SiteLogo.png" height="150" /><?php } ?>    
                </div>
            </div>
            <p id="trisys-cms-opuslaboris-index-welcome" ondblclick="TriSysCMS.Editor.Open(this);">
                Welcome to the Opus Laboris Recruitment web site, the place to visit for
                gardening labour. This site is designed for both property owners and gardeners.
                Its aim is to easily allow property owners to find suitable gardeners for
                any sized job.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo get_option('fldPromo1BgImage'); ?>" height="100" />
                    </div>
                    <div class="col-md-8">
                        <h2 class="pull-right">
                            <?php echo get_option('fldPromo1ReadMoreLink'); ?>
                        </h2>
                    </div>
                </div>
                <p id="trisys-cms-opuslaboris-index-candidates" ondblclick="TriSysCMS.Editor.Open(this);">
                    <?php echo get_option('fldPromo1Text'); ?>
                </p>
                <p>
                    <a class="btn btn-info" href="/vacancy-search">View Vacancies »</a>
                </p>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo get_option('fldPromo2BgImage'); ?>" height="100" />
                    </div>
                    <div class="col-md-8">
                        <h2 class="pull-right">
                           <?php echo get_option('fldPromo2ReadMoreLink'); ?>
                        </h2>
                    </div>
                </div>
                <p id="trisys-cms-opuslaboris-index-clients" ondblclick="TriSysCMS.Editor.Open(this);">
                   <?php echo get_option('fldPromo2Text'); ?>
                </p>
                <p>
                    <a class="btn btn-warning" href="/clientvacancies">Manage Vacancies »</a>
                </p>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Jobs.png" height="100" />
                    </div>
                    <div class="col-md-8">
                        <h2 class="pull-right">
                            Top 5 Jobs &nbsp; &nbsp; &nbsp;
                        </h2>
                    </div>
                </div>
                <ul class="list-group" id="topJobs">
                    <li class="list-group-item">
                        <span class="badge">...</span>
                        Calculating
                    </li>
                </ul>
                <p>
                </p>
            </div>
        </div>



    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "index",
                    CallbackFunction: function ()
                    {
                        // Show our top jobs
                        TriSysWeb.TopJobs.PopulateWidget("topJobs", 5);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);

                // CMS functionality
                TriSysCMS.PageManager.LoadContent(true);
            });
        });

    </script>

    



        



<?php get_footer(); ?>