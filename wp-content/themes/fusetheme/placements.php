﻿<?php  
/* 
* Template name: Placements Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        Placements
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/Placement.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                As a logged in candidate, you may search and view all of your current and historic placements and
                drill down to enter timesheets.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <!-- Search Box -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Placement Lookup
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Reference
                    </label>
                    <select id="cmbReference" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Job Title
                    </label>
                    <select id="cmbJobTitle" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Type
                    </label>
                    <select id="cmbType" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Location
                    </label>
                    <select id="cmbLocation" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Start Date
                    </label>
                    <select id="cmbStartDate" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        End Date
                    </label>
                    <select id="cmbEndDate" style="width:auto;"></select>
                </div>
                <div class="form-group">
                    <label>
                        Status
                    </label>
                    <select id="cmbStatus" style="width:auto;"></select>
                </div>
                <hr />
                <div class="btn-group">
                    <a class="btn btn-default" id="btnSearch" onclick="TriSysWeb.Pages.CandidatePlacements.SearchPersistence.PlacementSearchButton()">Search</a>
                </div>
            </div>
        </div>
    </div>   <!-- End of Search Box -->

    <div class="container">
        <div class="panel panel-default" id="pnlPlacements">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Placements
                </h3>
            </div>
            <div class="panel-body">
                <!-- The placement grid populated by JS -->
                <div id="divPlacementGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

   <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: true,
                    SQLLogin: true,
                    PageName: "placements",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Populate all lookups
                        TriSysWeb.Pages.CandidatePlacements.PopulateReferenceLookup("cmbReference");
                        TriSysWeb.Pages.CandidatePlacements.PopulateJobTitleLookup("cmbJobTitle");
                        TriSysWeb.Pages.CandidatePlacements.PopulateTypeLookup("cmbType");
                        TriSysWeb.Pages.CandidatePlacements.PopulateLocationLookup("cmbLocation");
                        TriSysWeb.Pages.CandidatePlacements.PopulateStatusLookup("cmbStatus");
                        TriSysWeb.Pages.CandidatePlacements.PopulateStartDateLookup("cmbStartDate");
                        TriSysWeb.Pages.CandidatePlacements.PopulateEndDateLookup("cmbEndDate");

                        // Show all placements after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.CandidatePlacements.SearchPersistence.ReloadLastSearchCriteriaAndReSearch, 500);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->