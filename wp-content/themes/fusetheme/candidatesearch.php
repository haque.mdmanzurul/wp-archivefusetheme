﻿<?php  
/* 
* Template name: Candidate Search Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-8">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/CandidateSearch.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>
                
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'As a client, you can conduct an ad-hoc search for suitable candidates within our database.
                You can search their skills profile and CV and compile a list of favourite candidates
                whos CV you may wish to read and potentially interview for a role within your organisation.' ); ?></p>
			<?php endif; ?>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default" id="pnlBuildCV">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Candidate Search
                </h3>
            </div>
            <div class="panel-body">

                <p>Search loading...</p>
            </div>
        </div>

    </div>
    <!-- Grid Actions -->
    <div class="container">
        <div class="panel panel-default" id="Div1">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-danger" id="btnRequestCandidateCV" onclick="RequestCandidateCV()">Request CV</a>
                    <a class="btn btn-default" id="btnBookInterview" onclick="BookInterview()">Book Interview</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Grid Actions -->

    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: true,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "candidatesearch",
                    ParentMenuName: "client",
                    CallbackFunction: function ()
                    {
                        // Show search...
                        //setTimeout(func, 50);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->