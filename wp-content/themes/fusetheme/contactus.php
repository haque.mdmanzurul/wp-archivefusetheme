﻿<?php  
/* 
* Template name: Contact us Page
*/
get_header(); ?>
    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-3" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/ContactUs.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Your Details
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group" style="width:100%; float:left;">
                    <label style="width:100%;">
                        Title
                    </label>

                    <input id="Contact_ContactTitle" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Forename(s)
                    </label>
                    <input type="text" class="form-control" name="txtForename" id="Contact_Forenames" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Surname
                    </label>
                    <input type="text" class="form-control" name="txtSurname" id="Contact_Surname" maxlength="50">
                </div>
                <div class="form-group">
                    <label>
                        Company
                    </label>
                    <input type="text" class="form-control" name="txtCompanyName" id="Contact_CompanyName" maxlength="50">
                </div>
                <!-- TELEPHONE NUMBERS -->
                <div class="form-group">
                    <label>
                        Home Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtHomeTel" id="Contact_HomeAddressTelNo" maxlength="20">
                </div>
                <div class="form-group">
                    <label>
                        Work Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtWorkTel" id="Contact_WorkTelNo" maxlength="20">
                </div>
                <div class="form-group">
                    <label>
                        Personal Mobile Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtMobTel" id="Contact_MobileTelNo" maxlength="20">
                </div>
                <div class="form-group">
                    <label>
                        Work Mobile Telephone Number
                    </label>
                    <input type="text" class="form-control" name="txtWorkMobTel" id="ContactConfigFields_WorkMobile" maxlength="20">
                </div>
                <!-- END OF TELEPHONE NUMBERS -->
                <!-- E-MAIL ADDRESSES -->
                <div class="form-group">
                    <label>
                        Work E-Mail Address
                    </label>
                    <input type="text" class="form-control" id="Contact_EMail" maxlength="100">
                </div>
                <div class="form-group">
                    <label>
                        Personal E-Mail Address
                    </label>
                    <input type="text" class="form-control" id="ContactConfigFields_AlternativeEMailAddress1" maxlength="100">
                </div>
                <!-- END OF E-MAIL ADDRESSES -->

                <div class="form-group">
                    <label>
                        Type of Enquiry
                    </label>

                    <input id="typeOfEnquiry" style="width:100%" />
                </div>
                <div class="form-group">
                    <label>
                        Message / Enquiry
                        <br>
                    </label>
                    <textarea style="width:100%; resize: none;" id="txtMessage" rows="5" class="form-control"></textarea>
                </div>
                <div class="btn-group">
                    <a class="btn btn-primary" id="btnSubmitContactUs" onclick="TriSysWeb.Pages.ContactUs.ButtonClick()">Submit</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h3>
            Head Office
        </h3>
        <div>
            Opus Laboris Recruitment
            <br>
            Wellington House
            <br>
            East Road
            <br>
            Cambridge
            <br>
            CB1 1BH
        </div>
    </div>
    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->

    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "contactus",
                    CallbackFunction: TriSysWeb.Pages.ContactUs.Load
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
</body>

</html>