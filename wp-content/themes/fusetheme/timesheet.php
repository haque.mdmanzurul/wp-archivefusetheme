﻿<?php  
/* 
* Template name: Timesheet Page
*/
get_header(); ?>
<style>
  .RatesNumberColumn
  {
	  text-align: right;
	  width: 130px;
  }
</style>

    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        Timesheet
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                 <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>               
                    <img src="<?php bloginfo('template_directory'); ?>/images/Timesheet.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                As a logged in candidate, you may enter your working times and request authorisation.
                As a logged in client, you may authorise time worked by your candidate.
            </p>
            <p style="text-align: center">
            </p>
        </div>
    </div>
    <!-- Placement Details -->
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Placement
                </h3>
            </div>
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th class="text-nowrap">Placement ID</th>
                                <td colspan="3"><div id="PlacementId"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Reference</th>
                                <td colspan="3"><div id="Reference"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Company</th>
                                <td colspan="3"><div id="CompanyName"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Contractor</th>
                                <td colspan="3"><div id="CandidateName"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Job Title</th>
                                <td colspan="3"><div id="JobTitle"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Type</th>
                                <td colspan="3"><div id="Type"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Site Address</th>
                                <td colspan="3"><div id="Location"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Start Date</th>
                                <td colspan="3"><div id="StartDate"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">End Date</th>
                                <td colspan="3"><div id="EndDate"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Line Manager</th>
                                <td colspan="3"><div id="ClientName"></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Timesheet Details -->
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheet
                </h3>
            </div>
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th class="text-nowrap">Timesheet ID</th>
                                <td colspan="3"><div id="TimesheetId"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Period</th>
                                <td colspan="3"><select id="cmbPeriod" style="width:auto;"></select></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Input Date</th>
                                <td colspan="3"><div id="InputDate"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Status</th>
                                <td colspan="3"><div id="Status"></div></td>
                            </tr>
                            <tr id="trDateAuthorised" style="display: none">
                                <th class="text-nowrap">Authorised</th>
                                <td colspan="3"><div id="AuthorisedDate"></div></td>
                            </tr>
                            <tr id="trPaid" style="display: none">
                                <th class="text-nowrap">Paid</th>
                                <td colspan="3"><div id="Paid"></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="divTimesheets">
        <div class="panel panel-default" id="pnlPlacements">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Working Hours
                </h3>
            </div>
            <div class="panel-body">
                <!-- TODO: Telerik Grid or Custom? -->
                <div id="divTimesheetHoursGrid" style="border-width: 1px; width: 100%; ">
                </div>
            </div>
        </div>

    </div>

    <div class="container" id="divRates">
        <div class="panel panel-default" id="pnlRates">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Hours, Rates &amp; Amounts
                </h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="amountsTable">
                        <tbody id="tbodyAmounts">
                            <tr>
                                <th></th>
                                <td style="text-align:right; width: 110px;"><b>Total Hours</b></td>
                                <td class="RatesNumberColumn"><b>Pay Rate</b></td>
                                <td class="RatesNumberColumn"><b>Charge Rate</b></td>
                                <td class="RatesNumberColumn"><b>Pay Amount</b></td>
                                <td class="RatesNumberColumn"><b>Charge Amount</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="container" id="divTimesheetSummary">
        <div class="panel panel-default" id="pnlTimesheetSummary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Timesheet Summary
                </h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="summaryTable">
                        <tbody>
                            <tr>
                                <th></th>
                                <td class="RatesNumberColumn"><b>Pay</b></td>
                                <td class="RatesNumberColumn"><b>Charge</b></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Currency</th>
                                <td class="RatesNumberColumn"><div id="summaryPayCurrency"></div></td>
                                <td class="RatesNumberColumn"><div id="summaryChargeCurrency"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Total Net Amount</th>
                                <td class="RatesNumberColumn"><div id="summaryTotalNetPay"></div></td>
                                <td class="RatesNumberColumn"><div id="summaryTotalNetCharge"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">VAT % Rate</th>
                                <td class="RatesNumberColumn"><div id="summaryPayVATPercentage"></div></td>
                                <td class="RatesNumberColumn"><div id="summaryChargeVATPercentage"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Total VAT Amount</th>
                                <td class="RatesNumberColumn"><div id="summaryPayVATAmount"></div></td>
                                <td class="RatesNumberColumn"><div id="summaryChargeVATAmount"></div></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Grand Total</th>
                                <td class="RatesNumberColumn"><div id="summaryPayGrandTotal"></div></td>
                                <td class="RatesNumberColumn"><div id="summaryChargeGrandTotal"></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Form Actions -->
    <div class="container">
        <div class="panel panel-default" id="pnlTimesheetActions">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Form Actions
                </h3>
            </div>
            <div class="panel-body">

                <div class="btn-group">
                    <a class="btn btn-primary" id="btnSaveTimesheet" onclick="TriSysWeb.Pages.Timesheet.SaveButtonClick()" style="display:none">Save</a>
                    <a class="btn btn-danger" id="btnRequestAuthorisation" onclick="TriSysWeb.Pages.Timesheet.RequestAuthorisation()" style="display:none">Request  Authorisation</a>
                    <a class="btn btn-danger" id="btnAuthoriseTimesheet" onclick="TriSysWeb.Pages.Timesheet.Authorise()" style="display:none">Authorise</a>
                    <a class="btn btn-warning" id="btnBack" onclick="history.go(-1);">Back to Previous Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Form Actions -->

    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientOrCandidateLogin: true,
                    SQLLogin: true,
                    PageName: "timesheet",
                    ParentMenuName: "candidate",
                    CallbackFunction: function ()
                    {
                        // Show placement and timesheet details after we can be sure that we are connected to the database
                        setTimeout(TriSysWeb.Pages.Timesheet.Load, 100);
                    }
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>
    
    <!-- End of Actions -->
<?php  get_footer(); ?>
    <!-- /container -->