﻿<?php  
/* 
* Template name: Forgot Password
*/
get_header(); ?>

    <div class="jumbotron">
        <div class="container">
            <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <div class="col-md-9">
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
                <div class="col-md-4" style="text-align:right">
                                <?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}else{
				?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ForgottenPassword.png" height="150"><?php } ?>    
                </div>
            </div>
            <p>
                <?php $content= get_the_content(); 
				echo $content;
				?>             
                
            </p>
              <?php endwhile; else : ?>
			<p><?php _e( 'No content' ); ?></p>
			<?php endif; ?>
            <p style="text-align: center">
            </p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Request Password
                </h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        E-Mail Address:
                    </label>
                    <input type="text" class="form-control" name="txtEmailAddress" id="txtEmailAddress" maxlength="100">
                </div>

                <hr />
                <div class="btn-group">
                    <a class="btn btn-primary" id="btnRequest" onclick="TriSysWeb.Pages.ForgottenPassword.RequestButtonClick()">Request</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="TriSysWeb-Footer"></div>
    </div>
    <!-- /container -->
    <script type="text/javascript">
        $(document).ready(function ()
        {
            TriSysLoad.ApexFramework.LoadDynamically(function ()
            {
                var documentReadyObject = {
                    ClientLogin: false,
                    CandidateLogin: false,
                    SQLLogin: true,
                    PageName: "forgottenpassword",
                    ParentMenuName: "candidate"
                };
                TriSysWeb.Pages.DocumentReady(documentReadyObject);
            });
        });

    </script>


</body>


</html>