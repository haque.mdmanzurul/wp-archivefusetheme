<?php
/**
 * Sets up theme defaults and registers the various WordPress features that
 * footbaltheme supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since footbaltheme 1.0
 */
function footbaltheme_setup() {
	/*
	 * Makes Twenty Twelve available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Twelve, use a find and replace
	 * to change 'twentytwelve' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'footbaltheme', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status', ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'footbaltheme' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'footbaltheme' ) );
	register_nav_menu( 'mobilemenu', __( 'Mobile Menu', 'footbaltheme' ) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop

}
add_action( 'after_setup_theme', 'footbaltheme_setup' );

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'CatrgoryThumb', 300, 9999 ); //300 pixels wide (and unlimited height)
	add_image_size( 'HomepageThumb', 220, 180, true ); //(cropped)
	add_image_size( 'PageThumb', 347, 497, true );
}


/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function footbaltheme_excerpt_length( $length ) {
	return 140;
}
add_filter( 'excerpt_length', 'footbaltheme_excerpt_length' );

/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function footbaltheme_excerpt_categorypost( $length ) {
	return 70;
}

/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function footbaltheme_excerpt_length2( $length ) {
	return 10;
}

if ( ! function_exists( 'footbaltheme_continue_reading_link' ) ) :
/**
 * Returns a "Continue Reading" link for excerpts
 */
function footbaltheme_continue_reading_link() {
	return ' <a class="readmore" href="'. esc_url( get_permalink() ) . '">' . __( 'Read More', 'footbaltheme' ) . '</a>';
}
endif; // footbaltheme_continue_reading_link

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and footbaltheme_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
function footbaltheme_auto_excerpt_more( $more ) {
	return ' &hellip;' . footbaltheme_continue_reading_link();
}
add_filter( 'excerpt_more', 'footbaltheme_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function footbaltheme_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= footbaltheme_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'footbaltheme_custom_excerpt_more' );


/**
 * Register our sidebars and widgetized areas. 
 *
 * @since footbaltheme 1.0
 */
function footbaltheme_widgets_init() {
	register_sidebar( array(
		'name' => __( 'About Page Sidebar', 'footbaltheme' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Contact Page Sidebar', 'footbaltheme' ),
		'id' => 'contact-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget contact %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Blog Sidebar', 'footbaltheme' ),
		'id' => 'blog-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Widget Three', 'footbaltheme' ),
		'id' => 'footer-three',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
}
add_action( 'widgets_init', 'footbaltheme_widgets_init' );
add_filter('widget_text', 'do_shortcode');
if ( ! isset( $content_width ) )
    $content_width = 900;
/**
* @footbaltheme Theme Options
*/
include('themeoptions/themeoptions.php');
?>