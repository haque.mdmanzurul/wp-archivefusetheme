<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-fusetheme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0uGoy)ApV;CX~5kQEXU4:<c9kg~rK!4RvHgG>afx4p.L+Mg*|s>ZuiJDE&TI6}`r');
define('SECURE_AUTH_KEY',  '</37|IsJCtwYA/>9XDe7YznG4rwj](pGnh,8Qn}g0C!]jIM%]JRoQ]}n0cELU3MJ');
define('LOGGED_IN_KEY',    '%&z{m8OcEAUe*3GB!-j##xWO9vs{bb=.lpu+L3w /tSB SCDj+@f^,~6WpBlC~*7');
define('NONCE_KEY',        ' +`117Dj!|/34O]j(WY8b`Kj2#!-|G96LPg)`e4=(oz- Hn$eO|,GXR Z]~$W@(x');
define('AUTH_SALT',        '{O&% BdlO]u(:BJ}tO;H[)*ZLelQ&/x/SS:OW~ZFfee$8=@+V{v68-2!E=em!o}H');
define('SECURE_AUTH_SALT', '6mOGvGQ}fvbEe#+|E[n9};m1n!-$fU%axafderP-Ly_&v52Bxve<B%EVVgLpypxb');
define('LOGGED_IN_SALT',   'D>ve j>0I*JDd=O6[%iirGo9e+&Pv6<@odZmph9kK:DvV}YMR=4kZwvahdR)2?Kb');
define('NONCE_SALT',       'h!qksBJ(C6&A)@(;VjKudnJw}G$1v$R@Ux=Wi%B:5YY`&?0+A3,9yXAsUQ$h>/UZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
